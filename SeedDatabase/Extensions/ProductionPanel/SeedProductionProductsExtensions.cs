﻿using System.Collections.Generic;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.SeedDatabase.Extensions.ProductionPanel
{
    public static class SeedProductionProductsExtensions
    {
        public static void FillWithRandomData(
            this List<ProductionProduct> productionproducts,
            HashSet<ProductIdAmountDiscountClass> productIdsAmounts,
            int orderInstanceId,
            string orderInstanceUuid,
            ProductionProduct parentProductionProduct,
            ApplicationDataDbContext context)
        {
            foreach (var productIdAmount in productIdsAmounts)
            {
                var product = context.Products.Find(productIdAmount.ProductId);
                if (product == null)
                    continue;

                var productionProduct = new ProductionProduct(
                    productIdAmount.ProductId,
                    product.UUID,
                    orderInstanceId,
                    orderInstanceUuid,
                    parentProductionProduct,
                    product.Parts.Sum(a => a.Amount) * productIdAmount.Amount);

                productionproducts.Add(productionProduct);
                context.SaveChanges();

                foreach (var partIdAmount in product.Parts)
                {
                    var productionPart = new ProductionPart(
                        partIdAmount.PartId,
                        partIdAmount.PartUUID,
                        productionProduct,
                        productIdAmount.Amount,
                        partIdAmount.Amount);

                    productionProduct.ProductionParts.Add(productionPart);
                    context.SaveChanges();
                }

                productionproducts.Add(productionProduct);
                context.SaveChanges();

                if (product.Products.Count > 0)
                {
                    productionProduct.ProductionProducts.FillWithRandomData(
                        product.Products,
                        orderInstanceId,
                        orderInstanceUuid,
                        productionProduct,
                        context);
                }

                context.SaveChanges();
            }
        }
    }
}
