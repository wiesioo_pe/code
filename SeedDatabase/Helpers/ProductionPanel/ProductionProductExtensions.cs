﻿using Batat.Models.Database.ProductionPanel;
using System.Collections.Generic;

namespace Batat.SeedDatabase.Helpers.ProductionPanel
{
    public static class ProductionProductExtensions
    {
        public static IEnumerable<ProductionPart> GetAllProductionParts(this ProductionProduct productionProduct)
        {
            foreach (var productionPart in productionProduct.ProductionParts)
            {
                yield return productionPart;
            }

            foreach (var innerProductionProduct in productionProduct.ProductionProducts)
            {
                foreach (var productionPart in innerProductionProduct.GetAllProductionParts())
                {
                    yield return productionPart;
                }
            }
        }
    }
}
