﻿using System;
using System.Collections.Generic;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedMachines(ApplicationDataDbContext context)
        {
            if (context?.Operators == null)
                return;

            // MASZYNA STANDARDOWA
            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "Standard",
                MachineTypeId = 1,
                Model = "Standard",
                Manufacturer = "Standard",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(3, context.Operators.Find(3)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Maszyna używana jako standardowa", new ApplicationUser())
                    }
            });

            context.SaveChanges();

            // Tutaj następne rekordy

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M01",
                MachineTypeId = 2,
                Model = "KK340",
                Manufacturer = "ESO Machines",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID),
                    new OperatorIdClass(4, context.Operators.Find(4)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Nie używać prędkości 2200", new ApplicationUser()),
                    new NoteClass("Serwis 29.11.2016", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M02",
                MachineTypeId = 2,
                Model = "KK380",
                Manufacturer = "ESO Machines",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(3, context.Operators.Find(3)?.UUID),
                    new OperatorIdClass(4, context.Operators.Find(4)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Brak uchwytu 250mm", new ApplicationUser()),
                    new NoteClass("Serwis 29.10.2017", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M03",
                MachineTypeId = 3,
                Model = "WK-1200",
                Manufacturer = "DrillMachinery",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(3, context.Operators.Find(3)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID),
                    new OperatorIdClass(6, context.Operators.Find(6)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - marzec 2015", new ApplicationUser()),
                    new NoteClass("Serwis 10.05.2017", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M04",
                MachineTypeId = 4,
                Model = "WKNP-50",
                Manufacturer = "BZMD",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID),
                    new OperatorIdClass(4, context.Operators.Find(4)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - marzec 2015", new ApplicationUser()),
                     new NoteClass("Awaria stołu obrotowego. Nie używać", new ApplicationUser()),
                    new NoteClass("Serwis 10.05.2017", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M05",
                MachineTypeId = 4,
                Model = "WKNP-51",
                Manufacturer = "FOD",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - styczeń 2010", new ApplicationUser()),
                     new NoteClass("Używać tylko chłodziwa EMU-50", new ApplicationUser()),
                    new NoteClass("Serwis 05.05.2017", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M06",
                MachineTypeId = 5,
                Model = "Strug8",
                Manufacturer = "FOD",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - brak", new ApplicationUser()),
                     new NoteClass("Bład krańcówki osłony przedniej - naprawić", new ApplicationUser()),
                    new NoteClass("Serwis 10.08.2016", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "M07",
                MachineTypeId = 5,
                Model = "Strug8",
                Manufacturer = "FOD",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(3, context.Operators.Find(3)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID),
                    new OperatorIdClass(6, context.Operators.Find(6)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - brak", new ApplicationUser()),
                     new NoteClass("Serwis 10.08.2016", new ApplicationUser())
                    }
            });


            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "S08",
                MachineTypeId = 6,
                Model = "Stanowisko 1",
                Manufacturer = "",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(4, context.Operators.Find(4)?.UUID),
                    new OperatorIdClass(7, context.Operators.Find(7)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - brak", new ApplicationUser()),
                    new NoteClass("Serwis 12.09.2016", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "S09",
                MachineTypeId = 6,
                Model = "Stanowisko 2",
                Manufacturer = "",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(4, context.Operators.Find(4)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - brak", new ApplicationUser()),
                     new NoteClass("Zepsuta sonda", new ApplicationUser()),
                    new NoteClass("Serwis 10.12.2016", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "S10",
                MachineTypeId = 6,
                Model = "Stanowisko 3",
                Manufacturer = "",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(1, context.Operators.Find(1)?.UUID),
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID),
                    new OperatorIdClass(3, context.Operators.Find(3)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Serwis 12.12.2015", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "S11",
                MachineTypeId = 7,
                Model = "Stanowisko 4",
                Manufacturer = "",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - brak", new ApplicationUser()),
                    new NoteClass("Serwis 12.12.2016", new ApplicationUser())
                    }
            });
            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "S12",
                MachineTypeId = 7,
                Model = "Stanowisko 5",
                Manufacturer = "",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - 12.12.2005", new ApplicationUser()),
                    new NoteClass("Brak osłony tarczy szlifierskiej", new ApplicationUser()),
                     new NoteClass("Serwis 10.12.2016", new ApplicationUser())
                    }
            });

            context.Machines.Add(new Machine()
            {
                UUID = Guid.NewGuid().ToString(),
                Code = "P13",
                MachineTypeId = 8,
                Model = "Plot4000",
                Manufacturer = "Gento",
                Operators = new HashSet<OperatorIdClass>() {
                    new OperatorIdClass(2, context.Operators.Find(2)?.UUID),
                    new OperatorIdClass(5, context.Operators.Find(5)?.UUID) },
                Notes = new List<NoteClass>() {
                    new NoteClass("Remont kapitalny - 12.12.2005", new ApplicationUser()),
                    new NoteClass("Brak osłony tarczy szlifierskiej", new ApplicationUser()),
                     new NoteClass("Serwis 10.12.2016", new ApplicationUser())
                    }
            });

            context.SaveChanges();
        }

    }
}