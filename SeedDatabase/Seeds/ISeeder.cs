﻿namespace Batat.SeedDatabase.Seeds
{
    public interface ISeeder
    {
        /// <summary>
        /// Seeds sample data
        /// </summary>
        void Seed();
    }
}
