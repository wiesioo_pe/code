﻿using Batat.Models.Authentication;
using Batat.Models.Database.DbContexts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void AddUsersAndRoles(ApplicationDataDbContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            // Add missing roles

            IdentityRole role = new IdentityRole("Administrator");
            try
            {
                roleManager.Create(role);
            }
            catch { }

            role = new IdentityRole("Operator");
            try
            {
                roleManager.Create(role);
            }
            catch { }

            role = new IdentityRole("Office");
            try
            {
                roleManager.Create(role);
            }
            catch { }

            role = new IdentityRole("Manager");
            try
            {
                roleManager.Create(role);
            }
            catch { }

            role = new IdentityRole("Chief");
            try
            {
                roleManager.Create(role);
            }
            catch { }

            // CREATE ADMININSTATORS AND MANAGERS
            var user = userManager.FindByName("Wieslaw.Piechowiak");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Wieslaw.Piechowiak",
                    Email = "wiesioo@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "wiesioo@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Administrator");
                }
                catch { }
            }


            user = userManager.FindByName("Marek.Dobrzynski");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Marek.Dobrzynski",
                    Email = "marek@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "marek@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Administrator");
                }
                catch { }
            }


            user = userManager.FindByName("Piotr.Zachwieja");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Piotr.Zachwieja",
                    Email = "zachwieja@mpmit.pl"
                };
                try
                {
                    userManager.Create(newUser, "zachwieja@mpmit.pl");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Administrator");
                }
                catch { }
            }


            // CREATE MANAGER
            user = userManager.FindByName("Krzysztof.Palicki");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Krzysztof.Palicki",
                    Email = "Krzysztof.Palicki@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "Krzysztof.Palicki@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Manager");
                }
                catch { }
            }


            // CREATE OFFICE
            user = userManager.FindByName("Asia.Slomkowska");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Asia.Slomkowska",
                    Email = "Asia.Slomkowska@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "Asia.Slomkowska@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Office");
                }
                catch { }
            }

            user = userManager.FindByName("Magda.Bak");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Magda.Bak",
                    Email = "Magda.Bak@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "Magda.Bak@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Office");
                }
                catch { }
            }

            user = userManager.FindByName("Kacper.Gawronski");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Kacper.Gawronski",
                    Email = "Kacper.Gawronski@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "Kacper.Gawronski@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Office");
                }
                catch { }
            }


            // CREATE CHIEF
            user = userManager.FindByName("Agata.Malinowska");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Agata.Malinowska",
                    Email = "Agata.Malinowska@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "Agata.Malinowska@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Chief");
                }
                catch { }
            }

            user = userManager.FindByName("Patryk.Wrzesiński");
            if (user == null)
            {
                var newUser = new ApplicationUser()
                {
                    UserName = "Patryk.Wrzesiński",
                    Email = "Patryk.Wrzesiński@gmail.com"
                };
                try
                {
                    userManager.Create(newUser, "Patryk.Wrzesiński@gmail.com");
                    userManager.SetLockoutEnabled(newUser.Id, false);
                    userManager.AddToRole(newUser.Id, "Chief");
                }
                catch { }
            }

        }
    }
}