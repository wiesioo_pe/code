﻿using System;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedMaterialTypes(ApplicationDataDbContext context)
        {

            context.MaterialTypes.Add(
                new MaterialType()
                {
                    UUID = Guid.NewGuid().ToString(),
                    Name = "Stal konstrukcyjna zwykła St3s",
                    Code = "St3s",
                    Desc = "Stal zwykła konstrukcyjna"
                });

            context.MaterialTypes.Add(
                new MaterialType()
                {
                    UUID = Guid.NewGuid().ToString(),
                    Name = "Stal konstrukcyjna podwyższonej jakości St45",
                    Code = "St45",
                    Desc = "Stal konstrukcyjna jakościowa"
                });

            context.MaterialTypes.Add(
                new MaterialType()
                {
                    UUID = Guid.NewGuid().ToString(),
                    Name = "Aluminium",
                    Code = "Alu",
                    Desc = "Aluminium"
                });

            context.MaterialTypes.Add(
                new MaterialType()
                {
                    UUID = Guid.NewGuid().ToString(),
                    Name = "Duraluminium",
                    Code = "DurAl",
                    Desc = "Duralu"
                });

            context.MaterialTypes.Add(
                new MaterialType()
                {
                    UUID = Guid.NewGuid().ToString(),
                    Name = "PCV",
                    Code = "PCV",
                    Desc = "PCV"
                });


            context.SaveChanges();
        }
    }

    public partial class SeedDatabase
    {
        public static void SeedMaterials(ApplicationDataDbContext context)
        {

            context.Materials.Add(
                new Material()
                {
                    UUID = Guid.NewGuid().ToString(),
                    MaterialTypeId = 1,
                    MaterialTypeUUID = context.MaterialTypes.Find(1)?.UUID,
                    Name = "Blacha 1mm stal st3s",
                    Code = "st3s-blch-1",
                    Desc = "Blacha 1mm stal st3s",
                    UnitCost = 10.0M,
                    Unit = "metr"
                });

            context.Materials.Add(
                new Material()
                {
                    UUID = Guid.NewGuid().ToString(),
                    MaterialTypeId = 1,
                    MaterialTypeUUID = context.MaterialTypes.Find(1)?.UUID,
                    Name = "Pręt 30mm stal st3s",
                    Code = "st3s-prt-30",
                    Desc = "Pręt 30mm stal st3s",
                    UnitCost = 10.0M,
                    Unit = "metr"
                });

            context.Materials.Add(
                new Material()
                {
                    UUID = Guid.NewGuid().ToString(),
                    MaterialTypeId = 1,
                    MaterialTypeUUID = context.MaterialTypes.Find(1)?.UUID,
                    Name = "Pręt 50mm stal st3s",
                    Code = "st3s-prt-50",
                    Desc = "Pręt 50mm stal st3s",
                    UnitCost = 69.0M,
                    Unit = "metr"
                });

            context.Materials.Add(
                new Material()
                {
                    UUID = Guid.NewGuid().ToString(),
                    MaterialTypeId = 1,
                    MaterialTypeUUID = context.MaterialTypes.Find(1)?.UUID,
                    Name = "Pręt 80mm stal st3s",
                    Code = "st3s-prt-80",
                    Desc = "Pręt 80mm stal st3s",
                    UnitCost = 160.0M,
                    Unit = "metr"
                });

            context.Materials.Add(
                new Material()
                {
                    UUID = Guid.NewGuid().ToString(),
                    MaterialTypeId = 2,
                    MaterialTypeUUID = context.MaterialTypes.Find(2)?.UUID,
                    Name = "Pręt 60mm stal st45",
                    Code = "st45-prt-60",
                    Desc = "Pręt 60mm stal st45",
                    UnitCost = 220.0M,
                    Unit = "metr"
                });

            context.Materials.Add(
                new Material()
                {
                    UUID = Guid.NewGuid().ToString(),
                    MaterialTypeId = 2,
                    MaterialTypeUUID = context.MaterialTypes.Find(2)?.UUID,
                    Name = "Pręt 80mm stal st45",
                    Code = "st45-prt-80",
                    Desc = "Pręt 80mm stal st45",
                    UnitCost = 320.0M,
                    Unit = "metr"
                });


            context.SaveChanges();
        }
    }
}