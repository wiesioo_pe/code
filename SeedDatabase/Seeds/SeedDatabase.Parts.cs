﻿using System.Collections.Generic;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedParts(ApplicationDataDbContext context)
        {
            // 1
            context.Parts.Add(new Part
            {
                Name = "Blat dębowy",
                Code = "B1",
                Desc = "Blat dębowy lity",
                Parameters = new List<ParameterIdValueClass>
                {
                    new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "32"),
                    new ParameterIdValueClass(10, context.Parameters.Find(10)?.UUID, "400"),
                    new ParameterIdValueClass(20, context.Parameters.Find(20)?.UUID, "BEZEL"),
                    new ParameterIdValueClass(21, context.Parameters.Find(21)?.UUID, "WX165"),
                },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 2
            context.Parts.Add(new Part
            {
                Name = "Blat sosnowy",
                Code = "B2",
                Desc = "Blat sosnowy lity",
                Parameters = new List<ParameterIdValueClass>
                {
                    new ParameterIdValueClass(16, context.Parameters.Find(16)?.UUID, "284"),
                    new ParameterIdValueClass(10, context.Parameters.Find(10)?.UUID, "400"),
                    new ParameterIdValueClass(20, context.Parameters.Find(20)?.UUID, "BEZEL"),
                    new ParameterIdValueClass(21, context.Parameters.Find(21)?.UUID, "SX165/2"),
                    new ParameterIdValueClass(26, context.Parameters.Find(26)?.UUID, "2"),
                },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 3
            context.Parts.Add(new Part
            {
                Name = "Blat kamienny",
                Code = "B3",
                Desc = "Blat kamienny marmur",
                Parameters = new List<ParameterIdValueClass>
                {
                    new ParameterIdValueClass(18, context.Parameters.Find(18)?.UUID, "PA10"),
                    new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0003_1"),
                    new ParameterIdValueClass(23, context.Parameters.Find(23)?.UUID, "Kołnierzowe")
                },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(8, context.Groups.Find(8)?.UUID)
                }
            });
            // 4
            context.Parts.Add(new Part
            {
                Name = "Blat z laminatu",
                Code = "B4",
                Desc = "Blat z laminatu epoksydowego",
                Parameters = new List<ParameterIdValueClass>
                {
                    new ParameterIdValueClass(16, context.Parameters.Find(16)?.UUID, "32"),
                    new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0004_1")
                },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 5
            context.Parts.Add(new Part
            {
                Name = "Blat bambusowy",
                Code = "B5",
                Desc = "Blat z bambusa",
                Parameters = new List<ParameterIdValueClass>
                {
                    new ParameterIdValueClass(24, context.Parameters.Find(24)?.UUID, "8"),
                    new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0005_1")
                },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(19, context.Groups.Find(19)?.UUID)
                }
            });
            // 6
            context.Parts.Add(new Part
            {
                Name = "Rama stala",
                Code = "R1",
                Desc = "Rama bez możliwości rozkładania",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(7, context.Parameters.Find(7)?.UUID, "260"),
                    new ParameterIdValueClass(25, context.Parameters.Find(2)?.UUID, "2,5")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(19, context.Groups.Find(19)?.UUID)
                }
            });


            // 7
            context.Parts.Add(new Part
            {
                Name = "Rama rozkladana",
                Code = "R2",
                Desc = "Rama z mechanizmem do rozkładania standard ",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(3, context.Parameters.Find(3)?.UUID, "1475"),
                    new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "40"),
                    new ParameterIdValueClass(10, context.Parameters.Find(10)?.UUID, "400"),
                    new ParameterIdValueClass(20, context.Parameters.Find(20)?.UUID, "BEZEL"),
                    new ParameterIdValueClass(21, context.Parameters.Find(21)?.UUID, "WX165")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 8 
            context.Parts.Add(new Part
            {
                Name = "Rama rozkladana XL",
                Code = "R2",
                Desc = "Rama z mechanizmem do rozkładania pwiekszonego stołu ",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(16, context.Parameters.Find(16)?.UUID, "320"),
                    new ParameterIdValueClass(10, context.Parameters.Find(10)?.UUID, "400"),
                    new ParameterIdValueClass(20, context.Parameters.Find(20)?.UUID, "BEZEL"),
                    new ParameterIdValueClass(21, context.Parameters.Find(21)?.UUID, "SX215/1"),
                    new ParameterIdValueClass(26, context.Parameters.Find(26)?.UUID, "4")
            },
                Groups = new HashSet<GroupIdClass>()
                {

                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 9
            context.Parts.Add(new Part
            {
                Name = "Noga prosta",
                Code = "N1",
                Desc = "Noga do stołu metalowa prosta",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(16, context.Parameters.Find(16)?.UUID, "560"),
                    new ParameterIdValueClass(10, context.Parameters.Find(10)?.UUID, "400"),
                    new ParameterIdValueClass(20, context.Parameters.Find(20)?.UUID, "BEZEL"),
                    new ParameterIdValueClass(21, context.Parameters.Find(21)?.UUID, "SX365/2"),
                    new ParameterIdValueClass(26, context.Parameters.Find(26)?.UUID, "4")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(17, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 10
            context.Parts.Add(new Part
            {
                Name = "Noga zdobiona",
                Code = "N2",
                Desc = "Noga do stołu metalowa zdobiona",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(18, context.Parameters.Find(18)?.UUID, "GJL-250"),
                    new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0015_1"),
                    new ParameterIdValueClass(23, context.Parameters.Find(23)?.UUID, "Kołnierzowe")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(17, context.Groups.Find(17)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(9, context.Groups.Find(9)?.UUID)
                }
            });
            // 11
            context.Parts.Add(new Part
            {
                Name = "Wkładka debowa",
                Code = "W1",
                Desc = "Wkładka do powiekszania stołu debowego",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(18, context.Parameters.Find(18)?.UUID, "PA10"),
                    new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0009_1"),
                    new ParameterIdValueClass(23, context.Parameters.Find(23)?.UUID, "Kołnierzowe")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(8, context.Groups.Find(8)?.UUID)
                }
            });
            // 12
            context.Parts.Add(new Part
            {
                Name = "Wkładka sosnowa",
                Code = "W2",
                Desc = "Wkładka do powiekszania stołu sosnowego",
                Parameters = new List<ParameterIdValueClass>
            {
                new ParameterIdValueClass(16, context.Parameters.Find(16)?.UUID, "40"),
                new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0010_1")
            },
                Groups = new HashSet<GroupIdClass>()
                {

                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });
            // 13
            context.Parts.Add(new Part
            {
                Name = "Wkładka kamienna",
                Code = "W3",
                Desc = "Wkładka do powiekszania stołu kamiennego",
                Parameters = new List<ParameterIdValueClass>
            {
                new ParameterIdValueClass(24, context.Parameters.Find(24)?.UUID, "12"),
                new ParameterIdValueClass(19, context.Parameters.Find(19)?.UUID, "P0011_1")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(19, context.Groups.Find(19)?.UUID)
                }
            });
            // 14
            context.Parts.Add(new Part
            {
                Name = "Wkładka z laminatu",
                Code = "W4",
                Desc = "Wkładka do powiekszania stołu z lamiantu",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(7, context.Parameters.Find(7)?.UUID, "360"),
                    new ParameterIdValueClass(25, context.Parameters.Find(25)?.UUID, "4")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(16, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(19, context.Groups.Find(19)?.UUID)
                }
            });
            // 15
            context.Parts.Add(new Part
            {
                Name = "Wkładka bambusowa",
                Code = "W5",
                Desc = "Wkładka do powiekszania stołu bambusowego",
                Parameters = new List<ParameterIdValueClass>
            {
                    new ParameterIdValueClass(3, context.Parameters.Find(3)?.UUID, "1475"),
                    new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "68"),
                    new ParameterIdValueClass(10, context.Parameters.Find(10)?.UUID, "400"),
                    new ParameterIdValueClass(20, context.Parameters.Find(20)?.UUID, "BEZEL"),
                    new ParameterIdValueClass(21, context.Parameters.Find(21)?.UUID, "WX365")
            },
                Groups = new HashSet<GroupIdClass>()
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(17, context.Groups.Find(16)?.UUID),
                    new GroupIdClass(18, context.Groups.Find(18)?.UUID),
                    new GroupIdClass(20, context.Groups.Find(20)?.UUID)
                }
            });




            context.SaveChanges();
        }
    }
}