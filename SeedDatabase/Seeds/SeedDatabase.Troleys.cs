﻿using Batat.Helpers.Extensions.ProductionPanel;
using Batat.Models.API.ProductionPanel;
using Batat.Models.Database.DbContexts;
using System;
using System.Linq;

namespace Batat.SeedDatabase.Seeds
{
    public class TroleySeeder : Seeder
    {
        #region Constructor

        public TroleySeeder(ApplicationDataDbContext context) : base(context)
        {
        }

        #endregion


        #region Public interface

        public override void Seed()
        {
            UpdateTrolleysRandomly();
        }

        #endregion


        #region Private methods

        private void UpdateTrolleysRandomly()
        {
            var trolleys = Context.Trolleys.ToList();
            foreach (var trolley in trolleys)
            {
                var operation = Context.Operations.Find(trolley.OperationId);
                if (!operation.MachinesTimesCosts.Any())
                    continue;

                var chosenMachineAndTime = operation.MachinesTimesCosts[GetRandomArrayIndex(operation.MachinesTimesCosts.Count)];
                var startTime = DateTime.Now;
                var trolleysOnMachine = Context.Trolleys.Where(a => a.MachineId == chosenMachineAndTime.MachineId).ToList();

                var now = DateTime.Now;
                var tommorowAt6 = new DateTime(now.Year, now.Month, now.Day, 6, 0, 0).AddDays(1);
                var lastEndTime = trolleysOnMachine.Any() ? trolleysOnMachine.Max(a => a.PlannedEndTime) : tommorowAt6;
                var nextDay = lastEndTime.Value.AddDays(1);
                var nextDayAt6 = new DateTime(nextDay.Year, nextDay.Month, nextDay.Day, 6, 0, 0);
                startTime = lastEndTime.Value.Hour < 14 ? lastEndTime.Value : nextDayAt6;

                var trolleyUpdateDTO = new TrolleyUpdateDTO()
                {
                    MachineId = chosenMachineAndTime.MachineId,
                    MachineUUID = chosenMachineAndTime.MachineUUID,
                    PlannedStartTime = startTime
                };

                TrolleyDatabaseHelper.UpdateTrolley(trolley.Id, trolley.UUID, trolleyUpdateDTO, Context);
            }
        }

        #endregion
    }
}