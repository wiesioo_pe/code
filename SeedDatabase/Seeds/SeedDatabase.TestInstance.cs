﻿using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedTestInstance(ApplicationDataDbContext Context)
        {
            SeedTestParts(Context);
            SeedTestProducts(Context);
            SeedTestOperationTypes(Context);
            SeedTestMachineTypes(Context);
            SeedTestMachines(Context);
            SeedTestOperationsAndTechnologies(Context);
            SeedTestSubjectsAndContacts(Context);
            SeedTestOrders(Context);
        }

        private static void SeedTestParts(ApplicationDataDbContext Context)
        {
            var part = new Part()
            {
                Code = "Part001",
                Name = "Kółko",
                Desc = "Okrągłe kółko"
            };
            Context.Parts.Add(part);

            part = new Part()
            {
                Code = "Part002",
                Name = "Trójkąt",
                Desc = "Trójkątny trójkąt"
            };
            Context.Parts.Add(part);

            part = new Part()
            {
                Code = "Part003",
                Name = "Kwadrat",
                Desc = "Kwadratowy kwadrat"
            };
            Context.Parts.Add(part);

            Context.SaveChanges();
        }

        private static void SeedTestProducts(ApplicationDataDbContext Context)
        {
            var kółko = Context.Parts.Find(1);
            var trójkąt = Context.Parts.Find(2);
            var kwadrat = Context.Parts.Find(3);

            var product1 = new Product()
            {
                Code = "Prod001",
                Name = "Dziesięć kółek",
                Desc = "Kilka (dokładnie dziesięć) kółek dookoła siebie",
                NetPrice = 100,
                Parts = new HashSet<PartIdAmountClass>()
                {
                    new PartIdAmountClass(kółko.Id, kółko.UUID, 10)
                }
            };
            Context.Products.Add(product1);

            var product2 = new Product()
            {
                Code = "Prod002",
                Name = "Dziesięć kółek, dwadzieścia trójkątów i sto kwadratów",
                Desc = "Różne kształty w jednym produkcie",
                NetPrice = 500,
                Parts = new HashSet<PartIdAmountClass>()
                {
                    new PartIdAmountClass(kółko.Id, kółko.UUID, 10),
                    new PartIdAmountClass(trójkąt.Id, trójkąt.UUID, 20),
                    new PartIdAmountClass(kwadrat.Id, kwadrat.UUID, 100)
                }
            };
            Context.Products.Add(product2);
            Context.SaveChanges();

            var product3 = new Product()
            {
                Code = "Prod003",
                Name = "Połączone produkty - kółka + różne kształty",
                Desc = "Duży produkt złożony z kółek i różnych kształtów",
                NetPrice = 100,
                Products = new HashSet<ProductIdAmountDiscountClass>()
                {
                    new ProductIdAmountDiscountClass(product1.Id, product1.UUID, 5, 10),
                    new ProductIdAmountDiscountClass(product2.Id, product2.UUID, 5, 10)
                }
            };
            Context.Products.Add(product3);
            Context.SaveChanges();
        }

        public static void SeedTestOperationTypes(ApplicationDataDbContext Context)
        {
            Context.OperationTypes.Add(new OperationType() { Name = "Cięcie gilotyną", Code = "OperType001", Desc = "Docinanie metali gilotyną do 10mm przekroju" });
            Context.OperationTypes.Add(new OperationType() { Name = "Wyginanie prętów krawędziówka", Code = "OperType002", Desc = "Wyginanie prętów na krawędziówce" });
            Context.OperationTypes.Add(new OperationType() { Name = "Spawanie TIG", Code = "OperType003", Desc = "Spawanie metodą TIG" });
            Context.OperationTypes.Add(new OperationType() { Name = "Szlifowanie", Code = "OperType004", Desc = "Szlifowanie ręczne" });

            Context.SaveChanges();
        }

        public static void SeedTestMachineTypes(ApplicationDataDbContext Context)
        {
            Context.MachineTypes.Add(new MachineType() { Name = "Standard", Code = "MachTypeStd", Desc = "Standard, standard, standard..." });
            Context.MachineTypes.Add(new MachineType() { Name = "Gilotyna", Code = "MachType002", Desc = "Gilotyna, gilotyna, gilotyna..." });
            Context.MachineTypes.Add(new MachineType() { Name = "Krawędziówka", Code = "MachType003", Desc = "Krawędziówka, krawędziówka,krawędziówka..." });
            Context.MachineTypes.Add(new MachineType() { Name = "Spawarka", Code = "MachType004", Desc = "Spawarka, spawarka, spawarka..." });
            Context.MachineTypes.Add(new MachineType() { Name = "Szlifierka", Code = "MachType005", Desc = "Szlifierka, szlifierka, szlifierka..." });

            Context.SaveChanges();
        }

        public static void SeedTestMachines(ApplicationDataDbContext Context)
        {
            var standardowa = Context.MachineTypes.Find(1);
            var gilotyna = Context.MachineTypes.Find(2);
            var krawędziówka = Context.MachineTypes.Find(3);
            var spawarka = Context.MachineTypes.Find(4);
            var szlifierka = Context.MachineTypes.Find(5);

            Context.Machines.Add(new Machine() { Manufacturer = "Standard", Model = "Standard", Code = "MachStd", MachineTypeId = standardowa.Id, MachineTypeUUID = standardowa.UUID });
            Context.Machines.Add(new Machine() { Manufacturer = "Fanuc", Model = "GiltX", Code = "Mach002", MachineTypeId = gilotyna.Id, MachineTypeUUID = gilotyna.UUID });
            Context.Machines.Add(new Machine() { Manufacturer = "GIMB", Model = "GoodBend", Code = "Mach003", MachineTypeId = krawędziówka.Id, MachineTypeUUID = krawędziówka.UUID });
            Context.Machines.Add(new Machine() { Manufacturer = "Stig", Model = "Tiggy 8", Code = "Mach004", MachineTypeId = spawarka.Id, MachineTypeUUID = spawarka.UUID });
            Context.Machines.Add(new Machine() { Manufacturer = "Handy", Model = "Basic V6", Code = "Mach005", MachineTypeId = szlifierka.Id, MachineTypeUUID = szlifierka.UUID });

            Context.SaveChanges();
        }

        private static void SeedTestOperationsAndTechnologies(ApplicationDataDbContext Context)
        {
            var kółko = Context.Parts.Find(1);
            var trójkąt = Context.Parts.Find(2);
            var kwadrat = Context.Parts.Find(3);

            var cięcie = Context.OperationTypes.Find(1);
            var gięcieKrawędziówka = Context.OperationTypes.Find(2);
            var spawanie = Context.OperationTypes.Find(3);
            var szlifowanie = Context.OperationTypes.Find(4);

            var standardowa = Context.Machines.Find(1);
            var gilotyna = Context.Machines.Find(2);
            var krawędziówka = Context.Machines.Find(3);
            var spawarka = Context.Machines.Find(4);
            var szlifierka = Context.Machines.Find(5);


            // TECHNOLOGY 1
            var technology = new Technology()
            {
                Code = "Techn001",
                Name = "Technologia robienia kółka - ważna kolejność",
                PartId = kółko.Id,
                PartUUID = kółko.UUID
            };
            Context.Technologies.Add(technology);
            Context.SaveChanges();

            var operation1 = new Operation()
            {
                Code = "Op001Techn001",
                Desc = "Docięcie pręta kółko (ważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = cięcie.Id,
                OperationTypeUUID = cięcie.UUID
            };
            Context.Operations.Add(operation1);

            var operation2 = new Operation()
            {
                Code = "Op002Techn001",
                Desc = "Wygięcie pręta kółko (ważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = gięcieKrawędziówka.Id,
                OperationTypeUUID = gięcieKrawędziówka.UUID
            };
            Context.Operations.Add(operation2);

            var operation3 = new Operation()
            {
                Code = "Op003Techn001",
                Desc = "Spawanie pręta kółko (ważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = spawanie.Id,
                OperationTypeUUID = spawanie.UUID
            };
            Context.Operations.Add(operation3);

            var operation4 = new Operation()
            {
                Code = "Op004Techn001",
                Desc = "Szlifowanie pręta kółko (ważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = szlifowanie.Id,
                OperationTypeUUID = szlifowanie.UUID
            };
            Context.Operations.Add(operation4);
            Context.SaveChanges();

            operation2.OperationsBefore.Add(new OperationBeforeIdClass(operation1.Id, operation1.UUID));
            operation3.OperationsBefore.Add(new OperationBeforeIdClass(operation2.Id, operation2.UUID));
            operation4.OperationsBefore.Add(new OperationBeforeIdClass(operation3.Id, operation3.UUID));

            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, standardowa.Id, standardowa.UUID, 3, 10, 2, 3));
            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, gilotyna.Id, gilotyna.UUID, 2, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, standardowa.Id, standardowa.UUID, 2, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, krawędziówka.Id, krawędziówka.UUID, 1, 3, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, standardowa.Id, standardowa.UUID, 2, 5, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, spawarka.Id, spawarka.UUID, 2, 3, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, standardowa.Id, standardowa.UUID, 3, 5, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, szlifierka.Id, szlifierka.UUID, 3, 5, 2, 3));

            technology.Operations.Add(new OperationIdClass(operation1.Id, operation1.UUID));
            technology.Operations.Add(new OperationIdClass(operation2.Id, operation2.UUID));
            technology.Operations.Add(new OperationIdClass(operation3.Id, operation3.UUID));
            technology.Operations.Add(new OperationIdClass(operation4.Id, operation4.UUID));
            Context.SaveChanges();


            // TECHNOLOGY 2
            technology = new Technology()
            {
                Code = "Techn001",
                Name = "Technologia robienia kółka - nieważna kolejność",
                PartId = kółko.Id,
                PartUUID = kółko.UUID
            };
            Context.Technologies.Add(technology);
            Context.SaveChanges();

            operation1 = new Operation()
            {
                Code = "Op001Techn001",
                Desc = "Docięcie pręta kółko (nieważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = cięcie.Id,
                OperationTypeUUID = cięcie.UUID
            };
            Context.Operations.Add(operation1);

            operation2 = new Operation()
            {
                Code = "Op002Techn001",
                Desc = "Wygięcie pręta kółko (nieważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = gięcieKrawędziówka.Id,
                OperationTypeUUID = gięcieKrawędziówka.UUID
            };
            Context.Operations.Add(operation2);

            operation3 = new Operation()
            {
                Code = "Op003Techn001",
                Desc = "Spawanie pręta kółko (nieważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = spawanie.Id,
                OperationTypeUUID = spawanie.UUID
            };
            Context.Operations.Add(operation3);

            operation4 = new Operation()
            {
                Code = "Op004Techn001",
                Desc = "Szlifowanie pręta kółko (nieważna kolejność)",
                PartId = kółko.Id,
                PartUUID = kółko.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = szlifowanie.Id,
                OperationTypeUUID = szlifowanie.UUID
            };
            Context.Operations.Add(operation4);
            Context.SaveChanges();

            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, standardowa.Id, standardowa.UUID, 3, 10, 2, 3));
            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, gilotyna.Id, gilotyna.UUID, 2, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, standardowa.Id, standardowa.UUID, 2, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, krawędziówka.Id, krawędziówka.UUID, 1, 3, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, standardowa.Id, standardowa.UUID, 2, 5, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, spawarka.Id, spawarka.UUID, 2, 3, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, standardowa.Id, standardowa.UUID, 3, 5, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, szlifierka.Id, szlifierka.UUID, 3, 5, 2, 3));

            technology.Operations.Add(new OperationIdClass(operation1.Id, operation1.UUID));
            technology.Operations.Add(new OperationIdClass(operation2.Id, operation2.UUID));
            technology.Operations.Add(new OperationIdClass(operation3.Id, operation3.UUID));
            technology.Operations.Add(new OperationIdClass(operation4.Id, operation4.UUID));
            Context.SaveChanges();

            // TECHNOLOGY 3
            technology = new Technology()
            {
                Code = "Techn002",
                Name = "Technologia robienia trójkąta",
                PartId = trójkąt.Id,
                PartUUID = trójkąt.UUID
            };
            Context.Technologies.Add(technology);
            Context.SaveChanges();

            operation1 = new Operation()
            {
                Code = "Op001Techn002",
                Desc = "Docięcie pręta trójkąt",
                PartId = trójkąt.Id,
                PartUUID = trójkąt.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = cięcie.Id,
                OperationTypeUUID = cięcie.UUID
            };
            Context.Operations.Add(operation1);

            operation2 = new Operation()
            {
                Code = "Op002Techn002",
                Desc = "Wygięcie pręta trójkąt",
                PartId = trójkąt.Id,
                PartUUID = trójkąt.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = gięcieKrawędziówka.Id,
                OperationTypeUUID = gięcieKrawędziówka.UUID
            };
            Context.Operations.Add(operation2);

            operation3 = new Operation()
            {
                Code = "Op003Techn002",
                Desc = "Spawanie pręta trójkąt",
                PartId = trójkąt.Id,
                PartUUID = trójkąt.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = spawanie.Id,
                OperationTypeUUID = spawanie.UUID
            };
            Context.Operations.Add(operation3);

            operation4 = new Operation()
            {
                Code = "Op004Techn002",
                Desc = "Szlifowanie pręta trójkąt",
                PartId = trójkąt.Id,
                PartUUID = trójkąt.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = szlifowanie.Id,
                OperationTypeUUID = szlifowanie.UUID
            };
            Context.Operations.Add(operation4);
            Context.SaveChanges();

            operation2.OperationsBefore.Add(new OperationBeforeIdClass(operation1.Id, operation1.UUID));
            operation3.OperationsBefore.Add(new OperationBeforeIdClass(operation2.Id, operation2.UUID));
            operation4.OperationsBefore.Add(new OperationBeforeIdClass(operation3.Id, operation3.UUID));

            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, standardowa.Id, standardowa.UUID, 2, 10, 2, 3));
            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, gilotyna.Id, gilotyna.UUID, 1.5M, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, standardowa.Id, standardowa.UUID, 1.5M, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, krawędziówka.Id, krawędziówka.UUID, 1, 3, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, standardowa.Id, standardowa.UUID, 2, 5, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, spawarka.Id, spawarka.UUID, 2, 3, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, standardowa.Id, standardowa.UUID, 2.5M, 5, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, szlifierka.Id, szlifierka.UUID, 2, 5, 2, 3));

            technology.Operations.Add(new OperationIdClass(operation1.Id, operation1.UUID));
            technology.Operations.Add(new OperationIdClass(operation2.Id, operation2.UUID));
            technology.Operations.Add(new OperationIdClass(operation3.Id, operation3.UUID));
            technology.Operations.Add(new OperationIdClass(operation4.Id, operation4.UUID));
            Context.SaveChanges();


            // TECHNOLOGY 4
            technology = new Technology()
            {
                Code = "Techn003",
                Name = "Technologia robienia kwadratu",
                PartId = kwadrat.Id,
                PartUUID = kwadrat.UUID
            };
            Context.Technologies.Add(technology);
            Context.SaveChanges();

            operation1 = new Operation()
            {
                Code = "Op001Techn003",
                Desc = "Docięcie pręta kwadrat",
                PartId = kwadrat.Id,
                PartUUID = kwadrat.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = cięcie.Id,
                OperationTypeUUID = cięcie.UUID
            };
            Context.Operations.Add(operation1);

            operation2 = new Operation()
            {
                Code = "Op002Techn003",
                Desc = "Wygięcie pręta kwadrat",
                PartId = kwadrat.Id,
                PartUUID = kwadrat.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = gięcieKrawędziówka.Id,
                OperationTypeUUID = gięcieKrawędziówka.UUID
            };
            Context.Operations.Add(operation2);

            operation3 = new Operation()
            {
                Code = "Op003Techn003",
                Desc = "Spawanie pręta kwadrat",
                PartId = kwadrat.Id,
                PartUUID = kwadrat.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = spawanie.Id,
                OperationTypeUUID = spawanie.UUID
            };
            Context.Operations.Add(operation3);

            operation4 = new Operation()
            {
                Code = "Op004Techn003",
                Desc = "Szlifowanie pręta kwadrat",
                PartId = kwadrat.Id,
                PartUUID = kwadrat.UUID,
                TechnologyId = technology.Id,
                TechnologyUUID = technology.UUID,
                OperationTypeId = szlifowanie.Id,
                OperationTypeUUID = szlifowanie.UUID
            };
            Context.Operations.Add(operation4);
            Context.SaveChanges();

            operation2.OperationsBefore.Add(new OperationBeforeIdClass(operation1.Id, operation1.UUID));
            operation3.OperationsBefore.Add(new OperationBeforeIdClass(operation2.Id, operation2.UUID));
            operation4.OperationsBefore.Add(new OperationBeforeIdClass(operation3.Id, operation3.UUID));

            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, standardowa.Id, standardowa.UUID, 1.2M, 10, 2, 3));
            operation1.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation1.Id, operation1.UUID, gilotyna.Id, gilotyna.UUID, 1, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, standardowa.Id, standardowa.UUID, 1, 5, 2, 3));
            operation2.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation2.Id, operation2.UUID, krawędziówka.Id, krawędziówka.UUID, 0.8M, 3, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, standardowa.Id, standardowa.UUID, 1, 5, 2, 3));
            operation3.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation3.Id, operation3.UUID, spawarka.Id, spawarka.UUID, 1, 3, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, standardowa.Id, standardowa.UUID, 1.3M, 5, 2, 3));
            operation4.MachinesTimesCosts.Add(new MachineIdTimeCostClass(operation4.Id, operation4.UUID, szlifierka.Id, szlifierka.UUID, 1.2M, 5, 2, 3));

            technology.Operations.Add(new OperationIdClass(operation1.Id, operation1.UUID));
            technology.Operations.Add(new OperationIdClass(operation2.Id, operation2.UUID));
            technology.Operations.Add(new OperationIdClass(operation3.Id, operation3.UUID));
            technology.Operations.Add(new OperationIdClass(operation4.Id, operation4.UUID));
            Context.SaveChanges();
        }

        private static void SeedTestSubjectsAndContacts(ApplicationDataDbContext Context)
        {
            var subject1 = new Subject()
            {
                Name = "Ikea Ltd",
                ShortName = "Ikea"
            };
            Context.Subjects.Add(subject1);

            var subject2 = new Subject()
            {
                Name = "Żabka Poland",
                ShortName = "Żabka"
            };
            Context.Subjects.Add(subject2);
            Context.SaveChanges();

            var contact1 = new Contact()
            {
                Name = "Łukasz",
                SubjectId = subject1.Id,
                SubjectUUID = subject1.UUID
            };
            Context.Contacts.Add(contact1);

            var contact2 = new Contact()
            {
                Name = "Monika",
                SubjectId = subject2.Id,
                SubjectUUID = subject2.UUID
            };
            Context.Contacts.Add(contact2);
            Context.SaveChanges();

            subject1.Contacts.Add(new ContactIdClass(contact1.Id, contact1.UUID));
            subject2.Contacts.Add(new ContactIdClass(contact2.Id, contact2.UUID));
            Context.SaveChanges();
        }

        private static void SeedTestOrders(ApplicationDataDbContext Context)
        {
            var subject1 = Context.Subjects.Find(1);
            var subject2 = Context.Subjects.Find(2);

            var dziesięćKółek = Context.Products.Find(1);
            var różneKształty = Context.Products.Find(2);
            var produktZłożony = Context.Products.Find(3);

            var order1 = new Order()
            {
                ReceiveDate = DateTime.Now,
                AccomplishDate = DateTime.Now.AddDays(14),
                DeliveryDate = DateTime.Now.AddDays(21),
                PriorityId = 1,
                SubjectId = subject1.Id,
                SubjectUUID = subject1.UUID,
                ContactId = subject1.Contacts.First().ContactId,
                ContactUUID = subject1.Contacts.First().ContactUUID,
                Products = new HashSet<ProductIdAmountDiscountClass>()
                {
                    new ProductIdAmountDiscountClass(dziesięćKółek.Id, dziesięćKółek.UUID, 10, 5)
                }
            };
            Context.Orders.Add(order1);

            var order2 = new Order()
            {
                ReceiveDate = DateTime.Now,
                AccomplishDate = DateTime.Now.AddDays(14),
                DeliveryDate = DateTime.Now.AddDays(21),
                PriorityId = 0,
                SubjectId = subject2.Id,
                SubjectUUID = subject2.UUID,
                ContactId = subject2.Contacts.First().ContactId,
                ContactUUID = subject2.Contacts.First().ContactUUID,
                Products = new HashSet<ProductIdAmountDiscountClass>()
                {
                    new ProductIdAmountDiscountClass(różneKształty.Id, różneKształty.UUID, 5, 10)
                }
            };
            Context.Orders.Add(order2);

            var order3 = new Order()
            {
                ReceiveDate = DateTime.Now,
                AccomplishDate = DateTime.Now.AddDays(14),
                DeliveryDate = DateTime.Now.AddDays(21),
                PriorityId = 0,
                SubjectId = subject2.Id,
                SubjectUUID = subject2.UUID,
                ContactId = subject2.Contacts.First().ContactId,
                ContactUUID = subject2.Contacts.First().ContactUUID,
                Products = new HashSet<ProductIdAmountDiscountClass>()
                {
                    new ProductIdAmountDiscountClass(produktZłożony.Id, produktZłożony.UUID, 2, 20)
                }
            };
            Context.Orders.Add(order3);
            Context.SaveChanges();

            var order4 = new Order()
            {
                ReceiveDate = DateTime.Now,
                AccomplishDate = DateTime.Now.AddDays(14),
                DeliveryDate = DateTime.Now.AddDays(21),
                PriorityId = 0,
                SubjectId = subject1.Id,
                SubjectUUID = subject1.UUID,
                ContactId = subject1.Contacts.First().ContactId,
                ContactUUID = subject1.Contacts.First().ContactUUID,
                Products = new HashSet<ProductIdAmountDiscountClass>()
                {
                    new ProductIdAmountDiscountClass(dziesięćKółek.Id, dziesięćKółek.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(różneKształty.Id, różneKształty.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(produktZłożony.Id, produktZłożony.UUID, 1, 0)
                }
            };
            Context.Orders.Add(order4);
            Context.SaveChanges();
        }
    }
}