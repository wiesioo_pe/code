﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        private static readonly Random Random = new Random();

        public static void SeedOrders(ApplicationDataDbContext Context)
        {
            var date = DateTime.Today.AddHours(7);
            foreach (var any in Enumerable.Repeat(new object(), 20))
            {
                var employee = DrawEmployee(Context);
                var subject = DrawSubject(Context);

                Context.Orders.Add(new Order
                {
                    ReceiveDate = date,
                    AccomplishDate = date.AddDays(20),
                    DeliveryDate = date.AddDays(30),
                    PriorityId = DrawPriorityId(),
                    SubjectId = subject.Id,
                    SubjectUUID = subject.UUID,
                    ContactId = Context.Contacts.Where(a => a.SubjectId == subject.Id).FirstOrDefault()?.Id ?? 0,
                    ContactUUID = Context.Contacts.Where(a => a.SubjectId == subject.Id).FirstOrDefault()?.UUID,
                    EmployeeId = employee.IntId,
                    EmployeeUUID = employee.Id,
                    Desc = "",
                    StateId = DrawStateId(),
                    Products = DrawProducts(Context)
                });

                date = date.AddDays(1);
            }

            Context.SaveChanges();
        }


        private static ApplicationUser DrawEmployee(ApplicationDataDbContext Context)
        {
            var employees = Context.Users.ToList();
            if (employees.Count == 0)
            {
                throw new Exception("Users list is empty!");
            }

            int index = GetRandomIndex(employees.Count);
            return employees[index];
        }

        private static Subject DrawSubject(ApplicationDataDbContext Context)
        {
            var subjects = Context.Subjects.ToList();
            if (subjects.Count == 0)
            {
                throw new Exception("Users list is empty!");
            }

            int index = GetRandomIndex(subjects.Count);
            return subjects[index];
        }

        private static int DrawStateId()
        {
            return GetRandomIndex(3);
        }

        private static int DrawPriorityId()
        {
            return GetRandomIndex(2) - 1;
        }

        private static HashSet<ProductIdAmountDiscountClass> DrawProducts(ApplicationDataDbContext Context)
        {
            var products = new HashSet<ProductIdAmountDiscountClass>();
            foreach (var any in Enumerable.Repeat(new object(), GetRandomIndex(6)))
            {
                var productId = GetRandomIndex(Context.Products.Count());
                if (products.Any(a => a.Id == productId))
                    continue;

                products.Add(new ProductIdAmountDiscountClass(productId, Context.Products.Find(productId).UUID, GetRandomIndex(20), 0M));
            }

            return products;
        }

        private static int GetRandomIndex(int count) => Random.Next(0, count - 1) + 1;
    }
}