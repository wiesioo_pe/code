﻿using System;
using System.Collections.Generic;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedSubjects(ApplicationDataDbContext context)
        {
            var generatedUUID = Guid.NewGuid().ToString();
            context.Contacts.Add(new Contact { Name = "Paul Vlag", SubjectId = 1, SubjectUUID = generatedUUID, Division = "Siedziba NL", Position = "Prezes", BusinessPhone = "665525327", OfficePhone = "0523026736", PrivatePhone = "667453285", Fax = "0523059842", eMail = "ceo@ikea.nl", Desc = "Wciąż zajęty" });
            context.Contacts.Add(new Contact { Name = "Michał Leszczyna", SubjectId = 1, SubjectUUID = generatedUUID, Division = "Oddział PL", Position = "Sprzedawca", BusinessPhone = "625867238", eMail = "michal@ikea.pl", Desc = "Można dzwonić" });
            context.Contacts.Add(new Contact { Name = "Janek Kleszka", SubjectId = 1, SubjectUUID = generatedUUID, Division = "Oddział", Position = "Kierowca", BusinessPhone = "558428945", eMail = "janek.kleszka@ikea.pl", Desc = "Spoko kierowca" });
            context.SaveChanges();
            context.Subjects.Add(new Subject
            {
                UUID = generatedUUID,
                Name = "IKEA BV",
                isCustomer = true,
                ShortName = "IKEA",
                Discount = 10.0M,
                VATRegNum = "NL5623873625",
                WWW = "www.ikea.pl",
                Street = "Amsterdam",
                City = "Ikeaweg 1",
                PostalCode = "6250KN",
                State = "Amsterdam",
                Phone = "(034)3022672",
                Desc = "Ikea to ikea",
                Contacts = new List<ContactIdClass> { new ContactIdClass(1, context.Contacts.Find(1)?.UUID), new ContactIdClass(2, context.Contacts.Find(2)?.UUID), new ContactIdClass(3, context.Contacts.Find(3)?.UUID) }
            });

            generatedUUID = Guid.NewGuid().ToString();
            context.Contacts.Add(new Contact { Name = "George Pig", SubjectId = 2, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Chief executive", BusinessPhone = "+1 414-352-8080", eMail = "george@inc.com", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Peppa Pig", SubjectId = 2, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Konstruktorka", BusinessPhone = "+1 414-352-8081", eMail = "peppa@inc.com", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Cindy Pig", SubjectId = 2, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Technolożka", BusinessPhone = "+1 414-352-8082", eMail = "cindy@inc.com", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Brownie Pig", SubjectId = 2, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Frezer", BusinessPhone = "+1 414-352-8082", eMail = "brownie@inc.com", Desc = "" });
            context.SaveChanges();
            context.Subjects.Add(new Subject
            {
                UUID = generatedUUID,
                Name = "Peppa furnitures inc.",
                isCustomer = true,
                isSupplier = true,
                ShortName = "Peppa.",
                Discount = 15.0M,
                VATRegNum = "US7382736453",
                WWW = "peppakid.com",
                Street = "7 Madison Road",
                City = "Brown Deer Park",
                PostalCode = "53209",
                State = "Milwaukee",
                Phone = "+1 414-352-8080",
                Desc = "Czas dostawy 21-30 dni",
                Contacts = new List<ContactIdClass> { new ContactIdClass(4, context.Contacts.Find(4)?.UUID), new ContactIdClass(5, context.Contacts.Find(5)?.UUID), new ContactIdClass(6, context.Contacts.Find(6)?.UUID), new ContactIdClass(7, context.Contacts.Find(7)?.UUID) }
            });

            generatedUUID = Guid.NewGuid().ToString();
            context.Contacts.Add(new Contact { Name = "Agata Kowalska", SubjectId = 3, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Prezes", BusinessPhone = "+91 48 621 311 111", eMail = "agata@agata.pl", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Stefan Nowak", SubjectId = 3, SubjectUUID = generatedUUID, Division = "Magazyn", Position = "Kierownik Działu Sprzedaży", BusinessPhone = "+48  5886 5454 44", eMail = "stefan.nowak@agata.pl", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Jan Mateja", SubjectId = 3, SubjectUUID = generatedUUID, Division = "Magazyn", Position = "Koordynator przesyłek", BusinessPhone = "+91 20 6624 3838", eMail = "jan.mateja@agata.pl", Desc = "" });
            context.SaveChanges();
            context.Subjects.Add(new Subject
            {
                UUID = generatedUUID,
                Name = "Agata Meble sp. z o.o.",
                isCustomer = true,
                ShortName = "Agata",
                Discount = 5.0M,
                VATRegNum = "PL7823945674",
                WWW = "agata.in",
                Street = "Meblowa 6",
                City = "Kalisz",
                PostalCode = "65-888",
                State = "Wlkp",
                Phone = "+48 96 243 440",
                Desc = "Nie płaci na czas",
                Contacts = new List<ContactIdClass> { new ContactIdClass(8, context.Contacts.Find(8)?.UUID), new ContactIdClass(9, context.Contacts.Find(9)?.UUID), new ContactIdClass(10, context.Contacts.Find(10)?.UUID) }
            });

            generatedUUID = Guid.NewGuid().ToString();
            context.Contacts.Add(new Contact { Name = "Tho Shazheng", SubjectId = 4, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Prezes", BusinessPhone = "023-89111369", eMail = "tho.shazheng@chongqing.ch", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Tianchen Hubei", SubjectId = 4, SubjectUUID = generatedUUID, Division = "Hala", Position = "Spedytor", BusinessPhone = "023-63733589", eMail = "Tianchen@chongqing.ch", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Yang Guang", SubjectId = 4, SubjectUUID = generatedUUID, Division = "Sprzedaż", Position = "Kierownik Produkcji", BusinessPhone = "023-63810322", eMail = "yang@chongqing.ch", Desc = "" });
            context.SaveChanges();
            context.Subjects.Add(new Subject
            {
                UUID = generatedUUID,
                Name = "Chongqing Furnitures Co., Ltd.",
                isCustomer = true,
                isSupplier = true,
                ShortName = "Chongqing China",
                Discount = 8.0M,
                VATRegNum = "CH9283621523",
                WWW = "chongqing.co",
                Street = "Shuangbei Laodong Road 21",
                City = "Chongqing",
                PostalCode = "400000",
                State = "Sha Ping Ba District",
                Phone = "023-63625529",
                Desc = "Luksusowe meble dla chinczyków",
                Contacts = new List<ContactIdClass> { new ContactIdClass(11, context.Contacts.Find(11)?.UUID), new ContactIdClass(12, context.Contacts.Find(12)?.UUID), new ContactIdClass(13, context.Contacts.Find(13)?.UUID) }
            });

            generatedUUID = Guid.NewGuid().ToString();
            context.Contacts.Add(new Contact { Name = "Maciej Klaus", SubjectId = 5, SubjectUUID = generatedUUID, Division = "Biuro", Position = "Kierownik Utrzymani8a Ruchu Lubin", BusinessPhone = "‎616279901", eMail = "maciej.klaus@bodzio.pl", Desc = "" });
            context.Contacts.Add(new Contact { Name = "Darek Kaźmierczak", SubjectId = 5, SubjectUUID = generatedUUID, Division = "Projektowanie", Position = "Handlowiec na Bydgoszcz", BusinessPhone = "(+48) 81 535 65 65", eMail = "dariusz.kazmierczak@bodzio.pl", Desc = "" });
            context.SaveChanges();
            context.Subjects.Add(new Subject
            {
                UUID = generatedUUID,
                Name = "Bodzio Meble S.A.",
                isCustomer = true,
                ShortName = "Bodzio",
                Discount = 20.0M,
                VATRegNum = "PL1359372359",
                WWW = "bodzio.pl",
                Street = "Marii Skłodowskiej-Curie 48",
                City = "Jawor",
                PostalCode = "59-301",
                State = "dolnośląskie",
                Phone = "+48 76 74 78 200",
                Desc = "",
                Contacts = new List<ContactIdClass> { new ContactIdClass(14, context.Contacts.Find(14)?.UUID), new ContactIdClass(15, context.Contacts.Find(15)?.UUID) }
            });


            context.SaveChanges();
        }
    }
}