﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedTechnologies(ApplicationDataDbContext context)
        {
            var operationTypeCount = context.OperationTypes.Count();
            var machinesCount = context.Machines.Count();
            var standardMachine = context.Machines.Find(1);

            var parts = context.Parts.ToList();
            foreach (var currentPart in parts)
            {
                var currentTechnology = new Technology
                {
                    UUID = Guid.NewGuid().ToString(),
                    Name = "Technologia - " + currentPart.Name,
                    PartId = currentPart.Id,
                    PartUUID = currentPart.UUID,
                };

                context.Technologies.Add(currentTechnology);
                context.SaveChanges();

                currentTechnology.Code = TechnologiesHelper.CreateTechnologyCode(currentPart.Id, currentTechnology.Id, context);
                var operationIdClasses = new List<OperationIdClass>();
                var operationsCount = Random.Next(10) + 2;

                var operationsBefore = new List<OperationBeforeIdClass>();
                for (int i = 0; i < operationsCount; i++)
                {
                    var currentOperation = new Operation
                    {
                        UUID = Guid.NewGuid().ToString(),
                        OperationTypeId = Random.Next(operationTypeCount) + 1,
                        TechnologyId = currentTechnology.Id,
                        TechnologyUUID = currentTechnology.UUID,
                        PartId = currentPart.Id,
                        PartUUID = currentPart.UUID,
                        OperationsBefore = operationsBefore
                    };

                    context.Operations.Add(currentOperation);
                    context.SaveChanges();

                    currentOperation.Code = OperationsHelper.CreateOperationCode(currentPart.Id, currentTechnology.Id, i, context);
                    var standardMachineUnitTime = Math.Round(Convert.ToDecimal(Random.NextDouble() * 4), 2) + 0.5M;
                    var standardMachinePcTime = Math.Round(Convert.ToDecimal(Random.NextDouble() - 0.5), 2) + 1.0M;
                    currentOperation.MachinesTimesCosts = new List<MachineIdTimeCostClass>
                    {
                        new MachineIdTimeCostClass(currentOperation.Id, currentOperation.UUID, standardMachine.Id, standardMachine.UUID, standardMachineUnitTime, standardMachinePcTime, 0M, 0M)
                    };

                    var numberOfMachinesInDatabase = context.Machines.Count();
                    var indexesOfDrawnMachines = Enumerable.Repeat(string.Empty, Random.Next(3) + 1).Select(a => Random.Next(numberOfMachinesInDatabase - 1) + 1).Distinct().OrderBy(a => a);
                    var machines = context.Machines.ToList();

                    foreach (var index in indexesOfDrawnMachines)
                    {
                        var currentMachine = machines[index];
                        var machineUnitTime = Math.Round(Convert.ToDecimal(Random.NextDouble() * 2 - 1), 2) + standardMachineUnitTime;
                        var machinePcTime = Math.Round(Convert.ToDecimal(Random.NextDouble() - 0.5), 2) + standardMachinePcTime;
                        var machineTimeCost = new MachineIdTimeCostClass(currentOperation.Id, currentOperation.UUID, currentMachine.Id, currentMachine.UUID, machineUnitTime, machinePcTime, 0M, 0M);

                        currentOperation.MachinesTimesCosts.Add(machineTimeCost);
                    }

                    context.Entry(currentOperation).State = EntityState.Modified;
                    context.SaveChanges();

                    operationIdClasses.Add(new OperationIdClass(currentOperation.Id, currentOperation.UUID));
                    operationsBefore = operationsBefore.ToList();
                    operationsBefore.Add(new OperationBeforeIdClass { OperationBeforeId = currentOperation.Id, OperationBeforeUUID = currentOperation.UUID });
                }

                currentTechnology.Operations = operationIdClasses;
                context.Entry(currentTechnology).State = EntityState.Modified;
                context.SaveChanges();
            }
       }
    }
}