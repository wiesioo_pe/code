﻿using Batat.Helpers.Extensions.ProductionPanel;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using System.Collections.Generic;
using System.Linq;

namespace Batat.SeedDatabase.Seeds
{
    public class TechnologyPicksSeeder : Seeder
    {
        #region Constructor

        public TechnologyPicksSeeder(ApplicationDataDbContext context) : base(context)
        {
        }

        #endregion


        #region Public interface

        public override void Seed()
        {
            ChooseTechnologiesRandomly();
        }

        #endregion


        #region Private methods

        private void ChooseTechnologiesRandomly()
        {
            ///
            /// CREATE PRODUCTION TECHNOLOGIES
            ///

            // Percent of ProductionParts with selected technology
            const double technologySelectionChance = 0.33;
            // Maximum amount of production parts in a single bundle
            const int maximumAmountOfPartsInOneBundle = 5;

            foreach (var part in this.Context.Parts.ToList())
            {
                // Skip if there's no technology for the current Part
                if (!this.Context.Technologies.Any(a => a.PartId == part.Id))
                {
                    continue;
                }

                var productionParts = new Queue<ProductionPart>(Context.ProductionParts.Where(a => a.PartId == part.Id));
                var availableTechnologies = this.Context.Technologies.Where(a => a.PartId == part.Id).ToList();
                TechnologyPickVM technologyPickVM = new TechnologyPickVM() { PartUUID = part.UUID };
                // Enumerate every production part and choose technology if chance allows
                while (productionParts.Any())
                {
                    var productionPart = productionParts.Dequeue();
                    // Check chance
                    if (!CheckChance(technologySelectionChance))
                    {
                        continue;
                    }

                    // Draw to do amount
                    var amount = GetRandomPositiveInteger(productionPart.ToDoAmount);
                    if (amount == 0)
                    {
                        continue;
                    }

                    technologyPickVM.Amounts.Add(new TechnologyPickProductionProductAmountVM()
                    {
						PlanAmount = amount,
						ProductionProductUUID = productionPart.ProductionProductUUID
					});

                    if (CheckChance(0.2)
                        || technologyPickVM.Amounts.Count == maximumAmountOfPartsInOneBundle
                        || !productionParts.Any())
                    {
                        technologyPickVM.TechnologyUUID = GetRandomTechnology(availableTechnologies).UUID;
                        TechnologyPickDatabaseHelper.PickTechnology(technologyPickVM, Context);
                        technologyPickVM.Amounts.Clear();
                    }
                }
            }
        }

        private Technology GetRandomTechnology(List<Technology> technologies)
        {
            var randomTechnology = technologies[GetRandomArrayIndex(technologies.Count())];

            return randomTechnology;
        }

        #endregion
    }
}