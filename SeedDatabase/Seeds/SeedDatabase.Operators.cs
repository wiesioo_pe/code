﻿using System;
using System.Collections.Generic;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedOperators(ApplicationDataDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var newUser = new ApplicationUser()
            {
                UserName = "Jerzy_Dudek",
                Email = "Jerzy.Dudek@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Jerzy.Dudek@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); }
                catch (Exception)
                {
                    // ignored
                }
            }
            catch (Exception)
            {
                // ignored
            }

            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Jerzy Dudek",
                Position = "Frezer",
                Function = "Operator",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("Porządny pracownik, dba o jakość wykonania", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Michal_Krzesinski",
                Email = "Michal.Krzesinski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Michal.Krzesinski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Michał Krzesiński",
                Position = "Krajacz",
                Function = "Krajacz",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("Jo jo jo, jo jo jo", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Marcin_Malinowski",
                Email = "Marcin.Malinowski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Marcin.Malinowski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Marcin Malinowski",
                Position = "Operator",
                Function = "Tokarz CNC",
                HourlyRate = 18.00M,
                OvertimeRate = 28.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("Nie obsługuje HAAS", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Jedrzej_Chomiczak",
                Email = "Jedrzej.Chomiczak@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Jedrzej.Chomiczak@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Jędrzej Chomiczak",
                Position = "Kierownik",
                Function = "Kierownik narzędziowni",
                HourlyRate = 22.00M,
                OvertimeRate = 36.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Waclaw_Zasada",
                Email = "Waclaw.Zasada@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Waclaw.Zasada@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Wacław Zasada",
                Position = "Ślusarz",
                Function = "Ślusarz",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("Idelanie wykańcza formy", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Piotr_Kwasniewski",
                Email = "Piotr.Kwasniewski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Piotr.Kwasniewski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Piotr Kwaśniewski",
                Position = "Operator",
                Function = "Tokarz CNC",
                HourlyRate = 22.00M,
                OvertimeRate = 28.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Marian_Bzdzmil",
                Email = "Marian.Bzdzmil@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Marian.Bzdzmil@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Marian Bźdzmił",
                Position = "Operator",
                Function = "Operator centrum CNC",
                HourlyRate = 29.00M,
                OvertimeRate = 38.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("Bardzo terminowy", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Jaroslaw_Przyborski",
                Email = "Jaroslaw.Przyborski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Jaroslaw.Przyborski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Jarosław Przyborski",
                Position = "Operator",
                Function = "Tokarz",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Lukasz_Bimbowski",
                Email = "Lukasz.Bimbowski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Lukasz.Bimbowski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Łukasz Bimbowski",
                Position = "Operator",
                Function = "Operator elektrodrążarki",
                HourlyRate = 15.00M,
                OvertimeRate = 25.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Michal_Kaminski",
                Email = "Michal.Kaminski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Michal.Kaminski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Michał Kamiński",
                Position = "Operator",
                Function = "Operator wtryskarki",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Waclaw_Pastuszewski",
                Email = "Waclaw.Pastuszewski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Waclaw.Pastuszewski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Wacław Pastuszewski",
                Position = "Operator",
                Function = "Operator wtryskarki",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Kamil_Slomkowski",
                Email = "Kamil.Slomkowski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Kamil.Slomkowski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Kamil Słomkowski",
                Position = "Operator",
                Function = "Operator wtryskarki",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Andrzej_Filipiak",
                Email = "Andrzej.Filipiak@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Andrzej.Filipiak@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Andrzej Filipiak",
                Position = "Operator",
                Function = "Operator wtryskarki",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Piotr_Zielinski",
                Email = "Piotr.Zielinski@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Piotr.Zielinski@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Piotr Zieliński",
                Position = "Operator",
                Function = "Szlifierz",
                HourlyRate = 12.00M,
                OvertimeRate = 18.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("brak", new ApplicationUser())
                    }
            });



            newUser = new ApplicationUser()
            {
                UserName = "Slawomir_Bajgel",
                Email = "Slawomir.Bajgel@gmail.com"
            };
            try
            {
                userManager.Create(newUser, "Slawomir.Bajgel@gmail.com");
                userManager.SetLockoutEnabled(newUser.Id, false);
                try { userManager.AddToRole(newUser.Id, "Operator"); } catch { }
            }
            catch
            { }
            context.Operators.Add(new Operator()
            {
                UUID = Guid.NewGuid().ToString(),
                EmployeeUUID = newUser.Id,
                Name = "Sławomir Bajgel",
                Position = "Operator",
                Function = "Operator centrum CNC",
                HourlyRate = 22.00M,
                OvertimeRate = 36.00M,
                Notes = new List<NoteClass>() {
                    new NoteClass("Niedoświadczony, nie dawać kosztownych zleceń", new ApplicationUser())
                    }
            });

            // Pomiędzy tymi komentarzami

            context.SaveChanges();
        }
    }
}