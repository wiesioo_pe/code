﻿using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedParameters(ApplicationDataDbContext context)
        {
            // ********************** Parametry dla wszystkich produktów **************************************************
            context.Parameters.Add(new Parameter { Name = "Moc", Unit = "KW" });                          //Parameter 1
            context.Parameters.Add(new Parameter { Name = "Moment wy", Unit = "Nm" });
            context.Parameters.Add(new Parameter { Name = "Obroty wy", Unit = "obr/min" });
            context.Parameters.Add(new Parameter { Name = "Przeciążalność", Unit = "%" });
            context.Parameters.Add(new Parameter { Name = "Sposób montażu", Unit = "" });                  //Parameter 5
            context.Parameters.Add(new Parameter { Name = "Średnica wału wyjściowego", Unit = "mm" });
            context.Parameters.Add(new Parameter { Name = "Długość", Unit = "mm" });
            context.Parameters.Add(new Parameter { Name = "Szerokość", Unit = "mm" });
            context.Parameters.Add(new Parameter { Name = "Wysokość", Unit = "mm" });
            // ********************** Parametry dla silników **************************************************
            context.Parameters.Add(new Parameter { Name = "Napięcie zasilania", Unit = "V" });             //Parameter 10
            context.Parameters.Add(new Parameter { Name = "Rodzaj chłodzenia", Unit = "" });
            // ********************** Parametry dla przekładni i motoreduktorów **************************************************
            context.Parameters.Add(new Parameter { Name = "Przełożenie", Unit = "" });                     //Parameter 12
            context.Parameters.Add(new Parameter { Name = "Liczba stopni", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Rodzaj zębów", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Kierunek instalacji", Unit = "" });
            // ********************** Parametry dla detali **************************************************
            context.Parameters.Add(new Parameter { Name = "Średnica", Unit = "mm" });                        //Parameter 16
            context.Parameters.Add(new Parameter { Name = "Liczba zębów", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Materiał", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Nr rysunku", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Dostawca", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Symbol", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Klasa", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Podłączenie", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Rozmiar", Unit = "" });
            context.Parameters.Add(new Parameter { Name = "Przekrój przewodu", Unit = "mm2" });
            context.Parameters.Add(new Parameter { Name = "Liczba par biegunów", Unit = "" });

            context.SaveChanges();
        }
    }
}