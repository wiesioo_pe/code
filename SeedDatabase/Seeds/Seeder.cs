﻿using Batat.Models.Database.DbContexts;
using System;

namespace Batat.SeedDatabase.Seeds
{
    public abstract class Seeder : ISeeder
    {
        public abstract void Seed();

        #region Dependencies

        protected readonly ApplicationDataDbContext Context;

        #endregion

        #region Constructor

        protected Seeder(ApplicationDataDbContext context)
        {
            this.Context = context;
        }

        #endregion


        #region Private fields

        // We are seeding the random generator to always seed with the same data
        protected readonly Random randomGenerator = new Random(1);

        #endregion

        /// <summary>
        /// Gets random integer value <0, maximumValue> by default
        /// or (0, maximumValue> if allowZero == false
        /// </summary>
        /// <param name="maximumValue"></param>
        /// <param name="allowZero"></param>
        /// <returns></returns>
        protected int GetRandomPositiveInteger(int maximumValue, bool allowZero = true)
        {
            double randomDouble = this.randomGenerator.NextDouble();
            if (allowZero)
            {
                var randomNumber = ++maximumValue * randomDouble;
                return (int)randomNumber;
            }
            else
            {
                var randomNumber = (maximumValue * randomDouble) + 1;
                return (int)randomNumber;
            }
        }

        /// <summary>
        /// Gets random array index <0, length)
        /// </summary>
        /// <param name="count"></param>
        /// <param name="startFromZero"></param>
        /// <returns></returns>
        protected int GetRandomArrayIndex(int count)
        {
            return GetRandomPositiveInteger(--count);
        }

        /// <summary>
        /// Gets random database index (0, length)
        /// </summary>
        /// <param name="count"></param>
        /// <param name="startFromZero"></param>
        /// <returns></returns>
        protected int GetRandomDatabaseIndex(int count)
        {
            return GetRandomPositiveInteger(count, allowZero: false);
        }

        protected bool CheckChance(double chance)
        {
            return this.randomGenerator.NextDouble() < chance;
        }
    }
}
