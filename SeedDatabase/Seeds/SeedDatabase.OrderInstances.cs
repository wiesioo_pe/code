﻿using System.Linq;
using Batat.Helpers.Extensions.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedOrderInstances(ApplicationDataDbContext Context)
        {
            var orders = Context.Orders.Where(a => a.StateId == 2).ToList();
            foreach (var order in orders)
            {
                var user = Context.Users.First();
                OrderDatabaseHelper.IntoProduction(order, user, Context);
            }
        }
    }
}