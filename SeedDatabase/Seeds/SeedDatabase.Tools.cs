﻿using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedToolTypes(ApplicationDataDbContext context)
        {

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Wiertło zwykłe",
                    Code = "WrtłZwkł",
                    Desc = "Zwykłe wiertło lite ze stali narzędziowej"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Wiertło składane",
                    Code = "WrtłSkł",
                    Desc = "Wiertło składane na płytki wymienne"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Frez czołowy zwykły",
                    Code = "FrzCzZwkł",
                    Desc = "Zwykły frez czołowy lity ze stali narzędziowej"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Frez czołowy składany",
                    Code = "FrzCzSkł",
                    Desc = "Frez składany czołowy na płytki wymienne"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Frez ślimakowy zwykły",
                    Code = "FrzŚlkZwkł",
                    Desc = "Zwykły frez ślimakowy lity ze stali narzędziowej"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Frez ślimakowy składany",
                    Code = "FrzŚlkSkł",
                    Desc = "Frez składany ślimakowy na płytki wymienne"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Nóż tokarski zdzierak zwykły",
                    Code = "NżTkZdzZwkł",
                    Desc = "Zwykły nóż tokarski zdzierak lity ze stali narzędziowej"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Nóż tokarski zdzierak składany",
                    Code = "NżTkZdzSkł",
                    Desc = "Składany nóż tokarski zdzierak na płytki wymienne"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Nóż tokarski wytaczak zwykły",
                    Code = "NżTkWtkZwkł",
                    Desc = "Zwykły nóż tokarski wytaczak lity ze stali narzędziowej"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Nóż tokarski wytaczak składany",
                    Code = "NżTkWtkSkł",
                    Desc = "Składany nóż tokarski wytaczak na płytki wymienne"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Płytka frezerska z węglika",
                    Code = "PłtkFrz",
                    Desc = "Płytka frezerska z węglika"
                });

            context.ToolTypes.Add(
                new ToolType
                {
                    Name = "Płytka frezerska ceramiczna",
                    Code = "PłtkFrzCer",
                    Desc = "Płytka frezerska ceramiczna"
                });


            context.SaveChanges();
        }
    }

    public partial class SeedDatabase
    {
        public static void SeedTools(ApplicationDataDbContext context)
        {

            context.Tools.Add(
                new Tool
                {
                    ToolTypeId = 1,
                    Name = "Wiertło zwykłe 13mm",
                    Code = "WrtłZwkł-13",
                    Desc = "Opis do Wiertło zwykłe 13mm",
                    UnitCost = 10.0M
                });

            context.Tools.Add(
                new Tool
                {
                    ToolTypeId = 1,
                    Name = "Wiertło zwykłe 15mm",
                    Code = "WrtłZwkł-15",
                    Desc = "Opis do Wiertło zwykłe 15mm",
                    UnitCost = 14.0M
                });

            context.Tools.Add(
                new Tool
                {
                    ToolTypeId = 1,
                    Name = "Wiertło zwykłe 17mm",
                    Code = "WrtłZwkł-17",
                    Desc = "Opis do Wiertło zwykłe 17mm",
                    UnitCost = 16.0M
                });

            context.Tools.Add(
                new Tool
                {
                    ToolTypeId = 3,
                    Name = "Frez czołowy składany 130mm",
                    Code = "FrzCzSkł-130",
                    Desc = "Opis do Frez czołowy składany 130mm",
                    UnitCost = 360.0M
                });

            context.Tools.Add(
                new Tool
                {
                    ToolTypeId = 3,
                    Name = "Frez czołowy składany 200mm",
                    Code = "FrzCzSkł-200",
                    Desc = "Opis do Frez czołowy składany 200mm",
                    UnitCost = 480.0M
                });

            context.Tools.Add(
            new Tool
            {
                ToolTypeId = 7,
                Name = "Nóż tokarski wytaczak składany 400mm",
                Code = "NżTkWtkSkł-400",
                Desc = "Opis do Nóż tokarski wytaczak składany 400mm",
                UnitCost = 320.0M
            });

            context.Tools.Add(
            new Tool
            {
                ToolTypeId = 9,
                Name = "Płytka frezerska U z węglika powlekana",
                Code = "PłtkFrzWglkPwl-4N",
                Desc = "Opis do Płytka frezerska U z węglika powlekana z czterema narożami użytkowymi",
                UnitCost = 13.0M
            });

            context.SaveChanges();
        }
    }
}