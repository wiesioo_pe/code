﻿using System;
using System.Collections.Generic;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedFixedCostCategories(ApplicationDataDbContext context)
        {
            if (context?.FixedCostCategories == null
                || context.FixedCostTypes == null
                || context.FixedCosts == null)
                return;

            // ********************** FIXED COST CATEGORIES **************************************************
            //1
            context.FixedCostCategories.Add(new FixedCostCategory
            {
                Name = "Ogólne",
                Desc = ""
            });
            //2
            context.FixedCostCategories.Add(new FixedCostCategory
            {
                Name = "Marketing",
                Desc = ""
            });
            //3
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Obsługa zamówień",
                Desc = ""
            });
            //4
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Planowanie produkcji",
                Desc = ""
            });
            //5
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Obsługa produkcji",
                Desc = ""
            });
            //6
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Kontrola jakości",
                Desc = ""
            });
            //7
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Utrzymanie ruchu",
                Desc = ""
            });
            //8
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Magazyn",
                Desc = ""
            });
            //9
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Narzędziownia",
                Desc = ""
            });
            //10
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Transport",
                Desc = ""
            });
            //11
            context.FixedCostCategories.Add(new FixedCostCategory()
            {
                Name = "Usługi zewnętrzne",
                Desc = ""
            });


            // ********************** ZAMÓWIENIA - KONIEC **************************************************

            context.SaveChanges();
        }

        public static void SeedFixedCostTypes(ApplicationDataDbContext context)
        {
            // ********************** Typy kosztów **************************************************

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Infrastruktura",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Ogród",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(1, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Parking",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(1, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pomieszczenie biurowe",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pomieszczenie pomocnicze",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pomieszczenie produkcyjne",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pomieszczenie socjalne",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Recepcja",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(1, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Teren wspólny",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(1, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Wyposażenie biurowe",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Wyposarzenie magazynowe",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context), new FixedCostCategoryIdClass(8, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Wyposażenie pomocnicze",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Wyposażenie produkcyjne",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "inne",
                Desc = "",
                ParentTypeId = 1,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Media",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Energia elektryczna",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Gaz",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Odbiór odpadów produkcyjnych",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Odbiór odpoadów komunalnych",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Ogrzewanie",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Woda",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 2,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "IT",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Internet",
                Desc = "",
                ParentTypeId = 3,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Sprzęt IT",
                Desc = "",
                ParentTypeId = 3,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Telefon",
                Desc = "",
                ParentTypeId = 3,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 3,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Oprogramowanie",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Biurowe",
                Desc = "",
                ParentTypeId = 4,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inżynierskie",
                Desc = "",
                ParentTypeId = 4,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Księgowe",
                Desc = "",
                ParentTypeId = 4,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(1, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Zarządzania",
                Desc = "",
                ParentTypeId = 4,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 4,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Marketing",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Akcje charytatywne",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Akcje promocyjne",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Materiały promocyjne",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Media społecznościowe",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Newsletter",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Promocja",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Reklama outdoor",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Reklama w mediach",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Reklama inna",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Sponsoring",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Strona WWW",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Targi branżowe",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Utrzymanie klienta",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 5,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Zapewnienie jakości",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Audyt jakościowy",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Certyfikacja jakościowa",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Maszyna pomiarowa",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context), new FixedCostCategoryIdClass(6, context), new FixedCostCategoryIdClass(10, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Przyrząd pomiarowy",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context), new FixedCostCategoryIdClass(6, context), new FixedCostCategoryIdClass(10, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Standardy i instrukcje",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Stanowisko kontroli jakości",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(6, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 6,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Logistyka",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Paliwo",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Samochód służbowy",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Samochód transportowy",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Samochód inny",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Urządzenie transportowe",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Podróże służbowe",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 7,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Usługi zewnętrzne",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Audyt",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Certyfikacja",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Domena internetowa",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Doradztwo",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Obsługa księgowa",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Obsługa prawna",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Serwer",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Szkolenia i konferencje",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Usługi internetowe inne",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Usługi remontowe",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Utrzymanie budynków",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 8,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Personel",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Kierowca",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pracownik biurowy",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pracownik ds. marketingu",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(2, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Kierownik",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Pracownik pomocniczy",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Specjalista/Inżynier",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 9,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Socjalne",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Dodatki socjalne",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Posiłki",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Środki higieny osobistej",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Środki utrzymania czystości",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Ubrania robocze",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Woda i napoje",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 10,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Podatki i opłaty",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Licencje",
                Desc = "",
                ParentTypeId = 11,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Opłaty bankowe",
                Desc = "",
                ParentTypeId = 11,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Podatek od nieruchomości",
                Desc = "",
                ParentTypeId = 11,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Podatek inny",
                Desc = "",
                ParentTypeId = 11,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Ubezpieczenie",
                Desc = "",
                ParentTypeId = 11,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 11,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Utrzymanie",
                Desc = "",
                ParentTypeId = 0,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Detale zamienne",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Czyściwo",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Materiały eksploatacyjne inne",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Oleje i smary",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Przeglądy serwisowe",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Remont maszyn",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(5, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Serwis/naprawa",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Utrzymanie ogrodu",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(1, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Utrzymanie pomieszczeń",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });

            context.FixedCostTypes.Add(new FixedCostType()
            {
                Name = "Inne",
                Desc = "",
                ParentTypeId = 12,
                Categories = new HashSet<FixedCostCategoryIdClass>() { new FixedCostCategoryIdClass(0, context) }
            });


            // ********************** Typy kosztów - KONIEC **************************************************

            context.SaveChanges();
        }

        public static void SeedFixedCosts(ApplicationDataDbContext context)
        {
            // ********************** FIXED COSTS **************************************************


            // ********************************************************************************************************************
            //MARKETING
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 3,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(12),
                Amount = 3000.0M,
                Desc = "Organizacja festynu dla mieszkańców",
                Period = 12,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 18,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(12),
                Amount = 5000.0M,
                Desc = "Ulotki, listy",
                Period = 12,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 24,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(12),
                Amount = 1500.0M,
                Desc = "Strona na fejsbuku + zdjęcia + obsługa strony",
                Period = 12,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 44,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 2900.0M,
                Desc = "Marta z marketingu",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 51,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 200.0M,
                Desc = "Biuro marketingu",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 61,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 400.0M,
                Desc = "Reklamy na samochodach obcych",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 62,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 100.0M,
                Desc = "Reklama na rondzie Hetmańskim",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 63,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 300.0M,
                Desc = "Reklama w radiu",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 64,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 400.0M,
                Desc = "Samochód przedstawiciela hadlowego",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 40,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 800.0M,
                Desc = "Paliwo przedstawiciel handlowy",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 96,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 2600.0M,
                Desc = "Jędrzej przedstawiciel",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 77,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(24),
                Amount = 1500.0M,
                Desc = "Opracowanie strony internetowej zlecone Super-WWW service",
                Period = 24,
                isRecurring = false
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 2,
                TypeId = 79,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(12),
                Amount = 8000.0M,
                Desc = "Targi w Kielcach i Łodzi",
                Period = 12,
                isRecurring = false
            });


            // ********************************************************************************************************************
            // Obsługa zamówień
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 3,
                TypeId = 72,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(36),
                Amount = 2800.0M,
                Desc = "Zakup nowego komputera",
                Period = 36,
                isRecurring = false
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 3,
                TypeId = 51,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 300.0M,
                Desc = "Biuro obsługi zamówień",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 3,
                TypeId = 43,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 2900.0M,
                Desc = "Gosia",
                Period = 1,
                isRecurring = true
            });

            // ********************************************************************************************************************
            // Planowanie produkcji
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 4,
                TypeId = 20,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(60),
                Amount = 3000.0M,
                Desc = "Zakup nowych mebli",
                Period = 60,
                isRecurring = false
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 4,
                TypeId = 38,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(12),
                Amount = 8000.0M,
                Desc = "Licencja programu Ipomea Batat Production Fellow",
                Period = 12,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 4,
                TypeId = 45,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 4500.0M,
                Desc = "Tomek kierownik",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 4,
                TypeId = 51,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 300.0M,
                Desc = "Biuro planowania produkcji",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 4,
                TypeId = 71,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(36),
                Amount = 3000.0M,
                Desc = "Zakup komputera planowania",
                Period = 36,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 4,
                TypeId = 71,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(36),
                Amount = 4000.0M,
                Desc = "Zakup monitora dotykowego 32''",
                Period = 36,
                isRecurring = true
            });


            // ********************************************************************************************************************
            // Obsługa produkcji
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 10,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 3000.0M,
                Desc = "Wynajem hali produkcyjnej",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 12,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 100.0M,
                Desc = "Pozostałe materiały -  ściernice, chłodziwo",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 22,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(120),
                Amount = 18000.0M,
                Desc = "Stoły i inne meble produkcyjne",
                Period = 120,
                isRecurring = false
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 27,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 800.0M,
                Desc = "Zakup płytek skrawających",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 29,
                StartDate = DateTime.Now,
                StopDate = (DateTime.Now).AddMonths(240),
                Amount = 250000.0M,
                Desc = "Zakup centrum obróbczego",
                Period = 240,
                isRecurring = false
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 33,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 200.0M,
                Desc = "Odbiór i segragacja złomu",
                Period = 1,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 37,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 400.0M,
                Desc = "",
                Period = 3,
                isRecurring = true
            });

            //
            context.FixedCosts.Add(new FixedCost()
            {
                CategoryId = 5,
                TypeId = 47,
                StartDate = DateTime.Now,
                StopDate = null,
                Amount = 2400.0M,
                Desc = "Jakub, obsługa produkcji, pomocnik",
                Period = 1,
                isRecurring = true
            });


            context.SaveChanges();
        }
    }
}