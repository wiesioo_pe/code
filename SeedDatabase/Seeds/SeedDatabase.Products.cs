﻿using System.Collections.Generic;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedProducts(ApplicationDataDbContext context)
        {
            // 1
            context.Products.Add(new Product
            {
                Name = "Stol debowy",
                Code = "ST1",
                Desc = "Stol debowy nierozkladany",
                NetPrice = 540.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(1, context.Parts.Find(1)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4)
                    },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });

            // 2
            context.Products.Add(new Product
            {
                Name = "Stol debowy rozkladany ",
                Code = "ST2",
                Desc = "Stol debowy rozkladany standard",
                NetPrice = 950.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(1, context.Parts.Find(1)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                        new PartIdAmountClass(11, context.Parts.Find(11)?.UUID, 1)
                    },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });


            // 3
            context.Products.Add(new Product
            {
                Name = "Stol debowy rozkladany XL",
                Code = "ST3",
                Desc = "Stol debowy rozkladany XL",
                NetPrice = 1620.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(1, context.Parts.Find(1)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                        new PartIdAmountClass(11, context.Parts.Find(11)?.UUID, 2)
                    },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(4, context.Groups.Find(4)?.UUID),
                    new GroupIdClass(6, context.Groups.Find(6)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 4
            context.Products.Add(new Product
            {
                Name = "Stol sosnowy",
                Code = "ST4",
                Desc = "Stol sosnowy nierozkladany",
                NetPrice = 280.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(2, context.Parts.Find(2)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 5
            context.Products.Add(new Product
            {
                Name = "Stol sosnowy ozdobny",
                Code = "ST5",
                Desc = "Stol sosnowy nierozkladany ozdobny",
                NetPrice = 280.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(2, context.Parts.Find(2)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(10, context.Parts.Find(10)?.UUID, 4)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 6
            context.Products.Add(new Product
            {
                Name = "Stol sosnowy rozkladany XL",
                Code = "ST6",
                Desc = "Stol sosnowy rozkladany XL",
                NetPrice = 380.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(2, context.Parts.Find(2)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                        new PartIdAmountClass(12, context.Parts.Find(12)?.UUID, 2)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 7
            context.Products.Add(new Product
            {
                Name = "Stol sosnowy rozkladany XL ozdobny",
                Code = "ST7",
                Desc = "Stol sosnowy rozkladany XL ozdobny",
                NetPrice = 300.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(2, context.Parts.Find(2)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(10, context.Parts.Find(10)?.UUID, 4),
                        new PartIdAmountClass(12, context.Parts.Find(12)?.UUID, 2)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });

            // 8
            context.Products.Add(new Product
            {
                Name = "Stol sosnowy rozkladany ozdobny",
                Code = "ST8",
                Desc = "Stol sosnowy rozkladany ozdobny",
                NetPrice = 420.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(2, context.Parts.Find(2)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(10, context.Parts.Find(10)?.UUID, 4),
                        new PartIdAmountClass(12, context.Parts.Find(12)?.UUID, 1)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 9
            context.Products.Add(new Product
            {
                Name = "Stol z laminatu",
                Code = "ST9",
                Desc = "Stol z laminatu nierozkladany",
                NetPrice = 640.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(4, context.Parts.Find(4)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(6, context.Groups.Find(6)?.UUID)
                }
            });
            // 10
            context.Products.Add(new Product
            {
                Name = "Stol z laminatu rozkladany",
                Code = "ST10",
                Desc = "Stol z laminatu rozkladany",
                NetPrice = 880.00M,
                Parts = new HashSet<PartIdAmountClass>
                    {
                        new PartIdAmountClass(4, context.Parts.Find(4)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                        new PartIdAmountClass(13, context.Parts.Find(13)?.UUID, 1)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>(),
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(3, context.Groups.Find(3)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID),
                    new GroupIdClass(6, context.Groups.Find(6)?.UUID)
                }
            });

            //context.SaveChanges();

            // 11
            context.Products.Add(new Product
            {
                Name = "Stol kamienny",
                Code = "ST11",
                Desc = "Stol z marmuru",
                NetPrice = 850.00M,
                Parts = new HashSet<PartIdAmountClass>
                {
                        new PartIdAmountClass(3, context.Parts.Find(3)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>
                {
                    new ProductIdAmountDiscountClass(1, context.Products.Find(1)?.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(4, context.Products.Find(4)?.UUID, 1, 0)
                },
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(5, context.Groups.Find(5)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });

            // 12
            context.Products.Add(new Product
            {
                Name = "Stol kamienny ozdobny",
                Code = "ST12",
                Desc = "Stol z marmuru, ozdobny",
                NetPrice = 950.00M,
                Parts = new HashSet<PartIdAmountClass>
                {
                        new PartIdAmountClass(3, context.Parts.Find(3)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(10, context.Parts.Find(10)?.UUID, 4)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>
                {
                    new ProductIdAmountDiscountClass(1, context.Products.Find(1)?.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(5, context.Products.Find(5)?.UUID, 1, 0)
                },
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(5, context.Groups.Find(5)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 13
            context.Products.Add(new Product
            {
                Name = "Stol bambusowy",
                Code = "ST13",
                Desc = "Stol bambusowy",
                NetPrice = 1300.00M,
                Parts = new HashSet<PartIdAmountClass>
                {
                        new PartIdAmountClass(5, context.Parts.Find(5)?.UUID, 1),
                        new PartIdAmountClass(6, context.Parts.Find(6)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>
                {
                    new ProductIdAmountDiscountClass(2, context.Products.Find(2)?.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(6, context.Products.Find(6)?.UUID, 1, 0)
                },
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(5, context.Groups.Find(5)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });
            // 14
            context.Products.Add(new Product
            {
                Name = "Stol bambusowy rozkladany",
                Code = "ST14",
                Desc = "Stol bambusowy rozkladany standard",
                NetPrice = 1650.00M,
                Parts = new HashSet<PartIdAmountClass>
                {
                        new PartIdAmountClass(5, context.Parts.Find(1)?.UUID, 5),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                        new PartIdAmountClass(15, context.Parts.Find(15)?.UUID, 1)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>
                {
                    new ProductIdAmountDiscountClass(2, context.Products.Find(2)?.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(7, context.Products.Find(7)?.UUID, 1, 0)
                },
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(5, context.Groups.Find(5)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });

            // 15
            context.Products.Add(new Product
            {
                Name = "Stol bambusowy rozkladany XL",
                Code = "ST13",
                Desc = "Stol bambusowy rozkladany XL",
                NetPrice = 2400.00M,
                Parts = new HashSet<PartIdAmountClass>
                {
                        new PartIdAmountClass(5, context.Parts.Find(5)?.UUID, 1),
                        new PartIdAmountClass(7, context.Parts.Find(7)?.UUID, 1),
                        new PartIdAmountClass(9, context.Parts.Find(9)?.UUID, 4),
                        new PartIdAmountClass(15, context.Parts.Find(15)?.UUID, 2)
                },
                Products = new HashSet<ProductIdAmountDiscountClass>
                {
                    new ProductIdAmountDiscountClass(3, context.Products.Find(3)?.UUID, 1, 0),
                    new ProductIdAmountDiscountClass(8, context.Products.Find(8)?.UUID, 1, 0)
                },
                Parameters = new List<ParameterIdValueClass>
                    {
                        new ParameterIdValueClass(4, context.Parameters.Find(4)?.UUID, "120"),
                        new ParameterIdValueClass(6, context.Parameters.Find(6)?.UUID, "110"),
                        new ParameterIdValueClass(12, context.Parameters.Find(12)?.UUID, "21.4"),
                        new ParameterIdValueClass(13, context.Parameters.Find(13)?.UUID, "3"),
                        new ParameterIdValueClass(14, context.Parameters.Find(14)?.UUID, "Skośne"),
                        new ParameterIdValueClass(15, context.Parameters.Find(15)?.UUID, "Poziomy")
                    },
                Groups = new HashSet<GroupIdClass>
                {
                    new GroupIdClass(5, context.Groups.Find(5)?.UUID),
                    new GroupIdClass(6, context.Groups.Find(6)?.UUID),
                    new GroupIdClass(7, context.Groups.Find(7)?.UUID)
                }
            });

            context.SaveChanges();
        }
    }
}