﻿using System;
using Batat.Models;
using Batat.Models.Database.DbContexts;

namespace Batat.SeedDatabase.Seeds
{
    public partial class SeedDatabase
    {
        public static void SeedGroups(ApplicationDataDbContext context)
        {
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR001/WOLNE", Name = "Wolne", Desc = "Długi czas na sztukę" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR002/SZYBK", Name = "Szybkie", Desc = "Krótki czas na sztukę" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR003/RED", Name = "Reduktory", Desc = "Same reduktory bez silnika" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR004/SILN", Name = "Silniki", Desc = "Same silniki bez reduktora" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR005/MOTORED", Name = "Motoreduktory", Desc = "Silniki wraz z reduktorem" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR006/KLUCZ", Name = "Kluczowe", Desc = "Kluczowe dla zakładu" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR007/PROD_GOT", Name = "Produkty gotowe", Desc = "Składane gotowe produkty" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR008/ALU", Name = "Aluminium", Desc = "Produkty z alu wytwarzane w ramach własnej produkcji" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR009/ZEL", Name = "Żeliwo", Desc = "Produkty z zeliwa wytwarzane w ramach własnej produkcji" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR010/GORN", Name = "Górnictwo", Desc = "Produkty dla górnictwa" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR011/ENER", Name = "Energetyka", Desc = "Produkty dla energetyki" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR012/KRUSZ", Name = "Kruszywa", Desc = "Produkty dla kruszyw" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR013/KO", Name = "Klienci ostateczni", Desc = "Ostateczni odbiorcy, użytkujący produktu" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR014/KU", Name = "Usługobiorcy", Desc = "Odbiorcy będący podwykonawcami ostateczych klientów" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR015/USLUGA", Name = "Usługi", Desc = "Części zamienne do produktów" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR016/MAG", Name = "Z magazynu", Desc = "Produkty dla których utrzymywany jest stan magazynowy" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR017/ZAM", Name = "Na zamówienie", Desc = "Produkty produkowane wyłacznie na zamówienie" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR018/PROD", Name = "Produkcja", Desc = "Produkcja własna" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR019/ZAKP", Name = "Zakup", Desc = "Detale lub produkty kupowane" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR020/ELE", Name = "Elektryczne", Desc = "Elementy elektryczne / uzwojenia(ze znaczną zawartoscia miedzi) wytwarzane w ramach własnej produkcji" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR021/MET", Name = "Obróbka", Desc = "Elementy obrabiane skrawaniem, wytwarzane w ramach własnej produkcji" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR022/TW", Name = "Tokarsko-wiertarskie", Desc = "" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR023/FW", Name = "Frezarsko-wiertarskie", Desc = "" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR024/FM", Name = "Frezowanie modułowe", Desc = "" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR025/SZ", Name = "Szlifierski", Desc = "" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR026/WT", Name = "Wtryskiwanie", Desc = "" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR027/OA", Name = "Odlewane", Desc = "" });
            context.Groups.Add(new Group() { UUID = Guid.NewGuid().ToString(), Code = "GR028/MO", Name = "Montaż", Desc = "" });

            context.SaveChanges();
        }

public static void SeedOperationTypes(ApplicationDataDbContext context)
{
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Klejenie" }); //1
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Docinanie" }); //2
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Struganie" }); //3
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Malowanie" }); //4
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Składanie" }); //5
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Wiercenie" }); //6
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Smarowanie" }); //7
    context.OperationTypes.Add(new OperationType() { UUID = Guid.NewGuid().ToString(), Name = "Toczenie" }); //8

    context.SaveChanges();
}

public static void SeedMachineTypes(ApplicationDataDbContext context)
{
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Standardowa" }); //1
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Prasa klejąca" }); //2
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Wiertarka" }); //3
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Ucinarka" }); //4
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Strugarka" }); //5
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Stanowisko malowania" }); //6
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Stanowisko montazowe" }); //7
    context.MachineTypes.Add(new MachineType() { UUID = Guid.NewGuid().ToString(), Name = "Ploter tnący" });  //8

    context.SaveChanges();
        }
    }
}