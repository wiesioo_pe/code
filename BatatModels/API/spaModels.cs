﻿using System.Collections.Generic;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Batat.Models.Identity;
using Microsoft.AspNet.Identity;
using SharedHelper = Batat.Models.Helpers.DatabasePanel.SharedHelper;

namespace Batat.Models.API
{
    public class ProductIdAmountDiscountVM
    {
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public decimal Discount { get; set; }

        public ProductIdAmountDiscountVM() { }

        public ProductIdAmountDiscountVM(int id, int amount, decimal discount)
        {
            ProductId = id;
            Amount = amount;
            Discount = discount;
        }
    }

    public class PartIdAmountVM
    {
        public int PartId { get; set; }
        public int Amount { get; set; }
        public PartIdAmountVM() { }

        public PartIdAmountVM(int id, int amount)
        {
            PartId = id;
            Amount = amount;
        }
    }


    public class ParameterIdValueVM
    {
        public int ParameterId { get; set; }
        public string Value { get; set; } = "";

        public ParameterIdValueVM() { }

        public ParameterIdValueVM(ParameterIdValueClass parameterIdValue)
        {
            ParameterId = parameterIdValue.ParameterId;
            Value = parameterIdValue.Value ?? "";
        }

        public ParameterIdValueVM(int parameterId, string value)
        {
            ParameterId = parameterId;
            Value = value ?? "";
        }
    }


    public class MachineIdTimesCostsVM
    {
        public int MachineId { get; set; }
        public string Time { get; set; } = "";
        public MachineIdTimesCostsVM() { }
    }


    public class OperationTypeVM
    {
        public int Id { get; set; }
        public string Name { get; set; } = "";
        public string Code { get; set; } = "";
        public string Desc { get; set; } = "";


        public OperationTypeVM() { }

        public OperationTypeVM(OperationType operationType)
        {
            Id = operationType.Id;
            Name = operationType.Name ?? "";
            Code = operationType.Code ?? "";
            Desc = operationType.Desc ?? "";
        }

        public OperationTypeVM(int operationTypeId, ApplicationDataDbContext context)
        {
            Id = operationTypeId;
            Name = OperationsTypesHelper.GetOperationTypeName(operationTypeId, context);
            Code = OperationsTypesHelper.GetOperationTypeCode(operationTypeId, context);
            Desc = OperationsTypesHelper.GetOperationTypeDescription(operationTypeId, context);
        }
    }



    public class EmployeeVM
    {
        public int Id { get; set; }
        public string UUID { get; set; } = "";
        public string Name { get; set; } = "";
        public string[] Roles { get; set; }

        public EmployeeVM() { }

        public EmployeeVM(ApplicationUser user, ApplicationUserManager userManager)
        {
            if(user != null)
            {
                Id = user.IntId;
                UUID = user.Id;
                Name = user.UserName;
                Roles = userManager.GetRoles(user.Id).ToArray();
            }
        }
    }

    public class OrderStateVM
    {
        public int OrderStateId { get; set; }
        public string Name { get; set; } = "";

        public OrderStateVM() { }

        public OrderStateVM(int stateId, string name)
        {
            OrderStateId = stateId;
            Name = name ?? "";
        }
    }


    public class MachineTypeVM
    {
        public int Id { get; set; }
        public string Name { get; set; } = "";
        public string Code { get; set; } = "";
        public string Desc { get; set; } = "";

        public MachineTypeVM() { }

        public MachineTypeVM(MachineType machineType)
        {
            Id = machineType.Id;
            Name = machineType.Name ?? "";
            Code = machineType.Code ?? "";
            Desc = machineType.Desc ?? "";
        }
    }

    public class FixedCostTypeVM
    {
        public int FixedCostTypeId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public int ParentTypeId { get; set; }
        public List<int> Categories { get; set; } = new List<int>();

        public FixedCostTypeVM() { }
        public FixedCostTypeVM(FixedCostType fixedCostType)
        {
            FixedCostTypeId = fixedCostType.Id;
            Name = fixedCostType.Name;
            Desc = fixedCostType.Desc;
            ParentTypeId = fixedCostType.ParentTypeId;
            foreach (var categoryId in fixedCostType.Categories)
            {
                Categories.Add(categoryId.FixedCostCategoryId);
            }
        }
    }

    public class FixedCostCategoryVM
    {
        public int FixedCostCategoryId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public FixedCostCategoryVM() { }
        public FixedCostCategoryVM(FixedCostCategory fixedCostCategory)
        {
            FixedCostCategoryId = fixedCostCategory.Id;
            Name = fixedCostCategory.Name;
            Desc = fixedCostCategory.Desc;
        }
    }


    public class ChangeVM
    {
        public int ChangeId { get; set; }
        public string Name { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeUUID { get; set; }
        public string Desc { get; set; }
        public string DateTime { get; set; }

        public ChangeVM() { }
        public ChangeVM(ChangeClass change)
        {
            ChangeId = change.ChangeId;
            Name = change.Name;
            Desc = change.Desc;
            EmployeeId = change.EmployeeId;
            EmployeeUUID = change.EmployeeUUID;
            DateTime = SharedHelper.GetTime(change.DateTime);
        }
    }

}