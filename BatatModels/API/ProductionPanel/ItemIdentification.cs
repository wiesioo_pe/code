﻿namespace Batat.Models.API.ProductionPanel
{
    public class ItemIdentification
    {
        public int Id { get; set; }

        public string UUID { get; set; }
    }
}
