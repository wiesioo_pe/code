﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Models.API.ProductionPanel
{
    public class ProductionPartVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public int TechnologyPickedAmount { get; set; }
        public List<ProductionTechnologyBundleVM> ProductionTechnologyBundles { get; set; } = new List<ProductionTechnologyBundleVM>();
        public DateTime LastUpdate { get; set; }

        public ProductionPartVM() { }
        public ProductionPartVM(
            ProductionPart productionPart,
            ApplicationDataDbContext context,
            bool loadFullData)
        {
            Id = productionPart.Id;
            UUID = productionPart.UUID;

            PartId = productionPart.PartId;
            PartUUID = productionPart.PartUUID;
            OrderInstanceId = productionPart.OrderInstanceId;
            OrderInstanceUUID = productionPart.OrderInstanceUUID;
            ProductionProductId = productionPart.ProductionProductId;
            ProductionProductUUID = productionPart.ProductionProductUUID;
            ToDoAmount = productionPart.ToDoAmount;
            PlannedPercent = productionPart.PlannedPercent;
            MadeAmount = productionPart.MadeAmount;
            TechnologyPickedAmount = productionPart.TechnologyPickedAmount;
            LastUpdate = productionPart.LastUpdate;

            var productionTechnologyBundleIds = context.ProductionTechnologies.Where(a => a.ProductionPartId == productionPart.Id).Select(b => b.ProductionTechnologyBundleId).Distinct().ToList();
            var productionTechnologyBundles = context.ProductionTechnologyBundles.Where(a => productionTechnologyBundleIds.Any(b => b == a.Id)).ToList();
            ProductionTechnologyBundles = productionTechnologyBundles.Select(a => new ProductionTechnologyBundleVM(a)).ToList();
        }
    }
}
