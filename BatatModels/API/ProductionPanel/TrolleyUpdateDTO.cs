﻿using System;

namespace Batat.Models.API.ProductionPanel
{
    public class TrolleyUpdateDTO
    {
        public int MachineId { get; set; }
        public string MachineUUID { get; set; }

        public DateTime PlannedStartTime { get; set; }
    }
}
