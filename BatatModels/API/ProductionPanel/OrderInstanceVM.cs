﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Models.API.ProductionPanel
{
    public class OrderInstanceVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int OrderId { get; set; }
        public string OrderUUID { get; set; }

        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }

        public DateTime LastUpdate { get; set; }


        public List<ProductionProductVM> ProductionProducts { get; set; } = new List<ProductionProductVM>();

        public OrderInstanceVM(
            OrderInstance orderInstance,
            ApplicationDataDbContext context,
            bool loadFullData)
        {
            Id = orderInstance.Id;
            UUID = orderInstance.UUID;

            OrderId = orderInstance.OrderId;
            OrderUUID = orderInstance.OrderUUID;

            ToDoAmount += orderInstance.ToDoAmount;
            PlannedPercent += orderInstance.PlannedPercent;
            MadeAmount += orderInstance.MadeAmount;
            LastUpdate = orderInstance.LastUpdate;

            foreach (var pp1 in orderInstance.ProductionProducts)
            {
                var productionProduct = new ProductionProductVM(pp1, context, loadFullData);
                ProductionProducts.Add(productionProduct);
            }
        }
    }
}
