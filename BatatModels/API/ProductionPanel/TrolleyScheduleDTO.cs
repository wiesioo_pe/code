﻿using System;

namespace Batat.Models.API.ProductionPanel
{
    public class TrolleyScheduleDTO
    {
        public int ProductionOperationBundleId { get; set; }
        public string ProductionOperationBundleUUID { get; set; }

        public DateTime PlannedStartTime { get; set; }

        public int MachineId { get; set; }
        public string MachineUUID { get; set; }
    }
}
