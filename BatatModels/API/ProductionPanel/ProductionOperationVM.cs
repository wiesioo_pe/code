﻿using System.Collections.Generic;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Models.API.ProductionPanel
{
    public class ProductionOperationVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionPartId { get; set; }
        public string ProductionPartUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ProductionTechnologyId { get; set; }
        public string ProductionTechnologyUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public List<BoxIdClassVM> Boxes { get; set; } = new List<BoxIdClassVM>();

        public ProductionOperationVM(
            ProductionOperation productionOperation,
            ApplicationDataDbContext context)
        {
            Id = productionOperation.Id;
            UUID = productionOperation.UUID;

            OperationId = productionOperation.OperationId;
            OperationUUID = productionOperation.OperationUUID;
            OrderInstanceId = productionOperation.OrderInstanceId;
            OrderInstanceUUID = productionOperation.OrderInstanceUUID;
            ProductionPartId = productionOperation.ProductionPartId;
            ProductionPartUUID = productionOperation.ProductionPartUUID;
            ProductionProductId = productionOperation.ProductionProductId;
            ProductionProductUUID = productionOperation.ProductionProductUUID;
            ProductionTechnologyId = productionOperation.ProductionTechnologyId;
            ProductionTechnologyUUID = productionOperation.ProductionTechnologyUUID;
            ToDoAmount = productionOperation.ToDoAmount;
            PlannedPercent = productionOperation.PlannedPercent;
            MadeAmount = productionOperation.MadeAmount;

            foreach (var boxIdClass in productionOperation.Boxes)
            {
                Boxes.Add(new BoxIdClassVM(boxIdClass));
            }
        }
    }
}
