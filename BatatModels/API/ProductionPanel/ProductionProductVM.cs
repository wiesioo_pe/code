﻿using System;
using System.Collections.Generic;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Models.API.ProductionPanel
{
    public class ProductionProductVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public List<ProductionProductVM> ProductionProducts { get; set; } = new List<ProductionProductVM>();
        public List<ProductionPartVM> ProductionParts { get; set; } = new List<ProductionPartVM>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        public ProductionProductVM() { }
        public ProductionProductVM(
            ProductionProduct productionProduct,
            ApplicationDataDbContext context,
            bool loadFullData)
        {
            Id = productionProduct.Id;
            UUID = productionProduct.UUID;

            ProductId = productionProduct.ProductId;
            ProductUUID = productionProduct.ProductUUID;
            OrderInstanceId = productionProduct.OrderInstanceId;
            OrderInstanceUUID = productionProduct.OrderInstanceUUID;
            ProductionProductId = productionProduct.ProductionProductId;
            ProductionProductUUID = productionProduct.ProductionProductUUID;
            ToDoAmount = productionProduct.ToDoAmount;
            PlannedPercent = productionProduct.PlannedPercent;
            MadeAmount = productionProduct.MadeAmount;

            foreach (var innerProductionProduct in productionProduct.ProductionProducts)
            {
                ProductionProducts.Add(new ProductionProductVM(innerProductionProduct, context, loadFullData));
            }

            foreach (var productionPart in productionProduct.ProductionParts)
            {
                ProductionParts.Add(new ProductionPartVM(productionPart, context, loadFullData));
            }

            LastUpdate = productionProduct.LastUpdate;
        }
    }
}
