﻿using System;
using System.Collections.Generic;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Models.API.ProductionPanel
{
    public class ProductionTechnologyVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionPartId { get; set; }
        public string ProductionPartUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ProductionTechnologyBundleId { get; set; }
        public string ProductionTechnologyBundleUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public List<ProductionOperationVM> ProductionOperations { get; set; } = new List<ProductionOperationVM>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        public ProductionTechnologyVM(
            ProductionTechnology productionTechnology,
            ApplicationDataDbContext context)
        {
            Id = productionTechnology.Id;
            UUID = productionTechnology.UUID;

            TechnologyId = productionTechnology.TechnologyId;
            TechnologyUUID = productionTechnology.TechnologyUUID;
            OrderInstanceId = productionTechnology.OrderInstanceId;
            OrderInstanceUUID = productionTechnology.OrderInstanceUUID;
            ProductionPartId = productionTechnology.ProductionPartId;
            ProductionPartUUID = productionTechnology.ProductionPartUUID;
            ProductionProductId = productionTechnology.ProductionProductId;
            ProductionProductUUID = productionTechnology.ProductionProductUUID;
            ProductionTechnologyBundleId = productionTechnology.ProductionTechnologyBundleId;
            ProductionTechnologyBundleUUID = productionTechnology.ProductionTechnologyBundleUUID;
            ToDoAmount = productionTechnology.ToDoAmount;
            PlannedPercent = productionTechnology.PlannedPercent;
            MadeAmount = productionTechnology.MadeAmount;

            foreach (var productionOperation in productionTechnology.ProductionOperations)
            {
                ProductionOperations.Add(new ProductionOperationVM(productionOperation, context));
            }

            LastUpdate = productionTechnology.LastUpdate;
        }
    }
}
