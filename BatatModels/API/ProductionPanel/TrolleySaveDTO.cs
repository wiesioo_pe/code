﻿using System;

namespace Batat.Models.API.ProductionPanel
{
    public class TrolleySaveDTO: TrolleyUpdateDTO
    {
        public int ProductionTechnologyBundleId { get; set; }
        public string ProductionTechnologyBundleUUID { get; set; }

        public int OperationId { get; set; }

        public string OperationUUID { get; set; }
    }
}
