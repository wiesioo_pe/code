﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Batat.Models.API
{

    public class ViewSettings
    {
        [Key]
        public int SettingsId { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Json { get; set; }
    }



    public class ViewSettingsDbContext : System.Data.Entity.DbContext
    {
        public DbSet<ViewSettings> ViewSettings { get; set; }

        public static ViewSettingsDbContext Create()
        {
            return new ViewSettingsDbContext();
        }
    }
}