﻿using Batat.Models.Database.ProductionPanel;
using System.Linq;

namespace Batat.Models.ProductionPanel.Extensions
{
    public static class OrderInstanceExtensions
    {
        public static (ProductionProduct ProductionProduct, ProductionProduct ProductionProductFather, ProductionProduct ProductionProductGrandfather) GetProductionProductInHierarchy(this OrderInstance orderInstance, string productionProductUUID)
        {
            ProductionProduct productionProduct = null;
            ProductionProduct productionProductFather = null;
            ProductionProduct productionProductGrandfather = null;

            if (orderInstance.ProductionProducts.Any(a => a.UUID == productionProductUUID))
            {
                productionProduct = orderInstance.ProductionProducts.Single(a => a.UUID == productionProductUUID);
            }

            else if (orderInstance.ProductionProducts.Any(a => a.ProductionProducts.Any(b => b.UUID == productionProductUUID)))
            {
                productionProductFather = orderInstance.ProductionProducts.Single(a => a.ProductionProducts.Any(b => b.UUID == productionProductUUID));
                productionProduct = productionProductFather.ProductionProducts.Single(a => a.UUID == productionProductUUID);
            }
            else if (orderInstance.ProductionProducts.Any(a => a.ProductionProducts.Any(b => b.ProductionProducts.Any(c => c.UUID == productionProductUUID))))
            {
                productionProductGrandfather = orderInstance.ProductionProducts.Single(a => a.ProductionProducts.Any(b => b.ProductionProducts.Any(c => c.UUID == productionProductUUID)));
                productionProductFather = productionProductGrandfather.ProductionProducts.Single(a => a.ProductionProducts.Any(b => b.UUID == productionProductUUID));
                productionProduct = productionProductFather.ProductionProducts.Single(a => a.UUID == productionProductUUID);
            }

            return (productionProduct, productionProductFather, productionProductGrandfather);
        }
    }
}
