﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.ProductionPanel
{
    public interface IProductionProductPrototype
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int Id { get; set; }
        string UUID { get; set; }

        int ProductId { get; set; }
        string ProductUUID { get; set; }
        int OrderInstanceId { get; set; }
        string OrderInstanceUUID { get; set; }
        int ProductionProductId { get; set; }
        string ProductionProductUUID { get; set; }
        int ToDoAmount { get; set; }
        int PlannedAmount { get; set; }
        int MadeAmount { get; set; }
        List<IProductionProductPrototype> ProductionProducts { get; set; }
        List<IProductionPartPrototype> ProductionParts { get; set; }
    }
}
