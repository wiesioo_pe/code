﻿using System.Collections.Generic;
using System.Linq;
using Batat.Models.API;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.API
{
    public class SpaHelper
    {
        public static List<OrderVM> GetSpaOrders(
            int? from,
            int? to,
            ApplicationDataDbContext context)
        {
            var spaOrders = new List<OrderVM>();

            if (context.Orders.Any(i => i.Id <= from && i.Id >= to))
            {
                var orders = context.Orders.Where(i => i.Id <= from && i.Id >= to).ToList();

                foreach (var order in orders)
                {
                    spaOrders.Add(new OrderVM(order));
                }
                spaOrders = spaOrders.OrderByDescending(i => i.Id).ToList();
            }

            return spaOrders;
        }

        public static List<int> GetProducts(
            int from,
            int to,
            ApplicationDataDbContext context)
        {
            var sentProductsLists = context.Orders.Where(item => item.Id <= from && item.Id >= to)
                    .Select(item2 => item2.Products.Select(i => i.ProductId)).ToList();

            var sentProducts = new List<int>();

            foreach (var list in sentProductsLists)
            {
                foreach (var item in list)
                {
                    if (!sentProducts.Contains(item))
                    {
                        sentProducts.Add(item);
                    }
                }
            }
            return sentProducts;
        }

        public static void GetParts(
            int productId,
            ApplicationDataDbContext context,
            ref List<int> partsList)
        {
            var product = context.Products.Find(productId);

            if(product != null)
            {
                if(product.Parts != null)
                {
                    // Get all direct parts
                    foreach (var partAmount in product.Parts)
                    {
                        if (!partsList.Contains(partAmount.PartId))
                        {
                            partsList.Add(partAmount.PartId);
                        }
                    }
                }

                // Get all indirect parts from products
                if (product.Products != null)
                {
                    foreach (var productAmount in product.Products)
                    {
                        GetParts(productAmount.ProductId, context, ref partsList);
                    }
                }
            }
        }

        public static void GetParts(
            List<int> productsList,
            ApplicationDataDbContext context,
            ref List<int> partsList)
        {
            foreach(var product in productsList)
            {
                GetParts(product, context, ref partsList);
            }
        }

        public static void GetTechnologies(
            List<int> partsList,
            ApplicationDataDbContext context,
            ref List<int> technologiesList)
        {
            foreach (var partId in partsList)
            {
                var technologies = context.Technologies.Where(i => i.PartId == partId).Select(j => j.Id).ToList();
                technologiesList.AddRange(technologies);
            }

            return;
        }

        public static void GetOperations(
            List<int> technologiesList,
            ApplicationDataDbContext context,
            ref List<int> operationsList)
        {
            foreach (var technologyId in technologiesList)
            {
                if (context.Technologies.Any(i => i.Id == technologyId))
                {
                    var operations = context.Technologies
                        .Single(i => i.Id == technologyId).Operations.Select(j => j.OperationId).ToList();
                    operationsList.AddRange(operations);
                }
            }

            return;
        }

        public static List<PartIdAmountVM> GetSpaPartsList(HashSet<PartIdAmountClass> parts)
        {
            var partsList = new List<PartIdAmountVM>();
            foreach (var partAmount in parts)
            {
                partsList.Add(new PartIdAmountVM(partAmount.PartId, partAmount.Amount));
            }
            return partsList;
        }

        public static List<ProductIdAmountDiscountVM> GetSpaProductsList(
            HashSet<ProductIdAmountDiscountClass> products)
        {
            var productsList = new List<ProductIdAmountDiscountVM>();
            foreach (var productAmount in products)
            {
                productsList.Add(new ProductIdAmountDiscountVM(productAmount.ProductId, productAmount.Amount, productAmount.Discount));
            }
            return productsList;
        }

        public static List<ParameterIdValueVM> GetSpaParametersList(List<ParameterIdValueClass> parameters)
        {
            var parametersList = new List<ParameterIdValueVM>();
            foreach (var parameterValue in parameters)
            {
                parametersList.Add(new ParameterIdValueVM(parameterValue.ParameterId, parameterValue.Value ?? ""));
            }
            return parametersList;
        }

        public static List<int> GetSpaSubjectsList(HashSet<SubjectIdClass> subjects)
        {
            var subjectsList = new List<int>();
            foreach (var subject in subjects)
            {
                subjectsList.Add(subject.SubjectId);
            }
            return subjectsList;
        }
    }
}