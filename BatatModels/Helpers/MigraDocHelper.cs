﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using BorderStyle = MigraDoc.DocumentObjectModel.BorderStyle;
using Row = MigraDoc.DocumentObjectModel.Tables.Row;

namespace Batat.Models.Helpers
{
    public class MigraDocHelper
    {
        public static Color TableBlue { get; private set; }
        public static Color TableBorder { get; private set; }

        public static void DefineStyles(Document document)
        {
            // Get the predefined style Normal.
            MigraDoc.DocumentObjectModel.Style style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Verdana";

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);


            // Create a new style called Table based on style Normal
            style = document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = "Times New Roman";
            style.Font.Size = 12;


            // Create a new style called Reference based on style Normal
            style = document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        public static Section CreatePageLayout(Document document, ApplicationDataDbContext context)
        {
            // Each MigraDoc document needs at least one section.
            Section section = document.AddSection();

            /*
            var logoPath = System.Web.HttpContext.Current.Server.MapPath(@"~\Content\images\IpomeaLogo.jpg");
            // Put a logo in the header
            Image image = section.Headers.Primary.AddImage(logoPath);
            image.Height = "2.5cm";
            image.LockAspectRatio = true;
            image.RelativeVertical = RelativeVertical.Line;
            image.RelativeHorizontal = RelativeHorizontal.Margin;
            image.Top = ShapePosition.Top;
            image.Left = ShapePosition.Right;
            image.WrapFormat.Style = WrapStyle.Through;

            // Create footer
            Paragraph paragraph = section.Footers.Primary.AddParagraph();
            paragraph.AddText("Ipomea Computing " + DateTime.Now.ToString("dd-MM-yyyy") + ", automatically generated document");
            paragraph.Format.Font.Size = 9;
            paragraph.Format.Alignment = ParagraphAlignment.Center;


            // Create the text frame for the address
            var addressFrame = section.AddTextFrame();
            addressFrame.Height = "3.0cm";
            addressFrame.Width = "7.0cm";
            addressFrame.Left = ShapePosition.Left;
            addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            addressFrame.Top = "5.0cm";
            addressFrame.RelativeVertical = RelativeVertical.Page;


            // Put sender in address frame
            paragraph = addressFrame.AddParagraph("PowerBooks Inc · Sample Street 42 · 56789 Cologne");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 7;
            paragraph.Format.SpaceAfter = 3;

            // Add the print date field
            paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "8cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("KARTA DETALU", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Cologne, ");
            paragraph.AddDateField("dd.MM.yyyy");

            */


            return section;
        }

        public static void CreatePartSheetPageContent(Section section, int operationBundleId, ApplicationDataDbContext context)
        {
            var partCode = "";
            var partName = "";
            var parameterStrings = new List<string>();
            var technologyName = "";
            var technologyCode = "";
            var bundleToDoAmount = "";
            var boxes = new List<Box>();

            if(operationBundleId > 0)
            {
                boxes = context.Boxes.Where(a => a.TrolleyId == operationBundleId).ToList();
                if(boxes.Count > 0)
                {
                    var part = context.Parts.Find(boxes[0].PartId);
                    if(part != null)
                    {
                        partCode = part.Code;
                        partName = part.Name;
                        foreach(var parameterIdValue in part.Parameters)
                        {
                            var parameter = context.Parameters.Find(parameterIdValue.ParameterId);
                            if (parameter == null)
                                continue;

                            var parameterString = parameter.Name + ": " + parameterIdValue.Value;
                            if(!String.IsNullOrEmpty(parameter.Unit))
                            {
                                parameterString += parameter.Unit;
                            }
                            parameterStrings.Add(parameterString);
                        }
                    }

                    var technology = context.Technologies.Find(boxes[0].TechnologyId);
                    if(technology != null)
                    {
                        technologyName = technology.Name;
                        technologyCode = technology.Code;
                    }

                    var trolley = context.Trolleys.Find(operationBundleId);
                    if(trolley != null)
                    {
                        bundleToDoAmount = trolley.ToDoAmount.ToString();
                    }
                }
            }


            // Create the item table
            var table = section.AddTable();
            table.Style = "Table";
            table.Borders.Color = MigraDocHelper.TableBorder;
            table.Borders.Width = 0.5;
            table.Borders.Left.Width = 0.5;
            table.Borders.Right.Width = 0.5;
            table.Rows.LeftIndent = 0;


            // Before you can add a row, you must define the columns
            var columns = new List<Column>();
            int columnCount = 18;
            for(var i = 1; i <= columnCount; i++)
            {
                var columnWidth = Math.Round(18D / columnCount, 1);
                var column = table.AddColumn(columnWidth + "cm");
                column.Format.Alignment = ParagraphAlignment.Right;
                columns.Add(column);
            }


            // Create rows of the table
            var rows = new List<Row>();
            int rowCount = 7;
            for (var i = 1; i <= rowCount; i++)
            {
                var row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Left;
                row.Height = "1.2cm";
                rows.Add(row);
            }



            // ROW[0]
            rows[0].TopPadding = "2mm";
            // Part Code cell
            rows[0].Cells[0].MergeRight = 3;
            rows[0].Cells[0].AddParagraph(partCode);
            rows[0].Format.Alignment = ParagraphAlignment.Left;
            rows[0].Cells[0].Format.Font.Size = "0.8cm";
            // Part Name cell
            rows[0].Cells[4].MergeRight = 7;
            rows[0].Cells[4].AddParagraph(partName);
            rows[0].Cells[4].Format.Alignment = ParagraphAlignment.Left;
            rows[0].Cells[4].Format.Font.Size = "0.6cm";
            // Order Barcode cell 
            rows[0].Cells[12].MergeRight = 5;
            rows[0].Cells[12].MergeDown = 1;
            rows[0].Cells[12].Format.Alignment = ParagraphAlignment.Center;
            var logoPath = System.Web.HttpContext.Current.Server.MapPath(@"~\Content\images\barcode.jpg");
            var image = rows[0].Cells[12].AddImage(logoPath);
            image.Height = "2.5cm";
            image.Width = "5cm";
            image.RelativeVertical = RelativeVertical.Line;
            image.RelativeHorizontal = RelativeHorizontal.Margin;
            image.Top = ShapePosition.Center;
            image.Left = ShapePosition.Center;
            image.WrapFormat.Style = WrapStyle.Through;

            // ROW[1]
            rows[1].TopPadding = "2mm";
            // Parameters cell
            rows[1].Cells[0].MergeRight = 10;
            rows[1].Cells[0].MergeDown = 1;
            rows[1].Format.Alignment = ParagraphAlignment.Left;
            rows[1].Format.Font.Size = "0.4cm";
            foreach (var parameterString in parameterStrings)
            {
                rows[1].Cells[0].AddParagraph(parameterString);
            }
            // Amount cell
            rows[2].Cells[12].MergeRight = 2;
            rows[2].Cells[12].AddParagraph("ILOŚĆ");
            rows[2].Cells[12].Format.Alignment = ParagraphAlignment.Left;
            rows[2].Cells[12].Format.Font.Size = "0.8cm";
            rows[2].Cells[15].MergeRight = 2;
            rows[2].Cells[15].AddParagraph(bundleToDoAmount);
            rows[2].Cells[15].Format.Alignment = ParagraphAlignment.Left;
            rows[2].Cells[15].Format.Font.Size = "0.8cm";
            // Employee cell
            rows[3].Cells[0].MergeRight = 3;
            rows[3].Cells[0].MergeDown = 2;
            // Technology cell
            rows[3].Cells[4].MergeRight = 7;
            rows[3].Cells[4].MergeDown = 2;
            rows[3].Cells[4].AddParagraph(technologyName);
            rows[3].Cells[4].AddParagraph(technologyCode);
            rows[3].Cells[4].Format.Alignment = ParagraphAlignment.Left;
            rows[3].Cells[4].Format.Font.Size = "0.5cm";
            // Notes cell
            rows[3].Cells[12].MergeRight = 5;
            rows[3].Cells[12].MergeDown = 2;
            rows[3].Cells[12].AddParagraph("Uwagi");
            rows[3].Cells[12].Format.Alignment = ParagraphAlignment.Left;
            rows[3].Cells[12].Format.Font.Size = "0.4cm";

            // Table Headers
            rows[6].Height = "0.6cm";
            rows[6].Format.Font.Size = "0.3cm";
            rows[6].Cells[0].MergeRight = 3;
            rows[6].Cells[0].AddParagraph("Zamówienie");
            rows[6].Cells[4].MergeRight = 0;
            rows[6].Cells[4].AddParagraph("Ilość");
            rows[6].Cells[5].MergeRight = 2;
            rows[6].Cells[5].AddParagraph("Produkt");
            rows[6].Cells[8].MergeRight = 3;
            rows[6].Cells[8].AddParagraph("Odbiorca");
            rows[6].Cells[12].MergeRight = 1;
            rows[6].Cells[12].AddParagraph("Kontakt");
            rows[6].Cells[14].MergeRight = 3;
            rows[6].Cells[14].AddParagraph("Termin");


            foreach(var box in boxes)
            {
                var row1 = table.AddRow();
                row1.Format.Alignment = ParagraphAlignment.Left;
                row1.Height = "1.2cm";
                row1.Format.Font.Size = "0.3cm";
                rows.Add(row1);
                var row2 = table.AddRow();
                row2.Format.Alignment = ParagraphAlignment.Left;
                row2.Height = "1.2cm";
                row2.Format.Font.Size = "0.3cm";
                rows.Add(row2);
                // Barcode cell
                row1.Cells[0].MergeRight = 3;
                row1.Cells[0].MergeDown = 1;
                row1.Cells[0].AddParagraph("Zamówienie nr: " + box.OrderId);
                logoPath = System.Web.HttpContext.Current.Server.MapPath(@"~\Content\images\barcode.jpg");
                image = row1.Cells[0].AddImage(logoPath);
                image.Height = "1.5cm";
                image.Width = "3cm";
                image.RelativeVertical = RelativeVertical.Line;
                image.RelativeHorizontal = RelativeHorizontal.Margin;
                image.Top = ShapePosition.Center;
                image.Left = ShapePosition.Center;
                image.WrapFormat.Style = WrapStyle.Through;
                // Amount cell
                row1.Cells[4].MergeRight = 0;
                row1.Cells[4].MergeDown = 1;
                row1.Cells[4].AddParagraph(box.ToDoAmount.ToString());
                // Product cell
                row1.Cells[5].MergeRight = 2;
                row1.Cells[5].MergeDown = 1;
                var product = context.Products.Find(box.ProductId);
                if(product != null)
                {
                    row1.Cells[5].AddParagraph(product.Name);
                }
                // Subject, Contact and Dates cell
                row1.Cells[8].MergeRight = 3;
                row1.Cells[8].MergeDown = 1;
                row1.Cells[12].MergeRight = 1;
                row1.Cells[12].MergeDown = 1;
                row1.Cells[14].MergeRight = 3;
                row1.Cells[14].MergeDown = 1;
                var order = context.Orders.Find(box.OrderId);
                if (order == null)
                    continue;

                var subject = context.Subjects.Find(order.SubjectId);
                if(subject != null)
                {
                    row1.Cells[8].AddParagraph(subject.Name);
                    row1.Cells[8].AddParagraph(subject.Street);
                    row1.Cells[8].AddParagraph(subject.PostalCode + " " + subject.City);
                    row1.Cells[8].AddParagraph(subject.State);
                }
                var contact = context.Contacts.Find(order.ContactId);
                if(contact != null)
                {
                    row1.Cells[12].AddParagraph(contact.Name);
                    if(contact.BusinessPhone != null)
                    {
                        row1.Cells[12].AddParagraph(contact.BusinessPhone);
                    }
                    if (contact.OfficePhone != null)
                    {
                        row1.Cells[12].AddParagraph(contact.OfficePhone);
                    }
                    if (contact.PrivatePhone != null)
                    {
                        row1.Cells[12].AddParagraph(contact.PrivatePhone);
                    }
                }
                row1.Cells[14].Format.Font.Size = "0.4cm";
                row1.Cells[14].AddParagraph(order.ReceiveDate.ToString("dd-MM-yyyy"));
                row1.Cells[14].AddParagraph(order.AccomplishDate.ToString("dd-MM-yyyy"));
                row1.Cells[14].AddParagraph(order.DeliveryDate.ToString("dd-MM-yyyy"));

            }

            /*
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Item");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[0].MergeDown = 1;
            row.Cells[1].AddParagraph("Title and Author");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[1].MergeRight = 3;
            row.Cells[5].AddParagraph("Extended Price");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[5].MergeDown = 1;
            */


            table.SetEdge(0, 0, 6, 2, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        public static void FillPartSheetContent(Document document)
        {

        }
    }
}