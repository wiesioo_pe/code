﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class OperatorEditHelper
    {
        public static void CheckIfOperatorExists(ref Operator operatorDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (operatorDb == null)
            {
                operatorDb = new Operator();
                operatorDb.Changes.Add(new ChangeClass("Utworzenie: operator", "Nowy operator utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var groupsIdsDbCopy = new HashSet<GroupIdClass>(operatorDb.Groups);
            if (operatorAndPhotoVm.Groups == null)
            {
                operatorAndPhotoVm.Groups = new List<GroupIdClassVM>();
            }
            foreach (var groupIdVm in operatorAndPhotoVm.Groups)
            {
                if (groupsIdsDbCopy.Any(a => a.GroupId == groupIdVm.GroupId))
                {
                    groupsIdsDbCopy.RemoveWhere(b => b.GroupId == groupIdVm.GroupId);
                }
                else
                {
                    operatorDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                        "Id grupy: " + groupIdVm.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdVm.GroupId, context), user));
                    operatorDb.Groups.Add(new GroupIdClass(groupIdVm.GroupId, context.Groups.Find(groupIdVm.GroupId)?.UUID));
                }
            }
            foreach (var groupIdDbCopy in groupsIdsDbCopy)
            {
                operatorDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                        "Id grupy: " + groupIdDbCopy.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdDbCopy.GroupId, context), user));
                operatorDb.Groups.RemoveWhere(c => c.GroupId == groupIdDbCopy.GroupId);
            }
        }

        public static void CheckNameChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (operatorAndPhotoVm.Name != operatorDb.Name)
            {
                operatorDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + operatorDb.Name + ", na: " + operatorAndPhotoVm.Name, user));
                operatorDb.Name = operatorAndPhotoVm.Name;
            }
        }

        public static void CheckPositionChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (operatorAndPhotoVm.Position != operatorDb.Position)
            {
                operatorDb.Changes.Add(new ChangeClass("Edycja: stanowisko",
                        "Z: " + operatorDb.Position + ", na: " + operatorAndPhotoVm.Position, user));
                operatorDb.Position = operatorAndPhotoVm.Position;
            }
        }

        public static void CheckFunctionChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (operatorAndPhotoVm.Function != operatorDb.Function)
            {
                operatorDb.Changes.Add(new ChangeClass("Edycja: funkcja",
                        "Z: " + operatorDb.Function + ", na: " + operatorAndPhotoVm.Function, user));
                operatorDb.Function = operatorAndPhotoVm.Function;
            }
        }

        public static void CheckHourlyRateChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (operatorAndPhotoVm.HourlyRate != operatorDb.HourlyRate)
            {
                operatorDb.Changes.Add(new ChangeClass("Edycja: stawka godzinowa",
                        "Z: " + $"{operatorDb.HourlyRate:N2}" + ", na: " + $"{operatorAndPhotoVm.HourlyRate:N2}", user));
                operatorDb.HourlyRate = operatorAndPhotoVm.HourlyRate ?? 0;
            }
        }

        public static void CheckOvertimeRateChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (operatorAndPhotoVm.OvertimeRate != operatorDb.OvertimeRate)
            {
                operatorDb.Changes.Add(new ChangeClass("Edycja: funkcja",
                        "Z: " + $"{operatorDb.OvertimeRate:N2}" + ", na: " + $"{operatorAndPhotoVm.OvertimeRate:N2}", user));
                operatorDb.OvertimeRate = operatorAndPhotoVm.OvertimeRate ?? 0;
            }
        }

        public static void CheckNotesChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            for (int i = operatorDb.Notes.Count - 1; i >= 0; i--)
            {
                if (operatorDb.Notes[i].Note != operatorAndPhotoVm.Notes[i].Note)
                {
                    if (String.IsNullOrWhiteSpace(operatorAndPhotoVm.Notes[i].Note))
                    {
                        operatorDb.Changes.Add(new ChangeClass("Usunięcie: uwaga",
                                "Nr: " + SharedHelper.GetNoteId(i) + ", treść: '"
                                + SharedHelper.GetNote(operatorDb.Notes[i].Note) + "'"
                                , user));
                        operatorDb.Notes.RemoveAt(i);
                        operatorAndPhotoVm.Notes.RemoveAt(i);
                    }
                    else
                    {
                        operatorDb.Changes.Add(new ChangeClass("Edycja: uwaga",
                            "Nr: " + SharedHelper.GetNoteId(i) + ", treść z: '"
                            + SharedHelper.GetNote(operatorDb.Notes[i].Note) + "', na: '"
                            + SharedHelper.GetNote(operatorAndPhotoVm.Notes[i].Note) + "'"
                            , user));
                        operatorDb.Notes[i].Note = operatorAndPhotoVm.Notes[i].Note;
                        operatorAndPhotoVm.Notes.RemoveAt(i);
                    }
                }
                else
                {
                    operatorAndPhotoVm.Notes.RemoveAt(i);
                }

            }

            foreach (var newNote in operatorAndPhotoVm.Notes)
            {
                operatorDb.Notes.Add(new NoteClass(newNote.Note, user));
                operatorDb.Changes.Add(new ChangeClass("Dodanie: uwaga",
                        "Nr: " + SharedHelper.GetNoteId(operatorDb.Notes.IndexOf(operatorDb.Notes.Last()))
                        + ", treść: '" + SharedHelper.GetNote(newNote.Note) + "'"
                        , user));
            }
        }

        public static void CheckPhotoChange(
            OperatorAndPhotoVM operatorAndPhotoVm,
            Operator operatorDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (operatorAndPhotoVm.PhotoFile.isNew)
            {
                // remove illegal characters from Operator Name
                string operatorNameForPath = operatorAndPhotoVm.Name;
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                operatorNameForPath = r.Replace(operatorNameForPath, "_");

                // Save URL in Operator.PhotoFileUrlObj
                var splittedFileName = operatorAndPhotoVm.PhotoFile.FileName.Split('.');
                var fileExtension = "";
                if (splittedFileName.Length > 1)
                {
                    fileExtension = splittedFileName.Last();
                }
                string fileName = operatorNameForPath;
                if (fileExtension != "")
                {
                    fileName += "." + fileExtension;
                }



                operatorAndPhotoVm.PhotoFileUrlObj.FileUrl = "/UserFiles/Operators/" + fileName;
                operatorAndPhotoVm.PhotoFileUrlObj.FileName = fileName;
                operatorAndPhotoVm.PhotoFileUrlObj.OriginalFileName = operatorAndPhotoVm.PhotoFile.FileName;
                operatorAndPhotoVm.PhotoFileUrlObj.FileType = operatorAndPhotoVm.PhotoFile.ContentType;
                operatorAndPhotoVm.PhotoFileUrlObj.FileSize = operatorAndPhotoVm.PhotoFile.Size;

                var base64StringWithoutHeader = operatorAndPhotoVm.PhotoFile.Content.Remove(0, operatorAndPhotoVm.PhotoFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Operators"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);

                // Delete old file
                if(!String.IsNullOrEmpty(operatorDb.PhotoFileUrlObj?.FileUrl))
                {
                    var oldFilePath = HttpContext.Current.Server.MapPath(operatorDb.PhotoFileUrlObj.FileUrl);
                    if (!String.IsNullOrEmpty(oldFilePath) && File.Exists(oldFilePath))
                    {
                        File.Delete(oldFilePath);
                    }
                }


                if (operatorDb.PhotoFileUrlObj == null)
                {
                    operatorDb.Changes.Add(new ChangeClass("Dodanie: zdjęcie",
                        "Z: " + operatorAndPhotoVm.PhotoFileUrlObj.FileName
                        + "(oryg.: " + operatorAndPhotoVm.PhotoFileUrlObj.OriginalFileName + ")"
                        , user));

                    operatorDb.PhotoFileUrlObj =
                        new OperatorFileUrlClass
                        {
                            FileName = operatorAndPhotoVm.PhotoFileUrlObj.FileName,
                            OriginalFileName = operatorAndPhotoVm.PhotoFileUrlObj.OriginalFileName,
                            FileSize = operatorAndPhotoVm.PhotoFileUrlObj.FileSize,
                            FileType = operatorAndPhotoVm.PhotoFileUrlObj.FileType,
                            FileUrl = operatorAndPhotoVm.PhotoFileUrlObj.FileUrl
                        };
                    context.Entry(operatorDb.PhotoFileUrlObj).State = EntityState.Added;
                }
                else
                {
                    operatorDb.Changes.Add(new ChangeClass("Edycja: zdjęcie",
                        "Z: " + operatorDb.PhotoFileUrlObj.FileName
                        + "(oryg.: " + operatorDb.PhotoFileUrlObj.OriginalFileName + ")"
                        + ", na: " + operatorAndPhotoVm.PhotoFileUrlObj.FileName
                        + "(oryg.: " + operatorAndPhotoVm.PhotoFileUrlObj.OriginalFileName + ")"
                        , user));

                    operatorDb.PhotoFileUrlObj.FileName = operatorAndPhotoVm.PhotoFileUrlObj.FileName;
                    operatorDb.PhotoFileUrlObj.OriginalFileName = operatorAndPhotoVm.PhotoFileUrlObj.OriginalFileName;
                    operatorDb.PhotoFileUrlObj.FileSize = operatorAndPhotoVm.PhotoFileUrlObj.FileSize;
                    operatorDb.PhotoFileUrlObj.FileType = operatorAndPhotoVm.PhotoFileUrlObj.FileType;
                    operatorDb.PhotoFileUrlObj.FileUrl = operatorAndPhotoVm.PhotoFileUrlObj.FileUrl;
                    context.Entry(operatorDb.PhotoFileUrlObj).State = EntityState.Modified;
                }

            }

        }

    }
}