﻿using System;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ParametersHelper
    {
        public static string GetParameterId(int parameterId)
        {
            return parameterId > 0 ? parameterId.ToString() : "(nieprawidłowy Id parametru)";
        }

        public static string GetParameterName(int parameterId, ApplicationDataDbContext context)
        {
            Parameter parameter = context.Parameters.Find(parameterId);
            if (parameter == null)
                return "(nieprawidłowy Id parametru)";

            return !String.IsNullOrWhiteSpace(parameter.Name) ? parameter.Name : "(pusta Name parametru)";
        }

        public static string GetParameterValue(string value)
        {
            return !String.IsNullOrWhiteSpace(value) ? value : "(pusta wartość parametru)";
        }
    }
}