﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class MaterialEditHelper
    {
        public static void CheckIfMaterialExists(ref Material materialDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (materialDb == null)
            {
                materialDb = new Material();
                materialDb.Changes.Add(new ChangeClass("Utworzenie: materiał", "Nowy materiał utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            MaterialVM materialVm,
            Material materialDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var groupsIdsDbCopy = new HashSet<GroupIdClass>(materialDb.Groups);
            if (materialVm.Groups == null) { materialVm.Groups = new List<GroupIdClassVM>(); }

            foreach (var groupIdVm in materialVm.Groups)
            {
                if (groupsIdsDbCopy.Any(a => a.GroupId == groupIdVm.GroupId))
                {
                    groupsIdsDbCopy.RemoveWhere(b => b.GroupId == groupIdVm.GroupId);
                }
                else
                {
                    materialDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                        "Id grupy: " + groupIdVm.GroupId + ", nazwa: "
                        + SharedHelper.GetGroupName(groupIdVm.GroupId, context), user));
                    materialDb.Groups.Add(new GroupIdClass(groupIdVm.GroupId, context.Groups.Find(groupIdVm.GroupId)?.UUID));
                }
            }
            foreach (var groupIdDbCopy in groupsIdsDbCopy)
            {
                materialDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                        "Id grupy: " + groupIdDbCopy.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdDbCopy.GroupId, context), user));
                materialDb.Groups.RemoveWhere(c => c.GroupId == groupIdDbCopy.GroupId);
            }
        }

        public static void CheckNameChange(
            MaterialVM materialVm,
            Material materialDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (materialVm.Name != materialDb.Name)
            {
                materialDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + materialDb.Name + ", na: " + materialVm.Name, user));
                materialDb.Name = materialVm.Name;
            }
        }

        public static void CheckCodeChange(
            MaterialVM materialVm,
            Material materialDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (materialVm.Code != materialDb.Code)
            {
                materialDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + materialDb.Code + ", na: " + materialVm.Code, user));
                materialDb.Code = materialVm.Code;
            }
        }

        public static void CheckDescChange(
            MaterialVM materialVm,
            Material materialDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (materialVm.Desc != materialDb.Desc)
            {
                materialDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + materialDb.Desc + ", na: " + materialVm.Desc, user));
                materialDb.Desc = materialVm.Desc;
            }
        }

        public static void CheckMaterialTypeIdChange(
            MaterialVM materialVm,
            Material materialDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (materialVm.MaterialTypeId != materialDb.MaterialTypeId)
            {
                materialDb.Changes.Add(new ChangeClass("Edycja: typ",
                        "Z: " + materialDb.MaterialTypeId + " - "
                        + MaterialsHelper.GetMaterialTypeName(materialDb.MaterialTypeId, context)
                        + ", na: " + materialVm.MaterialTypeId + " - "
                        + MaterialsHelper.GetMaterialTypeName(materialVm.MaterialTypeId, context), user));
                materialDb.MaterialTypeId = materialVm.MaterialTypeId;
            }
        }

        public static void CheckParametersChange(
            MaterialVM materialVm,
            Material materialDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            for (int i = materialDb.Parameters.Count - 1; i >= 0; i--)
            {
                var oldParameterValue = materialDb.Parameters[i];
                var newParameterValue = materialVm.Parameters[i];
                if (oldParameterValue.Value != newParameterValue.Value)
                {
                    if (String.IsNullOrWhiteSpace(newParameterValue.Value))
                    {
                        materialDb.Changes.Add(new ChangeClass("Usunięcie: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(oldParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(oldParameterValue.ParameterId, context)
                        + ", wartość: " + ParametersHelper.GetParameterValue(oldParameterValue.Value)
                        , user));
                        materialDb.Parameters.RemoveAt(i);
                        materialVm.Parameters.RemoveAt(i);
                    }
                    else
                    {
                        materialDb.Changes.Add(new ChangeClass("Edycja: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(oldParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(oldParameterValue.ParameterId, context)
                        + ", wartość z: " + ParametersHelper.GetParameterValue(oldParameterValue.Value)
                        + ", na: " + ParametersHelper.GetParameterValue(newParameterValue.Value)
                        , user));
                        oldParameterValue.Value = newParameterValue.Value;
                        materialVm.Parameters.RemoveAt(i);
                    }
                }
                else
                {
                    materialVm.Parameters.RemoveAt(i);
                }
            }

            foreach (var newParameterValue in materialVm.Parameters)
            {
                materialDb.Parameters.Add(new ParameterIdValueClass(newParameterValue));
                materialDb.Changes.Add(new ChangeClass("Dodanie: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(newParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(newParameterValue.ParameterId, context)
                        + ", wartość: " + ParametersHelper.GetParameterValue(newParameterValue.Value)
                        , user));
            }
        }

        public static void CheckUnitChange(MaterialVM materialVm, Material materialDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (materialVm.Unit != materialDb.Unit)
            {
                materialDb.Changes.Add(new ChangeClass("Edycja: jednostka",
                        "Z: " + materialDb.Unit + ", na: " + materialVm.Unit, user));
                materialDb.Unit = materialVm.Unit;
            }
        }

        public static void CheckUnitCostChange(MaterialVM materialVm, Material materialDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (materialVm.UnitCost != materialDb.UnitCost)
            {
                materialDb.Changes.Add(new ChangeClass("Edycja: koszt jednostki",
                        "Z: " + materialDb.UnitCost + ", na: " + materialVm.UnitCost, user));
                materialDb.UnitCost = materialVm.UnitCost ?? 0;
            }
        }

    }
}