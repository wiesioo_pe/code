﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class OrderEditHelper
    {
        public static void CheckIfOrderExists(
            OrderVM orderVm,
            ref Order orderDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (orderDb == null)
            {
                orderDb = new Order();
                orderDb.Changes.Add(new ChangeClass("Utworzenie: zamówienie", "Nowe zamówienie utworzone", user));
            }
        }

        public static void CheckGroupsChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var toDeleteHashSet = new HashSet<GroupIdClass>();
            foreach (var item in orderDb.Groups)
            {
                if (orderVm.Groups.Any(a => a.GroupId == item.GroupId))
                {
                    orderVm.Groups.RemoveAll(a => a.GroupId == item.GroupId);
                }
                else
                {
                    toDeleteHashSet.Add(item);
                }
            }
            foreach (var item in toDeleteHashSet)
            {
                orderDb.Groups.Remove(item);
                orderDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                            "Nr: " + item.GroupId
                            + ", nazwa: " + SharedHelper.GetGroupName(item.GroupId, context)
                            , user));
            }
            foreach (var addedGroup in orderVm.Groups)
            {
                if (context.Groups.Any(a => a.Id == addedGroup.GroupId))
                {
                    orderDb.Groups.Add(new GroupIdClass(addedGroup.GroupId, context.Groups.Find(addedGroup.GroupId)?.UUID));
                    orderDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                                "Nr: " + addedGroup.GroupId
                                + ", nazwa: " + SharedHelper.GetGroupName(addedGroup.GroupId, context)
                                , user));
                }
                else
                {
                    errorVm.Messages.Add("Grupa nr: " + addedGroup + " nie istnieje");
                }
            }
        }

        public static void CheckReceiveDateChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (orderVm.ReceiveDate != null)
            {
                if (orderVm.ReceiveDate == orderDb.ReceiveDate)
                    return;

                if(orderDb.ReceiveDate == DateTime.MinValue)
                {
                    orderDb.Changes.Add(new ChangeClass("Ustawienie: data wpłynięcia",
                        OrdersHelper.GetDate(orderVm.ReceiveDate), user));
                    orderDb.ReceiveDate = orderVm.ReceiveDate ?? DateTime.MinValue;
                }
                else
                {
                    orderDb.Changes.Add(new ChangeClass("Zmiana: data wpłynięcia",
                        "Z: " + OrdersHelper.GetDate(orderDb.ReceiveDate) + ", na: " + OrdersHelper.GetDate(orderVm.ReceiveDate), user));
                    orderDb.ReceiveDate = orderVm.ReceiveDate ?? DateTime.MinValue;
                }
            }
            else
            {
                errorVm.Messages.Add("Data wpłyniecia nie może być pusta");
                orderVm.ReceiveDate = DateTime.MinValue;
            }
        }

        public static void CheckAccomplishDateChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (orderVm.AccomplishDate != null)
            {
                if(orderVm.AccomplishDate >= orderVm.ReceiveDate)
                {
                    if (orderVm.AccomplishDate == orderDb.AccomplishDate)
                        return;

                    if (orderDb.AccomplishDate == DateTime.MinValue)
                    {
                        orderDb.Changes.Add(new ChangeClass("Ustawienie: termin wykonania",
                            OrdersHelper.GetDate(orderVm.AccomplishDate), user));
                        orderDb.AccomplishDate = orderVm.AccomplishDate ?? DateTime.MinValue;
                    }
                    else
                    {
                        orderDb.Changes.Add(new ChangeClass("Zmiana: termin wykonania",
                            "Z: " + OrdersHelper.GetDate(orderDb.AccomplishDate) + ", na: " + OrdersHelper.GetDate(orderVm.AccomplishDate), user));
                        orderDb.AccomplishDate = orderVm.AccomplishDate ?? DateTime.MinValue;
                    }
                }
                else
                {
                    errorVm.Messages.Add("Termin wykonania nie może wcześniejszy od daty wpłynięcia");
                }
            }
            else
            {
                errorVm.Messages.Add("Termin wykonania nie może być pusty");
                orderVm.AccomplishDate = DateTime.MinValue;
            }
        }

        public static void CheckDeliveryDateChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (orderVm.DeliveryDate != null)
            {
                if (orderVm.DeliveryDate >= orderVm.ReceiveDate && orderVm.DeliveryDate >= orderVm.AccomplishDate)
                {
                    if (orderVm.DeliveryDate == orderDb.DeliveryDate)
                        return;

                    if (orderDb.DeliveryDate == DateTime.MinValue)
                    {
                        orderDb.Changes.Add(new ChangeClass("Ustawienie: termin dostawy",
                            OrdersHelper.GetDate(orderVm.DeliveryDate), user));
                        orderDb.DeliveryDate = orderVm.DeliveryDate ?? DateTime.MinValue;
                    }
                    else
                    {
                        orderDb.Changes.Add(new ChangeClass("Zmiana: termin dostawy",
                            "Z: " + OrdersHelper.GetDate(orderDb.DeliveryDate) + ", na: " + OrdersHelper.GetDate(orderVm.DeliveryDate), user));
                        orderDb.DeliveryDate = orderVm.DeliveryDate ?? DateTime.MinValue;
                    }
                }
                else
                {
                    if(orderVm.DeliveryDate < orderVm.ReceiveDate)
                    {
                        errorVm.Messages.Add("Termin dostawy nie może wcześniejszy od daty wpłynięcia");
                    }
                    if (orderVm.DeliveryDate < orderVm.AccomplishDate)
                    {
                        errorVm.Messages.Add("Termin dostawy nie może wcześniejszy od terminu wykonania");
                    }
                }
            }
            else
            {
                errorVm.Messages.Add("Termin dostawy nie może być pusty");
                orderVm.DeliveryDate = DateTime.MinValue;
            }
        }

        public static void CheckPriorityChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (orderVm.PriorityId == 0 || orderVm.PriorityId == 1)
            {
                if (orderVm.PriorityId != orderDb.PriorityId)
                {
                    orderDb.Changes.Add(new ChangeClass("Zmiana: priorytet",
                            "Z: " + OrdersHelper.GetOrderPriorityName(orderDb.PriorityId) + ", na: " + OrdersHelper.GetOrderPriorityName(orderVm.PriorityId), user));
                    orderDb.PriorityId = orderVm.PriorityId;
                }
            }
            else
            {
                errorVm.Messages.Add("Nieprawidłowa wartość priorytetu (" + orderVm.PriorityId + ")");
            }

        }

        public static void CheckSubjectChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if(orderVm.SubjectId <= 0)
            {
                errorVm.Messages.Add("Odbiorca nie może być pusty");
            }

            if (context.Subjects.All(a => a.Id != orderVm.SubjectId))
            {
                errorVm.Messages.Add("Kontrahent nr: " + orderVm.SubjectId + " nie istnieje. Wybierz innego lub stwórz nowego kontrahenta");
            }

            var subject = context.Subjects.Find(orderVm.SubjectId);
            if (!subject.isCustomer)
            {
                errorVm.Messages.Add("Kontrahent nr: " + orderVm.SubjectId + ", '" + subject.Name + "' nie jest oznaczony jako odbiorca");
            }
            
            if (orderVm.SubjectId == orderDb.SubjectId)
            {
                return;
            }

            if(orderDb.SubjectId == 0)
            {
                orderDb.Changes.Add(new ChangeClass("Ustawienie: odbiorca",
                    OrdersHelper.GetSubjectName(orderVm.SubjectId, context), user));
            }
            else
            {
                orderDb.Changes.Add(new ChangeClass("Zmiana: odbiorca",
                    "Z: " + OrdersHelper.GetSubjectName(orderDb.SubjectId, context) + ", na: " + OrdersHelper.GetSubjectName(orderVm.SubjectId, context), user));
            }

            orderDb.SubjectId = subject.Id;
            orderDb.SubjectUUID = subject.UUID;
        }

        public static void CheckContactChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (orderVm.ContactId != 0 && context.Contacts.All(a => a.Id != orderVm.ContactId))
            {
                errorVm.Messages.Add("Kontakt nr: " + orderVm.ContactId + " nie istnieje. Wybierz inny lub stwórz nowy kontakt");
            }

            if (orderVm.ContactId == orderDb.ContactId)
            {
                return;
            }

            var contact = context.Contacts.Find(orderVm.ContactId);
                
            if (orderDb.ContactId == 0)
            {
                orderDb.Changes.Add(new ChangeClass("Ustawienie: kontakt",
                    OrdersHelper.GetContactName(orderVm.ContactId, context), user));
                orderDb.ContactId =  contact.Id;
                orderDb.ContactUUID = contact.UUID;
            }
            else if(orderVm.ContactId == 0)
            {
                orderDb.Changes.Add(new ChangeClass("Usunięcie: kontakt",
                    OrdersHelper.GetContactName(orderDb.ContactId, context), user));
                orderDb.ContactId = 0;
                orderDb.ContactUUID = null;
            }
            else
            {
                orderDb.Changes.Add(new ChangeClass("Zmiana: kontakt",
                    "Z: " + OrdersHelper.GetContactName(orderDb.ContactId, context) + ", na: " + OrdersHelper.GetContactName(orderVm.ContactId, context), user));
                orderDb.ContactId = contact.Id;
                orderDb.ContactUUID = contact.UUID;
            }
        }

        public static void CheckDescChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            orderVm.Desc = SharedHelper.NormalizeString(orderVm.Desc);
            orderDb.Desc = SharedHelper.NormalizeString(orderDb.Desc);

            if (orderVm.Desc == orderDb.Desc)
                return;

            if(orderDb.Desc == null)
            {
                orderDb.Changes.Add(new ChangeClass("Ustawienie: opis",
                    OrdersHelper.GetDescription(orderVm.Desc), user));
                orderDb.Desc = orderVm.Desc;
            }
            else if(orderVm.Desc == null)
            {
                orderDb.Changes.Add(new ChangeClass("Usunięcie: opis",
                    OrdersHelper.GetDescription(orderDb.Desc), user));
                orderDb.Desc = null;
            }
            else
            {
                orderDb.Changes.Add(new ChangeClass("Zmiana: opis",
                    "Z: " + OrdersHelper.GetDescription(orderDb.Desc) + ", na: " + OrdersHelper.GetDescription(orderVm.Desc), user));
                orderDb.Desc = orderVm.Desc;
            }
        }

        public static void CheckEmployeeChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (orderVm.EmployeeUUID != null && context.Users.All(a => a.Id != orderVm.EmployeeUUID))
            {
                errorVm.Messages.Add("Wybrany pracownik nie istnieje");
            }

            if (orderVm.EmployeeUUID == orderDb.EmployeeUUID)
            {
                return;
            }

            var oldEmployee = context.Users.Find(orderDb.EmployeeUUID);
            if (orderVm.EmployeeUUID == null)
            {
                orderDb.Changes.Add(new ChangeClass("Usunięcie: osoba odpowiedzialna", oldEmployee.UserName, user));
                orderDb.EmployeeUUID = null;
                orderDb.EmployeeId = 0;

                return;
            }

            var newEmployee = context.Users.Find(orderVm.EmployeeUUID);
            if (orderDb.EmployeeUUID == null)
            {
                orderDb.Changes.Add(new ChangeClass("Ustawienie: osoba odpowiedzialna", newEmployee.UserName, user));
                orderDb.EmployeeUUID = newEmployee.Id;
                orderDb.EmployeeId = newEmployee.IntId;
            }
            else
            {
                orderDb.Changes.Add(new ChangeClass("Zmiana: osoba odpowiedzialna",
                    "Z: " + oldEmployee.UserName + ", na: " + newEmployee.UserName, user));
                orderDb.EmployeeUUID = newEmployee.Id;
                orderDb.EmployeeId = newEmployee.IntId;
            }
        }

        public static void CheckProductsChange(
            OrderVM orderVm,
            Order orderDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedProducts = new HashSet<ProductIdAmountDiscountClass>();
            foreach (var productAmountDiscountDb in orderDb.Products)
            {
                if (orderVm.Products.Any(i => i.ProductId == productAmountDiscountDb.ProductId))
                {
                    var productAmountDiscountVm = orderVm.Products.First(i => i.ProductId == productAmountDiscountDb.ProductId);

                    if (productAmountDiscountDb.Amount != productAmountDiscountVm.Amount || productAmountDiscountDb.Discount != productAmountDiscountVm.Discount)
                    {
                        if (productAmountDiscountVm.Amount >= 0)
                        {
                            if (productAmountDiscountVm.Amount != productAmountDiscountDb.Amount)
                            {
                                if(productAmountDiscountDb.Amount == 0)
                                {
                                    orderDb.Changes.Add(new ChangeClass("Ustawienie: ilość produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", ilość: " + ProductsHelper.GetAmount(productAmountDiscountVm.Amount)
                                    , user));
                                    productAmountDiscountDb.Amount = productAmountDiscountVm.Amount ?? 0;
                                }
                                else
                                {
                                    orderDb.Changes.Add(new ChangeClass("Edycja: ilość produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", ilość z: " + ProductsHelper.GetAmount(productAmountDiscountDb.Amount)
                                    + ", na: " + ProductsHelper.GetAmount(productAmountDiscountVm.Amount)
                                    , user));
                                    productAmountDiscountDb.Amount = productAmountDiscountVm.Amount ?? 0;
                                }
                            }
                        }
                        else if(productAmountDiscountVm.Amount < 0)
                        {
                            errorVm.Messages.Add("Ilość produktu nie może być ujemna");
                        }
                        else if (productAmountDiscountVm.Amount == null)
                        {
                            errorVm.Messages.Add("Ilość produktu nie może być pusta");
                        }


                        if (productAmountDiscountVm.Discount >= 0 && productAmountDiscountVm.Discount <= 100)
                        {
                            if (productAmountDiscountVm.Discount != productAmountDiscountDb.Discount)
                            {
                                if(productAmountDiscountDb.Discount == 0)
                                {
                                    orderDb.Changes.Add(new ChangeClass("Ustawienie: rabat produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", rabat: " + ProductsHelper.GetDiscount(productAmountDiscountVm.Discount)
                                    , user));
                                    productAmountDiscountDb.Discount = productAmountDiscountVm.Discount ?? 0;
                                }
                                else
                                {
                                    orderDb.Changes.Add(new ChangeClass("Edycja: rabat produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", rabat z: " + ProductsHelper.GetDiscount(productAmountDiscountDb.Discount)
                                    + ", na: " + ProductsHelper.GetDiscount(productAmountDiscountVm.Discount)
                                    , user));
                                    productAmountDiscountDb.Discount = productAmountDiscountVm.Discount ?? 0;
                                }
                            }
                        }
                        else if (productAmountDiscountVm.Discount < 0)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być ujemny");
                        }
                        else if (productAmountDiscountVm.Discount > 100)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być większy od 100%");
                        }
                        else if (productAmountDiscountVm.Discount == null)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być pusty");
                        }
                    }
                    orderVm.Products.RemoveAll(a=>a.ProductId == productAmountDiscountVm.ProductId);
                }
                else
                {
                    orderDb.Changes.Add(new ChangeClass("Usunięcie: Produkt - ilość",
                            "Nr: " + productAmountDiscountDb.ProductId
                            + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                            + ", ilość: " + ProductsHelper.GetAmount(productAmountDiscountDb.Amount)
                            + ", rabat: " + ProductsHelper.GetDiscount(productAmountDiscountDb.Discount)
                            , user));
                    removedProducts.Add(productAmountDiscountDb);
                }
            }

            foreach (var oldProductToRemove in removedProducts)
            {
                orderDb.Products.Remove(oldProductToRemove);
            }

            foreach (var addedProductAmountDiscountVm in orderVm.Products)
            {
                if(context.Products.Any(a => a.Id == addedProductAmountDiscountVm.ProductId))
                {
                    if (addedProductAmountDiscountVm.Amount >= 0 && addedProductAmountDiscountVm.Discount >= 0 && addedProductAmountDiscountVm.Discount <= 100)
                    {
                        orderDb.Products.Add(new ProductIdAmountDiscountClass(addedProductAmountDiscountVm.ProductId, context.Products.Find(addedProductAmountDiscountVm.ProductId).UUID, addedProductAmountDiscountVm.Amount, addedProductAmountDiscountVm.Discount));
                        orderDb.Changes.Add(new ChangeClass("Dodanie: Product - ilość",
                                "Nr: " + addedProductAmountDiscountVm.ProductId
                                + ", nazwa: " + ProductsHelper.GetProductName(addedProductAmountDiscountVm.ProductId, context)
                                + ", ilość: " + ProductsHelper.GetAmount(addedProductAmountDiscountVm.Amount)
                                + ", rabat: " + ProductsHelper.GetDiscount(addedProductAmountDiscountVm.Discount)
                                , user));
                    }
                    else
                    {
                        if (addedProductAmountDiscountVm.Amount < 0)
                        {
                            errorVm.Messages.Add("Ilość produktu nie może być ujemna");
                        }
                        if (addedProductAmountDiscountVm.Discount < 0)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być ujemny");
                        }
                        if (addedProductAmountDiscountVm.Discount > 100)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być większy od 100%");
                        }
                    }
                }
                else
                {
                    errorVm.Messages.Add("Produkt nr: " + addedProductAmountDiscountVm.ProductId + " nie istnieje");
                }
            }
        }

        public static void CheckComplaintsChange(
            OrderVM orderVm,
            Order orderDb,
            List<ComplaintClass> complaintsToAdd,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            for (var i = orderDb.Complaints.Count - 1; i >= 0; i--)
            {
                var complaintDb = orderDb.Complaints[i];
                if (orderVm.Complaints.Any(a => a.Id == complaintDb.Id))
                {
                    var complaintVm = orderVm.Complaints.Single(a => a.Id == complaintDb.Id);

                    if (complaintVm.Amount >= 0)
                    {
                        if (complaintVm.Amount != complaintDb.Amount)
                        {
                            orderDb.Changes.Add(new ChangeClass("Zmiana: Reklamacja",
                                    "Zmiana ilości. Nr: " + complaintDb.Id
                                    + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                    + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                    + ", z: " + complaintDb.Amount
                                    + " , na: " + complaintVm.Amount
                                    , user));
                            complaintDb.Amount = complaintVm.Amount;
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Błąd reklamacji. Ilość reklamowanych produktów nie może być ujemna");
                    }

                    complaintVm.Desc = SharedHelper.NormalizeString(complaintVm.Desc);
                    complaintDb.Desc = SharedHelper.NormalizeString(complaintDb.Desc);
                    if (complaintVm.Desc != complaintDb.Desc)
                    {
                        if(complaintDb.Desc == null)
                        {
                            orderDb.Changes.Add(new ChangeClass("Zmiana: Reklamacja",
                                "Ustawienie opisu. Reklamacja nr: " + complaintDb.Id
                                + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                + ", opis: " + complaintVm.Desc
                                , user));
                            complaintDb.Desc = complaintVm.Desc;
                        }
                        else if (complaintVm.Desc == null)
                        {
                            orderDb.Changes.Add(new ChangeClass("Zmiana: Reklamacja",
                                "Usunięcie opisu. Reklamacja nr: " + complaintDb.Id
                                + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                + ", opis: " + complaintDb.Desc
                                , user));
                            complaintDb.Desc = complaintVm.Desc;
                        }
                        else
                        {
                            orderDb.Changes.Add(new ChangeClass("Zmiana: Reklamacja",
                                "Zmiana opisu. Reklamacja nr: " + complaintDb.Id
                                + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                + ", opis z: " + complaintDb.Desc
                                + " , na: " + complaintVm.Desc
                                , user));
                            complaintDb.Desc = complaintVm.Desc;
                        }
                    }

                    if(complaintVm.Cost >= 0)
                    {
                        if (complaintVm.Cost != complaintDb.Cost)
                        {
                            orderDb.Changes.Add(new ChangeClass("Zmiana: Reklamacja",
                                    "Zmiana kosztu. Nr: " + complaintDb.Id
                                    + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                    + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                    + ", z: " + complaintDb.Cost
                                    + " , na: " + complaintVm.Cost
                                    , user));

                            complaintDb.Cost = complaintVm.Cost ?? 0;
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Błąd reklamacji. Koszt reklamacji nie może być ujemny");
                    }

                    if (complaintVm.isTotal != complaintDb.isTotal)
                    {
                        orderDb.Changes.Add(new ChangeClass("Zmiana: Reklamacja",
                                "Zmiana zakresu. Nr: " + complaintDb.Id
                                + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                + ", z: " + OrdersHelper.GetComplaintIsTotalString(complaintDb.isTotal)
                                + " , na: " + OrdersHelper.GetComplaintIsTotalString(complaintVm.isTotal)
                                , user));

                        complaintDb.isTotal = complaintVm.isTotal ?? false;
                    }

                    orderVm.Complaints.Remove(complaintVm);
                }
                else
                {
                    orderDb.Changes.Add(new ChangeClass("Usunięcie: Reklamacja",
                                "Nr: " + complaintDb.Id
                                + ", produkt: " + ProductsHelper.GetProductName(complaintDb.ProductId, context)
                                + ", data: " + OrdersHelper.GetDate(complaintDb.Date)
                                + ", ilość: " + complaintDb.Amount + ", opis: " + complaintDb.Desc
                                + ", koszt: " + complaintDb.Cost + ", zakres: " + OrdersHelper.GetComplaintIsTotalString(complaintDb.isTotal)
                                , user));
                    orderDb.Complaints.RemoveAt(i);
                }
            }

            int index = -1;
            foreach (var complaintVm in orderVm.Complaints)
            {
                complaintVm.Id = index;

                if (context.Products.Any(a => a.Id == complaintVm.ProductId))
                {
                    if(orderDb.Products.Any(a => a.ProductId == complaintVm.ProductId))
                    {
                        if (complaintVm.Amount >= 0 && complaintVm.Cost >= 0)
                        {
                            complaintsToAdd.Add(new ComplaintClass(complaintVm));
                        }
                        else
                        {
                            if (complaintVm.Amount < 0)
                            {
                                errorVm.Messages.Add("Błąd reklamacji. Ilość reklamowanych produktów nie może być ujemna");
                            }
                            if (complaintVm.Cost < 0)
                            {
                                errorVm.Messages.Add("Błąd reklamacji. Koszt reklamacji nie może być ujemny");
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Błąd reklamacji. Produkt nr: " + complaintVm.ProductId + " nie jest częścią zamówienia");
                    }
                }
                else
                {
                    errorVm.Messages.Add("Błąd reklamacji. Produkt nr: " + complaintVm.ProductId + " nie istnieje");
                }

                index--;
            }

        }

        public static void SaveNewComplaints(
            Order orderDb,
            List<ComplaintClass> complaintsToAdd,
            ApplicationUser user,
            ApplicationDataDbContext context)
        {
            foreach(var complaintToAdd in complaintsToAdd)
            {
                orderDb.Complaints.Add(complaintToAdd);
                context.Entry(orderDb).State = EntityState.Modified;
                context.SaveChanges();
                orderDb.Changes.Add(new ChangeClass("Dodanie: Reklamacja",
                        "Nr: " + complaintToAdd.Id
                        + ", produkt: " + ProductsHelper.GetProductName(complaintToAdd.ProductId, context)
                        + ", data: " + OrdersHelper.GetDate(complaintToAdd.Date)
                        + ", ilość: " + complaintToAdd.Amount + ", opis: " + complaintToAdd.Desc
                        + ", koszt: " + complaintToAdd.Cost + ", zakres: " + OrdersHelper.GetComplaintIsTotalString(complaintToAdd.isTotal)
                        , user));
            }
        }

        public static Order AutorizeOrder(Order order, ApplicationUser user, ApplicationDataDbContext context)
        {
            if (order?.StateId == 0)
            {
                order.StateId = 1;
                order.Changes.Add(new ChangeClass("Autoryzacja zamówienia", "", user));
                context.Entry(order).State = EntityState.Modified;
                context.SaveChanges();
            }

            return order;
        }

        public static Order UnautorizeOrder(Order order, ApplicationUser user, ApplicationDataDbContext context)
        {
            if (order?.StateId == 1)
            {
                order.StateId = 0;
                order.Changes.Add(new ChangeClass("Cofnięcie autoryzacji zamówienia", "", user));
                context.Entry(order).State = EntityState.Modified;
                context.SaveChanges();
            }

            return order;
        }

        public static void IntoProduction(Order order, ApplicationUser user, ApplicationDataDbContext context)
        {
            if (order?.StateId == 1)
            {
                order.StateId = 2;
                order.Changes.Add(new ChangeClass("Przesłano zamówienie do produkcji", "", user));
                context.Entry(order).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

    }
}