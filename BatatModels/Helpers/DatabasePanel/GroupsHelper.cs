﻿using System;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public partial class SharedHelper
    {
        public static string GetGroupName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(brak nazwy)";
        }

        public static string GetGroupName(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return "(nie ustawiono grupy)";
            }

            var group = context.Groups.Find(id);
            if (group != null)
            {
                return GetGroupName(group.Name);
            }

            return "(brak grupy:" + id + ")";
        }

        public static int GetGroupId(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return 0;
            }

            var group = context.Groups.Find(id);
            if (group != null)
            {
                return id;
            }

            return -1;
        }

        public static string GetGroupCode(string code)
        {
            return !String.IsNullOrWhiteSpace(code) ? code : "(brak kodu)";
        }

        public static string GetGroupCode(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return "(nie ustawiono grupy)";
            }
            var group = context.Groups.Find(id);
            if (group != null)
            {
                return GetGroupCode(group.Code);
            }

            return "(brak grupy:" + id + ")";
        }

        public static string GetGroupDescription(string desc)
        {
            return !String.IsNullOrWhiteSpace(desc) ? desc : "(brak opisu)";
        }

        public static string GetGroupDescription(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return "(nie ustawiono grupy)";
            }

            var group = context.Groups.Find(id);
            if (group != null)
            {
                return GetGroupDescription(group.Desc);
            }

            return "(brak grupy:" + id + ")";
        }

    }
}