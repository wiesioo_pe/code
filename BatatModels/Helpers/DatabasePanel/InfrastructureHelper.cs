﻿using System;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class InfrastructureHelper
    {
        public static string GetUserName(string userId, ApplicationDataDbContext context)
        {
            if(userId == null)
            {
                return null;
            }

            var user = context.Users.Find(userId);

            if (user != null)
            {
                return "Użytkownik nr: " + userId;
            }

            return "(brak użytkownika nr: " + userId + ")";
        }

        public static string GetNote(string uwaga)
        {
            return !String.IsNullOrWhiteSpace(uwaga) ? uwaga : "(pusta uwaga)";
        }

        public static string GetNoteId(int noteNumber)
        {
            return noteNumber >= 0 ? (noteNumber + 1).ToString() : "(błędny Id uwagi)";
        }
    }
}