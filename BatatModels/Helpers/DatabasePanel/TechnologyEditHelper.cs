﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class TechnologyEditHelper
    {
        public static void CheckIfTechnologyExists(
            ref Technology technologyDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (technologyDb == null)
            {
                technologyDb = new Technology();
                technologyDb.Changes.Add(new ChangeClass("Utworzenie: Technologia", "Nowa technologia utworzona", user));
            }
        }

        public static void CheckGroupsChange(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedGroups = new HashSet<GroupIdClass>();
            foreach (var item in technologyDb.Groups)
            {
                if (technologyVm.Groups.Any(a => a.GroupId == item.GroupId))
                {
                    technologyVm.Groups.RemoveAll(a => a.GroupId == item.GroupId);
                }
                else
                {
                    removedGroups.Add(item);
                }
            }
            foreach (var removedGroup in removedGroups)
            {
                technologyDb.Groups.Remove(removedGroup);
                technologyDb.Changes.Add(new ChangeClass("Usunięcie: Grupa",
                            "Nr: " + removedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(removedGroup.GroupId, context)
                            , user));
            }
            foreach (var addedGroup in technologyVm.Groups)
            {
                if (context.Groups.Any(a => a.Id == addedGroup.GroupId))
                {
                    technologyDb.Groups.Add(new GroupIdClass(addedGroup.GroupId, context.Groups.Find(addedGroup.GroupId)?.UUID));
                    technologyDb.Changes.Add(new ChangeClass("Dodanie: Grupa",
                            "Dodanie grupy nr: " + addedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(addedGroup.GroupId, context)
                            , user));
                }
                else
                {
                    errorVm.Messages.Add("Grupa nr: " + addedGroup + " nie istnieje");
                }
            }
        }

        public static void CheckNameChange(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            technologyVm.Name = SharedHelper.NormalizeString(technologyVm.Name);
            technologyDb.Name = SharedHelper.NormalizeString(technologyDb.Name);

            if (technologyVm.Name != null)
            {
                if (technologyVm.Name != technologyDb.Name)
                {
                    if (technologyDb.Name != null)
                    {
                        technologyDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + technologyDb.Name + ", na: " + technologyVm.Name, user));
                        technologyDb.Name = technologyVm.Name;
                    }
                    else
                    {
                        technologyDb.Changes.Add(new ChangeClass("Ustawienie: nazwa",
                        technologyVm.Name, user));
                        technologyDb.Name = technologyVm.Name;
                    }
                }
            }
            else
            {
                errorVm.Messages.Add("Nazwa technologii nie może być pusta");
            }
        }

        public static bool CheckPartChange(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            bool didPartChange = false;

            if(technologyVm.PartId != 0)
            {
                if (technologyVm.PartId == technologyDb.PartId)
                    return false;

                if(technologyDb.PartId == 0)
                {
                    technologyDb.Changes.Add(new ChangeClass("Ustawienie: Detal",
                        "Nr: " + PartsHelper.GetPartId(technologyVm.PartId, context)
                               + ", nazwa: " + PartsHelper.GetPartName(technologyVm.PartId, context)
                        , user));
                }
                else
                {
                    technologyDb.Changes.Add(new ChangeClass("Edycja: Detal",
                        "Z nr: " + PartsHelper.GetPartId(technologyDb.PartId, context)
                                 + ", nazwa: " + PartsHelper.GetPartName(technologyDb.PartId, context)
                                 + ", na nr: " + PartsHelper.GetPartId(technologyVm.PartId, context)
                                 + ", nazwa: " + PartsHelper.GetPartName(technologyVm.PartId, context)
                        , user));
                }

                var part = context.Parts.Find(technologyVm.PartId);
                technologyDb.PartId = part.Id;
                technologyDb.PartUUID = part.UUID;

                didPartChange = true;
            }
            else
            {
                errorVm.Messages.Add("Detal nie może być pusty");
            }

            return didPartChange;
        }

        public static void UpdateOperationsCodes(
            Technology technologyDb,
            ApplicationDataDbContext context)
        {
            var allTechnologyOperations = context.Operations.Where(a => a.TechnologyId == technologyDb.Id).ToList();
            foreach (var operation in allTechnologyOperations)
            {
                operation.PartId = technologyDb.PartId;
                operation.Code = OperationsHelper.CreateOperationCode(technologyDb, operation.Id, context);
                context.Entry(operation).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public static void UpdateTechnologyCode(Technology technologyDb, ApplicationDataDbContext context)
        {
            technologyDb.Code = TechnologiesHelper.CreateTechnologyCode(technologyDb.Id, context);
        }

        public static bool CheckOperationsBeforeChange(
            OperationIdClassVM operationIdVm,
            Operation operationDb,
            Technology technologyDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var isChanged = false;
            var operationsBeforeIdsDb = operationDb.OperationsBefore;
            var operationsBeforeIdsVm = operationIdVm.OperationsBefore;
            var operationsBeforeIdsToRemoveDb = new HashSet<OperationBeforeIdClass>();

            foreach(var operationBeforeDb in operationsBeforeIdsDb)
            {
                if(operationsBeforeIdsVm.All(a => a.OperationBeforeId != operationBeforeDb.OperationBeforeId))
                {
                    operationsBeforeIdsToRemoveDb.Add(operationBeforeDb);
                }
                else
                {
                    operationsBeforeIdsVm.RemoveAll(a=>a.OperationBeforeId == operationBeforeDb.OperationBeforeId);
                }
            }

            foreach(var operationBeforeIdToRemoveDb in operationsBeforeIdsToRemoveDb)
            {
                technologyDb.Changes.Add(new ChangeClass("Zmiana: Operacja nr: " + operationDb.Id,
                            "Usunięcie operacji przed nr: " + operationBeforeIdToRemoveDb.OperationBeforeId
                            + " dla operacji nr: " + operationDb.Id
                            , user));
                operationsBeforeIdsDb.Remove(operationBeforeIdToRemoveDb);
                isChanged = true;
            }

            foreach(var newOperationBeforeId in operationsBeforeIdsVm)
            {
                if(context.Operations.Any(a => a.Id == newOperationBeforeId.OperationBeforeId))
                {
                    technologyDb.Changes.Add(new ChangeClass("Zmiana: Operacja nr: " + operationDb.Id,
                            "Dodanie operacji przed nr: " + newOperationBeforeId.OperationBeforeId
                            + " dla operacji nr: " + operationDb.Id
                            , user));
                    operationsBeforeIdsDb.Add(new OperationBeforeIdClass(newOperationBeforeId));
                    isChanged = true;
                }
                else
                {
                    errorVm.Messages.Add("Operacja o nr: " + newOperationBeforeId.OperationBeforeId + " nie istnieje");
                }
            }

            return isChanged;
        }

        public static void CheckOperationsErrors(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            foreach (var operationVm in technologyVm.Operations)
            {
                if (operationVm.OperationTypeId != null
                    && context.OperationTypes.Any(a => a.Id == operationVm.OperationTypeId))
                    continue;

                if (operationVm.OperationTypeId == null)
                {
                    errorVm.Messages.Add("Typ operacji nie może być pusty");
                }

                if (!context.OperationTypes.Any(a => a.Id == operationVm.OperationTypeId))
                {
                    errorVm.Messages.Add("Typ operacji o nr: "+ operationVm.OperationTypeId + " nie istnieje");
                }
            }
        }

        public static bool CheckOperationsChangeWithoutSaving(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationDataDbContext context)
        {
            // A new operation have been added
            if (technologyVm.Operations.Any(a => a.OperationId > 999999))
            {
                return true;
            }

            // Number of operations changed
            if(technologyDb.Operations.Count != technologyVm.Operations.Count)
            {
                return true;
            }
            
            for (var i = 0; i < technologyDb.Operations.Count; i++)
            {
                var operationIdDb = technologyDb.Operations[i];
                var operationIdVm = technologyVm.Operations[i];

                // The order of operations has changed
                if(operationIdDb.OperationId != operationIdVm.OperationId || operationIdDb.OperationUUID != operationIdVm.OperationUUID)
                {
                    return true;
                }

                var operationDb = context.Operations.Find(operationIdDb.OperationId);
                // Operation has changed: type, description, activness, start/stop collectiveness
                if(operationDb.OperationTypeId != operationIdVm.OperationTypeId 
                    || operationDb.Desc != operationIdVm.Desc
                    || operationDb.isActive != operationIdVm.isActive
                    || operationDb.isStartCollective != operationIdVm.isStartCollective
                    || operationDb.isEndCollective != operationIdVm.isEndCollective)
                {
                    return true;
                }

                // Operation has changed the number of operations before
                if (operationDb.OperationsBefore.Count != operationIdVm.OperationsBefore.Count)
                {
                    return true;
                }


                // Operation has changed any operation before
                foreach (var operationBeforeIdDb in operationDb.OperationsBefore)
                {
                    if(operationIdVm.OperationsBefore.All(a => a.OperationBeforeId != operationBeforeIdDb.OperationBeforeId))
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        public static bool CheckOperationsChange(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationUser user,
            ApplicationDataDbContext context)
        {
            bool newOperationsCreated = false;
            var allOperationTypes = context.OperationTypes.ToList();

            // Save new operations
            foreach (var operationVm in technologyVm.Operations)
            {
                if (operationVm.OperationId <= 999999)
                    continue;

                newOperationsCreated = true;

                var newOperation = new Operation();

                newOperation.Changes.Add(new ChangeClass("Utworzenie:",
                    "Utworzenie nowej operacji"
                    , user));

                newOperation.isActive = true;
                newOperation.Changes.Add(new ChangeClass("Ustawienie: status",
                    OperationsHelper.GetIsOperationActive(operationVm.isActive)
                    , user));

                newOperation.TechnologyId = technologyDb.Id;
                newOperation.TechnologyUUID = technologyDb.UUID;
                newOperation.PartId = technologyDb.PartId;
                newOperation.PartUUID = technologyDb.PartUUID;

                newOperation.OperationTypeId = operationVm.OperationTypeId ?? 0;
                newOperation.OperationTypeUUID = allOperationTypes.Single(a => a.Id == operationVm.OperationTypeId).UUID;

                newOperation.isStartCollective = operationVm.isStartCollective ?? false;
                newOperation.Changes.Add(new ChangeClass("Ustawienie: sposób rozpoczęcia",
                    OperationsHelper.GetIsStartCollective(operationVm.isStartCollective)
                    , user));

                newOperation.isEndCollective = operationVm.isEndCollective ?? false;
                newOperation.Changes.Add(new ChangeClass("Ustawienie: sposób zakończenia",
                    OperationsHelper.GetIsEndCollective(operationVm.isEndCollective)
                    , user));

                operationVm.Desc = SharedHelper.NormalizeString(operationVm.Desc);
                newOperation.Desc = operationVm.Desc;
                if (operationVm.Desc != null)
                {
                    newOperation.Changes.Add(new ChangeClass("Ustawienie: opis" + operationVm.OperationId,
                        operationVm.Desc
                        , user));
                }

                foreach (var operationBefore in operationVm.OperationsBefore)
                {
                    if (operationBefore.OperationBeforeId < 1 ||
                        operationBefore.OperationBeforeId >= context.Operations.Count())
                        continue;

                    newOperation.OperationsBefore.Add(new OperationBeforeIdClass(operationBefore.OperationBeforeId, context.Operations.Find(operationBefore.OperationBeforeId).UUID));
                    newOperation.Changes.Add(new ChangeClass("Dodanie: operacja przed",
                        "Nr: " + operationBefore.OperationBeforeId
                        , user));
                }

                newOperation.LastUpdate = DateTime.Now;

                context.Operations.Add(newOperation);
                context.SaveChanges();

                foreach (var operation2 in technologyVm.Operations)
                {
                    foreach (var operationBefore in operation2.OperationsBefore)
                    {
                        if (operationBefore.OperationBeforeId == operationVm.OperationId)
                        {
                            operationBefore.OperationBeforeId = newOperation.Id;
                        }
                    }
                }
                operationVm.OperationId = newOperation.Id;
            }

            var operationsThatExistInVm = new List<OperationIdClass>();
            var newlyCreatedOperationsList = new List<OperationIdClass>();
            foreach(var operationVm in technologyVm.Operations)
            {
                if(technologyDb.Operations.Any(a => a.OperationId == operationVm.OperationId))
                {
                    var operationIdDb = technologyDb.Operations.First(a => a.OperationId == operationVm.OperationId);
                    var operationDb = context.Operations.Find(operationIdDb.OperationId);
                    bool isChanged = false;

                    if (operationDb.isActive != operationVm.isActive)
                    {
                        operationDb.Changes.Add(new ChangeClass("Zmiana: status",
                            "Z: " + OperationsHelper.GetIsOperationActive(operationDb.isActive)
                            + ", na: " + OperationsHelper.GetIsOperationActive(operationVm.isActive)
                            , user));
                        operationDb.isActive = operationVm.isActive ?? true;
                        isChanged = true;
                    }

                    if (operationDb.isStartCollective != operationVm.isStartCollective)
                    {
                        operationDb.Changes.Add(new ChangeClass("Zmiana: sposób rozpoczęcia",
                            "Z: " + OperationsHelper.GetIsStartCollective(operationDb.isStartCollective)
                            + ", na: " + OperationsHelper.GetIsStartCollective(operationVm.isStartCollective)
                            , user));
                        operationDb.isStartCollective = operationVm.isStartCollective ?? false;
                        isChanged = true;
                    }

                    if (operationDb.isEndCollective != operationVm.isEndCollective)
                    {
                        operationDb.Changes.Add(new ChangeClass("Zmiana: sposób zakończenia",
                            "Z: " + OperationsHelper.GetIsEndCollective(operationDb.isEndCollective)
                            + ", na: " + OperationsHelper.GetIsEndCollective(operationVm.isEndCollective)
                            , user));
                        operationDb.isEndCollective = operationVm.isEndCollective ?? false;
                        isChanged = true;
                    }

                    if (operationDb.OperationTypeId != operationVm.OperationTypeId)
                    {
                        operationDb.Changes.Add(new ChangeClass("Zmiana: typ operacji",
                            "Z: " + OperationsHelper.GetOperationTypeName(operationDb.OperationTypeId, context)
                            + ", na: " + OperationsHelper.GetOperationTypeName(operationVm.OperationTypeId, context)
                            , user));
                        operationDb.OperationTypeId = operationVm.OperationTypeId ?? 0;
                        isChanged = true;
                    }

                    if (operationDb.Desc != operationVm.Desc)
                    {
                        operationDb.Changes.Add(new ChangeClass("Zmiana: opis",
                            "Z: " + operationDb.Desc
                            + ", na: " + operationVm.Desc
                            , user));
                        operationDb.Desc = operationVm.Desc;
                        isChanged = true;
                    }

                    for(var i = operationVm.OperationsBefore.Count - 1; i >= 0; i--)
                    {
                        var operationBeforeVm = operationVm.OperationsBefore[i];
                        var areOperationsBeforeChanged = false;
                        var newOperationsBefore = new List<OperationBeforeIdClass>();
                        if(operationDb.OperationsBefore.All(a => a.OperationBeforeId != operationBeforeVm.OperationBeforeId))
                        {
                            operationDb.Changes.Add(new ChangeClass("Zmiana: dodanie operacji przed",
                            "Nr: " + operationBeforeVm.OperationBeforeId
                            , user));
                            areOperationsBeforeChanged = true;
                            isChanged = true;
                            newOperationsBefore.Add(new OperationBeforeIdClass(operationBeforeVm));
                        }
                        else
                        {
                            newOperationsBefore.Add(new OperationBeforeIdClass(operationBeforeVm));
                            operationVm.OperationsBefore.RemoveAt(i);
                            operationDb.OperationsBefore.RemoveAll(a => a.OperationBeforeId == operationBeforeVm.OperationBeforeId);
                        }

                        foreach(var operationBeforeIdDb in operationDb.OperationsBefore)
                        {
                            operationDb.Changes.Add(new ChangeClass("Zmiana: usunięcie operacji przed",
                            "Nr: " + operationBeforeIdDb.OperationBeforeId
                            , user));
                            areOperationsBeforeChanged = true;
                            isChanged = true;
                        }

                        if(areOperationsBeforeChanged)
                        {
                            operationDb.OperationsBefore = newOperationsBefore;
                            isChanged = true;
                        }
                    }


                    if (technologyDb.Operations.IndexOf(operationIdDb) != technologyVm.Operations.IndexOf(operationVm))
                    {
                        technologyDb.Changes.Add(new ChangeClass("Zmiana: Operacja nr: " + operationVm.OperationId,
                            "Zmiana indeksu operacji z: " + (technologyDb.Operations.IndexOf(operationIdDb) + 1)
                            + ", na: " + (technologyVm.Operations.IndexOf(operationVm) + 1)
                            , user));
                        isChanged = true;
                    }

                    if(isChanged)
                    {
                        technologyDb.Changes.Add(new ChangeClass("Zmiana: Operacja nr: " + operationVm.OperationId,
                            "Zmiana właściwości operacji. Index: " + (technologyVm.Operations.IndexOf(operationVm) + 1)
                            , user));
                        operationDb.LastUpdate = DateTime.Now;
                        context.Entry(operationDb).State = EntityState.Modified;
                        context.SaveChanges();
                    }

                    operationsThatExistInVm.Add(operationIdDb);
                }
                else
                {
                    technologyDb.Changes.Add(new ChangeClass("Dodanie: Operacja nr: " + operationVm.OperationId,
                            "Dodanie operacji. Index: " + (technologyVm.Operations.IndexOf(operationVm) + 1)
                            , user));
                }

                newlyCreatedOperationsList.Add(new OperationIdClass(operationVm.OperationId, context.Operations.Find(operationVm.OperationId).UUID));
            }

            foreach(var operationThatExistInVm in operationsThatExistInVm)
            {
                technologyDb.Operations.Remove(operationThatExistInVm);
            }

            foreach(var removedOperation in technologyDb.Operations)
            {
                technologyDb.Changes.Add(new ChangeClass("Usunięcie: Operacja",
                        "Usunięcie operacji nr: " + removedOperation.OperationId
                        + ", kod: " + OperationsHelper.GetOperationCode(removedOperation.OperationId, context)
                        , user));
            }

            technologyDb.Operations.AddRange(newlyCreatedOperationsList);

            return newOperationsCreated;
        }

        public static void CheckNotesChange(
            TechnologyVM technologyVm,
            Technology technologyDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            // Lista uwag
            for (int i = technologyDb.Notes.Count - 1; i >= 0; i--)
            {
                if (technologyDb.Notes[i].Note != technologyVm.Notes[i].Note)
                {
                    if (String.IsNullOrWhiteSpace(technologyVm.Notes[i].Note))
                    {
                        technologyDb.Changes.Add(new ChangeClass("Usunięcie: uwaga",
                        "Nr: " + SharedHelper.GetNoteId(i) + ", treść: '"
                        + SharedHelper.GetNote(technologyDb.Notes[i].Note) + "'"
                        , user));
                        technologyDb.Notes.RemoveAt(i);
                        technologyVm.Notes.RemoveAt(i);
                    }
                    else
                    {
                        technologyDb.Changes.Add(new ChangeClass("Edycja: uwaga",
                            "Nr: " + SharedHelper.GetNoteId(i) + ", treść z: '"
                            + SharedHelper.GetNote(technologyDb.Notes[i].Note) + "', na: '"
                            + SharedHelper.GetNote(technologyVm.Notes[i].Note) + "'"
                            , user));
                        technologyDb.Notes[i].Note = technologyVm.Notes[i].Note;
                        technologyVm.Notes.RemoveAt(i);
                    }
                }
                else
                {
                    technologyVm.Notes.RemoveAt(i);
                }

            }

            foreach (var newNote in technologyVm.Notes)
            {
                if (!String.IsNullOrWhiteSpace(newNote.Note))
                {
                    technologyDb.Notes.Add(new NoteClass(newNote.Note, user));
                    technologyDb.Changes.Add(new ChangeClass("Dodanie: uwaga",
                            "Nr: " + SharedHelper.GetNoteId(technologyDb.Notes.IndexOf(technologyDb.Notes.Last()))
                            + ", treść: '" + SharedHelper.GetNote(newNote.Note) + "'"
                            , user));
                }
                else
                {
                    errorVm.Messages.Add("Nie można dodać pustej notatki");
                }
            }
        }

    }
}