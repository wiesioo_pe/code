﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ContactEditHelper
    {
        public static void CheckIfContactExists(ref Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactDb == null)
            {
                contactDb = new Contact();
                contactDb.Changes.Add(new ChangeClass("Utworzenie: kontakt", "Nowy kontakt utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            ContactAndPhotoVM contactAndPhotoVm,
            Contact contactDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var groupsIdsDbCopy = new HashSet<GroupIdClass>(contactDb.Groups);
            if (contactAndPhotoVm.Groups == null)
            {
                contactAndPhotoVm.Groups = new List<GroupIdClassVM>();
            }
            foreach (var groupIdVm in contactAndPhotoVm.Groups)
            {
                if (groupsIdsDbCopy.Any(a => a.GroupId == groupIdVm.GroupId))
                {
                    groupsIdsDbCopy.RemoveWhere(b => b.GroupId == groupIdVm.GroupId);
                }
                else
                {
                    contactDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                        "Id grupy: " + groupIdVm.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdVm.GroupId, context), user));
                    contactDb.Groups.Add(new GroupIdClass(groupIdVm.GroupId, context.Groups.Find(groupIdVm.GroupId)?.UUID));
                }
            }
            foreach (var groupIdDbCopy in groupsIdsDbCopy)
            {
                contactDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                        "Id grupy: " + groupIdDbCopy.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdDbCopy.GroupId, context), user));
                contactDb.Groups.RemoveWhere(c => c.GroupId == groupIdDbCopy.GroupId);
            }
        }

        public static void CheckNameChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.Name != contactDb.Name)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + contactDb.Name + ", na: " + contactAndPhotoVm.Name, user));
                contactDb.Name = contactAndPhotoVm.Name;
            }
        }

        public static void CheckSubjectChange(
            ContactAndPhotoVM contactAndPhotoVm,
            Contact contactDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (contactAndPhotoVm.SubjectId != contactDb.SubjectId)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: odbiorca",
                        "Z: " + SubjectsHelper.GetSubjectName(contactDb.SubjectId, context) + ", na: " + SubjectsHelper.GetSubjectName(contactAndPhotoVm.SubjectId, context), user));
                contactDb.SubjectId = contactAndPhotoVm.SubjectId;
            }
        }

        public static void CheckDivisionChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.Division != contactDb.Division)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: dział",
                        "Z: " + contactDb.Division + ", na: " + contactAndPhotoVm.Division, user));
                contactDb.Division = contactAndPhotoVm.Division;
            }
        }

        public static void CheckPositionChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.Position != contactDb.Position)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: stanowisko",
                        "Z: " + contactDb.Position + ", na: " + contactAndPhotoVm.Position, user));
                contactDb.Position = contactAndPhotoVm.Position;
            }
        }

        public static void CheckOfficePhoneChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.OfficePhone != contactDb.OfficePhone)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: telefon biurowy",
                        "Z: " + contactDb.OfficePhone + ", na: " + contactAndPhotoVm.OfficePhone, user));
                contactDb.OfficePhone = contactAndPhotoVm.OfficePhone;
            }
        }

        public static void CheckBusinessPhoneChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.BusinessPhone != contactDb.BusinessPhone)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: telefon służbowy",
                        "Z: " + contactDb.BusinessPhone + ", na: " + contactAndPhotoVm.BusinessPhone, user));
                contactDb.BusinessPhone = contactAndPhotoVm.BusinessPhone;
            }
        }

        public static void CheckPrivatePhoneChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.PrivatePhone != contactDb.PrivatePhone)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: telefon prywatny",
                        "Z: " + contactDb.PrivatePhone + ", na: " + contactAndPhotoVm.PrivatePhone, user));
                contactDb.PrivatePhone = contactAndPhotoVm.PrivatePhone;
            }
        }

        public static void CheckFaxChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.Fax != contactDb.Fax)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: fax",
                        "Z: " + contactDb.Fax + ", na: " + contactAndPhotoVm.Fax, user));
                contactDb.Fax = contactAndPhotoVm.Fax;
            }
        }

        public static void CheckeMailChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.eMail != contactDb.eMail)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: email",
                        "Z: " + contactDb.eMail + ", na: " + contactAndPhotoVm.eMail, user));
                contactDb.eMail = contactAndPhotoVm.eMail;
            }
        }

        public static void CheckDescChange(ContactAndPhotoVM contactAndPhotoVm, Contact contactDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (contactAndPhotoVm.Desc != contactDb.Desc)
            {
                contactDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + contactDb.Desc + ", na: " + contactAndPhotoVm.Desc, user));
                contactDb.Desc = contactAndPhotoVm.Desc;
            }
        }

        public static void CheckPhotoChange(
            ContactAndPhotoVM contactAndPhotoVm,
            Contact contactDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (contactAndPhotoVm.PhotoFile.isNew)
            {
                // remove illegal characters from Contact Name
                string contactNameForPath = contactAndPhotoVm.Name;
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                contactNameForPath = r.Replace(contactNameForPath, "_");

                // Save URL in Contact.PhotoFileUrlObj
                var splittedFileName = contactAndPhotoVm.PhotoFile.FileName.Split('.');
                var fileExtension = "";
                if (splittedFileName.Length > 1)
                {
                    fileExtension = splittedFileName.Last();
                }
                string fileName = contactNameForPath;
                if (fileExtension != "")
                {
                    fileName += "." + fileExtension;
                }


                contactAndPhotoVm.PhotoFileUrlObj.FileUrl = "/UserFiles/Contacts/" + fileName;
                contactAndPhotoVm.PhotoFileUrlObj.FileName = fileName;
                contactAndPhotoVm.PhotoFileUrlObj.OriginalFileName = contactAndPhotoVm.PhotoFile.FileName;
                contactAndPhotoVm.PhotoFileUrlObj.FileType = contactAndPhotoVm.PhotoFile.ContentType;
                contactAndPhotoVm.PhotoFileUrlObj.FileSize = contactAndPhotoVm.PhotoFile.Size;

                var base64StringWithoutHeader = contactAndPhotoVm.PhotoFile.Content.Remove(0, contactAndPhotoVm.PhotoFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Contacts"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);

                // Delete old file
                if (!String.IsNullOrEmpty(contactDb.PhotoFileUrlObj?.FileUrl))
                {
                    var oldFilePath = HttpContext.Current.Server.MapPath(contactDb.PhotoFileUrlObj.FileUrl);
                    if (!String.IsNullOrEmpty(oldFilePath) && File.Exists(oldFilePath))
                    {
                        File.Delete(oldFilePath);
                    }
                }


                if (contactDb.PhotoFileUrlObj == null)
                {
                    contactDb.Changes.Add(new ChangeClass("Dodanie: zdjęcie",
                        "Nazwa: " + contactAndPhotoVm.PhotoFileUrlObj.FileName
                        + "(oryg.: " + contactAndPhotoVm.PhotoFileUrlObj.OriginalFileName + ")"
                        , user));

                    contactDb.PhotoFileUrlObj =
                        new ContactFileUrlClass
                        {
                            FileName = contactAndPhotoVm.PhotoFileUrlObj.FileName,
                            OriginalFileName = contactAndPhotoVm.PhotoFileUrlObj.OriginalFileName,
                            FileSize = contactAndPhotoVm.PhotoFileUrlObj.FileSize,
                            FileType = contactAndPhotoVm.PhotoFileUrlObj.FileType,
                            FileUrl = contactAndPhotoVm.PhotoFileUrlObj.FileUrl
                        };

                    context.Entry(contactDb.PhotoFileUrlObj).State = EntityState.Added;
                }
                else
                {
                    contactDb.Changes.Add(new ChangeClass("Edycja: zdjęcie",
                        "Z: " + contactDb.PhotoFileUrlObj.FileName
                        + "(oryg.: " + contactDb.PhotoFileUrlObj.OriginalFileName + ")"
                        + ", na: " + contactAndPhotoVm.PhotoFileUrlObj.FileName
                        + "(oryg.: " + contactAndPhotoVm.PhotoFileUrlObj.OriginalFileName + ")"
                        , user));

                    contactDb.PhotoFileUrlObj.FileName = contactAndPhotoVm.PhotoFileUrlObj.FileName;
                    contactDb.PhotoFileUrlObj.OriginalFileName = contactAndPhotoVm.PhotoFileUrlObj.OriginalFileName;
                    contactDb.PhotoFileUrlObj.FileSize = contactAndPhotoVm.PhotoFileUrlObj.FileSize;
                    contactDb.PhotoFileUrlObj.FileType = contactAndPhotoVm.PhotoFileUrlObj.FileType;
                    contactDb.PhotoFileUrlObj.FileUrl = contactAndPhotoVm.PhotoFileUrlObj.FileUrl;
                    context.Entry(contactDb.PhotoFileUrlObj).State = EntityState.Modified;
                }
            }

        }
    }
}