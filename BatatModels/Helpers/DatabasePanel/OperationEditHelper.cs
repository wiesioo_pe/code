﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class OperationEditHelper
    {
        public static void CheckIfOperationExists(ref Operation operationDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (operationDb == null)
            {
                operationDb = new Operation();
                operationDb.Changes.Add(new ChangeClass("Utworzenie: Operacja", "Nowa operacja utworzona", user));
            }
        }

        public static void CheckGroupsChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedGroups = new HashSet<GroupIdClass>();
            foreach (var item in operationDb.Groups)
            {
                if (operationAndFilesVm.Groups.Any(a => a.GroupId == item.GroupId))
                {
                    operationAndFilesVm.Groups.RemoveAll(a => a.GroupId == item.GroupId);
                }
                else
                {
                    removedGroups.Add(item);
                }
            }
            foreach (var removedGroup in removedGroups)
            {
                operationDb.Groups.Remove(removedGroup);
                operationDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                            "Nr: " + removedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(removedGroup.GroupId, context)
                            , user));
            }
            foreach (var addedGroup in operationAndFilesVm.Groups)
            {
                if (context.Groups.Any(a => a.Id == addedGroup.GroupId))
                {
                    operationDb.Groups.Add(new GroupIdClass(addedGroup.GroupId, context.Groups.Find(addedGroup.GroupId)?.UUID));
                    operationDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                            "Nr: " + addedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(addedGroup.GroupId, context)
                            , user));
                }
                else
                {
                    errorVm.Messages.Add("Grupa nr: " + addedGroup.GroupId + " nie istnieje");
                }
            }
        }

        public static void CheckTypeChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (operationAndFilesVm.OperationTypeId != 0)
            {
                if (operationAndFilesVm.OperationTypeId != operationDb.OperationTypeId)
                {
                    if(operationDb.OperationTypeId == 0)
                    {
                        operationDb.Changes.Add(new ChangeClass("Ustawienie: rodzaj operacji",
                        OperationsHelper.GetOperationTypeName(operationAndFilesVm.OperationTypeId, context), user));
                        operationDb.OperationTypeId = operationAndFilesVm.OperationTypeId;
                    }
                    else
                    {
                        operationDb.Changes.Add(new ChangeClass("Edycja: rodzaj operacji",
                        "Z: " + OperationsHelper.GetOperationTypeName(operationDb.OperationTypeId, context)
                              + ", na: " + OperationsHelper.GetOperationTypeName(operationAndFilesVm.OperationTypeId, context), user));
                        operationDb.OperationTypeId = operationAndFilesVm.OperationTypeId;
                    }
                }
            }
            else
            {
                errorVm.Messages.Add("Typ operacji nie może być pusty");
            }
        }

        public static void CheckParametersChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedParameters = new HashSet<ParameterIdValueClass>();
            foreach (var parameterDb in operationDb.Parameters)
            {
                if (operationAndFilesVm.Parameters.Any(a => a.ParameterId == parameterDb.ParameterId))
                {
                    var parameterVm = operationAndFilesVm.Parameters.First(a => a.ParameterId == parameterDb.ParameterId);

                    parameterVm.Value = SharedHelper.NormalizeString(parameterVm.Value);
                    parameterDb.Value = SharedHelper.NormalizeString(parameterDb.Value);

                    if (parameterVm.Value != null)
                    {
                        if (parameterVm.Value != parameterDb.Value)
                        {
                            if (parameterDb.Value == null)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: wartość parameteru",
                                    "Nr: " + parameterDb.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(parameterDb.ParameterId, context)
                                    + ", wartość: " + parameterVm.Value
                                    , user));
                                parameterDb.Value = parameterVm.Value;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: wartość parameteru",
                                    "Nr: " + parameterDb.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(parameterDb.ParameterId, context)
                                    + ", wartość z: " + parameterDb.Value
                                    + ", na: " + parameterVm.Value
                                    , user));
                                parameterDb.Value = parameterVm.Value;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Wartość parametru nie może być pusta");
                    }
                    operationAndFilesVm.Parameters.RemoveAll(a => a.ParameterId == parameterDb.ParameterId);
                }
                else
                {
                    removedParameters.Add(parameterDb);
                }
            }
            foreach (var removedParameter in removedParameters)
            {
                operationDb.Parameters.Remove(removedParameter);
                operationDb.Changes.Add(new ChangeClass("Usunięcie: parametr",
                            "Nr: " + removedParameter.ParameterId
                            + ", nazwa: " + ParametersHelper.GetParameterName(removedParameter.ParameterId, context)
                            + ", wartość: " + removedParameter.Value
                            , user));
            }
            foreach (var addedParameter in operationAndFilesVm.Parameters)
            {
                addedParameter.Value = SharedHelper.NormalizeString(addedParameter.Value);

                if (context.Parameters.Any(a => a.Id == addedParameter.ParameterId))
                {
                    if (addedParameter.Value != null)
                    {
                        operationDb.Parameters.Add(new ParameterIdValueClass(addedParameter.ParameterId, context.Parameters.Find(addedParameter.ParameterId).UUID, addedParameter.Value));
                        operationDb.Changes.Add(new ChangeClass("Dodanie: parametr",
                                    "Nr: " + addedParameter.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(addedParameter.ParameterId, context)
                                    + ", wartość: " + addedParameter.Value
                                    , user));
                    }
                    else if (addedParameter.Value == null)
                    {
                        errorVm.Messages.Add("Wartość parametru nie może być pusta");
                    }
                }
                else
                {
                    errorVm.Messages.Add("Parametr o nr: " + addedParameter.ParameterId + " nie istnieje");
                }
            }
        }

        public static void CheckToolsChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedTools = new HashSet<ToolIdUtilizationClass>();

            foreach (var toolDb in operationDb.Tools)
            {
                if (operationAndFilesVm.Tools.Any(a => a.ToolId == toolDb.ToolId))
                {
                    var toolVm = operationAndFilesVm.Tools.First(a => a.ToolId == toolDb.ToolId);

                    if(toolVm.Utilization >= 0 && toolVm.Utilization <= 100)
                    {
                        if (toolVm.Utilization != toolDb.Utilization)
                        {
                            if(toolDb.Utilization == 0)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: wartość zużycia",
                                    "Nr: " + toolDb.ToolId
                                    + ", nazwa: " + ToolsHelper.GetToolName(toolDb.ToolId, context)
                                    + ", zużycie: " + ToolsHelper.GetUtilization(toolVm.Utilization)
                                    , user));
                                toolDb.Utilization = toolVm.Utilization ?? 0;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: wartość zużycia",
                                    "Nr: " + toolDb.ToolId
                                    + ", nazwa: " + ToolsHelper.GetToolName(toolDb.ToolId, context)
                                    + ", zużycie z: " + ToolsHelper.GetUtilization(toolDb.Utilization)
                                    + ", na: " + ToolsHelper.GetUtilization(toolVm.Utilization)
                                    , user));
                                toolDb.Utilization = toolVm.Utilization ?? 0;
                            }
                        }
                    }
                    else if (toolVm.Utilization < 0)
                    {
                        errorVm.Messages.Add("Wartość zużycia narzędzia nie może być mniejsza od zera (" + toolVm.Utilization + ")");
                    }
                    else if (toolVm.Utilization > 100)
                    {
                        errorVm.Messages.Add("Wartość zużycia narzędzia nie może być większa od 100% (" + toolVm.Utilization + ")");
                    }
                    operationAndFilesVm.Tools.RemoveAll(a => a.ToolId == toolDb.ToolId);
                }
                else
                {
                    removedTools.Add(toolDb);
                }
            }
            foreach (var removedTool in removedTools)
            {
                operationDb.Tools.Remove(removedTool);
                operationDb.Changes.Add(new ChangeClass("Usunięcie: narzędzie",
                            "Nr: " + removedTool.ToolId
                            + ", nazwa: " + ToolsHelper.GetToolName(removedTool.ToolId, context)
                            + ", zużycie: " + ToolsHelper.GetUtilization(removedTool.Utilization)
                            , user));
            }
            foreach (var addedTool in operationAndFilesVm.Tools)
            {
                if (context.Tools.Any(a => a.Id == addedTool.ToolId))
                {
                    if (addedTool.Utilization >= 0 && addedTool.Utilization <= 100)
                    {
                        operationDb.Tools.Add(new ToolIdUtilizationClass(addedTool));
                        operationDb.Changes.Add(new ChangeClass("Dodanie: narzędzie",
                                    "Nr: " + addedTool.ToolId
                                    + ", nazwa: " + ToolsHelper.GetToolName(addedTool.ToolId, context)
                                    + ", zużycie: " + ToolsHelper.GetUtilization(addedTool.Utilization)
                                    , user));
                    }
                    else
                    {
                        if (addedTool.Utilization < 0)
                        {
                            errorVm.Messages.Add("Wartość zużycia narzędzia nie może być mniejsza od zera (" + addedTool.Utilization + ")");
                        }
                        if (addedTool.Utilization > 100)
                        {
                            errorVm.Messages.Add("Wartość zużycia narzędzia nie może być większa od 100% (" + addedTool.Utilization + ")");
                        }
                    }
                }
                else
                {
                    errorVm.Messages.Add("Narzędzie o nr: "+ addedTool.ToolId + " nie istnieje");
                }
            }
        }

        public static void CheckMaterialsChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedMaterials = new HashSet<MaterialIdAmountClass>();
            foreach (var materialDb in operationDb.Materials)
            {
                if (operationAndFilesVm.Materials.Any(a => a.MaterialId == materialDb.MaterialId))
                {
                    var materialVm = operationAndFilesVm.Materials.First(a => a.MaterialId == materialDb.MaterialId);

                    if(materialVm.Amount >= 0)
                    {
                        if (materialVm.Amount != materialDb.Amount)
                        {
                            if(materialDb.Amount == 0)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: ilość materiału",
                                    "Nr: " + materialDb.MaterialId
                                    + ", nazwa: " + MaterialsHelper.GetMaterialName(materialDb.MaterialId, context)
                                    + ", ilość: " + materialVm.Amount.Value
                                    + " [" + MaterialsHelper.GetMaterialUnit(materialVm.MaterialId, context) + "]"
                                    , user));
                                materialDb.Amount = materialVm.Amount ?? 0;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: ilość materiału",
                                    "Nr: " + materialDb.MaterialId
                                    + ", nazwa: " + MaterialsHelper.GetMaterialName(materialDb.MaterialId, context)
                                    + ", ilość z: " + materialDb.Amount
                                    + " [" + MaterialsHelper.GetMaterialUnit(materialDb.MaterialId, context) + "]"
                                    + ", na: " + materialVm.Amount.Value
                                    + " [" + MaterialsHelper.GetMaterialUnit(materialVm.MaterialId, context) + "]"
                                    , user));
                                materialDb.Amount = materialVm.Amount ?? 0;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Ilość materiału nie może być ujemna (" + materialVm.Amount + ")");
                    }

                    operationAndFilesVm.Materials.RemoveAll(a => a.MaterialId == materialDb.MaterialId);
                }
                else
                {
                    removedMaterials.Add(materialDb);
                }
            }
            foreach (var removedMaterial in removedMaterials)
            {
                operationDb.Materials.Remove(removedMaterial);
                operationDb.Changes.Add(new ChangeClass("Usunięcie: materiał",
                            "Nr: " + removedMaterial.MaterialId
                            + ", nazwa: " + MaterialsHelper.GetMaterialName(removedMaterial.MaterialId, context)
                            + ", ilość: " + removedMaterial.Amount
                            + " [" + MaterialsHelper.GetMaterialUnit(removedMaterial.MaterialId, context) + "]"
                            , user));
            }
            foreach (var addedMaterial in operationAndFilesVm.Materials)
            {
                if (context.Materials.Any(a => a.Id == addedMaterial.MaterialId))
                {
                    if (addedMaterial.Amount >= 0)
                    {
                        operationDb.Materials.Add(new MaterialIdAmountClass(addedMaterial));
                        operationDb.Changes.Add(new ChangeClass("Dodanie: materiał",
                                "Nr: " + addedMaterial.MaterialId
                                + ", nazwa: " + MaterialsHelper.GetMaterialName(addedMaterial.MaterialId, context)
                                + ", ilość: " + addedMaterial.Amount.Value
                                + " [" + MaterialsHelper.GetMaterialUnit(addedMaterial.MaterialId, context) + "]"
                                , user));
                    }
                    else if (addedMaterial.Amount < 0)
                    {
                        errorVm.Messages.Add("Ilość materiału nie może być ujemna (" + addedMaterial.Amount + ")");
                    }
                }
                else
                {
                    errorVm.Messages.Add("Materiał o nr: " + addedMaterial.MaterialId + " nie istnieje");
                }
            }
        }

        public static void CheckMachinesChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedMachinesTimesCosts = new HashSet<MachineIdTimeCostClass>();

            foreach (var machineTimeCostDb in operationDb.MachinesTimesCosts)
            {
                if (operationAndFilesVm.MachinesTimesCosts.All(a => a.MachineId != machineTimeCostDb.MachineId))
                {
                    removedMachinesTimesCosts.Add(machineTimeCostDb);
                }

                var machineTimeCostVm = operationAndFilesVm.MachinesTimesCosts.First(a => a.MachineId == machineTimeCostDb.MachineId);

                if (machineTimeCostDb.isStatistical == false)
                {
                    if (machineTimeCostVm.UnitTime >= 0)
                    {
                        if (machineTimeCostVm.UnitTime != machineTimeCostDb.UnitTime)
                        {
                            if (machineTimeCostDb.UnitTime == 0)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: czas jednostkowy",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", czas w minutach: " + MachinesHelper.GetTime(machineTimeCostVm.UnitTime)
                                    , user));
                                machineTimeCostDb.UnitTime = machineTimeCostVm.UnitTime ?? 0;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: czas jednostkowy",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", czas w minutach, z: " + MachinesHelper.GetTime(machineTimeCostDb.UnitTime)
                                    + ", na: " + MachinesHelper.GetTime(machineTimeCostVm.UnitTime)
                                    , user));
                                machineTimeCostDb.UnitTime = machineTimeCostVm.UnitTime ?? 0;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Czas jednostkowy nie może być ujemny");
                    }

                    if (machineTimeCostVm.PCTime >= 0)
                    {
                        if (machineTimeCostVm.PCTime != machineTimeCostDb.PCTime)
                        {
                            if (machineTimeCostDb.PCTime == 0)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: czas przygotowawczo-zakończeniowy",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", czas w minutach: " + MachinesHelper.GetTime(machineTimeCostVm.PCTime)
                                    , user));
                                machineTimeCostDb.PCTime = machineTimeCostVm.PCTime ?? 0;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: czas przygotowawczo-zakończeniowy",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", czas w minutach, z: " + MachinesHelper.GetTime(machineTimeCostDb.PCTime)
                                    + ", na: " + MachinesHelper.GetTime(machineTimeCostVm.PCTime)
                                    , user));
                                machineTimeCostDb.PCTime = machineTimeCostVm.PCTime ?? 0;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Czas przygotowawczo-zakończeniowy nie może być ujemny");
                    }

                    if (machineTimeCostVm.ToolCost >= 0)
                    {
                        if (machineTimeCostVm.ToolCost != machineTimeCostDb.ToolCost)
                        {
                            if (machineTimeCostDb.ToolCost == 0)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: koszt narzędzi",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", koszt: " + MachinesHelper.GetCost(machineTimeCostVm.ToolCost)
                                    , user));
                                machineTimeCostDb.ToolCost = machineTimeCostVm.ToolCost ?? 0;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: koszt narzędzi",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", koszt, z: " + MachinesHelper.GetCost(machineTimeCostDb.ToolCost)
                                    + ", na: " + MachinesHelper.GetCost(machineTimeCostVm.ToolCost)
                                    , user));
                                machineTimeCostDb.ToolCost = machineTimeCostVm.ToolCost ?? 0;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Koszt narzędzi nie może być ujemny");
                    }

                    if (machineTimeCostVm.OtherCost >= 0)
                    {
                        if (machineTimeCostVm.OtherCost != machineTimeCostDb.OtherCost)
                        {
                            if (machineTimeCostDb.OtherCost == 0)
                            {
                                operationDb.Changes.Add(new ChangeClass("Ustawienie: pozostałe koszty",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", koszt: " + MachinesHelper.GetCost(machineTimeCostVm.OtherCost)
                                    , user));
                                machineTimeCostDb.OtherCost = machineTimeCostVm.OtherCost ?? 0;
                            }
                            else
                            {
                                operationDb.Changes.Add(new ChangeClass("Edycja: pozostałe koszty",
                                    "Nr maszyny: " + machineTimeCostDb.MachineId
                                    + ", kod: " + MachinesHelper.GetMachineCode(machineTimeCostDb.MachineId, context)
                                    + ", model: " + MachinesHelper.GetMachineModel(machineTimeCostDb.MachineId, context)
                                    + ", koszt, z: " + MachinesHelper.GetCost(machineTimeCostDb.OtherCost)
                                    + ", na: " + MachinesHelper.GetCost(machineTimeCostVm.OtherCost)
                                    , user));
                                machineTimeCostDb.OtherCost = machineTimeCostVm.OtherCost ?? 0;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Pozostałe koszty nie mogą być ujemne");
                    }
                }

                operationAndFilesVm.MachinesTimesCosts.RemoveAll(a => a.MachineId == machineTimeCostVm.MachineId);
            }

            foreach (var removedMachineTimeCost in removedMachinesTimesCosts)
            {
                operationDb.MachinesTimesCosts.Remove(removedMachineTimeCost);
                operationDb.Changes.Add(new ChangeClass("Usunięcie: maszyna-czas-koszt",
                            "Nr maszyny: " + removedMachineTimeCost.MachineId
                            + ", kod: " + MachinesHelper.GetMachineCode(removedMachineTimeCost.MachineId, context)
                            + ", model: " + MachinesHelper.GetMachineModel(removedMachineTimeCost.MachineId, context)
                            + ", czas jednostkowy w minutach: " + MachinesHelper.GetTime(removedMachineTimeCost.UnitTime)
                            + ", czas przygotowawczo-zakończeniowy w minutach: " + MachinesHelper.GetTime(removedMachineTimeCost.PCTime)
                            + ", koszt narzędzi: " + MachinesHelper.GetCost(removedMachineTimeCost.ToolCost)
                            + ", pozostałe koszty: " + MachinesHelper.GetCost(removedMachineTimeCost.OtherCost)
                            , user));
            }

            foreach (var newMachineTimeCost in operationAndFilesVm.MachinesTimesCosts)
            {
                var currentMachine = context.Machines.Find(newMachineTimeCost.MachineId);
                if (currentMachine == null)
                {
                    errorVm.Messages.Add("Maszyna o nr: " + newMachineTimeCost.MachineId + " nie istnieje");
                }

                if (newMachineTimeCost.UnitTime < 0 || newMachineTimeCost.PCTime < 0 || newMachineTimeCost.ToolCost < 0 || newMachineTimeCost.OtherCost < 0)
                {
                    if (newMachineTimeCost.UnitTime < 0)
                    {
                        errorVm.Messages.Add("Czas jednostkowy nie może być ujemny");
                    }
                    if (newMachineTimeCost.PCTime < 0)
                    {
                        errorVm.Messages.Add("Czas przygotowawczo-zakończeniowy nie może być ujemny");
                    }
                    if (newMachineTimeCost.ToolCost < 0)
                    {
                        errorVm.Messages.Add("Koszt narzędzi nie może być ujemny");
                    }
                    if (newMachineTimeCost.OtherCost < 0)
                    {
                        errorVm.Messages.Add("Pozostałe koszty nie mogą być ujemne");
                    }
                }

                newMachineTimeCost.MachineUUID = currentMachine.UUID;
                newMachineTimeCost.OperationId = operationDb.Id;
                newMachineTimeCost.OperationUUID = operationDb.UUID;

                operationDb.MachinesTimesCosts.Add(new MachineIdTimeCostClass(newMachineTimeCost));
                operationDb.Changes.Add(new ChangeClass("Dodanie: maszyna-czas-koszt",
                            "Nr maszyny: " + newMachineTimeCost.MachineId
                            + ", kod: " + MachinesHelper.GetMachineCode(newMachineTimeCost.MachineId, context)
                            + ", model: " + MachinesHelper.GetMachineModel(newMachineTimeCost.MachineId, context)
                            + ", czas jednostkowy w minutach: " + MachinesHelper.GetTime(newMachineTimeCost.UnitTime)
                            + ", czas przygotowawczo-zakończeniowy w minutach: " + MachinesHelper.GetTime(newMachineTimeCost.PCTime)
                            + ", koszt narzędzi: " + MachinesHelper.GetCost(newMachineTimeCost.ToolCost)
                            + ", pozostałe koszty: " + MachinesHelper.GetCost(newMachineTimeCost.OtherCost)
                            , user));
            }
        }

        public static void CheckNotesChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            for (int i = operationDb.Notes.Count - 1; i >= 0; i--)
            {
                if (operationDb.Notes[i].Note != operationAndFilesVm.Notes[i].Note)
                {
                    if (String.IsNullOrWhiteSpace(operationAndFilesVm.Notes[i].Note))
                    {
                        operationDb.Changes.Add(new ChangeClass("Usunięcie: uwaga",
                        "Nr: " + SharedHelper.GetNoteId(i) + ", treść: '"
                        + SharedHelper.GetNote(operationDb.Notes[i].Note) + "'"
                        , user));
                        operationDb.Notes.RemoveAt(i);
                        operationAndFilesVm.Notes.RemoveAt(i);
                    }
                    else
                    {
                        operationDb.Changes.Add(new ChangeClass("Edycja: uwaga",
                            "Nr: " + SharedHelper.GetNoteId(i) + ", treść z: '"
                            + SharedHelper.GetNote(operationDb.Notes[i].Note) + "', na: '"
                            + SharedHelper.GetNote(operationAndFilesVm.Notes[i].Note) + "'"
                            , user));
                        operationDb.Notes[i].Note = operationAndFilesVm.Notes[i].Note;
                        operationAndFilesVm.Notes.RemoveAt(i);
                    }
                }
                else
                {
                    operationAndFilesVm.Notes.RemoveAt(i);
                }

            }

            foreach (var newNote in operationAndFilesVm.Notes)
            {
                if(!String.IsNullOrWhiteSpace(newNote.Note))
                {
                    operationDb.Notes.Add(new NoteClass(newNote.Note, user));
                    operationDb.Changes.Add(new ChangeClass("Dodanie: uwaga",
                            "Nr: " + SharedHelper.GetNoteId(operationDb.Notes.IndexOf(operationDb.Notes.Last()))
                            + ", treść: '" + SharedHelper.GetNote(newNote.Note) + "'"
                            , user));
                }
                else
                {
                    errorVm.Messages.Add("Nie można dodać pustej notatki");
                }
            }
        }

        public static void CheckDescChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (operationAndFilesVm.Desc != operationDb.Desc)
            {
                operationDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + operationDb.Desc + ", na: " + operationAndFilesVm.Desc, user));
                operationDb.Desc = operationAndFilesVm.Desc;
            }
        }

        public static void CheckFilesChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            var removedFilesUrls = new HashSet<FileUrlClass>();

            foreach (var fileUrlClass in operationDb.FileUrlObjs)
            {
                if (operationAndFilesVm.FileUrlObjs.Any(a => a.FileName == fileUrlClass.FileName))
                {
                    operationAndFilesVm.FileUrlObjs.RemoveAll(a => a.FileName == fileUrlClass.FileName);
                }
                else
                {
                    removedFilesUrls.Add(fileUrlClass);
                }
            }

            foreach (var removedFileUrl in removedFilesUrls)
            {
                // Delete old file
                var oldFilePath = HttpContext.Current.Server.MapPath(removedFileUrl.FileUrl);
                if (File.Exists(oldFilePath))
                {
                    File.Delete(oldFilePath);
                }

                operationDb.FileUrlObjs.Remove(removedFileUrl);
                operationDb.Changes.Add(new ChangeClass("Usunięcie: plik",
                            "Nazwa: " + removedFileUrl.FileName
                            , user));
            }

            foreach (var addedFile in operationAndFilesVm.Files)
            {
                // remove illegal characters from Operation Name
                string operationIdForPath = operationAndFilesVm.Id.ToString();
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                operationIdForPath = r.Replace(operationIdForPath, "");

                // Save URL in Operation.FileUrl
                string fileName = operationIdForPath + " - " + addedFile.FileName;
                var newFileUrl = new FileUrlClass
                {
                    FileUrl = "/UserFiles/Operations/" + fileName,
                    FileName = fileName,
                    OriginalFileName = addedFile.FileName,
                    FileType = addedFile.ContentType
                };

                var base64StringWithoutHeader = addedFile.Content.Remove(0, addedFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Operations"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);


                operationDb.Changes.Add(new ChangeClass("Dodanie: plik",
                                            "Nazwa: " + addedFile.FileName
                                            , user));

                operationDb.FileUrlObjs.Add(newFileUrl);
            }
        }


        public static void CheckProcessSheetFileChange(
            OperationAndFilesVM operationAndFilesVm,
            Operation operationDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (operationAndFilesVm.ProcessSheetFile.isNew)
            {
                // remove illegal characters from Operation Name
                string operationCodeForPath = operationAndFilesVm.Code;
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                operationCodeForPath = r.Replace(operationCodeForPath, "_");

                // Save URL in Operation.FileUrl
                var splittedFileName = operationAndFilesVm.ProcessSheetFile.FileName.Split('.');
                var fileExtension = "";
                if(splittedFileName.Length > 1)
                {
                    fileExtension = splittedFileName.Last();
                }
                string fileName = "KT_" + operationCodeForPath;
                if(fileExtension != "")
                {
                    fileName += "." + fileExtension;
                }


                var newFileUrlObj = new OperationFileUrlClass
                {
                    FileUrl = "/UserFiles/Operations/" + fileName,
                    FileName = fileName,
                    OriginalFileName = operationAndFilesVm.ProcessSheetFile.FileName,
                    FileType = operationAndFilesVm.ProcessSheetFile.ContentType,
                    FileSize = operationAndFilesVm.ProcessSheetFile.Size
                };

                var base64StringWithoutHeader = operationAndFilesVm.ProcessSheetFile.Content.Remove(0, operationAndFilesVm.ProcessSheetFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Operations"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);



                if(operationDb.ProcessSheetFileUrlObj == null)
                {
                    operationDb.Changes.Add(new ChangeClass("Dodanie: karta technologiczna",
                                            "Nazwa: " + fileName
                                            + " (oryg.: " + operationAndFilesVm.ProcessSheetFile.FileName + ")"
                                            , user));

                    operationDb.ProcessSheetFileUrlObj = new OperationFileUrlClass
                    {
                        FileName = newFileUrlObj.FileName,
                        OriginalFileName = newFileUrlObj.OriginalFileName,
                        FileSize = newFileUrlObj.FileSize,
                        FileType = newFileUrlObj.FileType,
                        FileUrl = newFileUrlObj.FileUrl
                    };
                    context.Entry(operationDb.ProcessSheetFileUrlObj).State = EntityState.Added;
                }
                else
                {
                    operationDb.Changes.Add(new ChangeClass("Edycja: karta technologiczna",
                                            "Z: " + operationDb.ProcessSheetFileUrlObj.FileName
                                            + " (oryg.: " + operationDb.ProcessSheetFileUrlObj.OriginalFileName + ")"
                                            + ", na: " + newFileUrlObj.FileName
                                            + " (oryg.: " + operationAndFilesVm.ProcessSheetFile.FileName + ")"
                                            , user));

                    operationDb.ProcessSheetFileUrlObj.FileName = newFileUrlObj.FileName;
                    operationDb.ProcessSheetFileUrlObj.OriginalFileName = newFileUrlObj.OriginalFileName;
                    operationDb.ProcessSheetFileUrlObj.FileSize = newFileUrlObj.FileSize;
                    operationDb.ProcessSheetFileUrlObj.FileType = newFileUrlObj.FileType;
                    operationDb.ProcessSheetFileUrlObj.FileUrl = newFileUrlObj.FileUrl;
                    context.Entry(operationDb.ProcessSheetFileUrlObj).State = EntityState.Modified;
                }
            }
        }

    }
}