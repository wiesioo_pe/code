﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class SubjectsHelper
    {
        public static string GetSubjectName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(brak nazwy)";
        }

        public static string GetSubjectName(int? subjectId, ApplicationDataDbContext context)
        {
            var subject = context.Subjects.Find(subjectId);
            if (subject != null)
            {
                return subject.Name;
            }

            return "(nie znaleziono odbiorcy nr: " + subjectId + ")";
        }

        public static string GetSubjectShortName(int subjectId, ApplicationDataDbContext context)
        {
            var subject = context.Subjects.Find(subjectId);
            if (subject != null)
            {
                return subject.ShortName;
            }

            return "(nie znaleziono odbiorcy nr: " + subjectId + ")";
        }

        public static string GetSubjectDesc(int subjectId, ApplicationDataDbContext context)
        {
            var subject = context.Subjects.Find(subjectId);
            if (subject != null)
            {
                return subject.Desc;
            }

            return "(nie znaleziono odbiorcy nr: " + subjectId + ")";
        }

        public static string GetContactName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(brak imienia i nazwiska)";
        }

        public static string GetContactName(int contactId, ApplicationDataDbContext context)
        {
            var contact = context.Contacts.Find(contactId);
            if(contact != null)
            {
                return GetContactName(contact.Name);
            }

            return "(nie znaleziono osoby do kontaktu nr: " + contactId + ")";
        }

        public static List<Contact> GetAllContacts(ApplicationDataDbContext context)
        {
            var allContactsList = context.Contacts.ToList();
            return allContactsList;
        }

        public static List<SelectListItem> GetAllContactsSelectList(ApplicationDataDbContext context)
        {
            var allContactsSelectList = new List<SelectListItem>
            {
                new SelectListItem {Text = @"...", Value = null}
            };

            foreach (var item in context.Contacts.ToList())
            {
                allContactsSelectList.Add(new SelectListItem
                { Text = SubjectsHelper.GetContactName(item.Id, context),
                  Value = item.Id.ToString() });
            }

            return allContactsSelectList;
        }

        public static List<Subject> GetAllSubjects(ApplicationDataDbContext context)
        {
            var allSubjectsList = context.Subjects.ToList();
            return allSubjectsList;
        }

        public static List<SelectListItem> GetAllSubjectsSelectList(ApplicationDataDbContext context)
        {
            var allSubjectsSelectList = new List<SelectListItem> {new SelectListItem {Text = @"...", Value = null}};
            foreach (var item in context.Subjects.ToList())
            {
                allSubjectsSelectList.Add(new SelectListItem
                {
                    Text = SubjectsHelper.GetSubjectName(item.Id, context),
                    Value = item.Id.ToString()
                });
            }

            return allSubjectsSelectList;
        }
    }
}