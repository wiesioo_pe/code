﻿using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ContactsHelper
    {
        public static string GetContactName(int? contactId, ApplicationDataDbContext context)
        {
            var contact = context.Contacts.Find(contactId);
            if (contact != null)
            {
                return contact.Name;
            }

            return "(brak kontaktu:" + contactId + ")";
        }

        public static string GetContactDivision(int? contactId, ApplicationDataDbContext context)
        {
            var contact = context.Contacts.Find(contactId);
            if (contact != null)
            {
                return contact.Division;
            }

            return "(brak kontaktu:" + contactId + ")";
        }

        public static string GetContactPosition(int? contactId, ApplicationDataDbContext context)
        {
            var contact = context.Contacts.Find(contactId);
            if (contact != null)
            {
                return contact.Position;
            }

            return "(brak kontaktu:" + contactId + ")";
        }
    }
}