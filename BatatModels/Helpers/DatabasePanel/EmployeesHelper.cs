﻿using System;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class EmployeesHelper
    {
        public static string GetEmployeeName(string employeeId, ApplicationDataDbContext context)
        {
            if (String.IsNullOrWhiteSpace(employeeId))
            {
                return "(nie ustawiono użytkownika)";
            }

            var user = context.Users.Find(employeeId);
            if (user != null)
            {
                return user.UserName;
            }

            return "(brak użytkownika:" + employeeId + ")";
        }
    }
}