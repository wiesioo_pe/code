﻿using System;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class MachinesHelper
    {
        public static ChangeClass NewChange(string name, string desc, string user)
        {
            return new ChangeClass()
            {
                Name = name,
                EmployeeUUID = user,
                Desc = desc,
                DateTime = DateTime.Now
            };
        }

        public static string GetMachineTypeName(int? machineTypeId, ApplicationDataDbContext context)
        {
            var machineType = context.MachineTypes.Find(machineTypeId);
            if (machineType == null)
            {
                return "(Brak typu maszyny nr: " + machineTypeId + ")";
            }

            return machineType.Name;
        }

        public static string GetMachineCode(string code)
        {
            return !String.IsNullOrWhiteSpace(code) ? code : "(brak kodu)";
        }

        public static string GetMachineCode(int? machineId, ApplicationDataDbContext context)
        {
            var machine = context.Machines.Find(machineId);
            if (machine != null)
            {
                return GetMachineCode(machine.Code);
            }

            return "(Brak maszyny nr: " + machineId + ")";
        }

        public static string GetMachineType(string type)
        {
            return !String.IsNullOrWhiteSpace(type) ? type : "(brak typu)";
        }

        public static string GetMachineModel(string model)
        {
            return !String.IsNullOrWhiteSpace(model) ? model : "(brak modelu)";
        }

        public static string GetMachineModel(int? machineId, ApplicationDataDbContext context)
        {
            var machine = context.Machines.Find(machineId);
            if (machine != null)
            {
                return GetMachineModel(machine.Model);
            }

            return "(Brak maszyny nr: " + machineId + ")";
        }

        public static string GetMachineManufacturer(string manufacturer)
        {
            return !String.IsNullOrWhiteSpace(manufacturer) ? manufacturer : "(brak producenta)";
        }

        public static string GetMachineId(int? machineId, ApplicationDataDbContext context)
        {
            var machine = context.Machines.Find(machineId);
            if (machine != null)
            {
                return machineId.ToString();
            }

            return "(Brak maszyny nr: " + machineId + ")";
        }

        public static string GetTime(decimal? minutes)
        {
            return minutes != null ? $"{minutes:N1}%" : "(nie ustawiono)";
        }

        public static string GetCost(decimal? cost)
        {
            return cost != null ? $"{cost:N2}" : "(nie ustawiono)";
        }
    }
}