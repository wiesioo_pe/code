﻿using System;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class OperationsTypesHelper
    {

        public static string GetOperationTypeName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(brak nazwy)";
        }

        public static string GetOperationTypeName(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return "(nie ustawiono typu operacji)";
            }
            var type = context.OperationTypes.Find(id);
            if (type != null)
            {
                return GetOperationTypeName(type.Name);
            }

            return "(brak typu operacji:" + id + ")";
        }


        public static string GetOperationTypeCode(string code)
        {
            return !String.IsNullOrWhiteSpace(code) ? code : "(brak kodu)";
        }

        public static string GetOperationTypeCode(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return "(nie ustawiono typu operacji)";
            }
            var type = context.OperationTypes.Find(id);
            if (type != null)
            {
                return GetOperationTypeCode(type.Code);
            }

            return "(brak typu operacji:" + id + ")";
        }

        public static string GetOperationTypeDescription(string desc)
        {
            return !String.IsNullOrWhiteSpace(desc) ? desc : "(brak opisu)";
        }

        public static string GetOperationTypeDescription(int id, ApplicationDataDbContext context)
        {
            if (id == 0)
            {
                return "(nie ustawiono typu operacji)";
            }
            var type = context.OperationTypes.Find(id);
            if (type != null)
            {
                return GetOperationTypeDescription(type.Desc);
            }

            return "(brak typu operacji:" + id + ")";
        }

    }
}