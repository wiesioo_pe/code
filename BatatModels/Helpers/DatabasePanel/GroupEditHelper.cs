﻿using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class GroupEditHelper
    {
        public static void CheckIfGroupExists(ref Group groupDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (groupDb == null)
            {
                groupDb = new Group();
                groupDb.Changes.Add(new ChangeClass("Utworzenie: grupa", "Nowa grupa utworzona", user));
            }
        }

        public static void CheckNameChange(GroupVM groupVm, Group groupDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (groupVm.Name != groupDb.Name)
            {
                groupDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + groupDb.Name + ", na: " + groupVm.Name, user));
                groupDb.Name = groupVm.Name;
            }
        }

        public static void CheckCodeChange(GroupVM groupVm, Group groupDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (groupVm.Code != groupDb.Code)
            {
                groupDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + groupDb.Code + ", na: " + groupVm.Code, user));
                groupDb.Code = groupVm.Code;
            }
        }

        public static void CheckDescChange(GroupVM groupVm, Group groupDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (groupVm.Desc != groupDb.Desc)
            {
                groupDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + groupDb.Desc + ", na: " + groupVm.Desc, user));
                groupDb.Desc = groupVm.Desc;
            }
        }
    }
}