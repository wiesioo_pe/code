﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public static class OrdersHelper
    {
        public static void RefreshOrdersLists(ApplicationDataDbContext context)
        {
            var ordersDisplayList = new List<Order>();
            if (context.Orders.Any())
            {
                ordersDisplayList = context.Orders.Where(item => item.StateId < OrdersLists.State.Count - 1).OrderBy(i => i.StateId).ThenBy(i => i.DeliveryDate).ToList();
            }
            System.Web.HttpContext.Current.Session["OrdersDisplayList"] = ordersDisplayList;
        }

        public static void RefreshOrderOnLists(Order order)
        {
            var ordersDisplayList = (List<Order>)System.Web.HttpContext.Current.Session["OrdersDisplayList"];

            if (ordersDisplayList.Any(item => item.Id == order.Id))
            {
                var order2 = ordersDisplayList.Single(item => item.Id == order.Id);
                ordersDisplayList[ordersDisplayList.IndexOf(order2)] = order;
            }
            else
            {
                ordersDisplayList.Add(order);
            }
        }

        public static string GetOrderPriorityName(int? priorityId)
        {
            if(priorityId == null)
            {
                return "";
            }

            if (priorityId >= 0 && priorityId < OrdersLists.Priority.Count)
            {
                return OrdersLists.Priority[priorityId ?? 0];
            }

            return "(błąd)";
        }

        public static string GetSubjectName(int? subjectId, ApplicationDataDbContext context)
        {
            if (subjectId == null)
            {
                return "";
            }

            if (context.Subjects.Any(i => i.Id == subjectId))
            {
                return context.Subjects.Single(i => i.Id == subjectId).Name;
            }

            return "(błąd)";
        }

        public static string GetContactName(int? contactId, ApplicationDataDbContext context)
        {
            if (contactId == null)
            {
                return "";
            }

            if (context.Contacts.Any(i => i.Id == contactId))
            {
                return context.Contacts.Single(i => i.Id == contactId).Name;
            }

            return "(błąd)";
        }

        public static string GetDate(DateTime? data)
        {
            return data?.Date.ToString("dd-MM-yyyy") ?? "";
        }

        public static string GetProductName(int? productId, ApplicationDataDbContext context)
        {
            if (productId == null)
            {
                return "";
            }

            if (context.Products.Any(i => i.Id == productId))
            {
                return context.Products.Single(i => i.Id == productId).Name;
            }

            return "(błąd)";
        }

        public static string GetDescription(string desc)
        {
            if (desc == null)
            {
                return "(nie ustawiono)";
            }

            return desc != String.Empty ? desc : "(pusty opis)";
        }

        public static string GetCode(string kod)
        {
            if (kod == null)
            {
                return "";
            }

            return kod != String.Empty ? kod : "(pusty kod)";
        }

        public static string GetName(string name)
        {
            if (name == null)
            {
                return "";
            }

            return name != String.Empty ? name : "";
        }

        public static string GetParameterName(int? parameterId, ApplicationDataDbContext context)
        {
            if (parameterId == null)
            {
                return "";
            }

            return context.Parameters.Any(i => i.Id == parameterId) ? context.Parameters.Single(i => i.Id == parameterId).Name : "(błąd)";
        }


        public static string GetStateName(int? stateId)
        {
            if (stateId == null)
            {
                return "";
            }

            if (stateId >= 0 && stateId < OrdersLists.State.Count)
            {
                return OrdersLists.State[stateId ?? 0];
            }

            return "(błąd)";
        }

        public static string GetComplaintIsTotalString(bool? isTotal)
        {
            return isTotal == true ? "całkowita" : "częściowa";
        }


        public static bool IsProductOnLists(int productIdToCheck, int productIdToFilter, ApplicationDataDbContext context)
        {
            if (!context.Products.Any())
                return false;

            if (productIdToCheck == productIdToFilter)
            {
                return true;
            }

            if (!context.Products.Any(k => k.Id == productIdToCheck))
                return false;

            Product produktDoSprawdzenia = context.Products.Single(k => k.Id == productIdToCheck);
            if (produktDoSprawdzenia.Products != null && produktDoSprawdzenia.Products.Count > 0)
            {

                foreach (ProductIdAmountDiscountClass idAmount in produktDoSprawdzenia.Products)
                {
                    if (IsProductOnLists(idAmount.ProductId, productIdToFilter, context))
                    {
                        return true;
                    }
                }

                return false;
            }

            return false;
        }

        public static bool IsPartOnLists(int productIdToCheck, int partIdToFilter, ApplicationDataDbContext context)
        {
            if (!context.Products.Any())
                return false;

            if (!context.Products.Any(k => k.Id == productIdToCheck))
                return false;

            Product produktDoSprawdzenia =
                context.Products
                    .Include("Products")
                    .Include("Parts").Single(k => k.Id == productIdToCheck);

            if (produktDoSprawdzenia.Parts != null && produktDoSprawdzenia.Parts.Count > 0)
            {
                foreach (PartIdAmountClass idAmount in produktDoSprawdzenia.Parts)
                {
                    if (idAmount.PartId == partIdToFilter)
                    {
                        return true;
                    }
                }
            }


            if (produktDoSprawdzenia.Products == null || produktDoSprawdzenia.Products.Count <= 0)
                return false;

            foreach (ProductIdAmountDiscountClass idAmount in produktDoSprawdzenia.Products)
            {
                if (IsPartOnLists(idAmount.ProductId, partIdToFilter, context))
                {
                    return true;
                }
            }

            return false;
        }

    }
}

