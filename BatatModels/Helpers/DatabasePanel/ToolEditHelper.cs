﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ToolEditHelper
    {
        public static void CheckIfToolExists(ref Tool toolDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (toolDb == null)
            {
                toolDb = new Tool();
                toolDb.Changes.Add(new ChangeClass("Utworzenie: narzędzie", "Nowe narzędzie utworzone", user));
            }
        }

        public static void CheckGroupsChange(
            ToolVM toolVm,
            Tool toolDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var groupsIdsDbCopy = new HashSet<GroupIdClass>(toolDb.Groups);
            if (toolVm.Groups == null) { toolVm.Groups = new List<GroupIdClassVM>(); }

            foreach (var groupIdVm in toolVm.Groups)
            {
                if (groupsIdsDbCopy.Any(a => a.GroupId == groupIdVm.GroupId))
                {
                    groupsIdsDbCopy.RemoveWhere(b => b.GroupId == groupIdVm.GroupId);
                }
                else
                {
                    toolDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                        "Id grupy: " + groupIdVm.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdVm.GroupId, context), user));
                    toolDb.Groups.Add(new GroupIdClass(groupIdVm.GroupId, context.Groups.Find(groupIdVm.GroupId)?.UUID));
                }
            }
            foreach (var groupIdDbCopy in groupsIdsDbCopy)
            {
                toolDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                        "Id grupy: " + groupIdDbCopy.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdDbCopy.GroupId, context), user));
                toolDb.Groups.RemoveWhere(c => c.GroupId == groupIdDbCopy.GroupId);
            }
        }

        public static void CheckNameChange(ToolVM toolVm, Tool toolDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (toolVm.Name != toolDb.Name)
            {
                toolDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + toolDb.Name + ", na: " + toolVm.Name, user));
                toolDb.Name = toolVm.Name;
            }
        }

        public static void CheckCodeChange(ToolVM toolVm, Tool toolDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (toolVm.Code != toolDb.Code)
            {
                toolDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + toolDb.Code + ", na: " + toolVm.Code, user));
                toolDb.Code = toolVm.Code;
            }
        }

        public static void CheckDescChange(ToolVM toolVm, Tool toolDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (toolVm.Desc != toolDb.Desc)
            {
                toolDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + toolDb.Desc + ", na: " + toolVm.Desc, user));
                toolDb.Desc = toolVm.Desc;
            }
        }

        public static void CheckToolTypeIdChange(
            ToolVM toolVm,
            Tool toolDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (toolVm.ToolTypeId != toolDb.ToolTypeId)
            {
                toolDb.Changes.Add(new ChangeClass("Edycja: typ",
                        "Z: " + toolDb.ToolTypeId + " - " + ToolsHelper.GetToolTypeName(toolDb.ToolTypeId, context) 
                        + ", na: " + toolVm.ToolTypeId + " - " + ToolsHelper.GetToolTypeName(toolVm.ToolTypeId, context), user));
                toolDb.ToolTypeId = toolVm.ToolTypeId;
            }
        }

        public static void CheckParametersChange(
            ToolVM toolVm,
            Tool toolDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            for (int i = toolDb.Parameters.Count - 1; i >= 0; i--)
            {
                var oldParameterValue = toolDb.Parameters[i];
                var newParameterValue = toolVm.Parameters[i];
                if (oldParameterValue.Value != newParameterValue.Value)
                {
                    if (String.IsNullOrWhiteSpace(newParameterValue.Value))
                    {
                        toolDb.Changes.Add(new ChangeClass("Usunięcie: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(oldParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(oldParameterValue.ParameterId, context)
                        + ", wartość: " + ParametersHelper.GetParameterValue(oldParameterValue.Value)
                        , user));
                        toolDb.Parameters.RemoveAt(i);
                        toolVm.Parameters.RemoveAt(i);
                    }
                    else
                    {
                        toolDb.Changes.Add(new ChangeClass("Edycja: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(oldParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(oldParameterValue.ParameterId, context)
                        + ", wartość z: " + ParametersHelper.GetParameterValue(oldParameterValue.Value)
                        + ", na: " + ParametersHelper.GetParameterValue(newParameterValue.Value)
                        , user));
                        oldParameterValue.Value = newParameterValue.Value;
                        toolVm.Parameters.RemoveAt(i);
                    }
                }
                else
                {
                    toolVm.Parameters.RemoveAt(i);
                }
            }

            foreach (var newParameterValue in toolVm.Parameters)
            {
                toolDb.Parameters.Add(new ParameterIdValueClass(newParameterValue));
                toolDb.Changes.Add(new ChangeClass("Dodanie: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(newParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(newParameterValue.ParameterId, context)
                        + ", wartość: " + ParametersHelper.GetParameterValue(newParameterValue.Value)
                        , user));
            }
        }


        public static void CheckUnitCostChange(ToolVM toolVm, Tool toolDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (toolVm.UnitCost != toolDb.UnitCost)
            {
                toolDb.Changes.Add(new ChangeClass("Edycja: koszt jednostki",
                        "Z: " + toolDb.UnitCost + ", na: " + toolVm.UnitCost, user));
                toolDb.UnitCost = toolVm.UnitCost ?? 0;
            }
        }

    }
}