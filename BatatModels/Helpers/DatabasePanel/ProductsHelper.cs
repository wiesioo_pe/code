﻿using System;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ProductsHelper
    {
        public static string GetProductId(int productId, ApplicationDataDbContext context)
        {
            var product = context.Products.Find(productId);
            if (product != null)
            {
                return productId.ToString();
            }

            return "(nie znaleziono produktu nr: " + productId + ")";

        }

        public static string GetProductCode(string code)
        {
            return !String.IsNullOrWhiteSpace(code) ? code : "(brak kodu)";
        }

        public static string GetProductCode(int productId, ApplicationDataDbContext context)
        {
            var product = context.Products.Find(productId);
            if (product != null)
            {
                return GetProductCode(product.Code);
            }

            return "(nie znaleziono produktu nr: " + productId + ")";

        }

        public static string GetProductName(string productName)
        {
            return !String.IsNullOrWhiteSpace(productName) ? productName : "(brak nazwy)";
        }

        public static string GetProductName(int productId, ApplicationDataDbContext context)
        {
            var product = context.Products.Find(productId);
            if (product != null)
            {
                return GetProductName(product.Name);
            }

            return "(nie znaleziono produktu nr: " + productId + ")";

        }

        public static string GetProductDescription(string desc)
        {
            return !String.IsNullOrWhiteSpace(desc) ? desc : "(brak opisu)";
        }

        public static string GetProductDescription(int productId, ApplicationDataDbContext context)
        {
            var product = context.Products.Find(productId);
            if (product != null)
            {
                return GetProductDescription(product.Desc);
            }

            return "(nie znaleziono produktu nr: " + productId + ")";
        }

        public static string GetAmount(int? amount)
        {
            if(amount != null)
            {
                return amount.Value.ToString("0") + "szt";
            }

            return null;
        }

        public static string GetDiscount(decimal? discount)
        {
            if (discount != null)
            {
                return $"{discount.Value:N2}" + "%";
            }

            return null;
        }
    }
}