﻿using System;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public partial class SharedHelper
    {

        public static string GetUserName(string userId, ApplicationDataDbContext context)
        {
            var user = context.Users.Find(userId);

            if (user != null)
            {
                return user.UserName;
            }

            return "(brak użytkownika nr: " + userId + ")";
        }

        public static string GetNote(string uwaga)
        {
            return !String.IsNullOrWhiteSpace(uwaga) ? uwaga : "(pusta uwaga)";
        }

        public static string GetNoteId(int noteNumber)
        {
            return noteNumber >= 0 ? (noteNumber + 1).ToString() : "(błąd)";
        }

        public static string GetDate(DateTime? data)
        {
            return data?.Date.ToString("yyyy-MM-dd") ?? "";
        }

        public static string GetTime(DateTime? data)
        {
            return data?.ToString("yyyy-MM-ddTHH:mm:ss") ?? "";
        }

        public static string GetDate(DateTime data)
        {
            return data.Date.ToString("yyyy-MM-dd");
        }

        public static string GetTime(DateTime data)
        {
            return data.ToString("yyyy-MM-ddTHH:mm:ss");
        }


        public static string NormalizeString(string nullOrEmptyString)
        {
            if(String.IsNullOrWhiteSpace(nullOrEmptyString))
            {
                nullOrEmptyString = null;
            }

            return nullOrEmptyString;
        }
    }
}