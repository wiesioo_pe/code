﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class PartEditHelper
    {
        public static void CheckIfPartExists(ref Part partDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (partDb == null)
            {
                partDb = new Part();
                partDb.Changes.Add(new ChangeClass("Utworzenie: detal", "Nowy detal utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            PartAndFilesVM partAndFilesVm,
            Part partDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedGroups = new HashSet<GroupIdClass>();
            foreach (var groupDb in partDb.Groups)
            {
                if (partAndFilesVm.Groups.Any(a => a.GroupId == groupDb.GroupId))
                {
                    partAndFilesVm.Groups.RemoveAll(a => a.GroupId == groupDb.GroupId);
                }
                else
                {
                    removedGroups.Add(groupDb);
                }
            }
            foreach (var removedGroup in removedGroups)
            {
                partDb.Groups.Remove(removedGroup);
                partDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                            "Nr: " + removedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(removedGroup.GroupId, context)
                            , user));
            }
            foreach (var addedGroup in partAndFilesVm.Groups)
            {
                if (context.Groups.Any(a => a.Id == addedGroup.GroupId))
                {
                    partDb.Groups.Add(new GroupIdClass(addedGroup.GroupId, context.Groups.Find(addedGroup.GroupId)?.UUID));
                    partDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                            "Nr: " + addedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(addedGroup.GroupId, context)
                            , user));
                }
                else
                {
                    errorVm.Messages.Add("Grupa nr: " + addedGroup + " nie istnieje");
                }
            }
        }

        public static void CheckNameChange(PartAndFilesVM partAndFilesVm, Part partDb, ApplicationUser user, ErrorVM errorVm)
        {
            partAndFilesVm.Name = SharedHelper.NormalizeString(partAndFilesVm.Name);
            partDb.Name = SharedHelper.NormalizeString(partDb.Name);

            if (partAndFilesVm.Name != null)
            {
                if (partAndFilesVm.Name == partDb.Name)
                    return;

                if (partDb.Name != null)
                {
                    partDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + partDb.Name + ", na: " + partAndFilesVm.Name, user));
                    partDb.Name = partAndFilesVm.Name;
                }
                else
                {
                    partDb.Changes.Add(new ChangeClass("Ustawienie: nazwa",
                        partAndFilesVm.Name, user));
                    partDb.Name = partAndFilesVm.Name;
                }
            }
            else
            {
                errorVm.Messages.Add("Nazwa detalu nie może być pusta");
            }
        }

        public static void CheckCodeChange(PartAndFilesVM partAndFilesVm, Part partDb, ApplicationUser user, ErrorVM errorVm)
        {
            partAndFilesVm.Code = SharedHelper.NormalizeString(partAndFilesVm.Code);
            partDb.Code = SharedHelper.NormalizeString(partDb.Code);

            if (partAndFilesVm.Code != null)
            {
                if (partAndFilesVm.Code == partDb.Code)
                    return;

                if (partDb.Code != null)
                {
                    partDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + partDb.Code + ", na: " + partAndFilesVm.Code, user));
                    partDb.Code = partAndFilesVm.Code;
                }
                else
                {
                    partDb.Changes.Add(new ChangeClass("Ustawienie: kod",
                        partAndFilesVm.Code, user));
                    partDb.Code = partAndFilesVm.Code;
                }
            }
            else
            {
                errorVm.Messages.Add("Kod detalu nie może być pusty");
            }
        }

        public static void CheckDescChange(PartAndFilesVM partAndFilesVm, Part partDb, ApplicationUser user, ErrorVM errorVm)
        {
            partAndFilesVm.Desc = SharedHelper.NormalizeString(partAndFilesVm.Desc);
            partDb.Desc = SharedHelper.NormalizeString(partDb.Desc);

            if (partAndFilesVm.Desc == partDb.Desc)
                return;

            if (partDb.Desc != null)
            {
                partDb.Changes.Add(new ChangeClass("Edycja: opis",
                    "Z: " + partDb.Desc + ", na: " + partAndFilesVm.Desc, user));
                partDb.Desc = partAndFilesVm.Desc;
            }
            else
            {
                partDb.Changes.Add(new ChangeClass("Ustawienie: opis",
                    partAndFilesVm.Desc, user));
                partDb.Desc = partAndFilesVm.Desc;
            }
        }

        public static void CheckParametersChange(
            PartAndFilesVM partAndFilesVm,
            Part partDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedParameters = new HashSet<ParameterIdValueClass>();
            foreach (var parameterDb in partDb.Parameters)
            {
                if (partAndFilesVm.Parameters.Any(a => a.ParameterId == parameterDb.ParameterId))
                {
                    var parameterVm = partAndFilesVm.Parameters.First(a => a.ParameterId == parameterDb.ParameterId);

                    parameterVm.Value = SharedHelper.NormalizeString(parameterVm.Value);
                    parameterDb.Value = SharedHelper.NormalizeString(parameterDb.Value);

                    if (parameterVm.Value != null)
                    {
                        if (parameterVm.Value != parameterDb.Value)
                        {
                            if (parameterDb.Value == null)
                            {
                                partDb.Changes.Add(new ChangeClass("Ustawienie: wartość parameteru",
                                    "Nr: " + parameterDb.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(parameterDb.ParameterId, context)
                                    + ", wartość: " + parameterVm.Value
                                    , user));
                                parameterDb.Value = parameterVm.Value;
                            }
                            else
                            {
                                partDb.Changes.Add(new ChangeClass("Edycja: wartość parameteru",
                                    "Nr: " + parameterDb.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(parameterDb.ParameterId, context)
                                    + ", wartość z: " + parameterDb.Value
                                    + ", na: " + parameterVm.Value
                                    , user));
                                parameterDb.Value = parameterVm.Value;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Wartość parametru nie może być pusta");
                    }
                    partAndFilesVm.Parameters.RemoveAll(a => a.ParameterId == parameterDb.ParameterId);
                }
                else
                {
                    removedParameters.Add(parameterDb);
                }
            }
            foreach (var removedParameter in removedParameters)
            {
                partDb.Parameters.Remove(removedParameter);
                partDb.Changes.Add(new ChangeClass("Usunięcie: parametr",
                            "Nr: " + removedParameter.ParameterId
                            + ", nazwa: " + ParametersHelper.GetParameterName(removedParameter.ParameterId, context)
                            + ", wartość: " + removedParameter.Value
                            , user));
            }
            foreach (var addedParameter in partAndFilesVm.Parameters)
            {
                addedParameter.Value = SharedHelper.NormalizeString(addedParameter.Value);

                if (context.Parameters.Any(a => a.Id == addedParameter.ParameterId))
                {
                    if (addedParameter.Value != null)
                    {
                        partDb.Parameters.Add(new ParameterIdValueClass(addedParameter.ParameterId, context.Parameters.Find(addedParameter.ParameterId)?.UUID, addedParameter.Value));
                        partDb.Changes.Add(new ChangeClass("Dodanie: parametr",
                                    "Nr: " + addedParameter.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(addedParameter.ParameterId, context)
                                    + ", wartość: " + addedParameter.Value
                                    , user));
                    }
                    else if (addedParameter.Value == null)
                    {
                        errorVm.Messages.Add("Wartość parametru nie może być pusta");
                    }
                }
                else
                {
                    errorVm.Messages.Add("Parametr o nr: " + addedParameter.ParameterId + " nie istnieje");
                }
            }
        }

        public static void CheckFilesChange(
            PartAndFilesVM partAndFilesVm,
            Part partDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedFilesUrls = new HashSet<FileUrlClass>();

            foreach (var fileUrlClass in partDb.FileUrlObjs)
            {
                if (partAndFilesVm.FileUrlObjs.Any(a => a.FileName == fileUrlClass.FileName))
                {
                    partAndFilesVm.FileUrlObjs.RemoveAll(a => a.FileName == fileUrlClass.FileName);
                }
                else
                {
                    removedFilesUrls.Add(fileUrlClass);
                }
            }

            foreach (var removedFileUrl in removedFilesUrls)
            {
                // Delete old file
                var oldFilePath = HttpContext.Current.Server.MapPath(removedFileUrl.FileUrl);
                if (File.Exists(oldFilePath))
                {
                    File.Delete(oldFilePath);
                }

                partDb.FileUrlObjs.Remove(removedFileUrl);
                partDb.Changes.Add(new ChangeClass("Usunięcie: plik",
                            "Nazwa: " + removedFileUrl.FileName
                            , user));
                context.Entry(removedFileUrl).State = EntityState.Deleted;
            }

            foreach (var addedFile in partAndFilesVm.Files)
            {
                // remove illegal characters from Part Name
                string partCodeForPath = partAndFilesVm.Code;
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                partCodeForPath = r.Replace(partCodeForPath, "");

                // Save URL in Part.FileUrl
                string fileName = partCodeForPath + "_" + addedFile.FileName;
                var newFileUrl = new FileUrlClass
                {
                    FileUrl = "/UserFiles/Parts/" + fileName,
                    FileName = fileName,
                    OriginalFileName = addedFile.FileName,
                    FileType = addedFile.ContentType
                };

                var base64StringWithoutHeader = addedFile.Content.Remove(0, addedFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Parts"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);


                partDb.Changes.Add(new ChangeClass("Dodanie: plik",
                                            "Nazwa: " + addedFile.FileName
                                            , user));

                partDb.FileUrlObjs.Add(newFileUrl);
                context.Entry(newFileUrl).State = EntityState.Added;
            }
        }

    }
}