﻿using System;
using System.Collections.Generic;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class MachineEditHelper
    {
        public static void CheckIfMachineExists(ref Machine machineDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (machineDb == null)
            {
                machineDb = new Machine();
                machineDb.Changes.Add(new ChangeClass("Utworzenie: maszyna", "Nowa maszyna utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var groupsIdsDbCopy = new HashSet<GroupIdClass>(machineDb.Groups);
            if (machineVm.Groups == null) { machineVm.Groups = new List<GroupIdClassVM>(); }

            foreach (var groupIdVm in machineVm.Groups)
            {
                if (groupsIdsDbCopy.Any(a => a.GroupId == groupIdVm.GroupId))
                {
                    groupsIdsDbCopy.RemoveWhere(b => b.GroupId == groupIdVm.GroupId);
                }
                else
                {
                    machineDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                        "Id grupy: " + groupIdVm.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdVm.GroupId, context), user));
                    machineDb.Groups.Add(new GroupIdClass(groupIdVm.GroupId, context.Groups.Find(groupIdVm.GroupId)?.UUID));
                }
            }
            foreach (var groupIdDbCopy in groupsIdsDbCopy)
            {
                machineDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                        "Id grupy: " + groupIdDbCopy.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdDbCopy.GroupId, context), user));
                machineDb.Groups.RemoveWhere(c => c.GroupId == groupIdDbCopy.GroupId);
            }
        }

        public static void CheckCodeChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (machineVm.Code != machineDb.Code)
            {
                machineDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + machineDb.Code + ", na: " + machineVm.Code, user));
                machineDb.Code = machineVm.Code;
            }
        }

        public static void CheckMachineTypeIdChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (machineVm.MachineTypeId != machineDb.MachineTypeId)
            {
                machineDb.Changes.Add(new ChangeClass("Edycja: typ",
                        "Z: " + machineDb.MachineTypeId + " - " + MachinesHelper.GetMachineTypeName(machineDb.MachineTypeId, context) 
                        + ", na: " + machineVm.MachineTypeId + " - " + MachinesHelper.GetMachineTypeName(machineVm.MachineTypeId, context), user));
                machineDb.MachineTypeId = machineVm.MachineTypeId;
            }
        }

        public static void CheckModelChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (machineVm.Model != machineDb.Model)
            {
                machineDb.Changes.Add(new ChangeClass("Edycja: model",
                        "Z: " + machineDb.Model + ", na: " + machineVm.Model, user));
                machineDb.Model = machineVm.Model;
            }
        }

        public static void CheckManufacturerChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (machineVm.Manufacturer != machineDb.Manufacturer)
            {
                machineDb.Changes.Add(new ChangeClass("Edycja: producent",
                        "Z: " + machineDb.Manufacturer + ", na: " + machineVm.Manufacturer, user));
                machineDb.Manufacturer = machineVm.Manufacturer;
            }
        }

        public static void CheckNotesChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            for (int i = machineDb.Notes.Count - 1; i >= 0; i--)
            {
                if (machineDb.Notes[i].Note != machineVm.Notes[i].Note)
                {
                    if (String.IsNullOrWhiteSpace(machineVm.Notes[i].Note))
                    {
                        machineDb.Changes.Add(new ChangeClass("Usunięcie: uwaga",
                                "Nr: " + SharedHelper.GetNoteId(i) + ", treść: '"
                                + SharedHelper.GetNote(machineDb.Notes[i].Note) + "'"
                                , user));
                        machineDb.Notes.RemoveAt(i);
                        machineVm.Notes.RemoveAt(i);
                    }
                    else
                    {
                        machineDb.Changes.Add(new ChangeClass("Edycja: uwaga",
                            "Nr: " + SharedHelper.GetNoteId(i) + ", treść z: '"
                            + SharedHelper.GetNote(machineDb.Notes[i].Note) + "', na: '"
                            + SharedHelper.GetNote(machineVm.Notes[i].Note) + "'"
                            , user));
                        machineDb.Notes[i].Note = machineVm.Notes[i].Note;
                        machineVm.Notes.RemoveAt(i);
                    }
                }
                else
                {
                    machineVm.Notes.RemoveAt(i);
                }

            }

            foreach (var newNote in machineVm.Notes)
            {
                machineDb.Notes.Add(new NoteClass(newNote.Note, user));
                machineDb.Changes.Add(new ChangeClass("Dodanie: uwaga",
                        "Nr: " + SharedHelper.GetNoteId(machineDb.Notes.IndexOf(machineDb.Notes.Last()))
                        + ", treść: '" + SharedHelper.GetNote(newNote.Note) + "'"
                        , user));
            }
        }

        public static void CheckParametersChange(
            MachineVM machineVm,
            Machine machineDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            for (int i = machineDb.Parameters.Count - 1; i >= 0; i--)
            {
                var oldParameterValue = machineDb.Parameters[i];
                var newParameterValue = machineVm.Parameters[i];
                if (oldParameterValue.Value != newParameterValue.Value)
                {
                    if (String.IsNullOrWhiteSpace(newParameterValue.Value))
                    {
                        machineDb.Changes.Add(new ChangeClass("Usunięcie: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(oldParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(oldParameterValue.ParameterId, context)
                        + ", wartość: " + ParametersHelper.GetParameterValue(oldParameterValue.Value)
                        , user));
                        machineDb.Parameters.RemoveAt(i);
                        machineVm.Parameters.RemoveAt(i);
                    }
                    else
                    {
                        machineDb.Changes.Add(new ChangeClass("Edycja: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(oldParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(oldParameterValue.ParameterId, context)
                        + ", wartość z: " + ParametersHelper.GetParameterValue(oldParameterValue.Value)
                        + ", na: " + ParametersHelper.GetParameterValue(newParameterValue.Value)
                        , user));
                        oldParameterValue.Value = newParameterValue.Value;
                        machineVm.Parameters.RemoveAt(i);
                    }
                }
                else
                {
                    machineVm.Parameters.RemoveAt(i);
                }
            }

            foreach (var newParameterValue in machineVm.Parameters)
            {
                machineDb.Parameters.Add(new ParameterIdValueClass(newParameterValue));
                machineDb.Changes.Add(new ChangeClass("Dodanie: Parameter - wartość",
                        "Nr: " + ParametersHelper.GetParameterId(newParameterValue.ParameterId)
                        + ", Name: " + ParametersHelper.GetParameterName(newParameterValue.ParameterId, context)
                        + ", wartość: " + ParametersHelper.GetParameterValue(newParameterValue.Value)
                        , user));
            }
        }
    }
}