﻿using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class MaterialsHelper
    {
        public static string GetMaterialTypeName(
            int? materialTypeId,
            ApplicationDataDbContext context)
        {
            var materialType = context.MaterialTypes.Find(materialTypeId);
            if (materialType == null)
            {
                return "(Brak typu materiału nr: " + materialTypeId + ")";
            }

            return materialType.Name;
        }

        public static string GetMaterialName(
            int? materialId,
            ApplicationDataDbContext context)
        {
            Material material = context.Materials.Find(materialId);
            if (material != null)
            {
                return material.Name;
            }

            return "(Brak materiału nr: " + materialId + ")";
        }

        public static string GetMaterialUnit(
            int? materialId,
            ApplicationDataDbContext context)
        {
            Material material = context.Materials.Find(materialId);
            if (material != null)
            {
                return material.Unit;
            }

            return "(Brak materiału nr: " + materialId + ")";
        }

    }
}