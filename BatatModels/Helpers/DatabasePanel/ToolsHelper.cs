﻿using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ToolsHelper
    {
        public static string GetToolTypeName(int? toolTypeId, ApplicationDataDbContext context)
        {
            var toolType = context.ToolTypes.Find(toolTypeId);
            if (toolType == null)
            {
                return "(Brak typu narzędzia nr: " + toolTypeId + ")";
            }

            return toolType.Name;
        }

        public static string GetToolName(int? toolId, ApplicationDataDbContext context)
        {
            Tool tool = context.Tools.Find(toolId);
            if (tool != null)
            {
                return tool.Name;
            }

            return "(Brak narzędzia nr: " + toolId + ")";
        }

        public static string GetUtilization(decimal? utilization)
        {
            if (utilization != null)
            {
                return $"{utilization:N2}" + "%";
            }

            return null;
        }

    }
}