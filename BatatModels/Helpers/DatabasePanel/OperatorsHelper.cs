﻿using System;
using Batat.Models.Database.DatabasePanel;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class OperatorsHelper
    {
        public static string GetName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(Nie ustawiono)";
        }

        public static string GetPosition(string position)
        {
            return !String.IsNullOrWhiteSpace(position) ? position : "(Nie ustawiono)";
        }

        public static string GetFunction(string function)
        {
            return !String.IsNullOrWhiteSpace(function) ? function : "(Nie ustawiono)";
        }

        public static string GetRate(decimal? rate)
        {
            return rate > 0.0M ? $"{rate:N2}" : "(Nie ustawiono)";
        }

        public static string GetDateAndTime(DateTime? dateAndTime)
        {
            if (dateAndTime != null && dateAndTime != DateTime.MinValue)
            {
                return dateAndTime.Value.ToString("yyyy-MM-ddTHH:mm:ss");
            }

            return "(Nie ustawiono)";
        }

    }
}