﻿using System;
using System.Linq;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class TechnologiesHelper
    {
        public static string GetTechnologyCode(string code)
        {
            return !String.IsNullOrWhiteSpace(code) ? code : "(pusty kod technologii)";
        }

        public static string GetTechnologyCode(int technologyId, ApplicationDataDbContext context)
        {
            var technology = context.Technologies.Find(technologyId);
            if (technology != null)
            {
                return GetTechnologyCode(technology.Code);
            }

            return "(nie znaleziono technologii nr: " + technologyId + ")";
        }

        public static string CreateTechnologyCode(
            int technologyId,
            ApplicationDataDbContext context)
        {
            var technology = context.Technologies.Find(technologyId);
            if (technology == null)
                return null;

            var technologyNumber = context.Technologies.Count(a => a.PartId == technology.PartId) + 1;
            return PartsHelper.GetPartCode(technology.PartId, context) + "_T" + technologyNumber.ToString("00");

        }

        //For SeedDatabase purporses only
        public static string CreateTechnologyCode(
            int partId,
            int technologyId,
            ApplicationDataDbContext context)
        {
            return PartsHelper.GetPartCode(partId, context) + "_T" + technologyId.ToString("00");
        }

        public static string GetTechnologyName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(pusta Name technologii)";
        }

        public static string GetTechnologyName(int technologyId, ApplicationDataDbContext context)
        {
            var technology = context.Technologies.Find(technologyId);
            if (technology != null)
            {
                return GetTechnologyName(technology.Name);
            }

            return "(nie znaleziono technologii nr: " + technologyId + ")";
        }

        public static string GetTechnologyId(int technologyId, ApplicationDataDbContext context)
        {
            var technology = context.Technologies.Find(technologyId);
            if (technology != null)
            {
                return technologyId.ToString();
            }

            return "(nie znaleziono technologii nr: " + technologyId + ")";
        }

        public static string GetTechnologyOperationsIdsAsString(int technologyId, ApplicationDataDbContext context)
        {
            var technology = context.Technologies.Find(technologyId);
            if (technology == null)
                return "(nie znaleziono technologii nr: " + technologyId + ")";

            string operations = "";

            foreach(var operation in technology.Operations)
            {
                operations += operation.OperationId.ToString();
                operations += "|";
            }

            if(operations.Length > 0)
            {
                operations = operations.Remove(operations.Length - 1);
            }

            return operations;
        }

    }
}