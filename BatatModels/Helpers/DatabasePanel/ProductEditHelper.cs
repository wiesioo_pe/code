﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ProductEditHelper
    {
        public static void CheckIfProductExists(ref Product productDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (productDb == null)
            {
                productDb = new Product();
                productDb.Changes.Add(new ChangeClass("Utworzenie: produkt", "Nowy produkt utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            ProductAndFilesVM productAndFilesVm,
            Product productDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedGroups = new HashSet<GroupIdClass>();
            foreach (var groupDb in productDb.Groups)
            {
                if (productAndFilesVm.Groups.Any(a => a.GroupId == groupDb.GroupId))
                {
                    productAndFilesVm.Groups.RemoveAll(a => a.GroupId == groupDb.GroupId);
                }
                else
                {
                    removedGroups.Add(groupDb);
                }
            }
            foreach (var removedGroup in removedGroups)
            {
                productDb.Groups.Remove(removedGroup);
                productDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                            "Nr: " + removedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(removedGroup.GroupId, context)
                            , user));
            }
            foreach (var addedGroup in productAndFilesVm.Groups)
            {
                if (context.Groups.Any(a => a.Id == addedGroup.GroupId))
                {
                    productDb.Groups.Add(new GroupIdClass(addedGroup.GroupId, context.Groups.Find(addedGroup.GroupId)?.UUID));
                    productDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                            "Nr: " + addedGroup.GroupId
                            + " - " + SharedHelper.GetGroupName(addedGroup.GroupId, context)
                            , user));
                }
                else
                {
                    errorVm.Messages.Add("Grupa nr: " + addedGroup + " nie istnieje");
                }
            }
        }

        public static void CheckNameChange(ProductAndFilesVM productAndFilesVm, Product productDb, ApplicationUser user, ErrorVM errorVm)
        {
            productAndFilesVm.Name = SharedHelper.NormalizeString(productAndFilesVm.Name);
            productDb.Name = SharedHelper.NormalizeString(productDb.Name);

            if(productAndFilesVm.Name != null)
            {
                if (productAndFilesVm.Name == productDb.Name)
                    return;

                if(productDb.Name != null)
                {
                    productDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + productDb.Name + ", na: " + productAndFilesVm.Name, user));
                    productDb.Name = productAndFilesVm.Name;
                }
                else
                {
                    productDb.Changes.Add(new ChangeClass("Ustawienie: nazwa",
                        productAndFilesVm.Name, user));
                    productDb.Name = productAndFilesVm.Name;
                }
            }
            else
            {
                errorVm.Messages.Add("Nazwa produktu nie może być pusta");
            }
        }

        public static void CheckCodeChange(ProductAndFilesVM productAndFilesVm, Product productDb, ApplicationUser user, ErrorVM errorVm)
        {
            productAndFilesVm.Code = SharedHelper.NormalizeString(productAndFilesVm.Code);
            productDb.Code = SharedHelper.NormalizeString(productDb.Code);

            if (productAndFilesVm.Code != null)
            {
                if (productAndFilesVm.Code == productDb.Code)
                    return;

                if (productDb.Code != null)
                {
                    productDb.Changes.Add(new ChangeClass("Edycja: kod",
                        "Z: " + productDb.Code + ", na: " + productAndFilesVm.Code, user));
                    productDb.Code = productAndFilesVm.Code;
                }
                else
                {
                    productDb.Changes.Add(new ChangeClass("Ustawienie: kod",
                        productAndFilesVm.Code, user));
                    productDb.Code = productAndFilesVm.Code;
                }
            }
            else
            {
                errorVm.Messages.Add("Kod produktu nie może być pusty");
            }
        }

        public static void CheckDescChange(ProductAndFilesVM productAndFilesVm, Product productDb, ApplicationUser user, ErrorVM errorVm)
        {
            productAndFilesVm.Desc = SharedHelper.NormalizeString(productAndFilesVm.Desc);
            productDb.Desc = SharedHelper.NormalizeString(productDb.Desc);

            if (productAndFilesVm.Desc == productDb.Desc)
                return;

            if (productDb.Desc != null)
            {
                productDb.Changes.Add(new ChangeClass("Edycja: opis",
                    "Z: " + productDb.Desc + ", na: " + productAndFilesVm.Desc, user));
                productDb.Desc = productAndFilesVm.Desc;
            }
            else
            {
                productDb.Changes.Add(new ChangeClass("Ustawienie: opis",
                    productAndFilesVm.Desc, user));
                productDb.Desc = productAndFilesVm.Desc;
            }
        }

        public static void CheckNetPriceChange(ProductAndFilesVM productAndFilesVm, Product productDb, ApplicationUser user, ErrorVM errorVm)
        {
            if(productAndFilesVm.NetPrice >= 0)
            {
                if (productAndFilesVm.NetPrice == productDb.NetPrice)
                    return;

                if (productDb.NetPrice == 0)
                {
                    productDb.Changes.Add(new ChangeClass("Ustawienie: cena netto",
                        $"{productAndFilesVm.NetPrice:N2}", user));
                    productDb.NetPrice = productAndFilesVm.NetPrice;
                }
                else
                {
                    productDb.Changes.Add(new ChangeClass("Edycja: cena netto",
                        "Z: " + $"{productDb.NetPrice:N2}" + ", na: " + $"{productAndFilesVm.NetPrice:N2}", user));
                    productDb.NetPrice = productAndFilesVm.NetPrice;
                }
            }
            else
            {
                errorVm.Messages.Add("Cena produktu nie może być ujemna");
            }
        }

        public static void CheckProductsChange(
            ProductAndFilesVM productAndFilesVm,
            Product productDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedProducts = new HashSet<ProductIdAmountDiscountClass>();
            foreach (var productAmountDiscountDb in productDb.Products)
            {
                if (productAndFilesVm.Products.Any(i => i.ProductId == productAmountDiscountDb.ProductId))
                {
                    var productAmountDiscountVm = productAndFilesVm.Products.First(i => i.ProductId == productAmountDiscountDb.ProductId);

                    if (productAmountDiscountDb.Amount != productAmountDiscountVm.Amount || productAmountDiscountDb.Discount != productAmountDiscountVm.Discount)
                    {
                        if (productAmountDiscountVm.Amount >= 0)
                        {
                            if (productAmountDiscountVm.Amount != productAmountDiscountDb.Amount)
                            {
                                if (productAmountDiscountDb.Amount == 0)
                                {
                                    productDb.Changes.Add(new ChangeClass("Ustawienie: ilość produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", ilość: " + ProductsHelper.GetAmount(productAmountDiscountVm.Amount)
                                    , user));
                                    productAmountDiscountDb.Amount = productAmountDiscountVm.Amount ?? 0;
                                }
                                else
                                {
                                    productDb.Changes.Add(new ChangeClass("Edycja: ilość produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", ilość z: " + ProductsHelper.GetAmount(productAmountDiscountDb.Amount)
                                    + ", na: " + ProductsHelper.GetAmount(productAmountDiscountVm.Amount)
                                    , user));
                                    productAmountDiscountDb.Amount = productAmountDiscountVm.Amount ?? 0;
                                }
                            }
                        }
                        else
                        {
                            errorVm.Messages.Add("Ilość produktu nie może być ujemna");
                        }

                        if (productAmountDiscountVm.Discount >= 0 && productAmountDiscountVm.Discount <= 100)
                        {
                            if (productAmountDiscountVm.Discount != productAmountDiscountDb.Discount)
                            {
                                if (productAmountDiscountDb.Discount == 0)
                                {
                                    productDb.Changes.Add(new ChangeClass("Ustawienie: rabat produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", rabat: " + ProductsHelper.GetDiscount(productAmountDiscountVm.Discount)
                                    , user));
                                    productAmountDiscountDb.Discount = productAmountDiscountVm.Discount ?? 0;
                                }
                                else
                                {
                                    productDb.Changes.Add(new ChangeClass("Edycja: rabat produktu",
                                    "Nr: " + productAmountDiscountDb.ProductId
                                    + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                                    + ", rabat z: " + ProductsHelper.GetDiscount(productAmountDiscountDb.Discount)
                                    + ", na: " + ProductsHelper.GetDiscount(productAmountDiscountVm.Discount)
                                    , user));
                                    productAmountDiscountDb.Discount = productAmountDiscountVm.Discount ?? 0;
                                }
                            }
                        }
                        else if (productAmountDiscountVm.Discount < 0)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być ujemny");
                        }
                        else if (productAmountDiscountVm.Discount > 100)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być większy od 100%");
                        }
                    }
                    productAndFilesVm.Products.RemoveAll(a => a.ProductId == productAmountDiscountVm.ProductId);
                }
                else
                {
                    productDb.Changes.Add(new ChangeClass("Usunięcie: Produkt - ilość",
                            "Nr: " + productAmountDiscountDb.ProductId
                            + ", nazwa: " + ProductsHelper.GetProductName(productAmountDiscountDb.ProductId, context)
                            + ", ilość: " + ProductsHelper.GetAmount(productAmountDiscountDb.Amount)
                            + ", rabat: " + ProductsHelper.GetDiscount(productAmountDiscountDb.Discount)
                            , user));
                    removedProducts.Add(productAmountDiscountDb);
                }
            }

            foreach (var oldProductToRemove in removedProducts)
            {
                productDb.Products.Remove(oldProductToRemove);
            }

            foreach (var addedProductAmountDiscountVm in productAndFilesVm.Products)
            {
                if (context.Products.Any(a => a.Id == addedProductAmountDiscountVm.ProductId))
                {
                    if (addedProductAmountDiscountVm.Amount >= 0 && addedProductAmountDiscountVm.Discount >= 0 && addedProductAmountDiscountVm.Discount <= 100)
                    {
                        productDb.Products.Add(new ProductIdAmountDiscountClass(addedProductAmountDiscountVm.ProductId, context.Products.Find(addedProductAmountDiscountVm.ProductId).UUID, addedProductAmountDiscountVm.Amount, addedProductAmountDiscountVm.Discount));
                        productDb.Changes.Add(new ChangeClass("Dodanie: Product - ilość",
                                "Nr: " + addedProductAmountDiscountVm.ProductId
                                + ", nazwa: " + ProductsHelper.GetProductName(addedProductAmountDiscountVm.ProductId, context)
                                + ", ilość: " + ProductsHelper.GetAmount(addedProductAmountDiscountVm.Amount)
                                + ", rabat: " + ProductsHelper.GetDiscount(addedProductAmountDiscountVm.Discount)
                                , user));
                    }
                    else
                    {
                        if (addedProductAmountDiscountVm.Amount < 0)
                        {
                            errorVm.Messages.Add("Ilość produktu nie może być ujemna");
                        }
                        if (addedProductAmountDiscountVm.Discount < 0)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być ujemny");
                        }
                        if (addedProductAmountDiscountVm.Discount > 100)
                        {
                            errorVm.Messages.Add("Rabat produktu nie może być większy od 100%");
                        }
                    }
                }
                else
                {
                    errorVm.Messages.Add("Produkt nr: " + addedProductAmountDiscountVm.ProductId + " nie istnieje");
                }
            }
        }

        public static void CheckPartsChange(
            ProductAndFilesVM productAndFilesVm,
            Product productDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedParts = new HashSet<PartIdAmountClass>();
            foreach (var partAmountDb in productDb.Parts)
            {
                if (productAndFilesVm.Parts.Any(i => i.PartId == partAmountDb.PartId))
                {
                    var partAmountVm = productAndFilesVm.Parts.First(i => i.PartId == partAmountDb.PartId);

                    if(partAmountVm.Amount != null)
                    {
                        if (partAmountVm.Amount >= 0)
                        {
                            if (partAmountVm.Amount != partAmountDb.Amount)
                            {
                                if (partAmountDb.Amount == 0)
                                {
                                    productDb.Changes.Add(new ChangeClass("Ustawienie: ilość detalu",
                                    "Nr: " + partAmountDb.PartId
                                    + ", nazwa: " + PartsHelper.GetPartName(partAmountDb.PartId, context)
                                    + ", ilość: " + partAmountVm.Amount
                                    , user));
                                    partAmountDb.Amount = partAmountVm.Amount ?? 0;
                                }
                                else
                                {
                                    productDb.Changes.Add(new ChangeClass("Edycja: ilość detalu",
                                    "Nr: " + partAmountDb.PartId
                                    + ", nazwa: " + PartsHelper.GetPartName(partAmountDb.PartId, context)
                                    + ", ilość z: " + partAmountDb.Amount
                                    + ", na: " + partAmountVm.Amount
                                    , user));
                                    partAmountDb.Amount = partAmountVm.Amount ?? 0;
                                }
                            }
                        }
                        else
                        {
                            errorVm.Messages.Add("Ilość detalu nie może być ujemna");
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Nie podano ilości detalu");
                    }

                    productAndFilesVm.Parts.RemoveAll(a => a.PartId == partAmountVm.PartId);
                }
                else
                {
                    productDb.Changes.Add(new ChangeClass("Usunięcie: Detal - ilość",
                            "Nr: " + partAmountDb.PartId
                            + ", nazwa: " + PartsHelper.GetPartName(partAmountDb.PartId, context)
                            + ", ilość: " + ProductsHelper.GetAmount(partAmountDb.Amount)
                            , user));
                    removedParts.Add(partAmountDb);
                }
            }

            foreach (var partToRemove in removedParts)
            {
                productDb.Parts.Remove(partToRemove);
            }

            foreach (var partAmountVm in productAndFilesVm.Parts)
            {
                if (context.Parts.Any(a => a.Id == partAmountVm.PartId))
                {
                    if (partAmountVm.Amount >= 0)
                    {
                        productDb.Parts.Add(new PartIdAmountClass(partAmountVm.PartId, context.Parts.Find(partAmountVm.PartId)?.UUID, partAmountVm.Amount));
                        productDb.Changes.Add(new ChangeClass("Dodanie: Detal - ilość",
                                "Nr: " + partAmountVm.PartId
                                + ", nazwa: " + PartsHelper.GetPartName(partAmountVm.PartId, context)
                                + ", ilość: " + ProductsHelper.GetAmount(partAmountVm.Amount)
                                , user));
                    }
                    else
                    {
                        errorVm.Messages.Add("Ilość detalu nie może być ujemna");
                    }
                }
                else
                {
                    errorVm.Messages.Add("Detal nr: " + partAmountVm.PartId + " nie istnieje");
                }
            }
        }

        public static void CheckParametersChange(
            ProductAndFilesVM productAndFilesVm,
            Product productDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedParameters = new HashSet<ParameterIdValueClass>();
            foreach (var parameterDb in productDb.Parameters)
            {
                if (productAndFilesVm.Parameters.Any(a => a.ParameterId == parameterDb.ParameterId))
                {
                    var parameterVm = productAndFilesVm.Parameters.First(a => a.ParameterId == parameterDb.ParameterId);

                    parameterVm.Value = SharedHelper.NormalizeString(parameterVm.Value);
                    parameterDb.Value = SharedHelper.NormalizeString(parameterDb.Value);

                    if (parameterVm.Value != null)
                    {
                        if (parameterVm.Value != parameterDb.Value)
                        {
                            if (parameterDb.Value == null)
                            {
                                productDb.Changes.Add(new ChangeClass("Ustawienie: wartość parameteru",
                                    "Nr: " + parameterDb.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(parameterDb.ParameterId, context)
                                    + ", wartość: " + parameterVm.Value
                                    , user));
                                parameterDb.Value = parameterVm.Value;
                            }
                            else
                            {
                                productDb.Changes.Add(new ChangeClass("Edycja: wartość parameteru",
                                    "Nr: " + parameterDb.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(parameterDb.ParameterId, context)
                                    + ", wartość z: " + parameterDb.Value
                                    + ", na: " + parameterVm.Value
                                    , user));
                                parameterDb.Value = parameterVm.Value;
                            }
                        }
                    }
                    else
                    {
                        errorVm.Messages.Add("Wartość parametru nie może być pusta");
                    }
                    productAndFilesVm.Parameters.RemoveAll(a => a.ParameterId == parameterDb.ParameterId);
                }
                else
                {
                    removedParameters.Add(parameterDb);
                }
            }
            foreach (var removedParameter in removedParameters)
            {
                productDb.Parameters.Remove(removedParameter);
                productDb.Changes.Add(new ChangeClass("Usunięcie: parametr",
                            "Nr: " + removedParameter.ParameterId
                            + ", nazwa: " + ParametersHelper.GetParameterName(removedParameter.ParameterId, context)
                            + ", wartość: " + removedParameter.Value
                            , user));
            }
            foreach (var addedParameter in productAndFilesVm.Parameters)
            {
                addedParameter.Value = SharedHelper.NormalizeString(addedParameter.Value);

                if(context.Parameters.Any(a => a.Id == addedParameter.ParameterId))
                {
                    if (addedParameter.Value != null)
                    {
                        productDb.Parameters.Add(new ParameterIdValueClass(addedParameter.ParameterId, context.Parameters.Find(addedParameter.ParameterId)?.UUID, addedParameter.Value));
                        productDb.Changes.Add(new ChangeClass("Dodanie: parametr",
                                    "Nr: " + addedParameter.ParameterId
                                    + ", nazwa: " + ParametersHelper.GetParameterName(addedParameter.ParameterId, context)
                                    + ", wartość: " + addedParameter.Value
                                    , user));
                    }
                    else if (addedParameter.Value == null)
                    {
                        errorVm.Messages.Add("Wartość parametru nie może być pusta");
                    }
                }
                else
                {
                    errorVm.Messages.Add("Parametr o nr: " + addedParameter.ParameterId + " nie istnieje");
                }
            }
        }

        public static void CheckFilesChange(
            ProductAndFilesVM productAndFilesVm,
            Product productDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var removedFilesUrls = new HashSet<FileUrlClass>();

            foreach (var fileUrlClass in productDb.FileUrlObjs)
            {
                if (productAndFilesVm.FileUrlObjs.Any(a => a.FileName == fileUrlClass.FileName))
                {
                    productAndFilesVm.FileUrlObjs.RemoveAll(a => a.FileName == fileUrlClass.FileName);
                }
                else
                {
                    removedFilesUrls.Add(fileUrlClass);
                }
            }

            foreach (var removedFileUrl in removedFilesUrls)
            {
                // Delete old file
                var oldFilePath = HttpContext.Current.Server.MapPath(removedFileUrl.FileUrl);
                if (File.Exists(oldFilePath))
                {
                    File.Delete(oldFilePath);
                }

                productDb.FileUrlObjs.Remove(removedFileUrl);
                productDb.Changes.Add(new ChangeClass("Usunięcie: plik",
                            "Nazwa: " + removedFileUrl.FileName
                            , user));
                context.Entry(removedFileUrl).State = EntityState.Deleted;
            }

            foreach (var addedFile in productAndFilesVm.Files)
            {
                // remove illegal characters from Product Name
                string productIdForPath = productAndFilesVm.Id.ToString();
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                productIdForPath = r.Replace(productIdForPath, "");

                // Save URL in Product.FileUrl
                string fileName = productIdForPath + " - " + addedFile.FileName;
                var newFileUrl = new FileUrlClass
                {
                    FileUrl = "/UserFiles/Products/" + fileName,
                    FileName = fileName,
                    OriginalFileName = addedFile.FileName,
                    FileType = addedFile.ContentType
                };

                var base64StringWithoutHeader = addedFile.Content.Remove(0, addedFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Products"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);


                productDb.Changes.Add(new ChangeClass("Dodanie: plik",
                                            "Nazwa: " + addedFile.FileName
                                            , user));

                productDb.FileUrlObjs.Add(newFileUrl);
                context.Entry(newFileUrl).State = EntityState.Added;
            }
        }

    }
}