﻿using System;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class PartsHelper
    {

        public static string GetPartName(string name)
        {
            return !String.IsNullOrWhiteSpace(name) ? name : "(brak nazwy)";
        }

        public static string GetPartName(int? id, ApplicationDataDbContext context)
        {
            var part = context.Parts.Find(id);
            if (part != null)
            {
                return GetPartName(part.Name);
            }

            return "(brak detalu:" + id + ")";
        }

        public static string GetPartId(int? id, ApplicationDataDbContext context)
        {
            var part = context.Parts.Find(id);
            if (part != null)
            {
                return id.ToString();
            }

            return "(brak detalu:" + id + ")";
        }

        public static string GetPartCode(string code)
        {
            if (!String.IsNullOrWhiteSpace(code))
            {
                return code;
            }

            return "(brak kodu)";
        }

        public static string GetPartCode(int? id, ApplicationDataDbContext context)
        {
            var part = context.Parts.Find(id);
            if (part != null)
            {
                return GetPartCode(part.Code);
            }

            return "(brak detalu:" + id + ")";
        }

        public static string GetPartDescription(string desc)
        {
            return !String.IsNullOrWhiteSpace(desc) ? desc : "(brak opisu)";
        }

        public static string GetPartDescription(int? id, ApplicationDataDbContext context)
        {
            var part = context.Parts.Find(id);
            if (part != null)
            {
                return GetPartDescription(part.Desc);
            }

            return "(brak detalu:" + id + ")";
        }

    }
}