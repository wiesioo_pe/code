﻿using System.Collections.Generic;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class FixedCostEditHelper
    {
        public static void CheckIfFixedCostExists(ref FixedCost fixedCostDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (fixedCostDb == null)
            {
                fixedCostDb = new FixedCost();
                fixedCostDb.Changes.Add(new ChangeClass("Utworzenie: koszt stały", "Nowy koszt stały utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var toDeleteHashSet = new HashSet<GroupIdClass>();
            foreach (var item in fixedCostDb.Groups)
            {
                if (fixedCostVm.Groups.Any(a => a.GroupId == item.GroupId))
                {
                    fixedCostVm.Groups.RemoveAll(a => a.GroupId == item.GroupId);
                }
                else
                {
                    toDeleteHashSet.Add(item);
                }
            }

            foreach (var item in toDeleteHashSet)
            {
                fixedCostDb.Groups.Remove(item);
                fixedCostDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                            "Nr: " + item.GroupId
                            + ", nazwa: " + SharedHelper.GetGroupName(item.GroupId, context)
                            , user));
            }

            foreach (var item in fixedCostVm.Groups)
            {
                fixedCostDb.Groups.Add(new GroupIdClass(item.GroupId, context.Groups.Find(item.GroupId)?.UUID));
                fixedCostDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                            "Nr: " + item.GroupId
                            + ", nazwa: " + SharedHelper.GetGroupName(item.GroupId, context)
                            , user));
            }

        }

        public static void CheckCategoryChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (fixedCostVm.CategoryId != fixedCostDb.CategoryId)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: kategoria",
                        "Z: " + FixedCostsHelper.GetCategoryName(fixedCostDb.CategoryId, context) + ", na: " + FixedCostsHelper.GetCategoryName(fixedCostVm.CategoryId, context), user));
                fixedCostDb.CategoryId = fixedCostVm.CategoryId;
            }
        }

        public static void CheckTypeChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (fixedCostVm.TypeId != fixedCostDb.TypeId)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: typ",
                        "Z: " + FixedCostsHelper.GetTypeName(fixedCostDb.TypeId, context) + ", na: " + FixedCostsHelper.GetTypeName(fixedCostVm.TypeId, context), user));
                fixedCostDb.TypeId = fixedCostVm.TypeId;
            }
        }

        public static void CheckStartDateChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (fixedCostVm.StartDate != fixedCostDb.StartDate)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: data rozpoczęcia",
                        "Z: " + FixedCostsHelper.GetDate(fixedCostDb.StartDate) + ", na: " + FixedCostsHelper.GetDate(fixedCostVm.StartDate), user));
                fixedCostDb.StartDate = fixedCostVm.StartDate;
            }
        }

        public static void CheckStopDateChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (fixedCostVm.StopDate != fixedCostDb.StopDate)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: data zakończenia",
                        "Z: " + FixedCostsHelper.GetDate(fixedCostDb.StopDate) + ", na: " + FixedCostsHelper.GetDate(fixedCostVm.StopDate), user));
                fixedCostDb.StopDate = fixedCostVm.StopDate;
            }
        }

        public static void CheckAmountChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (fixedCostVm.Amount != fixedCostDb.Amount)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + FixedCostsHelper.GetAmount(fixedCostDb.Amount) + ", na: " + FixedCostsHelper.GetAmount(fixedCostVm.Amount), user));
                fixedCostDb.Amount = fixedCostVm.Amount;
            }
        }

        public static void CheckDescriptionChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (fixedCostVm.Desc == null) { fixedCostVm.Desc = ""; }
            if (fixedCostVm.Desc != fixedCostDb.Desc)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + FixedCostsHelper.GetDescription(fixedCostDb.Desc) + ", na: " + FixedCostsHelper.GetDescription(fixedCostVm.Desc), user));
                fixedCostDb.Desc = fixedCostVm.Desc;
            }
        }

        public static void CheckPeriodChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (fixedCostVm.Period != fixedCostDb.Period)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: okres rozl.",
                        "Z: " + FixedCostsHelper.GetPeriod(fixedCostDb.Period) + ", na: " + FixedCostsHelper.GetPeriod(fixedCostVm.Period), user));
                fixedCostDb.Period = fixedCostVm.Period;
            }
        }

        public static void CheckIsRecurringChange(
            FixedCostVM fixedCostVm,
            FixedCost fixedCostDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (fixedCostVm.isRecurring != fixedCostDb.isRecurring)
            {
                fixedCostDb.Changes.Add(new ChangeClass("Edycja: czy okresowy?",
                        "Z: " + FixedCostsHelper.GetIsRecurring(fixedCostDb.isRecurring) + ", na: " + FixedCostsHelper.GetIsRecurring(fixedCostVm.isRecurring), user));
                fixedCostDb.isRecurring = fixedCostVm.isRecurring;
            }
        }
    }
}