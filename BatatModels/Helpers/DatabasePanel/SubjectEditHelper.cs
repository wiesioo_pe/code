﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class SubjectEditHelper
    {
        public static void CheckIfSubjectExists(ref Subject subjectDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (subjectDb == null)
            {
                subjectDb = new Subject();
                subjectDb.Changes.Add(new ChangeClass("Utworzenie: odbiorca", "Nowy odbiorca utworzony", user));
            }
        }

        public static void CheckGroupsChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            var groupsIdsDbCopy = new HashSet<GroupIdClass>(subjectDb.Groups);
            if (subjectAndLogoVm.Groups == null)
            {
                subjectAndLogoVm.Groups = new List<GroupIdClassVM>();
            }
            foreach (var groupIdVm in subjectAndLogoVm.Groups)
            {
                if(groupsIdsDbCopy.Any(a => a.GroupId == groupIdVm.GroupId))
                {
                    groupsIdsDbCopy.RemoveWhere(b => b.GroupId == groupIdVm.GroupId);
                }
                else
                {
                    subjectDb.Changes.Add(new ChangeClass("Dodanie: grupa",
                        "Id grupy: " + groupIdVm.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdVm.GroupId, context), user));
                    subjectDb.Groups.Add(new GroupIdClass(groupIdVm.GroupId, context.Groups.Find(groupIdVm.GroupId)?.UUID));
                }
            }
            foreach (var groupIdDbCopy in groupsIdsDbCopy)
            {
                subjectDb.Changes.Add(new ChangeClass("Usunięcie: grupa",
                        "Id grupy: " + groupIdDbCopy.GroupId + ", nazwa: " + SharedHelper.GetGroupName(groupIdDbCopy.GroupId, context), user));
                subjectDb.Groups.RemoveWhere(c => c.GroupId == groupIdDbCopy.GroupId);
            }
        }

        public static void CheckNameChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.Name != subjectDb.Name)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + subjectDb.Name + ", na: " + subjectAndLogoVm.Name, user));
                subjectDb.Name = subjectAndLogoVm.Name;
            }
        }

        public static void CheckShortNameChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.ShortName != subjectDb.ShortName)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: skrót",
                        "Z: " + subjectDb.ShortName + ", na: " + subjectAndLogoVm.ShortName, user));
                subjectDb.ShortName = subjectAndLogoVm.ShortName;
            }
        }

        public static void CheckVatRegNumChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.VATRegNum != subjectDb.VATRegNum)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: NIP",
                        "Z: " + subjectDb.VATRegNum + ", na: " + subjectAndLogoVm.VATRegNum, user));
                subjectDb.VATRegNum = subjectAndLogoVm.VATRegNum;
            }
        }

        public static void CheckWwwChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.WWW != subjectDb.WWW)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: strona WWW",
                        "Z: " + subjectDb.WWW + ", na: " + subjectAndLogoVm.WWW, user));
                subjectDb.WWW = subjectAndLogoVm.WWW;
            }
        }

        public static void CheckStreetChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.Street != subjectDb.Street)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: ulica",
                        "Z: " + subjectDb.Street + ", na: " + subjectAndLogoVm.Street, user));
                subjectDb.Street = subjectAndLogoVm.Street;
            }
        }

        public static void CheckCityChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.City != subjectDb.City)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: miejscowość",
                        "Z: " + subjectDb.City + ", na: " + subjectAndLogoVm.City, user));
                subjectDb.City = subjectAndLogoVm.City;
            }
        }

        public static void CheckPostalCodeChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.PostalCode != subjectDb.PostalCode)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: kod pocztowy",
                        "Z: " + subjectDb.PostalCode + ", na: " + subjectAndLogoVm.PostalCode, user));
                subjectDb.PostalCode = subjectAndLogoVm.PostalCode;
            }
        }

        public static void CheckStateChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.State != subjectDb.State)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: województwo",
                        "Z: " + subjectDb.State + ", na: " + subjectAndLogoVm.State, user));
                subjectDb.State = subjectAndLogoVm.State;
            }
        }

        public static void CheckPhoneChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.Phone != subjectDb.Phone)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: telefon",
                        "Z: " + subjectDb.Phone + ", na: " + subjectAndLogoVm.Phone, user));
                subjectDb.Phone = subjectAndLogoVm.Phone;
            }
        }

        public static void CheckDiscountChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb, ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.Discount != subjectDb.Discount)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: rabat",
                        "Z: " + subjectDb.Discount + ", na: " + subjectAndLogoVm.Discount, user));
                subjectDb.Discount = subjectAndLogoVm.Discount ?? 0;
            }
        }

        public static void CheckDescChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb,
            ApplicationUser user,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.Desc != subjectDb.Desc)
            {
                subjectDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + subjectDb.Desc + ", na: " + subjectAndLogoVm.Desc, user));
                subjectDb.Desc = subjectAndLogoVm.Desc;
            }
        }

        public static void CheckContactsChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            bool subjectDbChanged = false;
            var contactsIdsDbCopy = new List<ContactIdClass>(subjectDb.Contacts);
            if(subjectAndLogoVm.Contacts == null)
            {
                subjectAndLogoVm.Contacts = new List<ContactIdClassVM>();
            }
            foreach (var contactIdVm in subjectAndLogoVm.Contacts)
            {
                var contactIdVmIndex = subjectAndLogoVm.Contacts.IndexOf(contactIdVm);

                if (contactsIdsDbCopy.Any(a => a.ContactId == contactIdVm.ContactId))
                {
                    var contactIdDb = subjectDb.Contacts.Single(b => b.ContactId == contactIdVm.ContactId);
                    var contactIdDbIndex = subjectDb.Contacts.IndexOf(contactIdDb);
                    if (contactIdDbIndex != contactIdVmIndex)
                    {
                        subjectDb.Changes.Add(new ChangeClass("Zmiana: indeks kontaktu",
                           "Id kontaktu: " + contactIdVm.ContactId + ", nazwa: " + ContactsHelper.GetContactName(contactIdVm.ContactId, context) 
                           + ", z indeksu: " + (contactIdDbIndex + 1) + ", na: " + (contactIdVmIndex + 1), user));
                        subjectDbChanged = true;
                    }
                    contactsIdsDbCopy.Remove(contactsIdsDbCopy.Single(d => d.ContactId == contactIdVm.ContactId));
                }
                else
                {
                    subjectDb.Changes.Add(new ChangeClass("Dodanie: kontakt",
                        "Id kontaktu: " + contactIdVm.ContactId + ", nazwa: " + ContactsHelper.GetContactName(contactIdVm.ContactId, context)
                        + ", indeks: " + (contactIdVmIndex + 1), user));
                    subjectDb.Contacts.Add(new ContactIdClass(contactIdVm.ContactId, context.Contacts.Find(contactIdVm.ContactId)?.UUID));
                    subjectDbChanged = true;
                }
            }

            foreach (var contactIdDbCopy in contactsIdsDbCopy)
            {
                subjectDb.Changes.Add(new ChangeClass("Usunięcie: kontakt",
                        "Id kontaktu: " + contactIdDbCopy.ContactId + ", nazwa: " + ContactsHelper.GetContactName(contactIdDbCopy.ContactId, context), user));
                subjectDb.Contacts.RemoveAll(c => c.ContactId == contactIdDbCopy.ContactId);
                subjectDbChanged = true;
            }

            if(subjectDbChanged)
            {
                subjectDb.Contacts.Clear();
                foreach (var contactId in subjectAndLogoVm.Contacts)
                {
                    subjectDb.Contacts.Add(new ContactIdClass(contactId.ContactId, context.Contacts.Find(contactId.ContactId)?.UUID));
                }
            }
        }

        public static void CheckLogoChange(
            SubjectAndLogoVM subjectAndLogoVm,
            Subject subjectDb, ApplicationUser user,
            ApplicationDataDbContext context,
            ErrorVM errorVm)
        {
            if (subjectAndLogoVm.LogoFile.isNew)
            {
                // remove illegal characters from Subject Name
                string subjectNameForPath = subjectAndLogoVm.Name;
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex($"[{Regex.Escape(regexSearch)}]");
                subjectNameForPath = r.Replace(subjectNameForPath, "_");

                // Save URL in Subject.LogoFileUrlObj
                var splittedFileName = subjectAndLogoVm.LogoFile.FileName.Split('.');
                var fileExtension = "";
                if (splittedFileName.Length > 1)
                {
                    fileExtension = splittedFileName.Last();
                }
                string fileName = subjectNameForPath;
                if (fileExtension != "")
                {
                    fileName += "." + fileExtension;
                }


                subjectAndLogoVm.LogoFileUrlObj.FileUrl = "/UserFiles/Subjects/" + fileName;
                subjectAndLogoVm.LogoFileUrlObj.FileName = fileName;
                subjectAndLogoVm.LogoFileUrlObj.OriginalFileName = subjectAndLogoVm.LogoFile.FileName;
                subjectAndLogoVm.LogoFileUrlObj.FileType = subjectAndLogoVm.LogoFile.ContentType;
                subjectAndLogoVm.LogoFileUrlObj.FileSize = subjectAndLogoVm.LogoFile.Size;

                var base64StringWithoutHeader = subjectAndLogoVm.LogoFile.Content.Remove(0, subjectAndLogoVm.LogoFile.Content.IndexOf(";base64,", StringComparison.Ordinal) + 8);
                var bytes = Convert.FromBase64String(base64StringWithoutHeader);

                var path = HttpContext.Current.Server.MapPath("~/UserFiles/Subjects"); //Path
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }

                //set the image path
                string wholePath = Path.Combine(path, fileName);

                byte[] fileBytes = Convert.FromBase64String(base64StringWithoutHeader);

                File.WriteAllBytes(wholePath, fileBytes);

                // Delete old file
                if (!String.IsNullOrEmpty(subjectDb.LogoFileUrlObj?.FileUrl))
                {
                    var oldFilePath = HttpContext.Current.Server.MapPath(subjectDb.LogoFileUrlObj.FileUrl);
                    if (!String.IsNullOrEmpty(oldFilePath) && File.Exists(oldFilePath))
                    {
                        File.Delete(oldFilePath);
                    }
                }



                if (subjectDb.LogoFileUrlObj == null)
                {
                    subjectDb.Changes.Add(new ChangeClass("Edycja: logo",
                       "Nazwa: " + subjectAndLogoVm.LogoFileUrlObj.FileName
                       + "(oryg.: " + subjectAndLogoVm.LogoFileUrlObj.OriginalFileName + ")"
                       , user));

                    subjectDb.LogoFileUrlObj =
                        new SubjectFileUrlClass
                        {
                            FileName = subjectAndLogoVm.LogoFileUrlObj.FileName,
                            OriginalFileName = subjectAndLogoVm.LogoFileUrlObj.OriginalFileName,
                            FileSize = subjectAndLogoVm.LogoFileUrlObj.FileSize,
                            FileType = subjectAndLogoVm.LogoFileUrlObj.FileType,
                            FileUrl = subjectAndLogoVm.LogoFileUrlObj.FileUrl
                        };
                    context.Entry(subjectDb.LogoFileUrlObj).State = EntityState.Added;
                }
                else
                {
                    subjectDb.Changes.Add(new ChangeClass("Edycja: logo",
                       "Z: " + subjectDb.LogoFileUrlObj.FileName
                       + "(oryg.: " + subjectDb.LogoFileUrlObj.OriginalFileName + ")"
                       + ", na: " + subjectAndLogoVm.LogoFileUrlObj.FileName
                       + "(oryg.: " + subjectAndLogoVm.LogoFileUrlObj.OriginalFileName + ")"
                       , user));

                    subjectDb.LogoFileUrlObj.FileName = subjectAndLogoVm.LogoFileUrlObj.FileName;
                    subjectDb.LogoFileUrlObj.OriginalFileName = subjectAndLogoVm.LogoFileUrlObj.OriginalFileName;
                    subjectDb.LogoFileUrlObj.FileSize = subjectAndLogoVm.LogoFileUrlObj.FileSize;
                    subjectDb.LogoFileUrlObj.FileType = subjectAndLogoVm.LogoFileUrlObj.FileType;
                    subjectDb.LogoFileUrlObj.FileUrl = subjectAndLogoVm.LogoFileUrlObj.FileUrl;
                    context.Entry(subjectDb.LogoFileUrlObj).State = EntityState.Modified;
                }

            }

        }

    }
}