﻿using System;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class OperationsHelper
    {
        public static string GetOperationTypeName(int? operationTypeId, ApplicationDataDbContext context)
        {
            if (operationTypeId > 0 && operationTypeId < context.OperationTypes.Count())
            {
                return context.OperationTypes.Single(a => a.Id == operationTypeId).Name;
            }

            return "(Brak typu operacji nr: " + operationTypeId + ")";
        }

        public static string GetOperationTypeNameByOperationId(int? operationId, ApplicationDataDbContext context)
        {
            if (!(operationId > 0))
                return "Operacja w przygotowaniu";

            var operation = context.Operations.Find(operationId);
            if (operation != null)
            {
                return GetOperationTypeName(operation.OperationTypeId, context);
            }

            return "(Brak operacji nr: " + operationId + ")";

        }

        public static string GetIsOperationActive(bool? isActive)
        {
            return isActive == true ? "aktywna" : "nieaktywna";
        }

        public static string GetIsStartCollective(bool? isStartCollective)
        {
            return isStartCollective == true ? "rozpocznij, gdy wszystkie detale zakończone" : "brak zdefiniowanego startu";
        }

        public static string GetIsEndCollective(bool? isEndCollective)
        {
            return isEndCollective == true ? "zakończ, gdy wszystkie detale dostępne" : "brak zdefiniowanego zakończenia";
        }

        public static string GetOperationCode(string code)
        {
            return !String.IsNullOrWhiteSpace(code) ? code : "(brak kodu)";
        }

        public static string GetOperationCode(int? operationId, ApplicationDataDbContext context)
        {
            if (operationId > 0)
            {
                var operation = context.Operations.Find(operationId);
                if (operation != null)
                {
                    return GetOperationCode(operation.Code);
                }

                return "(Brak operacji nr: " + operationId + ")";
            }

            return "Operacja w przygotowaniu";
        }

        public static string CreateOperationCode(
            Technology technologyDb,
            int operationId,
            ApplicationDataDbContext context)
        {
            OperationIdClass operationDb = null;
            if (technologyDb.Operations.Any(a => a.OperationId == operationId))
            {
                operationDb = technologyDb.Operations.Single(a => a.OperationId == operationId);
            }

            int operationNumber = technologyDb.Operations.IndexOf(operationDb) + 1;

            var code = technologyDb.Code + "_O" + operationNumber.ToString("000");
            return code;
        }

        // For SeedDatabase purporses
        public static string CreateOperationCode(
            int partId,
            int technologyId,
            int operationNumber,
            ApplicationDataDbContext context)
        {
            return PartsHelper.GetPartCode(partId, context) + "_T" + technologyId.ToString("00") + "_O" + operationNumber.ToString("000");
        }

        public static string GetOperationDesc(string desc)
        {
            return !String.IsNullOrWhiteSpace(desc) ? desc : "(brak opisu)";
        }

        public static string GetOperationDesc(int? operationId, ApplicationDataDbContext context)
        {
            if (operationId > 0)
            {
                var operation = context.Operations.Find(operationId);
                if (operation != null)
                {
                    return GetOperationDesc(operation.Desc);
                }

                return "(Brakoperacji nr: " + operationId + ")";
            }

            return "Operacja w przygotowaniu";
        }

        public static string GetOperationId(int? operationId, ApplicationDataDbContext context)
        {
            if(operationId > 0)
            {
                var operation = context.Operations.Find(operationId);

                if (operation != null)
                {
                    return operationId.ToString();
                }

                return "(Bark operacji nr: " + operationId + ")";
            }

            return "Operacja w przygotowaniu";
        }
    }
}