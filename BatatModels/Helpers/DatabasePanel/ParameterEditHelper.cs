﻿using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class ParameterEditHelper
    {
        public static void CheckIfParameterExists(ref Parameter parameterDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (parameterDb == null)
            {
                parameterDb = new Parameter();
                parameterDb.Changes.Add(new ChangeClass("Utworzenie: parametr", "Nowa grupa utworzona", user));
            }
        }

        public static void CheckNameChange(ParameterVM parameterVm, Parameter parameterDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (parameterVm.Name != parameterDb.Name)
            {
                parameterDb.Changes.Add(new ChangeClass("Edycja: nazwa",
                        "Z: " + parameterDb.Name + ", na: " + parameterVm.Name, user));
                parameterDb.Name = parameterVm.Name;
            }
        }

        public static void CheckUnitChange(ParameterVM parameterVm, Parameter parameterDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (parameterVm.Unit != parameterDb.Unit)
            {
                parameterDb.Changes.Add(new ChangeClass("Edycja: jednostka",
                        "Z: " + parameterDb.Unit + ", na: " + parameterVm.Unit, user));
                parameterDb.Unit = parameterVm.Unit;
            }
        }

        public static void CheckDescChange(ParameterVM parameterVm, Parameter parameterDb, ApplicationUser user, ErrorVM errorVm)
        {
            if (parameterVm.Desc != parameterDb.Desc)
            {
                parameterDb.Changes.Add(new ChangeClass("Edycja: opis",
                        "Z: " + parameterDb.Desc + ", na: " + parameterVm.Desc, user));
                parameterDb.Desc = parameterVm.Desc;
            }
        }
    }
}