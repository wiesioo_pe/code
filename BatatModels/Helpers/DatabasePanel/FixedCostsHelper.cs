﻿using System;
using System.Linq;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.DatabasePanel
{
    public class FixedCostsHelper
    {
        public static string GetDate(DateTime? data)
        {
            return data == null ? "(aktywny)" : SharedHelper.GetDate(data);
        }

        public static string GetCategoryName(int? fixedCostCategoryId, ApplicationDataDbContext context)
        {
            if (fixedCostCategoryId == null)
            {
                return "";
            }

            if (context.FixedCostCategories.Any(i => i.Id == fixedCostCategoryId))
            {
                return context.FixedCostCategories.Single(i => i.Id == fixedCostCategoryId).Name;
            }

            return "(błąd)";
        }

        public static string GetTypeName(int? fixedCostTypeId, ApplicationDataDbContext context)
        {
            if (fixedCostTypeId == null)
            {
                return "";
            }

            if (context.FixedCostCategories.Any(i => i.Id == fixedCostTypeId))
            {
                return context.FixedCostCategories.Single(i => i.Id == fixedCostTypeId).Name;
            }

            return "(błąd)";
        }

        public static string GetDescription(string desc)
        {
            return String.IsNullOrEmpty(desc) ? "" : desc;
        }

        public static string GetAmount(decimal amount)
        {
            return $"{amount:N2}";
        }

        public static string GetPeriod(int period)
        {
            return period.ToString();
        }

        public static string GetIsRecurring(bool isRecurring)
        {
            return isRecurring ? "tak" : "nie";
        }
    }
}