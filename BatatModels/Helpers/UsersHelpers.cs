﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers
{
    public class UsersHelpers
    {
        public static List<SelectListItem> GetAllUsersSelectList(ApplicationDataDbContext context)
        {
            var AllUsersSelectList = new List<SelectListItem>();
            AllUsersSelectList.Add(new SelectListItem()
                { Text = "...",
                  Value = "" });

            foreach (var item in context.Users.ToList())
            {
                AllUsersSelectList.Add(new SelectListItem()
                {
                    Text = item.UserName,
                    Value = item.Id.ToString()
                });
            }
            return AllUsersSelectList;
        }
    }
}