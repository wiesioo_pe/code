﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Helpers.ProductionPanel
{
    public partial class SeedDatabaseHelper
    {
        public static void ClearAll()
        {
            ApplicationDataDbContext applicationDataDbContext = new ApplicationDataDbContext();

            using (applicationDataDbContext)
            {
                applicationDataDbContext.Contacts.RemoveRange(applicationDataDbContext.Contacts);
                applicationDataDbContext.Subjects.RemoveRange(applicationDataDbContext.Subjects);
                applicationDataDbContext.Machines.RemoveRange(applicationDataDbContext.Machines);
                applicationDataDbContext.Operations.RemoveRange(applicationDataDbContext.Operations);
                applicationDataDbContext.Operators.RemoveRange(applicationDataDbContext.Operators);
                applicationDataDbContext.Orders.RemoveRange(applicationDataDbContext.Orders);
                applicationDataDbContext.Parameters.RemoveRange(applicationDataDbContext.Parameters);
                applicationDataDbContext.Parts.RemoveRange(applicationDataDbContext.Parts);
                applicationDataDbContext.Products.RemoveRange(applicationDataDbContext.Products);
                applicationDataDbContext.Technologies.RemoveRange(applicationDataDbContext.Technologies);

                applicationDataDbContext.SaveChanges();
            }
        }

        public static string DropAllTables()
        {
            ApplicationDataDbContext applicationDataDbContext = new ApplicationDataDbContext();
            string message = String.Empty;

            var tablesNamesList = new List<string>();
            GetTablesNamesForEntireDatabase(ref tablesNamesList, applicationDataDbContext);
            tablesNamesList = tablesNamesList.OrderByDescending(a => a).ToList();

            var connectionString = applicationDataDbContext.Database.Connection.ConnectionString;
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand;

            foreach (var tableName in tablesNamesList)
            {
                try
                {
                    sqlCommand = new SqlCommand("DROP TABLE [" + tableName + "]", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            GetTablesNamesForEntireDatabase(ref tablesNamesList, applicationDataDbContext);
            tablesNamesList = tablesNamesList.OrderByDescending(a => a).ToList();
            foreach (var tableName in tablesNamesList)
            {
                try
                {
                    sqlCommand = new SqlCommand("DROP TABLE [" + tableName + "]", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    message += @"<br/><br/>";
                    message += ex.Message;
                }
            }

            sqlConnection.Close();
            return message;
        }

        public static string DropAllTablesWithSqlQuery()
        {
            const string queryDropConstrains = @"while (exists(select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE = 'FOREIGN KEY'))
                begin
                 declare @sql nvarchar(2000)
                
                 SELECT TOP 1 @sql = ('ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME
                 + '] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']')
                
                 FROM information_schema.table_constraints
                
                 WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'
                
                 exec(@sql)
                
                 PRINT @sql
                end";

            const string queryDropTables = @"while (exists(select 1 from INFORMATION_SCHEMA.TABLES))
                begin
                 declare @sql nvarchar(2000)
                
                 SELECT TOP 1 @sql = ('DROP TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME
                 + ']')
                
                 FROM INFORMATION_SCHEMA.TABLES
                
                exec(@sql)
                
                 PRINT @sql
                end";


            ApplicationDataDbContext applicationDataDbContext = new ApplicationDataDbContext();
            string message = String.Empty;

            var connectionString = applicationDataDbContext.Database.Connection.ConnectionString;
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand;

            try
            {
                sqlCommand = new SqlCommand(queryDropConstrains, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                message += @"<br/><br/>";
                message += ex.Message;
            }

            try
            {
                sqlCommand = new SqlCommand(queryDropTables, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                message += @"<br/><br/>";
                message += ex.Message;
            }

            sqlConnection.Close();

            return message;
        }

        public static void GetTablesNamesForEntireDatabase(ref List<string> tablesNamesList, System.Data.Entity.DbContext dbContext)
        {
            var connectionString = dbContext.Database.Connection.ConnectionString;
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("SELECT name FROM sys.Tables", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                tablesNamesList.Add(sqlDataReader["name"].ToString());
            }

            sqlConnection.Close();
        }


        public static void GetTablesNamesForDbContext(ref List<string> tablesNamesList, System.Data.Entity.DbContext dbContext)
        {
            using (dbContext)
            {
                var metadata = ((IObjectContextAdapter)dbContext).ObjectContext.MetadataWorkspace;

                var tables = metadata.GetItemCollection(DataSpace.SSpace)
                  .GetItems<EntityContainer>()
                  .Single()
                  .BaseEntitySets
                  .OfType<EntitySet>()
                  .Where(s => !s.MetadataProperties.Contains("Type")
                    || s.MetadataProperties["Type"].ToString() == "Tables");

                foreach (var table in tables)
                {
                    var tableName = table.MetadataProperties.Contains("Table")
                        && table.MetadataProperties["Table"].Value != null
                      ? table.MetadataProperties["Table"].Value.ToString()
                      : table.Name;

                    tablesNamesList.Add(tableName);
                }
            }
        }
    }
}