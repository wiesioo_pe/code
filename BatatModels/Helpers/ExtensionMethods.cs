﻿namespace Batat.Models.Helpers
{
    public static class ExtensionMethods
    {
        public static bool IsBetween(this int number, int lower, int upper)
        {
            if((number > lower) && (number < upper))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsBetweenOrEqual(this int number, int lower, int upper)
        {
            if ((number >= lower) && (number <= upper))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}