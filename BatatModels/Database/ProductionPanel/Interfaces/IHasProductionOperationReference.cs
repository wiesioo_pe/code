﻿namespace Batat.Models.Database.ProductionPanel.Interfaces
{
    public interface IHasProductionOperationReference
    {
        int ProductionOperationId { get; set; }
        string ProductionOperationUUID { get; set; }
    }
}
