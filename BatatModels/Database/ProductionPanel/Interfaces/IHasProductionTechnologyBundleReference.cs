﻿namespace Batat.Models.Database.ProductionPanel.Interfaces
{
    public interface IHasProductionTechnologyBundleReference
    {
        int ProductionTechnologyBundleId { get; set; }
        string ProductionTechnologyBundleUUID { get; set; }
    }
}
