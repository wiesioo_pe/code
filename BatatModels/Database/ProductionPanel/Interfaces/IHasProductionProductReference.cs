﻿namespace Batat.Models.Database.ProductionPanel.Interfaces
{
    public interface IHasProductionProductReference
    {
        int ProductionProductId { get; set; }
        string ProductionProductUUID { get; set; }
    }
}
