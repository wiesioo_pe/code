﻿namespace Batat.Models.Database.ProductionPanel.Interfaces
{
    public interface IHasProductionPartReference
    {
        int ProductionPartId { get; set; }
        string ProductionPartUUID { get; set; }
    }
}
