﻿namespace Batat.Models.Database.ProductionPanel.Interfaces
{
    public interface IHasProductionTechnologyReference
    {
        int ProductionTechnologyId { get; set; }
        string ProductionTechnologyUUID { get; set; }
    }
}
