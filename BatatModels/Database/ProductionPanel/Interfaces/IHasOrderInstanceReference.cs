﻿namespace Batat.Models.Database.ProductionPanel.Interfaces
{
    public interface IHasOrderInstanceReference
    {
        int OrderInstanceId { get; set; }
        string OrderInstanceUUID { get; set; }
    }
}
