﻿using System.Collections.Generic;

namespace Batat.Models.Database.ProductionPanel
{
    public class TechnologyPickVM
    {
        public string PartUUID { get; set; }
        public string TechnologyUUID { get; set; }

        public List<TechnologyPickProductionProductAmountVM> Amounts { get; set; } = new List<TechnologyPickProductionProductAmountVM>();
    }

    public class TechnologyPickProductionProductAmountVM
    {
        public string ProductionProductUUID { get; set; }
        public int PlanAmount { get; set; }
    }
}