﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.ProductionPanel.Interfaces;

namespace Batat.Models.Database.ProductionPanel
{
    public class ProductionTechnology: IHasProductionTechnologyBundleReference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionPartId { get; set; }
        public string ProductionPartUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ProductionTechnologyBundleId { get; set; }
        public string ProductionTechnologyBundleUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public virtual List<ProductionOperation> ProductionOperations { get; set; } = new List<ProductionOperation>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        /// /// <summary>
        /// DO NOT USE!!! FOR ENTITY FRAMEWORK ONLY!!!
        /// </summary>
        public ProductionTechnology() { }

        public ProductionTechnology(
            ProductionPart productionPart,
            Technology technology,
            int toDoAmount,
            int productionTechnologyBundleId = 0,
            string productionTechnologyBundleUUID = null)
        {
            TechnologyId = technology.Id;
            TechnologyUUID = technology.UUID;
            OrderInstanceId = productionPart.OrderInstanceId;
            OrderInstanceUUID = productionPart.OrderInstanceUUID;
            ProductionPartId = productionPart.Id;
            ProductionPartUUID = productionPart.UUID;
            ProductionProductId = productionPart.ProductionProductId;
            ProductionProductUUID = productionPart.ProductionProductUUID;
            ToDoAmount = toDoAmount;
            ProductionTechnologyBundleId = productionTechnologyBundleId;
            ProductionTechnologyBundleUUID = productionTechnologyBundleUUID;
        }
    }
}