﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Database.ProductionPanel
{
    public class OrderInstance : IProductionProductsHolder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public int OrderId { get; set; }
        public string OrderUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public virtual List<ProductionProduct> ProductionProducts { get; set; } = new List<ProductionProduct>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        /// /// <summary>
        /// DO NOT USE!!! FOR ENTITY FRAMEWORK ONLY!!!
        /// </summary>
        public OrderInstance() { }

        private OrderInstance(Order order)
        {
            OrderId = order.Id;
            OrderUUID = order.UUID;
        }

        public static OrderInstance CreateInDatabase(
            Order order,
            ApplicationDataDbContext context)
        {
            var orderInstance = new OrderInstance(order);
            context.OrderInstances.Add(orderInstance);
            context.SaveChanges();

            orderInstance.ProductionProducts.Create(null, order.Products, orderInstance.Id, orderInstance.UUID, context);
            orderInstance.ToDoAmount = orderInstance.ProductionProducts.Sum(a => a.ToDoAmount);
            context.SaveChanges();

            return orderInstance;
        } 
    }

    public static class ProductionProductExtensions
    {
        public static void Create(
            this List<ProductionProduct> productionProducts,
            ProductionProduct parentProductionProduct,
            HashSet<ProductIdAmountDiscountClass> products,
            int orderInstanceId,
            string orderInstanceUUID,
            ApplicationDataDbContext context)
        {
            foreach (var productIdAmount in products)
            {
                var product = context.Products.Find(productIdAmount.ProductId);
                int toDoAmount = productIdAmount.Amount * product.GetToDoAmount(context);

                var productionProduct = new ProductionProduct(product.Id, product.UUID, orderInstanceId, orderInstanceUUID, parentProductionProduct, toDoAmount);
                productionProducts.Add(productionProduct);
                context.SaveChanges();

                productionProduct.ProductionProducts.Create(productionProduct, product.Products, orderInstanceId, orderInstanceUUID, context);
                productionProduct.ProductionParts.Create(productionProduct, productIdAmount.Amount, product.Parts, context);
                context.SaveChanges();
            }
        }
    }

    public static class ProductionPartExtensions
    {
        public static void Create(
            this List<ProductionPart> productionParts,
            ProductionProduct productionProduct,
            int productAmount,
            HashSet<PartIdAmountClass> parts,
            ApplicationDataDbContext context)
        {
            foreach (var partIdAmount in parts)
            {
                var part = context.Parts.Find(partIdAmount.PartId);
                var productionPart = new ProductionPart(part.Id, part.UUID, productionProduct, productAmount, partIdAmount.Amount);
                productionParts.Add(productionPart);
                context.SaveChanges();
            }
        }
    }
}