﻿using System.Collections.Generic;

namespace Batat.Models.Database.ProductionPanel
{
    public interface IProductionProductsHolder
    {
        List<ProductionProduct> ProductionProducts { get; set; }
    }
}
