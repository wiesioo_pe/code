﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Batat.Models.Database.ProductionPanel.Extensions
{
    public static class AmountsExtensions
    {
        public static void UpdateMadeAndPlannedAmounts(this ProductionOperation productionOperation, IEnumerable<Box> boxes)
        {
            productionOperation.MadeAmount = boxes.Sum(a => a.MadeAmount);
            productionOperation.PlannedPercent = boxes.Sum(a => a.ToDoAmount) / productionOperation.ToDoAmount * 100;
            productionOperation.LastUpdate = DateTime.Now;
        }

        public static void UpdateMadeAndPlannedAmounts(this ProductionTechnology productionTechnology)
        {
            productionTechnology.MadeAmount = productionTechnology.ProductionOperations.Min(a => a.MadeAmount);
            productionTechnology.PlannedPercent = productionTechnology.ProductionOperations.Sum(a => a.PlannedPercent) / productionTechnology.ProductionOperations.Count;
            productionTechnology.LastUpdate = DateTime.Now;
        }

        public static void UpdateMadeAndPlannedAmounts(this ProductionPart productionPart)
        {
            productionPart.MadeAmount = productionPart.ProductionTechnologies.Sum(a => a.MadeAmount);
            productionPart.PlannedPercent = productionPart.ProductionTechnologies.Sum(a => Math.Round(a.ToDoAmount * a.PlannedPercent / 100)) / productionPart.ToDoAmount * 100;
            productionPart.TechnologyPickedAmount = productionPart.ProductionTechnologies.Sum(a => a.ToDoAmount);
            productionPart.LastUpdate = DateTime.Now;
        }

        public static void UpdateMadeAndPlannedAmounts(this ProductionProduct productionProduct)
        {
            productionProduct.MadeAmount = productionProduct.ProductionParts.Min(a => a.MadeAmount);
            productionProduct.PlannedPercent = productionProduct.ProductionParts.Sum(a => a.ToDoAmount * a.PlannedPercent / 100) / productionProduct.ToDoAmount * 100;
            if (productionProduct.ProductionProducts.Any())
            {
                productionProduct.PlannedPercent += productionProduct.ProductionProducts.Sum(a => Math.Round(a.ToDoAmount * a.PlannedPercent / 100)) / productionProduct.ToDoAmount * 100;
            }
            productionProduct.LastUpdate = DateTime.Now;
        }

        public static void UpdateMadeAndPlannedAmounts(this OrderInstance orderInstance)
        {
            orderInstance.MadeAmount = orderInstance.ProductionProducts.Sum(a => a.MadeAmount);
            orderInstance.PlannedPercent = orderInstance.ProductionProducts.Sum(a => Math.Round(a.ToDoAmount * a.PlannedPercent / 100)) / orderInstance.ToDoAmount * 100;
            orderInstance.LastUpdate = DateTime.Now;
        }
    }
}
