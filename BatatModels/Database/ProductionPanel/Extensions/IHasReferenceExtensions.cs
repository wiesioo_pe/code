﻿using Batat.Models.Database.ProductionPanel.Interfaces;

namespace Batat.Models.Database.ProductionPanel.Extensions
{
    public static class IHasReferenceExtensions
    {
        public static void CopyOrderInstanceReference(this IHasOrderInstanceReference target, IHasOrderInstanceReference source)
        {
            target.OrderInstanceId = source.OrderInstanceId;
            target.OrderInstanceUUID = source.OrderInstanceUUID;
        }

        public static void CopyProductionPartReference(this IHasProductionPartReference target, IHasProductionPartReference source)
        {
            target.ProductionPartId = source.ProductionPartId;
            target.ProductionPartUUID = source.ProductionPartUUID;
        }

        public static void CopyProductionProductReference(this IHasProductionProductReference target, IHasProductionProductReference source)
        {
            target.ProductionProductId = source.ProductionProductId;
            target.ProductionProductUUID = source.ProductionProductUUID;
        }

        public static void CopyProductionTechnologyReference(this IHasProductionTechnologyReference target, IHasProductionTechnologyReference source)
        {
            target.ProductionTechnologyId = source.ProductionTechnologyId;
            target.ProductionTechnologyUUID = source.ProductionTechnologyUUID;
        }

        public static void CopyProductionTechnologyBundleReference(this IHasProductionTechnologyBundleReference target, IHasProductionTechnologyBundleReference source)
        {
            target.ProductionTechnologyBundleId = source.ProductionTechnologyBundleId;
            target.ProductionTechnologyBundleUUID = source.ProductionTechnologyBundleUUID;
        }

        public static void CopyProductionOperationReference(this IHasProductionOperationReference target, IHasProductionOperationReference source)
        {
            target.ProductionOperationId = source.ProductionOperationId;
            target.ProductionOperationUUID = source.ProductionOperationUUID;
        }
    }
}
