﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.ProductionPanel.Interfaces;

namespace Batat.Models.Database.ProductionPanel
{
    public class ProductionOperation : IHasOrderInstanceReference, IHasProductionPartReference, IHasProductionProductReference, IHasProductionTechnologyReference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionPartId { get; set; }
        public string ProductionPartUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ProductionTechnologyId { get; set; }
        public string ProductionTechnologyUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public virtual List<BoxIdClass> Boxes { get; set; } = new List<BoxIdClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        /// /// <summary>
        /// DO NOT USE!!! FOR ENTITY FRAMEWORK ONLY!!!
        /// </summary>
        public ProductionOperation() { }

        public ProductionOperation(OperationIdClass operationIdClass, int toDoAmount)
        {
            OperationId = operationIdClass.OperationId;
            OperationUUID = operationIdClass.OperationUUID;
            ToDoAmount = toDoAmount;
        }

        public ProductionOperation(ProductionTechnology productionTechnology, OperationIdClass operationIdClass, int toDoAmount)
        {
            OrderInstanceId = productionTechnology.OrderInstanceId;
            OrderInstanceUUID = productionTechnology.OrderInstanceUUID;
            ProductionPartId = productionTechnology.ProductionPartId;
            ProductionPartUUID = productionTechnology.ProductionPartUUID;
            ProductionProductId = productionTechnology.ProductionProductId;
            ProductionProductUUID = productionTechnology.ProductionProductUUID;
            ProductionTechnologyId = productionTechnology.Id;
            ProductionTechnologyUUID = productionTechnology.UUID;
            OperationId = operationIdClass.OperationId;
            OperationUUID = operationIdClass.OperationUUID;
            ToDoAmount = toDoAmount;
        }
    }
}