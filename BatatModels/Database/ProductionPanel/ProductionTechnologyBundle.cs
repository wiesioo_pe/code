﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Database.ProductionPanel
{
    public class ProductionTechnologyBundle
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int ToDoAmount { get; set; }
        public virtual List<ProductionTechnologyIdClass> ProductionTechnologies { get; set; } = new List<ProductionTechnologyIdClass>();

        /// /// <summary>
        /// DO NOT USE!!! FOR ENTITY FRAMEWORK ONLY!!!
        /// </summary>
        public ProductionTechnologyBundle() { }

        public ProductionTechnologyBundle(
            TechnologyPickVM technologyPickVm,
            ApplicationDataDbContext applicationDataDbContext)
        {
            var technology = applicationDataDbContext.Technologies.Single(a => a.UUID == technologyPickVm.TechnologyUUID);
            TechnologyId = technology.Id;
            TechnologyUUID = technology.UUID;

            var part = applicationDataDbContext.Parts.Single(a => a.UUID == technologyPickVm.PartUUID);
            PartId = part.Id;
            PartUUID = part.UUID;
        }
    }

    public class ProductionTechnologyBundleVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int ToDoAmount { get; set; }
        public List<ProductionTechnologyIdClassVM> ProductionTechnologies { get; set; } = new List<ProductionTechnologyIdClassVM>();

        public ProductionTechnologyBundleVM() { }

        public ProductionTechnologyBundleVM(ProductionTechnologyBundle productionTechnologyBundle)
        {
            Id = productionTechnologyBundle.Id;
            UUID = productionTechnologyBundle.UUID;
            TechnologyId = productionTechnologyBundle.TechnologyId;
            TechnologyUUID = productionTechnologyBundle.TechnologyUUID;
            PartId = productionTechnologyBundle.PartId;
            PartUUID = productionTechnologyBundle.PartUUID;
            ToDoAmount = productionTechnologyBundle.ToDoAmount;
            ProductionTechnologies = productionTechnologyBundle.ProductionTechnologies.Select(a => new ProductionTechnologyIdClassVM(a)).ToList();
        }
    }

    public class ProductionTechnologyBundleOrderProductAmount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int PlanAmount { get; set; }

        public ProductionTechnologyBundleOrderProductAmount() { }
        public ProductionTechnologyBundleOrderProductAmount(
            TechnologyPickProductionProductAmountVM productionProductAmount,
            ApplicationDataDbContext applicationDataDbContext)
        {
            var productionProduct = applicationDataDbContext.ProductionProducts
                .Single(a => a.UUID == productionProductAmount.ProductionProductUUID);
            ProductId = productionProduct.ProductId;

            var orderInstance = applicationDataDbContext.OrderInstances
                .Single(a => a.UUID == productionProduct.OrderInstanceUUID);
            OrderId = orderInstance.OrderId;

            PlanAmount = productionProductAmount.PlanAmount;
        }
    }

    public class ProductionTechnologyIdClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ProductionTechnologyId { get; set; }
        public string ProductionTechnologyUUID { get; set; }

        public ProductionTechnologyIdClass() { }
        public ProductionTechnologyIdClass(ProductionTechnology productionTechnology)
        {
            ProductionTechnologyId = productionTechnology.Id;
            ProductionTechnologyUUID = productionTechnology.UUID;
        }
    }

    public class ProductionTechnologyIdClassVM
    {
        public int ProductionTechnologyId { get; set; }
        public string ProductionTechnologyUUID { get; set; }

        public ProductionTechnologyIdClassVM() { }
        public ProductionTechnologyIdClassVM(ProductionTechnologyIdClass productionTechnologyIdClass)
        {
            ProductionTechnologyId = productionTechnologyIdClass.ProductionTechnologyId;
            ProductionTechnologyUUID = productionTechnologyIdClass.ProductionTechnologyUUID;
        }
    }
}