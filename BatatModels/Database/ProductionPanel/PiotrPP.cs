﻿using System.Collections.Generic;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Database.ProductionPanel
{
    public class PiotrPP_OrderInstanceVM
    {
        public string PartCode { get; set; }
        public string PartName { get; set; }
        public string PartUUID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductUUID { get; set; }
        public string OrderDetailUUID { get; set; }
        public string OrderName { get; set; }
        public string OrderRequestorName { get; set; }
        public string OrderRequestorUUID { get; set; }
        public string OrderUUID { get; set; }
        public string PartsDone { get; set; }
        public string PartsOrdered { get; set; }
        public string PartsScheduledPercent { get; set; }


        public PiotrPP_OrderInstanceVM() { }
        public PiotrPP_OrderInstanceVM(
            OrderInstance orderInstance,
            string partUuid,
            string productUuid,
            ApplicationDataDbContext context)
        {
            var productionParts = context.ProductionParts.Where(a => a.PartUUID == partUuid);
            PartsDone = productionParts.Sum(a => a.MadeAmount).ToString();
            PartsOrdered = productionParts.Sum(a => a.ToDoAmount).ToString();
            PartsScheduledPercent = (productionParts.Sum(a => a.PlannedPercent) / productionParts.Count()).ToString();

            if (context.Parts.Any(a => a.UUID == partUuid))
            {
                var part = context.Parts.Single(a => a.UUID == partUuid);
                PartUUID = part.UUID;
                PartCode = part.Code;
                PartName = part.Name;
                OrderDetailUUID = "z" + orderInstance.Id + "d" + part.Id;
            }

            if (context.Products.Any(a => a.UUID == productUuid))
            {
                var product = context.Products.Single(a => a.UUID == productUuid);
                ProductUUID = product.UUID;
                ProductCode = product.Code;
                ProductName = product.Name;
                OrderDetailUUID += "p" + product.Id;
            }

            var order = context.Orders.Find(orderInstance.OrderId);
            if (order != null)
            {
                OrderUUID = order.UUID;
                OrderName = "Zamówienie " + order.Id;
                OrderRequestorName = context.Subjects.Find(order.SubjectId)?.Name;
                OrderRequestorUUID = order.SubjectId.ToString();
            }
        }
    }



    public class PiotrPP_PartTechnologyVM
    {
        public string Code { get; set; }
        public string PartUUID { get; set; }
        public string Name { get; set; }
        public string TechnologyUUID { get; set; }

        public List<PiotrPP_Operation> operations { get; set; } = new List<PiotrPP_Operation>();


        public PiotrPP_PartTechnologyVM(Technology technology, ApplicationDataDbContext context)
        {
            Code = technology.Code;
            PartUUID = technology.PartUUID;
            Name = technology.Name;
            TechnologyUUID = technology.UUID;
            foreach (var operationId in technology.Operations)
            {
                var operation = context.Operations.Find(operationId.OperationId);
                if (operation != null)
                {
                    operations.Add(new PiotrPP_Operation(operation, context));
                }
            }
        }
    }



    public class PiotrPP_Operation
    {
        public string Code { get; set; }
        public string OperationUUID { get; set; }
        public decimal PreparationTime { get; set; }
        public string Type { get; set; }
        public decimal UnitaryTime { get; set; }

        public List<PiotrPP_Machine> Machines { get; set; } = new List<PiotrPP_Machine>();

        public PiotrPP_Operation(Operation operation, ApplicationDataDbContext context)
        {
            Code = operation.Code;
            //this.operationUUID = Operation.OperationId.ToString();
            OperationUUID = operation.UUID;
            var operationType = context.OperationTypes.Find(operation.OperationTypeId);
            if (operationType != null)
            {
                Type = operationType.Name;
            }

            foreach (var machine in operation.MachinesTimesCosts)
            {
                if (machine.MachineId == 1)
                {
                    PreparationTime = machine.PCTime * 60;
                    UnitaryTime = machine.UnitTime * 60;
                }

                Machines.Add(new PiotrPP_Machine(machine, context));
            }
        }
    }


    public class PiotrPP_Machine
    {
        public string Code { get; set; }
        public string UUID { get; set; }
        public decimal PreparationTime { get; set; }
        public decimal UnitaryTime { get; set; }
        public List<PiotrPP_PlainUUID> Operators = new List<PiotrPP_PlainUUID>();

        public PiotrPP_Machine(MachineIdTimeCostClass machineIdTimeCostClass, ApplicationDataDbContext context)
        {
            //this.UUID = MachineIdTimeCostClass.MachineId.ToString();
            PreparationTime = machineIdTimeCostClass.PCTime * 60;
            UnitaryTime = machineIdTimeCostClass.UnitTime * 60;

            var machine = context.Machines.Find(machineIdTimeCostClass.MachineId);
            if (machine == null)
                return;

            Code = machine.Code;
            UUID = machine.UUID;
            var machineOperators = machine.Operators.ToList();
            foreach (var oper in machineOperators)
            {
                var operInstance = context.Operators.Find(oper.OperatorId);
                var operVm = new PiotrPP_PlainUUID(operInstance?.UUID);
                this.Operators.Add(operVm);
            }

        }
    }

    public class PiotrPP_PlainUUID
    {
        public string UUID { get; set; }
        public PiotrPP_PlainUUID(string uuid)
        {
            UUID = uuid;
        }
    }


    public class PiotrPP_OrderPartApiModel
    {
        public List<PiotrPP_OrderProductPairs> OrderProductPairs;
        public string PartUUID;
    }

    public class PiotrPP_OrderProductPairs
    {
        public string OrderUUID { get; set; }
        public string ProductUUID { get; set; }
    }
}