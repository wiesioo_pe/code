﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel.Interfaces;

namespace Batat.Models.Database.ProductionPanel
{
    public class Box: IHasOrderInstanceReference, IHasProductionPartReference, IHasProductionProductReference, IHasProductionTechnologyReference
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int PrecedessorId { get; set; }
        public string PrecedessorUUID { get; set; }
        public int DescendantId { get; set; }
        public string DescendantUUID { get; set; }

        public int OrderId { get; set; }
        public string OrderUUID { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int ToDoAmount { get; set; }
        public int MadeAmount { get; set; }
        public int TrolleyId { get; set; }
        public string TrolleyUUID { get; set; }
        public int OperationBundleOrder { get; set; } = 1;

        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionPartId { get; set; }
        public string ProductionPartUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ProductionTechnologyId { get; set; }
        public string ProductionTechnologyUUID { get; set; }
        public int ProductionOperationId { get; set; }
        public string ProductionOperationUUID { get; set; }

        public virtual List<TrolleyEventClass> Events { get; set; } = new List<TrolleyEventClass>();
        public virtual List<NoteClass> Notes { get; set; } = new List<NoteClass>();
        public virtual List<MessageClass> Messages { get; set; } = new List<MessageClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class BoxVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }
        public int PrecedessorId { get; set; }
        public string PrecedessorUUID { get; set; }
        public int DescendantId { get; set; }
        public string DescendantUUID { get; set; }

        public int OrderId { get; set; }
        public string OrderUUID { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int MachineId { get; set; }
        public string MachineUUID { get; set; }
        public int OperatorId { get; set; }
        public string OperatorUUID { get; set; }
        public int ToDoAmount { get; set; }
        public int OrderAmount => ToDoAmount;
        public int MadeAmount { get; set; }
        public int OperationBundleId { get; set; }
        public string OperationBundleUUID { get; set; }
        public int OperationBundleOrder { get; set; }
        public bool OperationScheduled = true;

        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionPartId { get; set; }
        public string ProductionPartUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ProductionTechnologyId { get; set; }
        public string ProductionTechnologyUUID { get; set; }
        public int ProductionOperationId { get; set; }
        public string ProductionOperationUUID { get; set; }

        public List<TrolleyEventClassVM> Events { get; set; } = new List<TrolleyEventClassVM>();
        public List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<MessageClassVM> Messages { get; set; } = new List<MessageClassVM>();

        public DateTime LastUpdate { get; set; }


        public BoxVM() { }
        public BoxVM(Box box)
        {
            Id = box.Id;
            UUID = box.UUID;

            OrderId = box.OrderId; OrderUUID = box.OrderUUID;
            PartId = box.PartId; PartUUID = box.PartUUID;
            ProductId = box.ProductId; ProductUUID = box.ProductUUID;
            TechnologyId = box.TechnologyId; TechnologyUUID = box.TechnologyUUID;
            OperationId = box.OperationId; OperationUUID = box.OperationUUID;
            ToDoAmount = box.ToDoAmount;
            MadeAmount = box.MadeAmount;
            LastUpdate = box.LastUpdate;
            OperationBundleOrder = box.OperationBundleOrder;
            OperationBundleUUID = box.TrolleyUUID;
            OperationBundleId = box.TrolleyId;

            OrderInstanceId = box.OrderInstanceId;
            OrderInstanceUUID = box.OrderInstanceUUID;
            ProductionPartId = box.ProductionPartId;
            ProductionPartUUID = box.ProductionPartUUID;
            ProductionProductId = box.ProductionProductId;
            ProductionProductUUID = box.ProductionProductUUID;
            ProductionTechnologyId = box.ProductionTechnologyId;
            ProductionTechnologyUUID = box.ProductionTechnologyUUID;
            ProductionOperationId = box.ProductionOperationId;
            ProductionOperationUUID = box.ProductionOperationUUID;

            Events = box.Events.Select(a => new TrolleyEventClassVM(a)).ToList();
            Notes = box.Notes.Select(a => new NoteClassVM(a)).ToList();
            Messages = box.Messages.Select(a => new MessageClassVM(a)).ToList();
        }
    }

    public class BoxExtendedInfoVM : BoxVM
    {
        public PiotrPP_OrderInstanceVM OrderData;
        public PiotrPP_Operation OperationData;
        public decimal DefaultUnitaryTime { get { if (this.OperationData != null) { return this.OperationData.UnitaryTime; } else return 0; } }
        public decimal DefaultPreparationTime { get { if (this.OperationData != null) { return this.OperationData.PreparationTime; } else return 0;  } }

        public BoxExtendedInfoVM(
            Box box,
            ApplicationDataDbContext context)
            : base(box)
        {
            //this.ToDoAmount = 0;
            //this.MadeAmount = 0;
            var orderInstance = context.OrderInstances.First(a => a.OrderId == OrderId);
            this.OrderData = new PiotrPP_OrderInstanceVM(orderInstance, PartUUID, ProductUUID, context);

            //var InfrastructureDbContext = Batat.Models.InfrastructureDbContext.Create();
            //var Technology = TechnologiesDbContext.Technologies.Where(a => (a.PartId == this.PartId && a.TechnologyId == this.TechnologyId)).First();
            //this.operationData = new PiotrPP_PartTechnologyVM(Technology, TechnologiesDbContext, InfrastructureDbContext);

            var operation = context.Operations.Find(OperationId);
            if (operation != null)
            {
                this.OperationData = new PiotrPP_Operation(operation, context);
            }
        }
    }

    public class OperationForSchedulingVM : BoxVM
    {
        public List<BoxExtendedInfoVM> Operations = new List<BoxExtendedInfoVM>();

        public OperationForSchedulingVM(Box box) : base(box)
        {
            ToDoAmount = 0;
            MadeAmount = 0;
        }
    }


    public class TrolleyEventClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int EventType { get; set; }
        public DateTime Time { get; set; }
        public string Note { get; set; }
    }

    public class TrolleyEventClassVM
    {
        public int Id { get; set; }
        public int EventType { get; set; }
        public DateTime Time { get; set; }
        public string Note { get; set; }

        public TrolleyEventClassVM(TrolleyEventClass operationEventClass)
        {
            Id = operationEventClass.Id;
            EventType = operationEventClass.EventType;
            Time = operationEventClass.Time;
            Note = operationEventClass.Note;
        }

    }

    public enum EventType
    {

    }

    public class BoxIdClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int BoxId { get; set; }
        public string BoxUUID { get; set; }

        public BoxIdClass() { }
        public BoxIdClass(int BoxId, string BoxUUID)
        {
            this.BoxId = BoxId;
            this.BoxUUID = BoxUUID;
        }
    }

    public class BoxIdClassVM
    {
        public int BoxId { get; set; }
        public string BoxUUID { get; set; }

        public BoxIdClassVM(BoxIdClass BoxIdClass)
        {
            BoxId = BoxIdClass.BoxId;
            BoxUUID = BoxIdClass.BoxUUID;
        }
    }
}