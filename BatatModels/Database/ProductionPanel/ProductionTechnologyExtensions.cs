﻿namespace Batat.Models.Database.ProductionPanel
{
    public static class ProductionTechnologyExtensions
    {
        public static void UpdateProductionTechnologyBundleId(
            this ProductionTechnology productionTechnology,
            ProductionTechnologyBundle productionTechnologyBundle)
        {
            productionTechnology.ProductionTechnologyBundleId = productionTechnologyBundle.Id;
            productionTechnology.ProductionTechnologyBundleUUID = productionTechnologyBundle.UUID;
        }
    }
}
