﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.ProductionPanel
{
    public class ProductionPart
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public  int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public int TechnologyPickedAmount { get; set; }

        public virtual List<ProductionTechnology> ProductionTechnologies { get; set; } = new List<ProductionTechnology>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        /// /// <summary>
        /// DO NOT USE!!! FOR ENTITY FRAMEWORK ONLY!!!
        /// </summary>
        public ProductionPart() { }

        public ProductionPart(
            int partId,
            string partUuid,
            ProductionProduct productionProduct,
            int productsAmount,
            int toDoAmount)
        {
            PartId = partId;
            PartUUID = partUuid;
            ToDoAmount = toDoAmount * productsAmount;
            OrderInstanceId = productionProduct.OrderInstanceId;
            OrderInstanceUUID = productionProduct.OrderInstanceUUID;
            ProductionProductId = productionProduct.Id;
            ProductionProductUUID = productionProduct.UUID;
        }
    }
}