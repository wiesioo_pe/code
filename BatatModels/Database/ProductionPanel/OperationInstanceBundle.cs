﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.ProductionPanel.Interfaces;

namespace Batat.Models.Database.ProductionPanel
{
    public class Trolley: IHasProductionTechnologyBundleReference
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int ToDoAmount { get; set; }
        public int MadeAmount { get; set; }

        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int MachineId { get; set; }
        public string MachineUUID { get; set; }
        public int OperatorId { get; set; }
        public string OperatorUUID { get; set; }

        public int ProductionTechnologyBundleId { get; set; }
        public string ProductionTechnologyBundleUUID { get; set; }

        public int StateId { get; set; } = 0;
        public int ScheduleErrors { get; set; } = 0;
        public bool isMachineTimeTaken { get; set; } = false;

        public DateTime? PlannedStartTime { get; set; }
        public DateTime? PlannedEndTime { get; set; }

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        public virtual List<BoxIdClass> Boxes { get; set; } = new List<BoxIdClass>();
        public virtual List<TrolleyEventClass> Events { get; set; } = new List<TrolleyEventClass>();
        public virtual List<NoteClass> Notes { get; set; } = new List<NoteClass>();
        public virtual List<MessageClass> Messages { get; set; } = new List<MessageClass>();
    }

    public class TrolleyVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public int ToDoAmount { get; set; }
        public int MadeAmount { get; set; }

        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int MachineId { get; set; }
        public string MachineUUID { get; set; }
        public int OperatorId { get; set; }
        public string OperatorUUID { get; set; }

        public int ProductionTechnologyBundleId { get; set; }
        public string ProductionTechnologyBundleUUID { get; set; }

        public int StateId { get; set; } = 0;
        public int ScheduleErrors { get; set; } = 0;
        public bool isMachineTimeTaken { get; set; }

        public DateTime? PlannedStartTime { get; set; }
        public DateTime? PlannedEndTime { get; set; }

        public DateTime LastUpdate { get; set; }

        public List<BoxIdClassVM> Boxes { get; set; } = new List<BoxIdClassVM>();
        public List<TrolleyEventClassVM> Events { get; set; } = new List<TrolleyEventClassVM>();
        public List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<MessageClassVM> Messages { get; set; } = new List<MessageClassVM>();


        public TrolleyVM(Trolley trolley)
        {
            Id = trolley.Id;
            UUID = trolley.UUID;
            ToDoAmount = trolley.ToDoAmount;
            MadeAmount = trolley.MadeAmount;
            OperationId = trolley.OperationId;
            OperationUUID = trolley.OperationUUID;
            TechnologyId = trolley.TechnologyId;
            TechnologyUUID = trolley.TechnologyUUID;
            PartId = trolley.PartId;
            PartUUID = trolley.PartUUID;
            MachineId = trolley.MachineId;
            MachineUUID = trolley.MachineUUID;
            OperatorId = trolley.OperatorId;
            OperatorUUID = trolley.OperatorUUID;
            ProductionTechnologyBundleId = trolley.ProductionTechnologyBundleId;
            ProductionTechnologyBundleUUID = trolley.ProductionTechnologyBundleUUID;
            StateId = trolley.StateId;
            ScheduleErrors = trolley.ScheduleErrors;
            isMachineTimeTaken = trolley.isMachineTimeTaken;
            PlannedStartTime = trolley.PlannedStartTime;
            PlannedEndTime = trolley.PlannedEndTime;
            LastUpdate = trolley.LastUpdate;

            Boxes = trolley.Boxes.Select(a => new BoxIdClassVM(a)).ToList();
            Events = trolley.Events.Select(a => new TrolleyEventClassVM(a)).ToList();
            Notes = trolley.Notes.Select(a => new NoteClassVM(a)).ToList();
            Messages = trolley.Messages.Select(a => new MessageClassVM(a)).ToList();
        }
    }
}