﻿namespace Batat.Models.Database.ProductionPanel
{
    public static class ProductionTechnologyBundleExtensions
    {
        public static void AddProductionTechnology(this ProductionTechnologyBundle productionTechnologyBundle, ProductionTechnology productionTechnology)
        {
            productionTechnologyBundle.ProductionTechnologies.Add(new ProductionTechnologyIdClass(productionTechnology));
            productionTechnologyBundle.ToDoAmount += productionTechnology.ToDoAmount;
        }
    }
}
