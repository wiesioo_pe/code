﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.ProductionPanel
{
    public class ProductionProduct: IProductionProductsHolder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int OrderInstanceId { get; set; }
        public string OrderInstanceUUID { get; set; }
        public int ProductionProductId { get; set; }
        public string ProductionProductUUID { get; set; }
        public int ToDoAmount { get; set; }
        public decimal PlannedPercent { get; set; }
        public int MadeAmount { get; set; }
        public virtual List<ProductionProduct> ProductionProducts { get; set; } = new List<ProductionProduct>();
        public virtual List<ProductionPart> ProductionParts { get; set; } = new List<ProductionPart>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;

        /// /// <summary>
        /// DO NOT USE!!! FOR ENTITY FRAMEWORK ONLY!!!
        /// </summary>
        public ProductionProduct() { }

        public ProductionProduct(
            int productId,
            string productUuid,
            int orderInstanceId,
            string orderInstanceUUID,
            ProductionProduct parentProductionProduct,
            int toDoAmount)
        {
            ProductId = productId;
            ProductUUID = productUuid;
            ToDoAmount = toDoAmount;
            OrderInstanceId = orderInstanceId;
            OrderInstanceUUID = orderInstanceUUID;
            ProductionProductId = parentProductionProduct?.Id ?? 0;
            ProductionProductUUID = parentProductionProduct?.UUID;
        }
    }
}