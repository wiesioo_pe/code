﻿using System;

namespace Batat.Models.Database.ProductionPanel
{
    public class BoxScheduleVM
    {
        public string BoxUUID { get; set; }
        public string TrolleyUUID { get; set; }
        public string MachineUUID { get; set; }
        public int Amount { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
