﻿using System.Data.Entity;
using Batat.Models.API;
using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.MachineMonitorLite;
using Batat.Models.Database.ProductionPanel;
using Batat.Models.Helpers.ProductionPanel;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Batat.Models.Database.DbContexts
{
    public class ApplicationDataDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Technology> Technologies { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Operator> Operators { get; set; }
        public DbSet<Tool> Tools { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<OperationType> OperationTypes { get; set; }
        public DbSet<MachineType> MachineTypes { get; set; }
        public DbSet<ToolType> ToolTypes { get; set; }
        public DbSet<MaterialType> MaterialTypes { get; set; }
        public DbSet<ViewSettings> ViewSettings { get; set; }
        public DbSet<FixedCostType> FixedCostTypes { get; set; }
        public DbSet<FixedCostCategory> FixedCostCategories { get; set; }
        public DbSet<FixedCost> FixedCosts { get; set; }

        // MACHINE MONITOR LITE

        public DbSet<MachineMonitorLiteStatus> MachineMonitoLiteStatuses { get; set; }
        public DbSet<MachineState> MachineStates { get; set; }

        // PRODUCTION PANEL

        public DbSet<OrderInstance> OrderInstances { get; set; }
        public DbSet<ProductionPart> ProductionParts { get; set; }
        public DbSet<ProductionProduct> ProductionProducts { get; set; }
        public DbSet<ProductionTechnology> ProductionTechnologies { get; set; }
        public DbSet<ProductionOperation> ProductionOperations { get; set; }
        public DbSet<Box> Boxes { get; set; }
        public DbSet<ProductionTechnologyBundle> ProductionTechnologyBundles { get; set; }
        public DbSet<Trolley> Trolleys { get; set; }


        public ApplicationDataDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {

            base.OnModelCreating(modelBuilder);
            // modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();

            // Add any configuration or mapping stuff here

            // ORDERS
            modelBuilder.Entity<Order>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("OrderGroups"));

            modelBuilder.Entity<Order>()
                .HasMany(a => a.Products).WithMany()
                .Map(a => a.ToTable("OrderProducts"));

            modelBuilder.Entity<Order>()
                .HasMany(a => a.Complaints).WithMany()
                .Map(a => a.ToTable("OrderComplaints"));

            modelBuilder.Entity<Order>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("OrderChanges"));


            // PRODUCTS
            modelBuilder.Entity<Product>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("ProductGroups"));

            modelBuilder.Entity<Product>()
                .HasMany(a => a.Products).WithMany()
                .Map(a => a.ToTable("ProductProducts"));

            modelBuilder.Entity<Product>()
                .HasMany(a => a.Parts).WithMany()
                .Map(a => a.ToTable("ProductParts"));

            modelBuilder.Entity<Product>()
                .HasMany(a => a.Parameters).WithMany()
                .Map(a => a.ToTable("ProductParameters"));

            modelBuilder.Entity<Product>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("ProductChanges"));

            modelBuilder.Entity<Product>()
                .HasMany(a => a.FileUrlObjs).WithMany()
                .Map(a => a.ToTable("ProductFiles"));


            // PARTS
            modelBuilder.Entity<Part>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("PartGroups"));

            modelBuilder.Entity<Part>()
                .HasMany(a => a.Attachments).WithMany()
                .Map(a => a.ToTable("PartAttachments"));

            modelBuilder.Entity<Part>()
                .HasMany(a => a.Parameters).WithMany()
                .Map(a => a.ToTable("PartParameters"));

            modelBuilder.Entity<Part>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("PartChanges"));

            modelBuilder.Entity<Part>()
                .HasMany(a => a.FileUrlObjs).WithMany()
                .Map(a => a.ToTable("PartFiles"));



            // GROUPS
            modelBuilder.Entity<Group>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("GroupChanges"));

            modelBuilder.Entity<Parameter>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("ParameterChanges"));

            // TECHNOLOGIES
            modelBuilder.Entity<Technology>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("TechnologyGroups"));

            modelBuilder.Entity<Technology>()
               .HasMany(a => a.Operations).WithMany()
               .Map(a => a.ToTable("TechnologyOperations"));

            modelBuilder.Entity<Technology>()
                .HasMany(a => a.Notes).WithMany()
                .Map(a => a.ToTable("TechnologyNotes"));

            modelBuilder.Entity<Technology>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("TechnologyChanges"));


            // OPERATIONS
            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("OperationGroups"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Parameters).WithMany()
                .Map(a => a.ToTable("OperationParameters"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Tools).WithMany()
                .Map(a => a.ToTable("OperationTools"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Materials).WithMany()
                .Map(a => a.ToTable("OperationMaterials"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.MachinesTimesCosts).WithMany()
                .Map(a => a.ToTable("OperationMachines"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Notes).WithMany()
                .Map(a => a.ToTable("OperationNotes"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("OperationChanges"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.Attachments).WithMany()
                .Map(a => a.ToTable("OperationAttachments"));

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.OperationsBefore).WithMany()
                .Map(a => a.ToTable("OperationOperationsBefore"));

            modelBuilder.Entity<Operation>()
                .HasOptional(a => a.ProcessSheetFileUrlObj)
                .WithRequired(a => a.Operation);

            modelBuilder.Entity<Operation>()
                .HasMany(a => a.FileUrlObjs).WithMany()
                .Map(a => a.ToTable("OperationFiles"));


            // MACHINES
            modelBuilder.Entity<Machine>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("MachineGroups"));

            modelBuilder.Entity<Machine>()
                .HasMany(a => a.Parameters).WithMany()
                .Map(a => a.ToTable("MachineParameters"));

            modelBuilder.Entity<Machine>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("MachineChanges"));

            modelBuilder.Entity<Machine>()
                .HasMany(a => a.Notes).WithMany()
                .Map(a => a.ToTable("MachineNotes"));

            // OPERATORS
            modelBuilder.Entity<Operator>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("OperatorGroups"));

            modelBuilder.Entity<Operator>()
                .HasMany(a => a.Notes).WithMany()
                .Map(a => a.ToTable("OperatorNotes"));

            modelBuilder.Entity<Operator>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("OperatorChanges"));

            modelBuilder.Entity<Operator>()
                .HasOptional(a => a.PhotoFileUrlObj)
                .WithRequired(a => a.Operator);


            // CUSTOMERS
            modelBuilder.Entity<Subject>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("SubjectGroups"));

            modelBuilder.Entity<Subject>()
                .HasMany(a => a.Contacts).WithMany()
                .Map(a => a.ToTable("SubjectContacts"));

            modelBuilder.Entity<Subject>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("SubjectChanges"));

            modelBuilder.Entity<Subject>()
                .HasOptional(a => a.LogoFileUrlObj)
                .WithRequired(a => a.Subject);

            // CONTACTS
            modelBuilder.Entity<Contact>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("ContactGroups"));

            modelBuilder.Entity<Contact>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("ContactChanges"));

            modelBuilder.Entity<Contact>()
                .HasOptional(a => a.PhotoFileUrlObj)
                .WithRequired(a => a.Contact);

            // FIXED COSTS
            modelBuilder.Entity<FixedCost>()
                .HasMany(a => a.Groups).WithMany()
                .Map(a => a.ToTable("FixedCostGroups"));

            modelBuilder.Entity<FixedCost>()
                .HasMany(a => a.Changes).WithMany()
                .Map(a => a.ToTable("FixedCostChanges"));

            //FIXED COST TYPES
            modelBuilder.Entity<FixedCostType>()
                .HasMany(a => a.Categories).WithMany()
                .Map(a => a.ToTable("FixedCostTypeCategories"));


            // TOOLS
            modelBuilder.Entity<Tool>()
            .HasMany(a => a.Groups).WithMany()
            .Map(a => a.ToTable("ToolGroups"));

            modelBuilder.Entity<Tool>()
            .HasMany(a => a.Parameters).WithMany()
            .Map(a => a.ToTable("ToolParameters"));

            modelBuilder.Entity<Tool>()
            .HasMany(a => a.Changes).WithMany()
            .Map(a => a.ToTable("ToolChanges"));

            // MATERIALS
            modelBuilder.Entity<Material>()
            .HasMany(a => a.Groups).WithMany()
            .Map(a => a.ToTable("MaterialGroups"));

            modelBuilder.Entity<Material>()
            .HasMany(a => a.Parameters).WithMany()
            .Map(a => a.ToTable("MaterialParameters"));

            modelBuilder.Entity<Material>()
            .HasMany(a => a.Changes).WithMany()
            .Map(a => a.ToTable("MaterialChanges"));

            // PRODUCTION PANEL
            // Box
            modelBuilder.Entity<Box>()
            .HasMany(a => a.Events).WithMany()
            .Map(a => a.ToTable("BoxEvents"));

            modelBuilder.Entity<Box>()
            .HasMany(a => a.Notes).WithMany()
            .Map(a => a.ToTable("BoxNotes"));

            modelBuilder.Entity<Box>()
            .HasMany(a => a.Messages).WithMany()
            .Map(a => a.ToTable("BoxMessages"));

            // Box
            modelBuilder.Entity<Trolley>()
            .HasMany(a => a.Boxes).WithMany()
            .Map(a => a.ToTable("TrolleyBoxes"));

            modelBuilder.Entity<Trolley>()
            .HasMany(a => a.Events).WithMany()
            .Map(a => a.ToTable("TrolleyEvents"));

            modelBuilder.Entity<Trolley>()
            .HasMany(a => a.Notes).WithMany()
            .Map(a => a.ToTable("TrolleyNotes"));

            modelBuilder.Entity<Trolley>()
            .HasMany(a => a.Messages).WithMany()
            .Map(a => a.ToTable("TrolleyMessages"));

            // OrderInstance
            modelBuilder.Entity<OrderInstance>()
            .HasMany(a => a.ProductionProducts).WithMany()
            .Map(a => a.ToTable("OrderInstanceProductionProducts"));

            modelBuilder.Entity<ProductionProduct>()
            .HasMany(a => a.ProductionProducts).WithMany()
            .Map(a => a.ToTable("ProductionProductProductionProducts"));

            modelBuilder.Entity<ProductionProduct>()
                .HasMany(a => a.ProductionParts).WithMany()
                .Map(a => a.ToTable("ProductionProductProductionParts"));

            modelBuilder.Entity<ProductionPart>()
            .HasMany(a => a.ProductionTechnologies).WithMany()
            .Map(a => a.ToTable("ProductionPartProductionTechnologies"));

            modelBuilder.Entity<ProductionTechnology>()
            .HasMany(a => a.ProductionOperations).WithMany()
            .Map(a => a.ToTable("ProductionTechnologyProductionOperations"));

            modelBuilder.Entity<ProductionOperation>()
            .HasMany(a => a.Boxes).WithMany()
            .Map(a => a.ToTable("ProductionOperationBoxes"));


            // ProductionTechnologyBundle
            modelBuilder.Entity<ProductionTechnologyBundle>()
            .HasMany(a => a.ProductionTechnologies).WithMany()
            .Map(a => a.ToTable("ProductionTechnologyBundleProductionTechnologies"));
        }

        public static ApplicationDataDbContext Create()
        {
            return new ApplicationDataDbContext();
        }



        public class RecreateDatabaseInitializer : IDatabaseInitializer<ApplicationDataDbContext>
        {
            public void InitializeDatabase(ApplicationDataDbContext applicationDataDbContext)
            {
                if (applicationDataDbContext.Database.Exists())
                {
                    if (!applicationDataDbContext.Database.CompatibleWithModel(true))
                    {
                        SeedDatabaseHelper.DropAllTables();
                    }
                }
            }
        }


        static ApplicationDataDbContext()
            {
#if DEBUG
                System.Data.Entity.Database.SetInitializer<ApplicationDataDbContext>(null);
            //    Database.SetInitializer<ApplicationDataDbContext>(new RecreateDatabaseInitializer());


#else


#endif
        }

    }
}