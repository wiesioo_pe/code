﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Batat.Models.Database.MachineMonitorLite
{
    public class MachineMonitorLiteStatus
    {
        [Key]
        public int Id { get; set; }

        public int MachineId { get; set; }

        public string MachineUUID { get; set; }

        public string StatusJson { get; set; }
    }
}
