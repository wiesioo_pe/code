﻿namespace Batat.Models.Database.MachineMonitorLite
{
    public class DeviceConfigDTO
    {
        public int configNumber { get; set; }

        public string ssid1 { get; set; }

        public int firmware { get; set; }

        public string pass1 { get; set; }

        public string ssid2 { get; set; }

        public string pass2 { get; set; }

        public string serwer { get; set; }

        public int timeoutWifi { get; set; }

        public int timeoutSerwer { get; set; }

        public int connectionDelay { get; set; }

        public int connectionLimit { get; set; }

        public int interval { get; set; }

        public int counter1_enable { get; set; }

        public int counter2_enable { get; set; }
    }
}
