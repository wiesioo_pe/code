﻿namespace Batat.Models.Database.MachineMonitorLite
{
    public class ConnectionErrorDTO
    {
        public ulong timestamp { get; set; }

        public int code { get; set; }
    }
}
