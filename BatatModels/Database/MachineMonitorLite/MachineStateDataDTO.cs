﻿namespace Batat.Models.Database.MachineMonitorLite
{
    public class MachineStateDataDTO
    {
        public ulong timestamp { get; set; }

        public int state { get; set; }

        public long counter1 { get; set; }

        public long counter2 { get; set; }
    }
}
