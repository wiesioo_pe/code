﻿namespace Batat.Models.Database.MachineMonitorLite
{
    public class MachineStatusDTO
    {
        public string monitorID { get; set; }

        public int firmware { get; set; }

        public int configNumber { get; set; }

        public string SSID { get; set; }

        public int rssi { get; set; }

        public bool restart { get; set; }

        public ulong conTimestamp { get; set; }

        public DeviceConfigDTO deviceConfig { get; set; }

        public ConnectionErrorDTO[] conErrData { get; set; }

        public MachineStateDataDTO[] stateData { get; set; }
    }
}
