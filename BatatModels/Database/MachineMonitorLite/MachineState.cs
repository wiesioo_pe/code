﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Batat.Models.Database.MachineMonitorLite
{
    public class MachineState
    {
        [Key]
        public int Id { get; set; }

        public int MachineId { get; set; }

        public string MachineUUID { get; set; }

        public DateTime Time { get; set; }

        public int State { get; set; }

        public long Counter1 { get; set; }

        public long Counter2 { get; set; }

        public MachineState() { }

        public MachineState(MachineStateDataDTO machineStateDataDTO, int machineId, string machineUUID, DateTime requestTime, ulong connectionTimestamp)
        {
            MachineId = machineId;
            MachineUUID = machineUUID;
            Time = requestTime.AddSeconds(machineStateDataDTO.timestamp - connectionTimestamp);
            State = machineStateDataDTO.state;
            Counter1 = machineStateDataDTO.counter1;
            Counter2 = machineStateDataDTO.counter2;
        }
    }
}
