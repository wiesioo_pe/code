﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Batat.Models.Database.DatabasePanel
{
    // ZAMÓWIENIE
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public DateTime ReceiveDate { get; set; }
        public DateTime AccomplishDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int PriorityId { get; set; }
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }
        public int ContactId { get; set; }
        public string ContactUUID { get; set; }
        public string Desc { get; set; }
        public int StateId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeUUID { get; set; }
        public virtual HashSet<ProductIdAmountDiscountClass> Products { get; set; } = new HashSet<ProductIdAmountDiscountClass>();
        public virtual List<ComplaintClass> Complaints { get; set; } = new List<ComplaintClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class OrderVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public DateTime? ReceiveDate { get; set; }
        public DateTime? AccomplishDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int PriorityId { get; set; }
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }
        public int ContactId { get; set; }
        public string ContactUUID { get; set; }
        public string Desc { get; set; }
        public int StateId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeUUID { get; set; }
        public List<ProductIdAmountDiscountClassVM> Products { get; set; } = new List<ProductIdAmountDiscountClassVM>();
        public List<ComplaintClassVM> Complaints { get; set; } = new List<ComplaintClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public OrderVM() { }

        public OrderVM(Order order)
        {
            Id = order.Id;
            UUID = order.UUID;

            ReceiveDate = order.ReceiveDate;
            AccomplishDate = order.AccomplishDate;
            DeliveryDate = order.DeliveryDate;
            PriorityId = order.PriorityId;
            SubjectId = order.SubjectId;
            SubjectUUID = order.SubjectUUID;
            ContactId = order.ContactId;
            ContactUUID = order.ContactUUID;
            Desc = order.Desc;
            StateId = order.StateId;
            EmployeeId = order.EmployeeId;
            EmployeeUUID = order.EmployeeUUID;
            LastUpdate = order.LastUpdate;

            foreach (var group in order.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var product in order.Products)
            {
                Products.Add(new ProductIdAmountDiscountClassVM(product));
            }
            foreach (var complaint in order.Complaints)
            {
                Complaints.Add(new ComplaintClassVM(complaint));
            }
            foreach (var change in order.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
        }
    }

    public class ComplaintClass
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int Amount { get; set; }
        public DateTime Date { get; set; }
        public string Desc { get; set; }
        public decimal Cost { get; set; }
        public bool isTotal { get; set; }

        public ComplaintClass() { }
        public ComplaintClass(ComplaintClassVM complaintClassVm)
        {
            Id = complaintClassVm.Id;
            ProductId = complaintClassVm.ProductId;
            ProductUUID = complaintClassVm.ProductUUID;
            Amount = complaintClassVm.Amount;
            Date = complaintClassVm.Date;
            Desc = complaintClassVm.Desc;
            Cost = complaintClassVm.Cost ?? 0;
            isTotal = complaintClassVm.isTotal ?? false;
        }
    }

    public class ComplaintClassVM
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int Amount { get; set; }
        public DateTime Date { get; set; }
        public string Desc { get; set; }
        public decimal? Cost { get; set; }
        public bool? isTotal { get; set; }

        public ComplaintClassVM(ComplaintClass complaintClass)
        {
            Id = complaintClass.Id;
            ProductId = complaintClass.ProductId;
            ProductUUID = complaintClass.ProductUUID;
            Amount = complaintClass.Amount;
            Date = complaintClass.Date;
            Desc = complaintClass.Desc;
            Cost = complaintClass.Cost;
            isTotal = complaintClass.isTotal;
        }
    }


    // LISTY TOWARZYSZĄCE


    public static class OrdersLists
    {
        public static List<string> State { get; set; } = new List<string> { "W przygotowaniu", "Autoryzowane", "W produkcji", "Zakończone" };

        public static List<string> Priority { get; set; } = new List<string> { "Normalny", "Wysoki" };

    }
}