﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Database.DatabasePanel
{
    public class Technology
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Code { get; set; }
        public string Name { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public virtual List<OperationIdClass> Operations { get; set; } = new List<OperationIdClass>();
        public virtual List<NoteClass> Notes { get; set; } = new List<NoteClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class TechnologyVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Code { get; set; }
        public string Name { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public List<OperationIdClassVM> Operations { get; set; } = new List<OperationIdClassVM>();
        public List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public TechnologyVM() { }
        public TechnologyVM(Technology technology, ApplicationDataDbContext context)
        {
            Id = technology.Id;
            UUID = technology.UUID;

            Code = technology.Code;
            Name = technology.Name;
            PartId = technology.PartId;
            PartUUID = technology.PartUUID;
            LastUpdate = technology.LastUpdate;

            foreach (var group in technology.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in technology.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var operationIdClass in technology.Operations)
            {
                Operations.Add(new OperationIdClassVM(operationIdClass, context));
            }
            foreach (var note in technology.Notes)
            {
                Notes.Add(new NoteClassVM(note));
            }
        }
    }
}