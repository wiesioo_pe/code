﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.DatabasePanel
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Name { get; set; }
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public string OfficePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string PrivatePhone { get; set; }
        public string Fax { get; set; }
        public string eMail { get; set; }
        public string Desc { get; set; }
        public virtual ContactFileUrlClass PhotoFileUrlObj { get; set; }
        public virtual List<ChangeClass> Changes { get; set; } = new List<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class ContactVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public string OfficePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string PrivatePhone { get; set; }
        public string Fax { get; set; }
        public string eMail { get; set; }
        public string Desc { get; set; }
        public ContactFileUrlClassVM PhotoFileUrlObj { get; set; } = new ContactFileUrlClassVM();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public ContactVM() { }

        public ContactVM(Contact Contact)
        {
            Id = Contact.Id;
            UUID = Contact.UUID;

            Name = Contact.Name;
            SubjectId = Contact.SubjectId;
            SubjectUUID = Contact.SubjectUUID;
            Division = Contact.Division;
            Position = Contact.Position;
            OfficePhone = Contact.OfficePhone;
            BusinessPhone = Contact.BusinessPhone;
            PrivatePhone = Contact.PrivatePhone;
            Fax = Contact.Fax;
            eMail = Contact.eMail;
            Desc = Contact.Desc;
            PhotoFileUrlObj = new ContactFileUrlClassVM(Contact.PhotoFileUrlObj);
            LastUpdate = Contact.LastUpdate;

            foreach (var Group in Contact.Groups)
            {
                Groups.Add(new GroupIdClassVM(Group));
            }
            foreach (var Change in Contact.Changes)
            {
                Changes.Add(new ChangeClassVM(Change));
            }
        }
    }

    public class ContactAndPhotoVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public string OfficePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string PrivatePhone { get; set; }
        public string Fax { get; set; }
        public string eMail { get; set; }
        public string Desc { get; set; }
        public BatatFile PhotoFile { get; set; } = new BatatFile();
        public ContactFileUrlClassVM PhotoFileUrlObj { get; set; } = new ContactFileUrlClassVM();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }
    }


    public class ContactFileUrlClass
    {
        [Key, ForeignKey("Contact")]
        public int Id { get; set; }
        // Foreign keys
        public virtual Contact Contact { get; set; }


        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public ContactFileUrlClass() { }
        public ContactFileUrlClass(ContactFileUrlClassVM ContactFileUrlClassVM)
        {
            FileUrl = ContactFileUrlClassVM.FileUrl;
            FileName = ContactFileUrlClassVM.FileName;
            OriginalFileName = ContactFileUrlClassVM.OriginalFileName;
            FileSize = ContactFileUrlClassVM.FileSize;
            FileType = ContactFileUrlClassVM.FileType;
        }
    }

    public class ContactFileUrlClassVM
    {
        public int Id { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public ContactFileUrlClassVM() { }
        public ContactFileUrlClassVM(ContactFileUrlClass contactFileUrlClass)
        {
            if (contactFileUrlClass != null)
            {
                Id = contactFileUrlClass.Id;
                FileUrl = contactFileUrlClass.FileUrl;
                FileName = contactFileUrlClass.FileName;
                OriginalFileName = contactFileUrlClass.OriginalFileName;
                FileType = contactFileUrlClass.FileType;
                FileSize = contactFileUrlClass.FileSize;
            }
        }
    }
}