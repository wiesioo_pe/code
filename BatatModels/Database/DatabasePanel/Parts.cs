﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Batat.Models.Database.DatabasePanel
{
    public class Part
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Code { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public virtual List<FileUrlClass> FileUrlObjs { get; set; } = new List<FileUrlClass>();
        public virtual HashSet<AttachmentClass> Attachments { get; set; } = new HashSet<AttachmentClass>();
        public virtual List<ParameterIdValueClass> Parameters { get; set; } = new List<ParameterIdValueClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class PartVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Code { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public List<FileUrlClassVM> FileUrlObjs { get; set; } = new List<FileUrlClassVM>();
        public List<AttachmentClassVM> Attachments { get; set; } = new List<AttachmentClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public PartVM() { }

        public PartVM(Part part)
        {
            Id = part.Id;
            UUID = part.UUID;

            Code = part.Code;
            Name = part.Name;
            Desc = part.Desc;
            LastUpdate = part.LastUpdate;

            foreach (var group in part.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in part.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var attachment in part.Attachments)
            {
                Attachments.Add(new AttachmentClassVM(attachment));
            }
            foreach (var parameter in part.Parameters)
            {
                Parameters.Add(new ParameterIdValueClassVM(parameter));
            }
            foreach (var fileUrl in part.FileUrlObjs)
            {
                FileUrlObjs.Add(new FileUrlClassVM(fileUrl));
            }
        }
    }

    public class PartAndFilesVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Code { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public List<FileUrlClassVM> FileUrlObjs { get; set; } = new List<FileUrlClassVM>();
        public List<BatatFile> Files { get; set; } = new List<BatatFile>();
        public List<AttachmentClassVM> Attachments { get; set; } = new List<AttachmentClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }
    }
}