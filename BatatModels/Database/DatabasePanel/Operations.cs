﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.DatabasePanel
{
    public class Operation
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public bool isActive { get; set; } = false;
        public bool isStartCollective { get; set; } = false;
        public bool isEndCollective { get; set; } = false;
        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Code { get; set; }
        public string Desc { get; set; }
        public int OperationTypeId { get; set; }
        public string OperationTypeUUID { get; set; }
        public string ToolsDesc { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public virtual OperationFileUrlClass ProcessSheetFileUrlObj { get; set; }
        public virtual List<FileUrlClass> FileUrlObjs { get; set; } = new List<FileUrlClass>();
        public virtual List<ToolIdUtilizationClass> Tools { get; set; } = new List<ToolIdUtilizationClass>();
        public virtual List<MaterialIdAmountClass> Materials { get; set; } = new List<MaterialIdAmountClass>();
        public virtual List<OperationBeforeIdClass> OperationsBefore { get; set; } = new List<OperationBeforeIdClass>();
        public virtual List<ParameterIdValueClass> Parameters { get; set; } = new List<ParameterIdValueClass>();
        public virtual List<MachineIdTimeCostClass> MachinesTimesCosts { get; set; } = new List<MachineIdTimeCostClass>();
        public virtual List<NoteClass> Notes { get; set; } = new List<NoteClass>();
        public virtual List<AttachmentClass> Attachments { get; set; } = new List<AttachmentClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class OperationVM
    {
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public bool? isActive { get; set; } = false;
        public bool? isStartCollective { get; set; } = false;
        public bool? isEndCollective { get; set; } = false;
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Code { get; set; }
        public string Desc { get; set; }
        public int OperationTypeId { get; set; }
        public string OperationTypeUUID { get; set; }
        public string ToolsDesc { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public OperationFileUrlClassVM ProcessSheetFileUrlObj { get; set; } = new OperationFileUrlClassVM();
        public List<FileUrlClassVM> FileUrlObjs { get; set; } = new List<FileUrlClassVM>();
        public List<ToolIdUtilizationClassVM> Tools { get; set; } = new List<ToolIdUtilizationClassVM>();
        public List<MaterialIdAmountClassVM> Materials { get; set; } = new List<MaterialIdAmountClassVM>();
        public List<OperationBeforeIdClassVM> OperationsBefore { get; set; } = new List<OperationBeforeIdClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<MachineIdTimeCostClassVM> MachinesTimesCosts { get; set; } = new List<MachineIdTimeCostClassVM>();
        public List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<AttachmentClassVM> Attachments { get; set; } = new List<AttachmentClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public OperationVM() { }

        public OperationVM(Operation operation)
        {
            Id = operation.Id;
            UUID = operation.UUID;

            TechnologyId = operation.TechnologyId;
            TechnologyUUID = operation.TechnologyUUID;
            Code = operation.Code;
            Desc = operation.Desc;
            OperationTypeId = operation.OperationTypeId;
            OperationTypeUUID = operation.OperationTypeUUID;
            ToolsDesc = operation.ToolsDesc;
            PartId = operation.PartId;
            PartUUID = operation.PartUUID;
            LastUpdate = operation.LastUpdate;
            isActive = operation.isActive;
            isStartCollective = operation.isStartCollective;
            isEndCollective = operation.isEndCollective;
            ProcessSheetFileUrlObj = new OperationFileUrlClassVM(operation.ProcessSheetFileUrlObj);

            foreach (var group in operation.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in operation.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var operationsBeforeId in operation.OperationsBefore)
            {
                OperationsBefore.Add(new OperationBeforeIdClassVM(operationsBeforeId));
            }
            foreach (var parameterIdValue in operation.Parameters)
            {
                Parameters.Add(new ParameterIdValueClassVM(parameterIdValue));
            }
            foreach (var toolIdUtilization in operation.Tools)
            {
                Tools.Add(new ToolIdUtilizationClassVM(toolIdUtilization));
            }
            foreach (var materialIdAmount in operation.Materials)
            {
                Materials.Add(new MaterialIdAmountClassVM(materialIdAmount));
            }
            foreach (var machineIdTimeCost in operation.MachinesTimesCosts)
            {
                MachinesTimesCosts.Add(new MachineIdTimeCostClassVM(machineIdTimeCost));
            }
            foreach (var note in operation.Notes)
            {
                Notes.Add(new NoteClassVM(note));
            }
            foreach (var fileUrl in operation.FileUrlObjs)
            {
                FileUrlObjs.Add(new FileUrlClassVM(fileUrl));
            }
        }
    }

    public class OperationAndFilesVM
    {
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public int TechnologyId { get; set; }
        public string TechnologyUUID { get; set; }
        public bool? isActive { get; set; } = false;
        public bool? isStartCollective { get; set; } = false;
        public bool? isEndCollective { get; set; } = false;
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Code { get; set; }
        public string Desc { get; set; }
        public int OperationTypeId { get; set; }
        public string OperationTypeUUID { get; set; }
        public string ToolsDesc { get; set; }
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public OperationFileUrlClassVM ProcessSheetFileUrlObj { get; set; } = new OperationFileUrlClassVM();
        public BatatFile ProcessSheetFile { get; set; } = new BatatFile();
        public List<BatatFile> Files { get; set; } = new List<BatatFile>();
        public List<FileUrlClassVM> FileUrlObjs { get; set; } = new List<FileUrlClassVM>();
        public List<ToolIdUtilizationClassVM> Tools { get; set; } = new List<ToolIdUtilizationClassVM>();
        public List<MaterialIdAmountClassVM> Materials { get; set; } = new List<MaterialIdAmountClassVM>();
        public List<OperationBeforeIdClassVM> OperationsBefore { get; set; } = new List<OperationBeforeIdClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<MachineIdTimeCostClassVM> MachinesTimesCosts { get; set; } = new List<MachineIdTimeCostClassVM>();
        public List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<AttachmentClassVM> Attachments { get; set; } = new List<AttachmentClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }
    }


    public interface IOperationBeforeId
    {
        int OperationBeforeId { get; set; }
    }

    public class OperationBeforeIdClass : IOperationBeforeId
    {
        [Key]
        public int Id { get; set; }
        public int OperationBeforeId { get; set; }
        public string OperationBeforeUUID { get; set; }

        public OperationBeforeIdClass() { }

        public OperationBeforeIdClass(int operationBeforeId, string operationBeforeUuid)
        {
            OperationBeforeId = operationBeforeId;
            OperationBeforeUUID = operationBeforeUuid;
        }
        public OperationBeforeIdClass(OperationBeforeIdClassVM operationBeforeIdClassVm)
        {
            OperationBeforeId = operationBeforeIdClassVm.OperationBeforeId;
            OperationBeforeUUID = operationBeforeIdClassVm.OperationBeforeUUID;
        }
    }

    public class OperationBeforeIdClassVM : IOperationBeforeId
    {
        public int OperationBeforeId { get; set; }
        public string OperationBeforeUUID { get; set; }


        public OperationBeforeIdClassVM(OperationBeforeIdClass operationBeforeIdClass)
        {
            OperationBeforeId = operationBeforeIdClass.OperationBeforeId;
            OperationBeforeUUID = operationBeforeIdClass.OperationBeforeUUID;
        }
    }

    public class OperationFileUrlClass
    {
        [Key, ForeignKey("Operation")]
        public int FileUrlId { get; set; }
        // Foreign keys
        public virtual Operation Operation { get; set; }


        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public OperationFileUrlClass() { }
        public OperationFileUrlClass(OperationFileUrlClassVM operationFileUrlClassVm)
        {
            FileUrl = operationFileUrlClassVm.FileUrl;
            FileName = operationFileUrlClassVm.FileName;
            OriginalFileName = operationFileUrlClassVm.OriginalFileName;
            FileSize = operationFileUrlClassVm.FileSize;
            FileType = operationFileUrlClassVm.FileType;
        }
    }

    public class OperationFileUrlClassVM
    {
        public int FileUrlId { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public OperationFileUrlClassVM() { }
        public OperationFileUrlClassVM(OperationFileUrlClass operationFileUrlClass)
        {
            if(operationFileUrlClass != null)
            {
                FileUrlId = operationFileUrlClass.FileUrlId;
                FileUrl = operationFileUrlClass.FileUrl;
                FileName = operationFileUrlClass.FileName;
                OriginalFileName = operationFileUrlClass.OriginalFileName;
                FileType = operationFileUrlClass.FileType;
                FileSize = operationFileUrlClass.FileSize;
            }
        }
    }
}