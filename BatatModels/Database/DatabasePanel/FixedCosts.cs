﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Database.DatabasePanel
{
    public class FixedCost
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public int CategoryId { get; set; }
        public int TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? StopDate { get; set; }
        public decimal Amount { get; set; }
        public string Desc { get; set; }
        public int Period { get; set; }
        public bool isRecurring { get; set; }
        public virtual List<ChangeClass> Changes { get; set; } = new List<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class FixedCostVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public int CategoryId { get; set; }
        public int TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? StopDate { get; set; }
        public decimal Amount { get; set; }
        public string Desc { get; set; }
        public int Period { get; set; }
        public bool isRecurring { get; set; }
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public FixedCostVM() { }

        public FixedCostVM(FixedCost FixedCost)
        {
            Id = FixedCost.Id;
            UUID = FixedCost.UUID;

            CategoryId = FixedCost.CategoryId;
            TypeId = FixedCost.TypeId;
            StartDate = FixedCost.StartDate;
            StopDate = FixedCost.StopDate;
            Amount = FixedCost.Amount;
            Desc = FixedCost.Desc;
            Period = FixedCost.Period;
            isRecurring = FixedCost.isRecurring;
            LastUpdate = FixedCost.LastUpdate;

            foreach (var Group in FixedCost.Groups)
            {
                Groups.Add(new GroupIdClassVM(Group));
            }
            foreach (var Change in FixedCost.Changes)
            {
                Changes.Add(new ChangeClassVM(Change));
            }
        }
    }

    public class FixedCostType
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }
        public string Desc { get; set; }
        public int ParentTypeId { get; set; }
        public virtual HashSet<FixedCostCategoryIdClass> Categories { get; set; } = new HashSet<FixedCostCategoryIdClass>();
    }

    public class FixedCostCategoryIdClass
    {
        [Key]
        public int Id { get; set; }
        public int FixedCostCategoryId { get; set; }
        public string FixedCostCategoryUUID { get; set; }

        public FixedCostCategoryIdClass() { }
        public FixedCostCategoryIdClass(int FixedCostCategoryId, ApplicationDataDbContext context)
        {
            this.FixedCostCategoryId = FixedCostCategoryId;
            var FixedCostCategory = context.FixedCostCategories.Find(FixedCostCategoryId);
            if (FixedCostCategory != null)
            {
                FixedCostCategoryUUID = FixedCostCategory.UUID;
            }
        }
    }

    public class FixedCostCategory
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }
        public string Desc { get; set; }
    }
}