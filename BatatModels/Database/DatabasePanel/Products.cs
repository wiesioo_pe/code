﻿using Batat.Models.Database.DbContexts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;

namespace Batat.Models.Database.DatabasePanel
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public decimal NetPrice { get; set; }
        public virtual HashSet<ProductIdAmountDiscountClass> Products { get; set; } = new HashSet<ProductIdAmountDiscountClass>();
        public virtual HashSet<PartIdAmountClass> Parts { get; set; } = new HashSet<PartIdAmountClass>();
        public virtual List<ParameterIdValueClass> Parameters { get; set; } = new List<ParameterIdValueClass>();
        public virtual List<FileUrlClass> FileUrlObjs { get; set; } = new List<FileUrlClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public static class ProductExtensions
    {
        public static int GetToDoAmount(this Product product, ApplicationDataDbContext context)
        {
            int productsAmount = 0;
            if (product == null || product.Products == null)
            {
                Debugger.Break();
            }

            foreach(var productIdAmount in product.Products)
            {
                var internalProduct = context.Products.Find(productIdAmount.ProductId);
                productsAmount += internalProduct.GetToDoAmount(context) * productIdAmount.Amount;
            }

            var partsAmount = product.Parts.Sum(a => a.Amount);
            return partsAmount + productsAmount;
        }
    }

    public class ProductVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public decimal NetPrice { get; set; }
        public List<ProductIdAmountDiscountClassVM> Products { get; set; } = new List<ProductIdAmountDiscountClassVM>();
        public List<PartIdAmountClassVM> Parts { get; set; } = new List<PartIdAmountClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<FileUrlClassVM> FileUrlObjs { get; set; } = new List<FileUrlClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public ProductVM() { }

        public ProductVM(Product product)
        {
            Id = product.Id;
            UUID = product.UUID;

            Name = product.Name;
            Code = product.Code;
            Desc = product.Desc;
            NetPrice = product.NetPrice;
            LastUpdate = product.LastUpdate;

            foreach (var group in product.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in product.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var productIdAmountDiscount in product.Products)
            {
                Products.Add(new ProductIdAmountDiscountClassVM(productIdAmountDiscount));
            }
            foreach (var partIdAmount in product.Parts)
            {
                Parts.Add(new PartIdAmountClassVM(partIdAmount));
            }
            foreach (var parameterIdValue in product.Parameters)
            {
                Parameters.Add(new ParameterIdValueClassVM(parameterIdValue));
            }
            foreach (var fileUrl in product.FileUrlObjs)
            {
                FileUrlObjs.Add(new FileUrlClassVM(fileUrl));
            }
        }
    }

    public class ProductAndFilesVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public decimal NetPrice { get; set; }
        public List<ProductIdAmountDiscountClassVM> Products { get; set; } = new List<ProductIdAmountDiscountClassVM>();
        public List<PartIdAmountClassVM> Parts { get; set; } = new List<PartIdAmountClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<FileUrlClassVM> FileUrlObjs { get; set; } = new List<FileUrlClassVM>();
        public List<BatatFile> Files { get; set; } = new List<BatatFile>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }
    }
}