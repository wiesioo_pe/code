﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.DatabasePanel
{
    public class Tool
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public int ToolTypeId { get; set; }
        public decimal UnitCost { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public virtual List<ParameterIdValueClass> Parameters { get; set; } = new List<ParameterIdValueClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class ToolVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public int ToolTypeId { get; set; }
        public decimal? UnitCost { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public ToolVM() { }
        public ToolVM(Tool tool)
        {
            Id = tool.Id;
            UUID = tool.UUID;

            ToolTypeId = tool.ToolTypeId;
            Name = tool.Name;
            Code = tool.Code;
            Desc = tool.Desc;
            UnitCost = tool.UnitCost;
            LastUpdate = tool.LastUpdate;

            foreach (var group in tool.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in tool.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var parameter in tool.Parameters)
            {
                Parameters.Add(new ParameterIdValueClassVM(parameter));
            }
        }
    }

    public class ToolIdUtilizationClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ToolId { get; set; }
        public decimal Utilization { get; set; }

        public ToolIdUtilizationClass() { }
        public ToolIdUtilizationClass(ToolIdUtilizationClassVM toolIdClassVm)
        {
            ToolId = toolIdClassVm.ToolId ?? 0;
            Utilization = toolIdClassVm.Utilization ?? 0;
        }
    }

    public class ToolIdUtilizationClassVM
    {
        public int? ToolId { get; set; }
        public decimal? Utilization { get; set; }

        public ToolIdUtilizationClassVM(ToolIdUtilizationClass toolIdClass)
        {
            ToolId = toolIdClass.ToolId;
            Utilization = toolIdClass.Utilization;
        }
    }

    public class ToolPurchase
    {
        [Key]
        public int ToolPurchaseId { get; set; }
        public int ToolId { get; set; }
        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public decimal Cost { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public string EmployeeUUID { get; set; }
        public decimal UtilizationLevel { get; set; }
        public string Desc { get; set; }
    }

    public class ToolType
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
    }
}