﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace Batat.Models.Database.DatabasePanel
{
    public class Operator
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public string EmployeeUUID { get; set; }
        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Name { get; set; }
        public string Position { get; set; }
        public string Function { get; set; }
        public decimal HourlyRate { get; set; }
        public decimal OvertimeRate { get; set; }
        public virtual OperatorFileUrlClass PhotoFileUrlObj { get; set; }
        public virtual List<NoteClass> Notes { get; set; } = new List<NoteClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class OperatorVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public string EmployeeUUID { get; set; }
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public string Position { get; set; }
        public string Function { get; set; }
        public decimal? HourlyRate { get; set; }
        public decimal? OvertimeRate { get; set; }
        public OperatorFileUrlClassVM PhotoFileUrlObj { get; set; } = new OperatorFileUrlClassVM();
        public virtual List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public OperatorVM() { }

        public OperatorVM(Operator Operator)
        {
            Id = Operator.Id;
            UUID = Operator.UUID;

            EmployeeUUID = Operator.EmployeeUUID;
            Name = Operator.Name;
            Position = Operator.Position;
            Function = Operator.Function;
            HourlyRate = Operator.HourlyRate;
            OvertimeRate = Operator.OvertimeRate;
            LastUpdate = Operator.LastUpdate;
            PhotoFileUrlObj = new OperatorFileUrlClassVM(Operator.PhotoFileUrlObj);

            foreach (var group in Operator.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in Operator.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var note in Operator.Notes)
            {
                Notes.Add(new NoteClassVM(note));
            }
        }
    }

    public class OperatorAndPhotoVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public string EmployeeUUID { get; set; }
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public string Position { get; set; }
        public string Function { get; set; }
        public decimal? HourlyRate { get; set; }
        public decimal? OvertimeRate { get; set; }
        public OperatorFileUrlClassVM PhotoFileUrlObj { get; set; } = new OperatorFileUrlClassVM();
        public BatatFile PhotoFile { get; set; } = new BatatFile();
        public virtual List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }
    }


    public class OperatorFileUrlClass
    {
        [Key, ForeignKey("Operator")]
        public int FileUrlId { get; set; }
        // Foreign keys
        public virtual Operator Operator { get; set; }


        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public OperatorFileUrlClass() { }
        public OperatorFileUrlClass(OperatorFileUrlClassVM operatorFileUrlClassVm)
        {
            FileUrl = operatorFileUrlClassVm.FileUrl;
            FileName = operatorFileUrlClassVm.FileName;
            OriginalFileName = operatorFileUrlClassVm.OriginalFileName;
            FileSize = operatorFileUrlClassVm.FileSize;
            FileType = operatorFileUrlClassVm.FileType;
        }
    }

    public class OperatorFileUrlClassVM
    {
        public int FileUrlId { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public OperatorFileUrlClassVM() { }
        public OperatorFileUrlClassVM(OperatorFileUrlClass operatorFileUrlClass)
        {
            if (operatorFileUrlClass != null)
            {
                FileUrlId = operatorFileUrlClass.FileUrlId;
                FileUrl = operatorFileUrlClass.FileUrl;
                FileName = operatorFileUrlClass.FileName;
                OriginalFileName = operatorFileUrlClass.OriginalFileName;
                FileType = operatorFileUrlClass.FileType;
                FileSize = operatorFileUrlClass.FileSize;
            }
        }
    }
}