﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.DatabasePanel
{
    public class Subject
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public bool isCustomer { get; set; } = true;
        public bool isSupplier { get; set; } = false;
        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal Discount { get; set; }
        public string VATRegNum { get; set; }
        public string WWW { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Desc { get; set; }
        public virtual SubjectFileUrlClass LogoFileUrlObj { get; set; }
        public virtual List<ContactIdClass> Contacts { get; set; } = new List<ContactIdClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class SubjectVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public bool? isCustomer { get; set; }
        public bool? isSupplier { get; set; }
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal? Discount { get; set; }
        public string VATRegNum { get; set; }
        public string WWW { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Desc { get; set; }
        public SubjectFileUrlClassVM LogoFileUrlObj { get; set; } = new SubjectFileUrlClassVM();
        public List<ContactIdClassVM> Contacts { get; set; } = new List<ContactIdClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public SubjectVM() { }
        public SubjectVM(Subject subject)
        {
            Id = subject.Id;
            UUID = subject.UUID;

            isCustomer = subject.isCustomer;
            isSupplier = subject.isSupplier;
            Name = subject.Name;
            ShortName = subject.ShortName;
            Discount = subject.Discount;
            VATRegNum = subject.VATRegNum;
            WWW = subject.WWW;
            Street = subject.Street;
            City = subject.City;
            PostalCode = subject.PostalCode;
            State = subject.State;
            Phone = subject.Phone;
            Desc = subject.Desc;
            LogoFileUrlObj = new SubjectFileUrlClassVM(subject.LogoFileUrlObj);
            LastUpdate = subject.LastUpdate;

            foreach (var group in subject.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in subject.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var contactId in subject.Contacts)
            {
                Contacts.Add(new ContactIdClassVM(contactId));
            }
        }
    }

    public class SubjectAndLogoVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public bool? isCustomer { get; set; }
        public bool? isSupplier { get; set; }
        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal? Discount { get; set; }
        public string VATRegNum { get; set; }
        public string WWW { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Desc { get; set; }
        public SubjectFileUrlClassVM LogoFileUrlObj { get; set; } = new SubjectFileUrlClassVM();
        public BatatFile LogoFile { get; set; } = new BatatFile();
        public List<ContactIdClassVM> Contacts { get; set; } = new List<ContactIdClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }
    }



    public class ContactIdClass
    {
        [Key]
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string ContactUUID { get; set; }

        public ContactIdClass() { }
        public ContactIdClass(int contactId, string contactUuid)
        {
            ContactId = contactId;
            ContactUUID = contactUuid;
        }
    }

    public class ContactIdClassVM
    {
        public int ContactId { get; set; }
        public string ContactUUID { get; set; }

        public ContactIdClassVM() { }
        public ContactIdClassVM(ContactIdClass contactIdClass)
        {
            ContactId = contactIdClass.ContactId;
            ContactUUID = contactIdClass.ContactUUID;
        }
    }



    public class SubjectFileUrlClass
    {
        [Key, ForeignKey("Subject")]
        public int FileUrlId { get; set; }
        // Foreign keys
        public virtual Subject Subject { get; set; }


        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public SubjectFileUrlClass() { }
        public SubjectFileUrlClass(SubjectFileUrlClassVM subjectFileUrlClassVm)
        {
            FileUrl = subjectFileUrlClassVm.FileUrl;
            FileName = subjectFileUrlClassVm.FileName;
            OriginalFileName = subjectFileUrlClassVm.OriginalFileName;
            FileSize = subjectFileUrlClassVm.FileSize;
            FileType = subjectFileUrlClassVm.FileType;
        }
    }

    public class SubjectFileUrlClassVM
    {
        public int FileUrlId { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public SubjectFileUrlClassVM() { }
        public SubjectFileUrlClassVM(SubjectFileUrlClass subjectFileUrlClass)
        {
            if (subjectFileUrlClass != null)
            {
                FileUrlId = subjectFileUrlClass.FileUrlId;
                FileUrl = subjectFileUrlClass.FileUrl;
                FileName = subjectFileUrlClass.FileName;
                OriginalFileName = subjectFileUrlClass.OriginalFileName;
                FileType = subjectFileUrlClass.FileType;
                FileSize = subjectFileUrlClass.FileSize;
            }
        }
    }
}
 