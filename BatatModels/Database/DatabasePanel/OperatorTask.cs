﻿namespace Batat.Models.Database.DatabasePanel
{
    public class OperatorTaskVM
    {
    }

    public class OperatorTaskEvent
    {
        public int TrolleyId { get; set; }
        public string TrolleyUUID { get; set; }
        public int EventType { get; set; }
        public string Note { get; set; }
    }
}