﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Batat.Models.Database.DatabasePanel
{
    public class Machine
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public string Code { get; set; }
        public int MachineTypeId { get; set; }
        public string MachineTypeUUID { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public virtual List<NoteClass> Notes { get; set; } = new List<NoteClass>();
        public virtual List<ParameterIdValueClass> Parameters { get; set; } = new List<ParameterIdValueClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();
        public virtual HashSet<OperatorIdClass> Operators { get; set; } = new HashSet<OperatorIdClass>();
        public decimal PurchaseCost { get; set; }
        public decimal AmortisationPeriod { get; set; }
        public decimal UtilisationLevel { get; set; }
        public decimal Accessibility { get; set; }

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class MachineVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public string Code { get; set; }
        public int MachineTypeId { get; set; }
        public string MachineTypeUUID { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public List<NoteClassVM> Notes { get; set; } = new List<NoteClassVM>();
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();
        public List<OperatorIdClassVM> Operators { get; set; } = new List<OperatorIdClassVM>();
        public decimal? PurchaseCost { get; set; }
        public decimal? AmortisationPeriod { get; set; }
        public decimal? UtilisationLevel { get; set; }
        public decimal? Accessibility { get; set; }

        public DateTime LastUpdate { get; set; }

        public MachineVM() { }

        public MachineVM(Machine machine)
        {
            Id = machine.Id;
            UUID = machine.UUID;

            Code = machine.Code;
            MachineTypeId = machine.MachineTypeId;
            MachineTypeUUID = machine.MachineTypeUUID;
            Model = machine.Model;
            Manufacturer = machine.Manufacturer;
            PurchaseCost = machine.PurchaseCost;
            AmortisationPeriod = machine.AmortisationPeriod;
            UtilisationLevel = machine.UtilisationLevel;
            Accessibility = machine.Accessibility;
            LastUpdate = machine.LastUpdate;

            foreach (var group in machine.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in machine.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var note in machine.Notes)
            {
                Notes.Add(new NoteClassVM(note));
            }
            foreach (var parameterIdValue in machine.Parameters)
            {
                Parameters.Add(new ParameterIdValueClassVM(parameterIdValue));
            }
            foreach (var operatorId in machine.Operators)
            {
                Operators.Add(new OperatorIdClassVM(operatorId));
            }
        }
    }
}