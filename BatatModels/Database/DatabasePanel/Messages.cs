﻿using System.Collections.Generic;

namespace Batat.Models.Database.DatabasePanel
{
    public class Messages
    {
        public List<Error> Errors { get; set; } = new List<Error>();
    }

    public class Error
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}