﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Batat.Models.Database.DatabasePanel
{
    // PARAMETR
    public class Parameter
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }
        public string Unit { get; set; }
        public string Desc { get; set; }
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class ParameterVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public string Name { get; set; }
        public string Unit { get; set; }
        public string Desc { get; set; }
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }

        public ParameterVM() { }

        public ParameterVM(Parameter parameter)
        {
            Id = parameter.Id;
            UUID = parameter.UUID;

            Name = parameter.Name;
            Unit = parameter.Unit;
            Desc = parameter.Desc;
            LastUpdate = parameter.LastUpdate;

            foreach (var change in parameter.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
        }
    }
}