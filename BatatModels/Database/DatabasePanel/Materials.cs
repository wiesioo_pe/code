﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Batat.Models.Database.DatabasePanel
{
    public class Material
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public int MaterialTypeId { get; set; }
        public string MaterialTypeUUID { get; set; }
        public decimal UnitCost { get; set; }
        public string Unit { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public virtual List<ParameterIdValueClass> Parameters { get; set; } = new List<ParameterIdValueClass>();
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class MaterialVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public List<GroupIdClassVM> Groups { get; set; } = new List<GroupIdClassVM>();
        public int MaterialTypeId { get; set; }
        public string MaterialTypeUUID { get; set; }
        public decimal? UnitCost { get; set; }
        public string Unit { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public List<ParameterIdValueClassVM> Parameters { get; set; } = new List<ParameterIdValueClassVM>();
        public List<ChangeClassVM> Changes { get; set; } = new List<ChangeClassVM>();

        public DateTime LastUpdate { get; set; }


        public MaterialVM() { }
        public MaterialVM(Material material)
        {
            Id = material.Id;
            UUID = material.UUID;

            MaterialTypeId = material.MaterialTypeId;
            MaterialTypeUUID = material.MaterialTypeUUID;
            Name = material.Name;
            Code = material.Code;
            Desc = material.Desc;
            Unit = material.Unit;
            UnitCost = material.UnitCost;
            LastUpdate = material.LastUpdate;

            foreach (var group in material.Groups)
            {
                Groups.Add(new GroupIdClassVM(group));
            }
            foreach (var change in material.Changes)
            {
                Changes.Add(new ChangeClassVM(change));
            }
            foreach (var parameter in material.Parameters)
            {
                Parameters.Add(new ParameterIdValueClassVM(parameter));
            }
        }
    }

    public class MaterialIdAmountClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int MaterialId { get; set; }
        public string MaterialUUID { get; set; }
        public decimal Amount { get; set; }

        public MaterialIdAmountClass() { }
        public MaterialIdAmountClass(MaterialIdAmountClassVM materialIdClassVm)
        {
            MaterialId = materialIdClassVm.MaterialId;
            MaterialUUID = materialIdClassVm.MaterialUUID;
            Amount = materialIdClassVm.Amount ?? 0;
        }
    }

    public class MaterialIdAmountClassVM
    {
        public int MaterialId { get; set; }
        public string MaterialUUID { get; set; }
        public decimal? Amount { get; set; }

        public MaterialIdAmountClassVM() { }
        public MaterialIdAmountClassVM(MaterialIdAmountClass materialIdClass)
        {
            MaterialId = materialIdClass.MaterialId;
            MaterialUUID = materialIdClass.MaterialUUID;
            Amount = materialIdClass.Amount;
        }
    }

    public class MaterialPurchase
    {
        [Key]
        public int Id { get; set; }
        public int MaterialId { get; set; }
        public string MaterialUUID { get; set; }
        public virtual HashSet<GroupIdClass> Groups { get; set; } = new HashSet<GroupIdClass>();
        public decimal Cost { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public string EmployeeUUID { get; set; }
        public decimal AmountLevel { get; set; }
        public string Desc { get; set; }
    }

    public class MaterialType
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
    }
}