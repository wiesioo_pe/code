﻿namespace Batat.Models.Database.DatabasePanel
{
    public class BatatFile
    {
        public bool isNew { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string Content { get; set; }
        public uint Size { get; set; }
    }
}