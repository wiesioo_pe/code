﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Batat.Models.Authentication;
using Batat.Models.Database.DbContexts;

namespace Batat.Models.Database.DatabasePanel
{
    public class GroupIdClass
    {
        [Key]
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string GroupUUID { get; set; }

        public GroupIdClass() { }
        public GroupIdClass(int groupId, string groupUuid)
        {
            GroupId = groupId;
            GroupUUID = groupUuid;
        }
    }

    public class GroupIdClassVM
    {
        public int GroupId { get; set; }

        public GroupIdClassVM() { }
        public GroupIdClassVM(GroupIdClass groupIdClass)
        {
            GroupId = groupIdClass.GroupId;
        }
    }

    public class ParameterIdValueClass
    {
        [Key]
        public int Id { get; set; }
        public int ParameterId { get; set; }
        public string ParameterUUID { get; set; }
        public string Value { get; set; }


        public ParameterIdValueClass() { }
        public ParameterIdValueClass(int parameterId, string parameterUuid, string value)
        {
            ParameterId = parameterId;
            ParameterUUID = parameterUuid;
            Value = value; }

        public ParameterIdValueClass(ParameterIdValueClassVM parameterIdValueClassVm)
        {
            ParameterId = parameterIdValueClassVm.ParameterId;
            ParameterUUID = parameterIdValueClassVm.ParameterUUID;
            Value = parameterIdValueClassVm.Value;
        }
    }

    public class ParameterIdValueClassVM
    {
        public int ParameterId { get; set; }
        public string ParameterUUID { get; set; }
        public string Value { get; set; }

        public ParameterIdValueClassVM() { }
        public ParameterIdValueClassVM(int parameterId, string parameterUuid, string value)
        {
            ParameterId = parameterId;
            ParameterUUID = parameterUuid;
            Value = value;
        }
        public ParameterIdValueClassVM(ParameterIdValueClass parameterIdValueClass)
        {
            ParameterId = parameterIdValueClass.ParameterId;
            ParameterUUID = parameterIdValueClass.ParameterUUID;
            Value = parameterIdValueClass.Value;
        }
    }

    public class MachineIdTimeCostClass
    {
        [Key]
        public int Id { get; set; }
        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int MachineId { get; set; }
        public string MachineUUID { get; set; }
        public decimal UnitTime { get; set; }
        public decimal PCTime { get; set; }
        public decimal ToolCost { get; set; }
        public decimal OtherCost { get; set; }
        public bool isStatistical { get; set; }

        public MachineIdTimeCostClass() { }
        public MachineIdTimeCostClass(int operationId, string operationUuid, int machineId, string machineUuid,
            decimal unitTime, decimal pcTime, decimal toolCost, decimal otherCost)
        {
            OperationId = operationId;
            OperationUUID = operationUuid;
            MachineId = machineId;
            MachineUUID = machineUuid;
            UnitTime = unitTime;
            PCTime = pcTime;
            ToolCost = toolCost;
            OtherCost = otherCost;
        }

        public MachineIdTimeCostClass(int operationId, int machineId, decimal unitTime, decimal pcTime,
            decimal toolCost, decimal otherCost, ApplicationDataDbContext context)
        {
            OperationId = operationId;
            OperationUUID = context.Operations.Find(operationId)?.UUID;
            MachineId = machineId;
            MachineUUID = context.Machines.Find(machineId)?.UUID;
            UnitTime = unitTime;
            PCTime = pcTime;
            ToolCost = toolCost;
            OtherCost = otherCost;
        }

        public MachineIdTimeCostClass(MachineIdTimeCostClassVM machineIdTimeCostClassVm)
        {
            OperationId = machineIdTimeCostClassVm.OperationId;
            OperationUUID = machineIdTimeCostClassVm.OperationUUID;
            MachineId = machineIdTimeCostClassVm.MachineId;
            MachineUUID = machineIdTimeCostClassVm.MachineUUID;
            UnitTime = machineIdTimeCostClassVm.UnitTime ?? 0;
            PCTime = machineIdTimeCostClassVm.PCTime ?? 0;
            ToolCost = machineIdTimeCostClassVm.ToolCost ?? 0;
            OtherCost = machineIdTimeCostClassVm.OtherCost ?? 0;
        }
    }

    public class MachineIdTimeCostClassVM
    {
        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public int MachineId { get; set; }
        public string MachineUUID { get; set; }
        public decimal? UnitTime { get; set; }
        public decimal? PCTime { get; set; }
        public decimal? ToolCost { get; set; }
        public decimal? OtherCost { get; set; }
        public bool isStatistical { get; set; }

        public MachineIdTimeCostClassVM() { }
        public MachineIdTimeCostClassVM(int operationId, string operationUuid, int machineId, string machineUuid,
            decimal unitTime, decimal pcTime, decimal toolCost, decimal otherCost, bool isStatistical)
        {
            OperationId = operationId;
            OperationUUID = operationUuid;
            MachineId = machineId;
            MachineUUID = machineUuid;
            UnitTime = unitTime;
            PCTime = pcTime;
            ToolCost = toolCost;
            OtherCost = otherCost;
            this.isStatistical = isStatistical;
        }

        public MachineIdTimeCostClassVM(MachineIdTimeCostClass machineIdTimeCostClass)
        {
            OperationId = machineIdTimeCostClass.OperationId;
            OperationUUID = machineIdTimeCostClass.OperationUUID;
            MachineId = machineIdTimeCostClass.MachineId;
            MachineUUID = machineIdTimeCostClass.MachineUUID;
            UnitTime = machineIdTimeCostClass.UnitTime;
            PCTime = machineIdTimeCostClass.PCTime;
            ToolCost = machineIdTimeCostClass.ToolCost;
            OtherCost = machineIdTimeCostClass.OtherCost;
            isStatistical = machineIdTimeCostClass.isStatistical;
        }
    }

    public class NoteClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NoteId { get; set; }
        public string Note { get; set; }
        public DateTime DateTime { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeUUID { get; set; }

        public NoteClass() { }
        public NoteClass(string note, ApplicationUser user)
        {
            Note = note;
            DateTime = DateTime.Now;
            EmployeeId = user?.IntId ?? 0;
            EmployeeUUID = user?.Id;
        }
    }

    public class NoteClassVM
    {
        public int NoteId { get; set; }
        public string Note { get; set; }
        public DateTime? DateTime { get; set; }
        public string EmployeeUUID { get; set; }

        public NoteClassVM() { }
        public NoteClassVM(string note) { Note = note; }
        public NoteClassVM(NoteClass noteClass)
        {
            NoteId = noteClass.NoteId;
            Note = noteClass.Note;
            DateTime = noteClass.DateTime;
            EmployeeUUID = noteClass.EmployeeUUID;
        }
    }

    public class MessageClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
        public string EmployeeUUID { get; set; }
        public bool isRead { get; set; }

        public MessageClass() { }
        public MessageClass(string message) { Message = message; }
    }

    public class MessageClassVM
    {
        public int MessageId { get; set; }
        public string Message { get; set; }
        public DateTime? DateTime { get; set; }
        public string EmployeeUUID { get; set; }
        public bool isRead { get; set; }

        public MessageClassVM() { }
        public MessageClassVM(MessageClass messageClass)
        {
            MessageId = messageClass.MessageId;
            Message = messageClass.Message;
            DateTime = messageClass.DateTime;
            EmployeeUUID = messageClass.EmployeeUUID;
            isRead = messageClass.isRead;
        }
    }

    public class ChangeClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChangeId { get; set; }
        public string Name { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeUUID { get; set; }
        public string Desc { get; set; }
        public DateTime DateTime { get; set; }

        public ChangeClass() { }

        public ChangeClass(string name, string desc, ApplicationUser user)
        {
            Name = name;
            Desc = desc;
            EmployeeId = user?.IntId ?? 0;
            EmployeeUUID = user?.Id;
            DateTime = DateTime.Now;
        }
    }

    public class ChangeClassVM
    {
        public int ChangeId { get; set; }
        public string Name { get; set; }
        public string EmployeeUUID { get; set; }
        public string Desc { get; set; }
        public DateTime? DateTime { get; set; }

        public ChangeClassVM() { }
        public ChangeClassVM(ChangeClass changeClass)
        {
            ChangeId = changeClass.ChangeId;
            Name = changeClass.Name;
            EmployeeUUID = changeClass.EmployeeUUID;
            Desc = changeClass.Desc;
            DateTime = changeClass.DateTime;
        }
        public ChangeClassVM(string name, string desc, string login)
        {
            Name = name;
            Desc = desc;
            EmployeeUUID = login;
            DateTime = System.DateTime.Now;
        }
    }

    public class AttachmentClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AttachmentId { get; set; }
        public byte[] Content { get; set; }
        public int ContentType { get; set; }
    }

    public class AttachmentClassVM
    {
        public int AttachmentId { get; set; }
        public byte[] Content { get; set; }
        public int? ContentType { get; set; }

        public AttachmentClassVM() { }
        public AttachmentClassVM(AttachmentClass attachmentClass)
        {
            AttachmentId = attachmentClass.AttachmentId;
            Content = attachmentClass.Content;
            ContentType = attachmentClass.ContentType;
        }
    }

    public class ProductIdAmountDiscountClass
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int Amount { get; set; }
        public decimal Discount { get; set; }

        public ProductIdAmountDiscountClass() { }
        public ProductIdAmountDiscountClass(int productId, string productUuid, int? amount, decimal? discount)
        {
            ProductId = productId;
            ProductUUID = productUuid;
            Amount = amount ?? 0;
            Discount = discount ?? 0;
        }
    }

    public class ProductIdAmountDiscountClassVM
    {
        public int ProductId { get; set; }
        public string ProductUUID { get; set; }
        public int? Amount { get; set; }
        public decimal? Discount { get; set; }

        public ProductIdAmountDiscountClassVM() { }
        public ProductIdAmountDiscountClassVM(int id, string productUuid, int? amount, decimal? discount)
        {
            ProductId = id;
            ProductUUID = productUuid;
            Amount = amount ?? 0;
            Discount = discount ?? 0;
        }
        public ProductIdAmountDiscountClassVM(ProductIdAmountDiscountClass productIdAmountDiscountClass)
        {
            ProductId = productIdAmountDiscountClass.ProductId;
            ProductUUID = productIdAmountDiscountClass.ProductUUID;
            Amount = productIdAmountDiscountClass.Amount;
            Discount = productIdAmountDiscountClass.Discount;
        }
    }

    public class PartIdAmountClass
    {
        [Key]
        public int Id { get; set; }

        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int Amount { get; set; }

        public PartIdAmountClass() { }
        public PartIdAmountClass(int id, string partUuid, int? amount)
        {
            PartId = id;
            PartUUID = partUuid;
            Amount = amount ?? 0;
        }
    }

    public class PartIdAmountClassVM
    {
        public int PartId { get; set; }
        public string PartUUID { get; set; }
        public int? Amount { get; set; }

        public PartIdAmountClassVM() { }
        public PartIdAmountClassVM(int id, string partUuid, int amount)
        {
            PartId = id;
            PartUUID = partUuid;
            Amount = amount;
        }
        public PartIdAmountClassVM(PartIdAmountClass partIdAmountClass)
        {
            PartId = partIdAmountClass.PartId;
            PartUUID = partIdAmountClass.PartUUID;
            Amount = partIdAmountClass.Amount;
        }
    }

    public class SubjectIdClass
    {
        [Key]
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }

        public SubjectIdClass() { }
        public SubjectIdClass(int subjectId, string subjectUuid)
        {
            SubjectId = subjectId;
            SubjectUUID = subjectUuid;
        }
    }

    public class SubjectIdClassVM
    {
        public int SubjectId { get; set; }
        public string SubjectUUID { get; set; }

        public SubjectIdClassVM() { }
        public SubjectIdClassVM(int subjectId, string subjectUuid)
        {
            SubjectId = subjectId;
            SubjectUUID = subjectUuid;
        }
    }

    public class OperationIdClass
    {
        [Key]
        public int Id { get; set; }
        public int OperationId { get; set; }
        public string OperationUUID { get; set; }

        public OperationIdClass() { }
        public OperationIdClass(int operationId, string operationUuid)
        {
            OperationId = operationId;
            OperationUUID = operationUuid;
        }
        public OperationIdClass(OperationIdClassVM operationIdClassVm)
        {
            OperationId = operationIdClassVm.OperationId;
            OperationUUID = operationIdClassVm.OperationUUID;
        }
    }

    public class OperationIdClassVM
    {
        public int OperationId { get; set; }
        public string OperationUUID { get; set; }
        public bool? isActive { get; set; }
        public bool? isStartCollective { get; set; }
        public bool? isEndCollective { get; set; }
        public int? OperationTypeId { get; set; }
        public string Desc { get; set; }

        public List<OperationBeforeIdClassVM> OperationsBefore;

        public List<MachineIdTimeCostClassVM> MachinesTimesCosts;

        public OperationIdClassVM() { }

        public OperationIdClassVM(OperationIdClass operationIdClass, ApplicationDataDbContext context)
        {
            OperationId = operationIdClass.OperationId;
            OperationUUID = operationIdClass.OperationUUID;
            var operation = context.Operations.Find(operationIdClass.OperationId);
            if (operation != null)
            {
                isActive = operation.isActive;
                isStartCollective = operation.isStartCollective;
                isEndCollective = operation.isEndCollective;
                OperationTypeId = operation.OperationTypeId;
                Desc = operation.Desc;

                OperationsBefore = operation.OperationsBefore.Select(a => new OperationBeforeIdClassVM(a)).ToList();
                MachinesTimesCosts = operation.MachinesTimesCosts.Select(a => new MachineIdTimeCostClassVM(a)).ToList();
            }
        }
    }


    public class OperatorIdClass
    {
        [Key]
        public int Id { get; set; }
        public int OperatorId { get; set; }
        public string OperatorUUID { get; set; }

        public OperatorIdClass() { }
        public OperatorIdClass(int operatorId, string operatorUuid)
        {
            OperatorId = operatorId;
            OperatorUUID = operatorUuid;
        }
    }

    public class OperatorIdClassVM
    {
        public int OperatorId { get; set; }
        public string OperatorUUID { get; set; }

        public OperatorIdClassVM() { }
        public OperatorIdClassVM(OperatorIdClass operatorIdClass)
        {
            OperatorId = operatorIdClass.OperatorId;
            OperatorUUID = operatorIdClass.OperatorUUID;
        }
    }

    public class FileUrlClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FileUrlId { get; set; }

        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public FileUrlClass() { }
        public FileUrlClass(FileUrlClassVM fileUrlClassVm)
        {
            FileUrl = fileUrlClassVm.FileUrl;
            FileName = fileUrlClassVm.FileName;
            OriginalFileName = fileUrlClassVm.OriginalFileName;
            FileSize = fileUrlClassVm.FileSize;
            FileType = fileUrlClassVm.FileType;
        }
    }

    public class FileUrlClassVM
    {
        public int FileUrlId { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public uint FileSize { get; set; }

        public FileUrlClassVM() { }
        public FileUrlClassVM(FileUrlClass fileUrlClass)
        {
            FileUrlId = fileUrlClass.FileUrlId;
            FileUrl = fileUrlClass.FileUrl;
            FileName = fileUrlClass.FileName;
            OriginalFileName = fileUrlClass.OriginalFileName;
            FileType = fileUrlClass.FileType;
            FileSize = fileUrlClass.FileSize;
        }
    }
}