﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Batat.Models
{
    public class Depreciated
    {
        public class OrdersIndexSettingsDB
        {
            [Key]
            public int OrdersIndexSettings { get; set; }

            public string UserName { get; set; }

            public bool? GroupsColumnVisible { get; set; }
            public bool? ReceiveDateColumnVisible { get; set; }
            public bool? DeliveryDateColumnVisible { get; set; }
            public bool? SubjectColumnVisible { get; set; }
            public bool? ContactColumnVisible { get; set; }
            public bool? DescColumnVisible { get; set; }
            public bool? EmployeeColumnVisible { get; set; }
            public bool? NotesColumnVisible { get; set; }

            public string OrderedBy { get; set; }
            public bool? OrderedAscending { get; set; }

            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? ReceiveDateFrom { get; set; }
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? ReceiveDateTo { get; set; }
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? DeliveryDateFrom { get; set; }
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? DeliveryDateTo { get; set; }


            public OrdersIndexSettingsDB() { }

            public OrdersIndexSettingsDB(OrdersIndexSettingsVM OrdersIndexSettingsVM, string UserName)
            {
                this.UserName = UserName;

                GroupsColumnVisible = OrdersIndexSettingsVM.GroupsColumnVisible;
                ReceiveDateColumnVisible = OrdersIndexSettingsVM.ReceiveDateColumnVisible;
                DeliveryDateColumnVisible = OrdersIndexSettingsVM.DeliveryDateColumnVisible;
                SubjectColumnVisible = OrdersIndexSettingsVM.SubjectColumnVisible;
                ContactColumnVisible = OrdersIndexSettingsVM.ContactColumnVisible;
                DescColumnVisible = OrdersIndexSettingsVM.DescColumnVisible;
                EmployeeColumnVisible = OrdersIndexSettingsVM.EmployeeColumnVisible;
                NotesColumnVisible = OrdersIndexSettingsVM.NotesColumnVisible;

                OrderedBy = OrdersIndexSettingsVM.OrderedBy;
                OrderedAscending = OrdersIndexSettingsVM.OrderedAscending;

                ReceiveDateFrom = OrdersIndexSettingsVM.ReceiveDateFrom;
                ReceiveDateTo = OrdersIndexSettingsVM.ReceiveDateTo;
                DeliveryDateFrom = OrdersIndexSettingsVM.DeliveryDateFrom;
                DeliveryDateTo = OrdersIndexSettingsVM.DeliveryDateTo;
            }
        }

        public class OrdersIndexSettingsVM
        {
            public bool? GroupsColumnVisible { get; set; }
            public bool? ReceiveDateColumnVisible { get; set; }
            public bool? DeliveryDateColumnVisible { get; set; }
            public bool? SubjectColumnVisible { get; set; }
            public bool? ContactColumnVisible { get; set; }
            public bool? DescColumnVisible { get; set; }
            public bool? EmployeeColumnVisible { get; set; }
            public bool? NotesColumnVisible { get; set; }

            public string OrderedBy { get; set; }
            public bool? OrderedAscending { get; set; }

            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? ReceiveDateFrom { get; set; }
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? ReceiveDateTo { get; set; }
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? DeliveryDateFrom { get; set; }
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
            public DateTime? DeliveryDateTo { get; set; }

            public string SelectedGroup { get; set; }
            public string SelectedSubject { get; set; }
            public string SelectedEmployee { get; set; }
            public string SelectedOrderState { get; set; }



            public OrdersIndexSettingsVM() { }

            public OrdersIndexSettingsVM(dynamic OrdersIndexSettings)
            {
                GroupsColumnVisible = (bool?)OrdersIndexSettings.GroupsColumnVisible;
                ReceiveDateColumnVisible = (bool?)OrdersIndexSettings.ReceiveDateColumnVisible;
                DeliveryDateColumnVisible = (bool?)OrdersIndexSettings.DeliveryDateColumnVisible;
                SubjectColumnVisible = (bool?)OrdersIndexSettings.SubjectColumnVisible;
                ContactColumnVisible = (bool?)OrdersIndexSettings.ContactColumnVisible;
                DescColumnVisible = (bool?)OrdersIndexSettings.DescColumnVisible;
                EmployeeColumnVisible = (bool?)OrdersIndexSettings.EmployeeColumnVisible;
                NotesColumnVisible = (bool?)OrdersIndexSettings.NotesColumnVisible;

                OrderedBy = (string)OrdersIndexSettings.OrderedBy;
                OrderedAscending = (bool?)OrdersIndexSettings.OrderedAscending;

                SelectedGroup = (string)OrdersIndexSettings.SelectedGroup;
                SelectedSubject = (string)OrdersIndexSettings.SelectedSubject;
                SelectedEmployee = (string)OrdersIndexSettings.SelectedEmployee;
                SelectedOrderState = (string)OrdersIndexSettings.SelectedOrderState;

                try
                {
                    ReceiveDateFrom = DateTime.ParseExact((string)OrdersIndexSettings.ReceiveDateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                catch { }

                try
                {
                    ReceiveDateTo = DateTime.ParseExact((string)OrdersIndexSettings.ReceiveDateTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                catch { }
                try
                {
                    DeliveryDateFrom = DateTime.ParseExact((string)OrdersIndexSettings.DeliveryDateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                catch { }
                try
                {
                    DeliveryDateTo = DateTime.ParseExact((string)OrdersIndexSettings.DeliveryDateTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                catch { }
            }
        }
    }
}