﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Batat.Models.Database.DatabasePanel;

namespace Batat.Models
{
    // GROUPS
    public class Group
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public virtual HashSet<ChangeClass> Changes { get; set; } = new HashSet<ChangeClass>();

        public DateTime LastUpdate { get; set; } = DateTime.Now;
    }

    public class GroupVM
    {
        public int Id { get; set; }
        public string UUID { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }

        public DateTime LastUpdate { get; set; }


        public GroupVM() { }

        public GroupVM(Group group)
        {
            Id = group.Id;
            UUID = group.UUID;

            Name = group.Name;
            Code = group.Code;
            Desc = group.Desc;
            LastUpdate = group.LastUpdate;
        }
    }

    public class OperationType
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
    }

    public class MachineType
    {
        [Key]
        public int Id { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
    }


    public class ErrorVM
    {
        public string Header { get; set; }
        public List<string> Messages { get; set; } = new List<string>();
    }
}