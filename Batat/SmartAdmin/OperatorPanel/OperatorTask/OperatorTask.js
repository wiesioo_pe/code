﻿"use strict";
console.log("Załadowałem OperatorTask.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {

    // COMPONENT
    angular.module("app").directive("operatorTask", function () {
        return {
            scope: {
                Trolley: "=?ngDataTrolley",
                Items: "=?ngDataItems",
                isWidgetCollapsed: "=ngDataIsWidgetCollapsed",
                CurrentTrolleyId: '=ngDataCurrentTrolleyId'
            },
            templateUrl: "/SmartAdmin/OperatorPanel/OperatorTask/OperatorTask.html?" + Math.random(),
            controller: OperatorTaskController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });

    OperatorTaskController.$inject = ['$scope', '$timeout', 'FeaturesService', 'TrolleyService'];
    function OperatorTaskController($scope, $timeout, FeaturesService, TrolleyService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.StartButtonPopoverHtml =
                '<button style="margin: 1em;" class="btn btn-circle btn-xl txt-color-white bg-color-greenLight" ng-if="callbacks.buttonStart != null" ng-click="callbacks.buttonStart(' + "'Start'" + ')"><i class="fa fa-play"></i></button>'
                + '<button style="margin: 1em;" class="btn btn-circle btn-xl txt-color-white bg-color-pink" ng-if="callbacks.buttonPause != null" ng-click="callbacks.buttonPause(' + "'Pause'" + ')"><i class="fa fa-pause"></i></button>'
                + '<button style="margin: 1em;" class="btn btn-circle btn-xl txt-color-white bg-color-red" ng-if="callbacks.buttonStop != null" ng-click="callbacks.buttonStop(' + "'Stop'" + ')"><i class="fa fa-stop"></i></button>'
                + '<button style="margin: 1em;" class="btn btn-circle btn-xl txt-color-white bg-color-darken" ng-if="callbacks.buttonEnd != null" ng-click="callbacks.buttonEnd(' + "'End'" + ')"><i class="fa fa-check-circle"></i></button>'
                ;
            $ctrl.popoverCallbacks = {};
            changeStartButton();
        };

        var dereg$watchTrolley = $scope.$watch('$ctrl.Trolley', function () {
            if ($ctrl.Trolley != null) {
                $timeout(function () { }, 0);
                dereg$watchTrolley();
            }
        });
        // END HOOKS

        this.widgetHeaderClick = function () {
            $timeout(function () {
                if ($ctrl.Trolley.StateId !== 1) {
                    $ctrl.isWidgetCollapsed = !$ctrl.isWidgetCollapsed;
                }
            }, 0);
        };

        this.widgetMessagesClick = function () {

        };

        this.report = function (action) {
            if (action === 'Start') {
                FeaturesService.smartTabletModalOKCancelWithTextarea("Startuj zadanie", "Możesz dodać notatkę w razie potrzeby", "Notatka...", function (Note) {
                    TrolleyService.report$TrolleyEvent($ctrl.Trolley.Id, 200, Note)
                        .then(function () { $timeout(function () { changeStartButton(); }, 0); }, function () { });
                }, function () { });
            } else if (action === 'Pause') {
                FeaturesService.smartTabletModalOKCancelWithTextarea("Pauzuj zadanie", "Możesz dodać notatkę w razie potrzeby", "Notatka...", function (Note) {
                    TrolleyService.report$TrolleyEvent($ctrl.Trolley.Id, 210, Note)
                        .then(function () { $timeout(function () { changeStartButton(); }, 0); }, function () { });
                }, function () { });
            } else if (action === 'Stop') {
                FeaturesService.smartTabletModalOKCancelWithTextarea("Przerwij zadanie", "Opisz krótko powód przerwania", "Notatka...", function (Note) {
                    TrolleyService.report$TrolleyEvent($ctrl.Trolley.Id, 220, Note)
                        .then(function () { $timeout(function () { changeStartButton(); }, 0); }, function () { });
                }, function () { });
            } else if (action === 'End') {
                FeaturesService.smartTabletModalOKCancelWithTextarea("Zakończ zadanie", "Wpisz ilość wykonanych sztuk", "Notatka...", function (Note) {
                    TrolleyService.report$TrolleyEvent($ctrl.Trolley.Id, 300, Note)
                        .then(function () {
                            $timeout(function () {
                                changeStartButton();
                                $ctrl.popoverCallbacks = [null, null, null, null];
                            }, 0);
                        }, function () { });
                }, function () { });
            } else if (action === 'Amount') {
                FeaturesService.smartTabletModalOKCancelWithTextarea("Zgłoś ilość wykonanych sztuk", "Wpisz ilość wykonanych sztuk", "Notatka...", function (Note) {
                    TrolleyService.report$TrolleyEvent($ctrl.Trolley.Id, 1000, Note)
                        .then(function () { $timeout(function () { changeStartButton(); }, 0); }, function () { });
                }, function () { });
            } else if (action === 'Error') {
                FeaturesService.smartTabletModalOKCancelWithTextarea("Zgłoś błąd/awarię", "Opisz krótko sytuację", "Notatka...", function (Note) {
                    TrolleyService.report$TrolleyEvent($ctrl.Trolley.Id, 233, Note)
                        .then(function () { $timeout(function () { changeStartButton(); }, 0); }, function () { });
                }, function () { });
            }

        };

        function changeStartButton() {
            if ($ctrl.Trolley.StateId === 0) {
                $ctrl.StartButtonClass = 'bg-color-gray';
                $ctrl.StartButtonIconClass = 'fa-clock-o';
                $ctrl.popoverCallbacks.buttonStart = $ctrl.report;
                $ctrl.popoverCallbacks.buttonPause = null;
                $ctrl.popoverCallbacks.buttonStop = null;
                $ctrl.popoverCallbacks.buttonEnd = $ctrl.report;
            } else if ($ctrl.Trolley.StateId === 1) {
                $ctrl.StartButtonClass = 'bg-color-greenLight';
                $ctrl.StartButtonIconClass = 'fa-play';
                $ctrl.popoverCallbacks.buttonStart = null;
                $ctrl.popoverCallbacks.buttonPause = $ctrl.report;
                $ctrl.popoverCallbacks.buttonStop = $ctrl.report;
                $ctrl.popoverCallbacks.buttonEnd = $ctrl.report;
            } else if ($ctrl.Trolley.StateId === 2) {
                $ctrl.StartButtonClass = 'bg-color-pink';
                $ctrl.StartButtonIconClass = 'fa-pause';
                $ctrl.popoverCallbacks.buttonStart = $ctrl.report;
                $ctrl.popoverCallbacks.buttonPause = null;
                $ctrl.popoverCallbacks.buttonStop = $ctrl.report;
                $ctrl.popoverCallbacks.buttonEnd = $ctrl.report;
            } else if ($ctrl.Trolley.StateId === 3) {
                $ctrl.StartButtonClass = 'bg-color-red';
                $ctrl.StartButtonIconClass = 'fa-stop';
                $ctrl.popoverCallbacks.buttonStart = $ctrl.report;
                $ctrl.popoverCallbacks.buttonPause = $ctrl.report;
                $ctrl.popoverCallbacks.buttonStop = null;
                $ctrl.popoverCallbacks.buttonEnd = $ctrl.report;
            } else if ($ctrl.Trolley.StateId === 4) {
                $ctrl.StartButtonClass = 'bg-color-darken';
                $ctrl.StartButtonIconClass = 'fa-check-circle';
                $ctrl.popoverCallbacks.buttonStart = null;
                $ctrl.popoverCallbacks.buttonPause = null;
                $ctrl.popoverCallbacks.buttonStop = null;
                $ctrl.popoverCallbacks.buttonEnd = null;
            } else if ($ctrl.Trolley.StateId === 5) {
               // Ignored for now 
            } else if ($ctrl.Trolley.StateId === 6) {
                $ctrl.StartButtonClass = 'bg-color-orange';
                $ctrl.StartButtonIconClass = 'fa-warning-sign';
                $ctrl.popoverCallbacks.buttonStart = $ctrl.report;
                $ctrl.popoverCallbacks.buttonPause = $ctrl.report;
                $ctrl.popoverCallbacks.buttonStop = $ctrl.report;
                $ctrl.popoverCallbacks.buttonEnd = $ctrl.report;
            }
        }

        // INIT
        this._$onInit();
    }
})();