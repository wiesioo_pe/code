﻿"use strict";
console.log("Załadowałem OperatorDoneViewController.js");
/// <reference path="~/app/scripts/angular.js" />



(function () {

    angular.module("app").controller('OperatorDoneViewController', OperatorDoneViewController);

    // CONTROLLER
    OperatorDoneViewController.$inject = ['$rootScope', '$scope', '$timeout', '$q'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'OperatorPanelService', 'CurrentUserEntry'];

    function OperatorDoneViewController($rootScope, $scope, $timeout, $q
        , InfrService, ItemsService, ViewStateService, OperatorPanelService, CurrentUserEntry) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.CurrentUser = null;
            $ctrl.areWidgetsCollapsed = {};

            var promises = {};
            promises.ItemsPromise = OperatorPanelService.get$Items();
            promises.CurrentUserPromise = CurrentUserEntry.get$$One().$promise;

            promises.ItemsPromise.then(function (response) {
                $ctrl.Items = response;
                console.warn($ctrl.Items);
            }, function (error) { });

            promises.CurrentUserPromise.then(function (response) {
                $ctrl.CurrentUser = response;
                console.warn($ctrl.CurrentUser);
            }, function (error) { console.warn(error) });

            $q.all(promises).then(function (response) {
                $timeout(function () {
                    let Operator = InfrService.findFirst($ctrl.Items.Operators, function (elem) {
                        return elem.EmployeeId === $ctrl.CurrentUser.EmployeeId;
                    });

                    if (InfrService.isObject(Operator)) {
                        let OperatorId = Operator.OperatorId;
                        if (OperatorId != null) {
                            $ctrl.MachineTrolleys = $.grep($ctrl.Items.Boxes, function (elem, index) {
                                return ((elem.OperatorId === OperatorId) && (elem.StateId == 4));
                            });
                        } else {
                            $ctrl.MachineTrolleys = $.grep($ctrl.Items.Boxes, function (elem, index) {
                                return ((elem.OperatorId === 1) && (elem.StateId == 4));
                            });
                        }
                    } else {
                        $ctrl.MachineTrolleys = $.grep($ctrl.Items.Boxes, function (elem, index) {
                            return ((elem.OperatorId === 1) && (elem.StateId == 4));
                        });
                    }
                    for (let MachineTrolley of $ctrl.MachineTrolleys) {
                        $ctrl.areWidgetsCollapsed["Id" + MachineTrolley.Id] = true;
                    }
                }, 0);
            }, function (error) { });
        }
        // END OF HOOKS





        // INIT
        this._$onInit();

    }


})();