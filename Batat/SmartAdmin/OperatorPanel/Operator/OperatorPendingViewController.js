﻿"use strict";
console.log("Załadowałem OperatorPendingViewController.js");


(function () {

    angular.module("app").controller("OperatorPendingViewController", operatorPendingViewController);

    // CONTROLLER
    operatorPendingViewController.$inject = ["$scope", "$timeout", "$interval", "$q"
        , "InfrService", "OperatorPanelService", "CurrentUserEntry"];

    function operatorPendingViewController($scope, $timeout, $interval, $q
        , infrService, OperatorPanelService, currentUserEntry) {

        let $ctrl = this;
        $scope.$ctrl = this;
        $ctrl.Items = {};
        

        // LIFECYCLE HOOKS

        this._$onInit = function () {
            $ctrl.CurrentUser = null;
            $ctrl.areWidgetsCollapsed = {};
            $ctrl.areDoneVisible = false;

            $ctrl.CurrentUserPromise = currentUserEntry.get$$One().$promise.then(function (response) {
                $ctrl.CurrentUser = response;
                console.warn($ctrl.CurrentUser);
            }, function (error) { console.warn(error); });

            let itemsPromises = [
                OperatorPanelService.get$Machines().then(function (data) { $ctrl.Items.Machines = data; }),
                OperatorPanelService.get$Trolleys().then(function (data) { $ctrl.Items.Trolleys = data; }),
                OperatorPanelService.get$Parts().then(function (data) { $ctrl.Items.Parts = data; }),
                OperatorPanelService.get$Operations().then(function (data) { $ctrl.Items.Operations = data; }),
                OperatorPanelService.get$Employees().then(function (data) { $ctrl.Items.Employees = data; })
            ];

            $q.all(itemsPromises).then(function () {
                $timeout(function () {
                    let machine = infrService.findFirst($ctrl.Items.Machines, function (elem) {
                        return elem.Id === 1;
                    });

                    if (infrService.isObject(machine)) {
                        let machineId = machine.Id;
                        if (machineId != null) {
                            $ctrl.MachineTrolleys = $.grep($ctrl.Items.Trolleys, function (elem) {
                                return elem.MachineId === machineId;
                            });
                        } else {
                            $ctrl.MachineTrolleys = $.grep($ctrl.Items.Trolleys, function (elem) {
                                return elem.OperatorId === 1;
                            });
                        }
                    } else {
                        $ctrl.MachineTrolleys = $.grep($ctrl.Items.Trolleys, function (elem) {
                            return elem.OperatorId === 1;
                        });
                    }

                    $ctrl.CollapseAll();
                    $ctrl.MachineTrolleys.sort(function (a, b) {
                        return a.momentPlannedStartTime.isBefore(b.momentPlannedStartTime) ? 1 : -1;
                    });
                    $timeout($ctrl.ScrollToCurrentTime, 0);
                    $interval(findCurrentTrolley, 30000);
                    $interval(getNewestItems, 15000);
                }, 0);


            }, function (error) { });

        };

        // END OF HOOKS

        this.CollapseAll = function () {
            for (let operatorTrolley of $ctrl.MachineTrolleys) {
                if (operatorTrolley.StateId === 0) {
                    $ctrl.areWidgetsCollapsed["Id" + operatorTrolley.Id] = true;
                } else {
                    $ctrl.areWidgetsCollapsed["Id" + operatorTrolley.Id] = false;
                }
            }
        };

        this.ScrollToCurrentTime = function () {
            var foundElementIndex = findCurrentTrolley();

            if (foundElementIndex >= 0) {
                angular.element("#content").animate({
                    scrollTop: 0
                }, 0);
                var contentOffsetTop = angular.element("#content").offset().top;
                var itemOffsetTop = angular.element("#TrolleysListItem" + $ctrl.MachineTrolleys[foundElementIndex].Id).offset().top;

                if (itemOffsetTop != null) {
                    angular.element("#content").animate({
                        scrollTop: itemOffsetTop - contentOffsetTop - 20
                    }, 0);
                    $ctrl.areWidgetsCollapsed["Id" + $ctrl.MachineTrolleys[foundElementIndex].Id] = false;

                }
            }
        };

        function findCurrentTrolley() {
            console.warn("Finding current Trolley");
            let now = moment();
            let foundElementIndex = infrService.findIndexOfFirst($ctrl.MachineTrolleys, function (elem) {
                let isBeforeNow = elem.momentPlannedStartTime.isBefore(now) && elem.StateId !== 4;
                return isBeforeNow;
            });

            if ($ctrl.CurrentTrolleyId !== $ctrl.MachineTrolleys[foundElementIndex].Id) {
                $timeout(function () {
                    $ctrl.CurrentTrolleyId = $ctrl.MachineTrolleys[foundElementIndex].Id;
                }, 0);
            }

            console.warn("Found index:" + foundElementIndex);
            return foundElementIndex;
        }



        function getNewestItems() {
            OperatorPanelService.get$NewestItems().then(function () { $timeout(function () { }, 0); });
        }



        // INIT
        this._$onInit();
    }


})();