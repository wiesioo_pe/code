﻿"use strict";
console.log("Załadowałem batat directives.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    angular.module("batat").directive("operatorPanelNavButtons", ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'OperatorPanel/Operator/OperatorPanelNavButtons.html',
            scope: {
                areDoneVisible: '=ngDataAreDoneVisible',
                ShowDone: '&ngFuncShowDone',
                HideDone: '&ngFuncHideDone',
                CollapseAll: '&ngFuncCollapseAll',
                ScrollToCurrentTime: '&ngFuncScrollToCurrentTime'
            },
            link: routeLoadingIndicatorLink,
            controller: OperatorPanelNavButtonsController,
            controllerAs: '$ctrl'
        };
    }]);

    function routeLoadingIndicatorLink($scope, $element, $attrs) {
        $(document.getElementById('left-panel')).append($element);
    }

    OperatorPanelNavButtonsController.$inject = ['$scope', '$interval', 'MachineMonitorLiteState', '$http'];
    function OperatorPanelNavButtonsController($scope, $interval, MachineMonitorLiteState, $http) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            SetDefaultMachineState();

            $interval(function () {

                $http.get('http://test.batat.webserwer.pl/api/MachineMonitorLite/State/Last').
                    then(function (response) {
                        let data = response.data;

                        // Indicator
                        if (data.State == null || data.State < 0) {
                            SetDefaultMachineState;
                            return;
                        }

                        // Indicator icon
                        if (data.Time != null) {
                            let time = moment(data.Time, "YYYY-MM-DDTHH:mm:ss");
                            let thirtySecondsAgo = moment(Date.now()).subtract(30, 's');
                            if (time.isBefore(thirtySecondsAgo)) {
                                $ctrl.MachineState.indicatorIconClasses = ["fa-clock-o"];
                            } else {
                                $ctrl.MachineState.indicatorIconClasses = ["fa-mixcloud"];
                            }
                        } else {
                            $ctrl.MachineState.indicatorIconClasses = ["fa-exclamation-circle"];
                        }

                        if (data.State === $ctrl.MachineState.State) {
                            return;
                        }

                        $ctrl.MachineState.indicatorClasses = [];
                        $ctrl.MachineState.State = data.State;
                        switch (data.State) {
                            case 1:
                                $ctrl.MachineState.indicatorClasses = ["bg-color-orange"];
                                break;
                            case 2:
                                $ctrl.MachineState.indicatorClasses = ["bg-color-greenLight"];
                                break;
                            case 3:
                                $ctrl.MachineState.indicatorClasses = ["bg-color-red"];
                                break;
                        }
                    },
                        function (response) {
                            SetDefaultMachineState();
                            // Indicator icon
                            if (data.Time != null) {
                                $ctrl.MachineState.indicatorIconClasses = ["fa-exclamation-circle"];
                            }
                        });
            }, 1000);
        };
        // END HOOKS

        $ctrl._$onInit();

        function SetDefaultMachineState() {
            $ctrl.MachineState = {
                State: -1,
                indicatorClasses: ["bg-color-darken"],
                indicatorIconClasses: ["fa-rss"]
            };
        }
    }
})();

