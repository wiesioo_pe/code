"use strict";

(function () {

    angular.module('app')
        
        .config(function ($urlRouterProvider, $stateProvider) {

            $stateProvider

            .state('app', {
                abstract: true,
                views: {
                    root: {
                        templateUrl: 'app/layout/OperatorPanel.layout.tpl.html',
                        controller: 'OperatorPanelRootController'
                    }
                }
            })

            .state('app.operatorpendingview', {
                url: '/Operator/Pending',
                data: {
                    title: 'Operator/Pending'
                },
                views: {
                    "content@app": {
                        templateUrl: 'OperatorPanel/Operator/Pending.html?',
                        controller: 'OperatorPendingViewController',
                        controllerAs: '$ctrl',
                        resolve: { }
                    }
                }
            })

            .state('app.operatordoneview', {
                url: '/Operator/Done',
                data: {
                    title: 'Operator/Done'
                },
                views: {
                    "content@app": {
                        templateUrl: 'OperatorPanel/Operator/Done.html?',
                        controller: 'OperatorDoneViewController',
                        controllerAs: '$ctrl',
                        resolve: {}
                    }
                }
            })

            .state('app.machineview', {
                url: '/Machine',
                data: {
                    title: 'Machine'
                },
                views: {
                    "content@app": {
                        templateUrl: 'OperatorPanel/Machine/Index.html?' + Math.random(),
                        controller: 'MachineViewController',
                        controllerAs: '$ctrl',
                        resolve: {}
                    }
                }
            })

            $urlRouterProvider.otherwise('/Operator/Pending');

        })

})();
