﻿"use strict";
console.log("Załadowałem app OperatorPanelRootController.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {

    angular.module("app").controller("OperatorPanelRootController", OperatorPanelRootController);

    OperatorPanelRootController.$inject = [];

    function OperatorPanelRootController() {

        var $ctrl = this;
        
        // LIFECYCLE HOOKS
        this._$onInit = function () {

        };
        // END OF HOOKS

        // INIT
        this._$onInit();
    }

})();


