﻿"use strict";
console.log("Załadowałem OperatorPanelService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("OperatorPanelService", OperatorPanelService);

    OperatorPanelService.$inject = ["$q", "ItemsService", "TrolleyService"];

    function OperatorPanelService($q, ItemsService, TrolleyService) {

    // SERVICE DATA

        let self = this;

        this.get$Machines = function () {
            return ItemsService.$$downloadSingleItems("Machines");
        };

        this.get$Parts = function () {
            return ItemsService.$$downloadSingleItems("Parts");
        };

        this.get$Operations = function () {
            return ItemsService.$$downloadSingleItems("Operations");
        };

        this.get$Employees = function () {
            return ItemsService.$$downloadSingleItems("Employees");
        };

        this.get$Trolleys = function () {
            return TrolleyService.get$Trolleys();
        };

        var cache = {
            Items: {},
            Filters: {
                
            },
            ViewState: {
                ViewSettings: {
                    isOrderedAscending: false,
                    OrderedBy: 'OrderId' 
                }
            }
        };

        var promises = {
            Items: {}
        };


    // INDEX METHODS

        this.getItemsNames = function () {
            return ["Trolleys"];
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded."; });
        };

        this.get$NewestItems = function () {
            return ItemsService.updateAllItems(this.getItemsNames()).then(function (response) {
                return true;
            }, function (error) {
                throw false;
            });
        };

    }

})();