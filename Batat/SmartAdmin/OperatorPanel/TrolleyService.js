﻿"use strict";
console.log("Załadowałem TrolleyService.js");
/// <reference path="~/app/scripts/angular.js" />

(function() {

    // SERVCICE
    angular.module("app").service("TrolleyService", TrolleyService);

    TrolleyService.$inject = ["$q", "$timeout", "$filter", "InfrService", "ItemsService", "TrolleyEventsApiEntry"];

    function TrolleyService($q, $timeout, $filter, InfrService, ItemsService, TrolleyEventsApiEntry) {

        let self = this;

        function InitializeService() {
            self.OperationsPromise = ItemsService.$$downloadSingleItems("Operations")
                .then(function (data) { self.Operations = data; return data; });

            self.OperationTypesPromise = ItemsService.$$downloadSingleItems("OperationTypes")
                .then(function (data) { self.OperationTypes = data; return data; });

            self.TrolleysPromise = ItemsService.$$downloadSingleItems("Trolleys")
                .then(function (data) { self.Trolleys = data; return data; });

            self.MachinesPromise = ItemsService.$$downloadSingleItems("Machines")
                .then(function (data) { self.Machines = data; return data; });

            self.AllDownloadedPromise = $q.all([self.OperationsPromise, self.OperationTypesPromise, self.TrolleysPromise, self.MachinesPromise]).then(function () {
                for (let trolley of self.Trolleys) {
                    self.setTrolleySpecificProperties(trolley);
                }
            });
        }

        this.get$Trolleys = function () {
            return self.AllDownloadedPromise.then(function () { return self.Trolleys; });
        };

        this.report$TrolleyEvent = function (TrolleyId, EventType, Note) {
            return TrolleyEventsApiEntry.report$$event({ TrolleyId: TrolleyId, EventType: EventType, Note: Note })
                .$promise.then(function (response) {
                    // if saved insert or replace inside table
                    let item = InfrService.findFirst(self.Trolleys, function (e) { return e.Id === response.Id; });
                    if (item !== null) {
                        self.setTrolleySpecificProperties(response);
                        InfrService.copyObjectProperties(item, response);
                    } else {
                        console.error("Trolley not found");
                    }

                    $timeout(function () { }, 0);
                    return response;
                }, function (error) {
                    throw error;
                });
            };

        this.setTrolleySpecificProperties = function(Trolley) {
            Trolley.MadeRatio = Math.trunc(100 * Trolley.MadeAmount) / Trolley.ToDoAmount / 100;
            Trolley.MadePercentage = Math.trunc(Trolley.MadeRatio * 100);

            Trolley.momentPlannedStartTime = moment(Trolley.PlannedStartTime, "YYYY-MM-DDTHH:mm:ss");
            Trolley.PlannedStartDate = Trolley.momentPlannedStartTime.format("YYYY-MM-DD");
            Trolley.PlannedStartHour = Trolley.momentPlannedStartTime.format("HH:mm");
            Trolley.PlannedStartDayOfWeek = Trolley.momentPlannedStartTime.format("dddd");
            Trolley.momentPlannedEndTime = moment(Trolley.PlannedEndTime, "YYYY-MM-DDTHH:mm:ss");
            Trolley.PlannedEndDate = Trolley.momentPlannedEndTime.format("YYYY-MM-DD");
            Trolley.PlannedEndHour = Trolley.momentPlannedEndTime.format("HH:mm");
            Trolley.PlannedEndDayOfWeek = Trolley.momentPlannedStartTime.format("dddd");
            let ms = Trolley.momentPlannedEndTime.diff(Trolley.momentPlannedStartTime);
            let d = moment.duration(ms);
            Trolley.Duration = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");

            Trolley.OperationTypeName = $filter("getOperationTypeProperty")($filter("getOperationProperty")(Trolley.OperationId, "OperationTypeId", self.Operations), "Name", self.OperationTypes);
            Trolley.MachineName = $filter("getMachineProperty")(Trolley.MachineId, "Model", self.Machines) + " - " + $filter("getMachineProperty")(Trolley.MachineId, "Manufacturer", self.Machines);
        };

        InitializeService();
    }

})();