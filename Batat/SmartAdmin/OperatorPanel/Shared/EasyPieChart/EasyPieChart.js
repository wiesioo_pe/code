﻿angular.module("app").directive('customEasyPieChart', function () {

    let element;

    customEasyPieChartController.$inject = ['$scope'];
    function customEasyPieChartController($scope) {
        $scope.$watch('percent', function () {
            $(element).data('easyPieChart').update($scope.percent);
        });
    }

    return {
        restrict: 'C',
        scope: {
            percent: '='
        },
        link: function (scope, elem, attrs) {
            element = elem[0];
            $(element).easyPieChart();
            $(element).data('easyPieChart').update(scope.percent);
        },
        controller: customEasyPieChartController
    };
});