
"use strict";

angular.module('SmartAdmin.UI').directive('smartPopoverHtml', function ($sce) {
    return {
        restrict: "A",
        link: function(scope, element, attributes){
            var options = {};
            options.content = $sce.trustAsHtml(attributes.smartPopoverHtml);
            options.placement = attributes.popoverPlacement || 'top';
            options.html = true;
            options.trigger =  attributes.popoverTrigger || 'click';
            options.title =  attributes.popoverTitle || attributes.title;
            element.popover(options)

        }

    };
});
