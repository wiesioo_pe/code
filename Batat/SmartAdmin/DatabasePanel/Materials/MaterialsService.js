﻿"use strict";
console.log("Załadowałem MaterialsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("MaterialsService", MaterialsService);


    MaterialsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];
    
    function MaterialsService($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var MaterialsService = this;
        

        // SERVICE DATA

        var data = {
            ItemsNames: ['Materials', 'Groups', 'Parameters', 'MaterialTypes']
            }

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        }

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        }

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        }

        this.getMainItemsName = function () { return "Materials"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.CodeColumnWidth = "5em";
            ViewData.TypeColumnWidth = "7em";
            ViewData.NameColumnWidth = "6em";
            ViewData.ParametersColumnWidth = "6em";
            ViewData.DescColumnWidth = "15em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        }

        this.filterItems = function () {
            //console.log(cache.Items.Materials);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Materials) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("MaterialsService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _TypeIdFilter = cache.ViewState.ViewSettings.MaterialTypeIdFilter;
                var _CodeFilter = cache.ViewState.ViewSettings.CodeFilter;
                var _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                var _DescFilter = cache.ViewState.ViewSettings.DescFilter;
                var _ParameterIdFilter = cache.ViewState.ViewSettings.ParameterIdFilter;
                var _ParameterFromFilter = cache.ViewState.ViewSettings.ParameterFromFilter;
                var _ParameterToFilter = cache.ViewState.ViewSettings.ParameterToFilter;

                // for each __item in Materials
                for (var i = 0; i < cache.Items.Materials.length; i++) {

                    var _Item = cache.Items.Materials[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    // Check if in group
                    var _isInGroup = false;
                    if (_GroupIdFilter == "All") {
                        _isInGroup = true;
                    } else {
                        var _matchedMaterials = _Item.Groups.filter(function (__item) {
                            return __item.GroupId == _GroupIdFilter;
                        });
                        if (_matchedMaterials[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check if of Type
                    var _isOfType = false;
                    if (_TypeIdFilter == "All") {
                        _isOfType = true;
                    } else {
                        if (_Item.MaterialTypeId == _TypeIdFilter) {
                            _isOfType = true;
                        }
                    }

                    // Check Material code filters
                    var _doesMaterialCodeContain = false;

                    if (_CodeFilter == "") {
                        _doesMaterialCodeContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Code.toLowerCase();
                        _LowerCaseFilter = _CodeFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesMaterialCodeContain = true;
                        }
                    }

                    // Check Material name filters
                    var _doesMaterialNameContain = false;

                    if (_NameFilter == "") {
                        _doesMaterialNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesMaterialNameContain = true;
                        }
                    }

                    // Check Material desc filters
                    var _doesMaterialDescContain = false;

                    if (_DescFilter == "") {
                        _doesMaterialDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesMaterialDescContain = true;
                        }
                    }

                    // Check Parameter filters
                    var _isParameterBetween = false, _isParameterFrom = false, _isParameterTo = false;

                    if (_ParameterIdFilter == "All") {
                        _isParameterFrom = true;
                        _isParameterTo = true;
                    } else {
                        var _MatchedParameters = _Item.Parameters.filter(function (__item) {
                            return __item.Id == _ParameterIdFilter;
                        });
                        if (_MatchedParameters[0] != null) {
                            if (_ParameterFromFilter == "") {
                                _isParameterFrom = true;
                            }
                            else {
                                // If Parameter Value and _ParameterFromFilter Input are numbers
                                if (!(isNaN(_MatchedParameters[0].Value)) && !(isNaN(_ParameterFromFilter))) {
                                    if (Number(_MatchedParameters[0].Value) >= Number(_ParameterFromFilter)) {
                                        _isParameterFrom = true;
                                    }
                                }
                                    // but if they're strings
                                else {
                                    if (_MatchedParameters[0].Value >= _ParameterFromFilter) {
                                        _isParameterFrom = true;
                                    }
                                }
                            }
                            if (_ParameterToFilter == "") {
                                _isParameterTo = true;
                            }
                            else {

                                // If Parameter Value and _ParameterToFilter Input are numbers
                                if (!(isNaN(_MatchedParameters[0].Value)) && !(isNaN(_ParameterToFilter))) {
                                    if (Number(_MatchedParameters[0].Value) <= Number(_ParameterToFilter)) {
                                        _isParameterTo = true;
                                    }
                                }
                                    // but if they're strings
                                else {
                                    if (_MatchedParameters[0].Value <= _ParameterToFilter) {
                                        _isParameterTo = true;
                                    }
                                }
                            }
                        }
                    }

                    _isParameterBetween = _isParameterFrom && _isParameterTo;

                    // Set _Item visibility
                    _Item.Visible = (_isInGroup && _isOfType && _doesMaterialCodeContain && _doesMaterialNameContain && _doesMaterialDescContain && _isParameterBetween);

                }
            }
        }

        this.saveViewState = function () {
            console.log('MaterialsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("MaterialsIndex", cache.ViewState);
        }

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        }

        this.clearViewState = function () {
            ViewStateService.clearViewState("MaterialsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        }

        this.onIndexDataDownloadCompleted = function (scope) {
            
        }

        this.getIndexHeaderDictionary = function () {
            
            var Dictionary = {};

            Dictionary.ItemsNameString = "Materiały";
            Dictionary.ActiveItemString = "Materiał nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowe materiał";
            Button.Href = function () {
                return "#/Materials/Edit?Id=0";
            };
            Button.Click = function () {
                MaterialsService.set$ActiveItemId(0);
            }
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Materials/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Materials/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        }

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Kod";
            Dictionary.ColumnsNames[2] = "Typ";
            Dictionary.ColumnsNames[3] = "Nazwa";
            Dictionary.ColumnsNames[4] = "Parametry";
            Dictionary.ColumnsNames[5] = "Opis";
            return Dictionary;
        }

        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    MaterialsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return MaterialsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        }


        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return MaterialsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    MaterialsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        }



        // EDIT METHODS

        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Materials, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        }


        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        }

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        }

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        }

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Materials", cache.ActiveItem)
                .then(
                function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    MaterialsService.set$ActiveItemId(response.Id);
                    return true;
                },
                function (error) {
                    throw error.data;
                });
        }

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                function (response) {
                     var SelectOptions = {};
                    //MaterialType options
                    SelectOptions.MaterialType = [];
                    for (var MaterialType of cache.Items.MaterialTypes) {
                        var Option = {};
                        Option.Name = MaterialType.Code + ' - ' + MaterialType.Name;
                        Option.Desc = MaterialType.Desc;
                        Option.Value = MaterialType.MaterialTypeId;
                        SelectOptions.MaterialType.push(Option);
                    }
                    return SelectOptions;
                }, function (error) { throw false; });
        }

        this.isEditDisabled = function () {
            return false;
        }



        // Private functions

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.MaterialTypeIdFilter = "All";
            cache.ViewState.ViewSettings.CodeFilter = "";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";
            cache.ViewState.ViewSettings.ParameterIdFilter = "All";
            cache.ViewState.ViewSettings.ParameterFromFilter = "";
            cache.ViewState.ViewSettings.ParameterToFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Code
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Type
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Parameters
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // Desc

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("MaterialsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

})();