"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.materials', {
            url: '/Materials',
            data: {
                title: 'Materials'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Materials/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['MaterialsService', function (MaterialsService) {
                            return MaterialsService.get$Items();
                        }],
                        MainItemsService: ['MaterialsService', function (MaterialsService) {
                            return MaterialsService;
                        }],
                    }
                }
            }
        })

        .state('app.materialedit', {
            url: '/Materials/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Materials/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['MaterialsService', function (MaterialsService) {
                            return MaterialsService.get$Items();
                        }],
                        MainItemsService: ['MaterialsService', function (MaterialsService) {
                            return MaterialsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();