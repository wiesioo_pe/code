﻿"use strict";
console.log("Załadowałem ContactsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("ContactsService", ContactsService);


    ContactsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function ContactsService($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var ContactsService = this;


        // SERVICE DATA

        var data = {
            ItemsNames: ['Contacts', 'Subjects', 'Groups']
        };

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        };

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        };

        this.getMainItemsName = function () { return "Contacts"; };

        this.getViewData = function () {

            var table = document.getElementsByClassName('index-table-container')[0].children[0];
            var fontSize = parseFloat(getStyle(table, 'font-size').replace("px", ""));
            var tableWidth = table.offsetWidth;

            var ViewWidths = [];
            ViewWidths[0] = 6; // Id
            ViewWidths[1] = 10; // Groups
            ViewWidths[2] = 10; // Name
            ViewWidths[3] = 10; // Subject
            ViewWidths[4] = 8; // Division
            ViewWidths[5] = 8; // Position
            ViewWidths[6] = 8; // Phones
            ViewWidths[7] = 8; // eMail
            ViewWidths[8] = 20; // Desc
            ViewWidths[9] = 8; // Photo
            ViewWidths[10] = 8; // Icon

            var sum = 0;
            for (var i = 0; i < cache.ViewState.ViewSettings.ColumnsVisibility.length; i++) {
                if (cache.ViewState.ViewSettings.ColumnsVisibility[i] === true) {
                    sum += ViewWidths[i];
                }
            }

            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.NameColumnWidth = "7em";
            ViewData.SubjectColumnWidth = "6em";
            ViewData.DivisionColumnWidth = "7em";
            ViewData.PositionColumnWidth = "6em";
            ViewData.PhonesColumnWidth = "15em";
            ViewData.eMailColumnWidth = "6em";
            ViewData.DescColumnWidth = "10em";
            ViewData.PhotoColumnWidth = "3em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        };

        this.filterItems = function () {
            //console.log(cache.Items);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Contacts) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("ContactsService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _SubjectIdFilter = cache.ViewState.ViewSettings.SubjectIdFilter;
                var _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                var _DivisionFilter = cache.ViewState.ViewSettings.DivisionFilter;
                var _PositionFilter = cache.ViewState.ViewSettings.PositionFilter;
                var _PhonesFilter = cache.ViewState.ViewSettings.PhonesFilter;
                var _eMailFilter = cache.ViewState.ViewSettings.eMailFilter;
                var _DescFilter = cache.ViewState.ViewSettings.DescFilter;

                // for each __item in Contacts
                for (var i = 0; i < cache.Items.Contacts.length; i++) {

                    var _Item = cache.Items.Contacts[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    // Check if in group
                    var _isInGroup = false;
                    if (_GroupIdFilter === "All") {
                        _isInGroup = true;
                    } else {
                        var _matchedContacts = _Item.Groups.filter(function (__item) {
                            return __item.GroupId === _GroupIdFilter;
                        });
                        if (_matchedContacts[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check if for Subject
                    var _isForSubject = false;
                    if (_SubjectIdFilter === "All") {
                        _isForSubject = true;
                    } else {
                        if (_Item.SubjectId === _SubjectIdFilter) {
                            _isForSubject = true;
                        }
                    }

                    // Check Contact Name filters
                    var _doesContactNameContain = false;

                    if (_NameFilter === "") {
                        _doesContactNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesContactNameContain = true;
                        }
                    }

                    // Check Contact Division filters
                    var _doesContactDivisionContain = false;

                    if (_DivisionFilter === "") {
                        _doesContactDivisionContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Division.toLowerCase();
                        _LowerCaseFilter = _DivisionFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesContactDivisionContain = true;
                        }
                    }

                    // Check Contact Position filters
                    var _doesContactPositionContain = false;

                    if (_PositionFilter === "") {
                        _doesContactPositionContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Position.toLowerCase();
                        _LowerCaseFilter = _PositionFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesContactPositionContain = true;
                        }
                    }

                    // Check Contact Phones filters
                    var _doesContactPhonesContain = false;

                    if (_PhonesFilter === "") {
                        _doesContactPhonesContain = true;
                    }
                    else {
                        var _doesOfficePhoneContain = false;
                        var _doesBusinessPhoneContain = false;
                        var _doesPrivatePhoneContain = false;
                        var _doesFaxContain = false;
                        _LowerCaseProperty = _Item.OfficePhone.toLowerCase();
                        _LowerCaseFilter = _PhonesFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesOfficePhoneContain = true;
                        }
                        _LowerCaseProperty = _Item.BusinessPhone.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesBusinessPhoneContain = true;
                        }
                        _LowerCaseProperty = _Item.PrivatePhone.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesPrivatePhoneContain = true;
                        }
                        _LowerCaseProperty = _Item.Fax.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesFaxContain = true;
                        }
                        _doesContactPhonesContain = _doesOfficePhoneContain || _doesBusinessPhoneContain || _doesPrivatePhoneContain || _doesFaxContain;
                    }

                    // Check Contact eMail filters
                    var _doesContacteMailContain = false;

                    if (_eMailFilter === "") {
                        _doesContacteMailContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.eMail.toLowerCase();
                        _LowerCaseFilter = _eMailFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesContacteMailContain = true;
                        }
                    }

                    // Check Contact Desc filters
                    var _doesContactDescContain = false;

                    if (_DescFilter === "") {
                        _doesContactDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesContactDescContain = true;
                        }
                    }

                    // Set _Item visibility
                    _Item.Visible = (_isInGroup && _isForSubject && _doesContactNameContain && _doesContactDivisionContain
                        && _doesContactPositionContain && _doesContactPhonesContain && _doesContacteMailContain && _doesContactDescContain);

                }
            }
        };

        this.saveViewState = function () {
            console.log('ContactsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("ContactsIndex", cache.ViewState);
        };

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        };

        this.clearViewState = function () {
            ViewStateService.clearViewState("ContactsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        };

        this.onIndexDataDownloadCompleted = function (scope) {

        };

        this.getIndexHeaderDictionary = function () {

            var Dictionary = {};

            Dictionary.ItemsNameString = "Kontakty";
            Dictionary.ActiveItemString = "Kontakt nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowy kontakt";
            Button.Href = function () {
                return "#/Contacts/Edit?Id=0";
            };
            Button.Click = function () {
                ContactsService.set$ActiveItemId(0);
            };
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Contacts/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Contacts/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Imię i nazwisko";
            Dictionary.ColumnsNames[2] = "Firma";
            Dictionary.ColumnsNames[3] = "Dział";
            Dictionary.ColumnsNames[4] = "Stanowisko";
            Dictionary.ColumnsNames[5] = "Telefony";
            Dictionary.ColumnsNames[6] = "e-mail";
            Dictionary.ColumnsNames[7] = "Opis";
            Dictionary.ColumnsNames[8] = "Zdjęcie";
            return Dictionary;
        };

        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    ContactsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return ContactsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };


        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return ContactsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    ContactsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        };



        // EDIT METHODS

        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Contacts, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        };

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        };

        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        };

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        };

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Contacts", cache.ActiveItem)
                .then(
                    function (response) {
                        cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                        ContactsService.set$ActiveItemId(response.Id);
                        return true;
                    },
                    function (error) {
                        throw error.data;
                    });
        };

        this.isEditDisabled = function () {
            return false;
        };

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                    function (response) {
                        let SelectOptions = {};
                        //Subject options
                        SelectOptions.Subject = [];
                        for (let Subject of cache.Items.Subjects) {
                            let Option = {};
                            Option.Name = Subject.Id + ' - ' + Subject.ShortName;
                            Option.Desc = Subject.Name;
                            Option.Value = Subject.Id;
                            SelectOptions.Subject.push(Option);
                        }

                        return SelectOptions;
                    }, function (error) { throw false; });
        };


        // Private functions

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.SubjectIdFilter = "All";
            cache.ViewState.ViewSettings.DivisionFilter = "";
            cache.ViewState.ViewSettings.PositionFilter = "";
            cache.ViewState.ViewSettings.PhonesFilter = "";
            cache.ViewState.ViewSettings.eMailFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Subject
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Division
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Position
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // Phones
            cache.ViewState.ViewSettings.ColumnsVisibility[6] = true; // eMail
            cache.ViewState.ViewSettings.ColumnsVisibility[7] = true; // Desc
            cache.ViewState.ViewSettings.ColumnsVisibility[8] = true; // Photo

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("ContactsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

})();