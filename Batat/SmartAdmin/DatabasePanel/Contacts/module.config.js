"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

            .state('app.contacts', {
                url: '/Contacts',
                data: {
                    title: 'Contacts'
                },
                views: {
                    "content@app": {
                        templateUrl: 'DatabasePanel/Contacts/Index.html?' + Math.random(),
                        controller: 'IndexViewController',
                        controllerAs: '$ctrl',
                        resolve: {
                            Items: ['ContactsService', function (ContactsService) {
                                return ContactsService.get$Items();
                            }],
                            MainItemsService: ['ContactsService', function (ContactsService) {
                                return ContactsService;
                            }]
                        }
                    }
                }
            })

            .state('app.editcontact', {
                url: '/Contacts/Edit',
                data: {
                    title: 'Edit'
                },
                views: {
                    "content@app": {
                        templateUrl: 'DatabasePanel/Contacts/Edit.html?' + Math.random(),
                        controller: 'EditViewController',
                        controllerAs: '$ctrl',
                        resolve: {
                            Items: ['ContactsService', function (ContactsService) {
                                return ContactsService.get$Items();
                            }],
                            MainItemsService: ['ContactsService', function (ContactsService) {
                                return ContactsService;
                            }]
                        }
                    }
                }
            });

    }]);
})();