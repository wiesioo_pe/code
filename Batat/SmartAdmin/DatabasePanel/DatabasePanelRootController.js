﻿"use strict";
console.log("Załadowałem app DatabasePanelRootController.js");
/// <reference path="~/app/scripts/angular.js" />

angular.module("app").controller("DatabasePanelRootController",
    ['$scope', '$injector', '$location', '$window', 'InfrService', 'ItemsService',
        function ($scope, $injector, $location, $window, InfrService, ItemsService) {


            var Controller = this;
            Controller.isViewLoaded = { Index: {}, Edit: {} };
            for(var itemsName of ItemsService.getItemsNames()) {
                Controller.isViewLoaded.Index[itemsName] = false;
                Controller.isViewLoaded.Edit[itemsName] = false;
            }


            $scope.IndexItemsNames = {};
        
            function ChangeViewTo (Name) {
                Controller.ActiveView = Name;
                console.log("");
                console.log("************************** Path change **************************" + $location.path());
                console.log('View Changed to: ' + Controller.ActiveView);
                console.log("");
            }

            function ChangeLocationTo(Path) {
                $location.path(Path);
            }

            this.onPathChanged = function () {
                // Get where you are first and inject service
                var CurrentPath = $location.path().split('/');
                var ItemsName = CurrentPath[1];

                if (!ItemsService.isValidItemsName(ItemsName)) {
                    return;
                }

                var MainItemsService = $injector.get(ItemsName + 'Service');

                if (CurrentPath[2] === 'Edit') {
                    if (InfrService.isStringANumber(CurrentPath[3])) {

                        if (CurrentPath[4] === 'Clone') {
                            MainItemsService.set$ActiveItemIdAsClone(parseInt(CurrentPath[3]));
                        } else {
                            MainItemsService.set$ActiveItemId(parseInt(CurrentPath[3]));
                        }
                    }
                    Controller.isViewLoaded.Edit[ItemsName] = true;
                    ChangeViewTo(ItemsName + 'Edit');
                } else if (CurrentPath[2] === 'Authorize') {
                    MainItemsService.authorize$Order(parseInt(CurrentPath[3]));
                    ChangeLocationTo(ItemsName);
                } else if (CurrentPath[2] === 'Unauthorize') {
                    MainItemsService.unauthorize$Order(parseInt(CurrentPath[3]));
                    ChangeLocationTo(ItemsName);
                } else if (CurrentPath[2] === 'IntoProduction') {
                    MainItemsService.sendIntoProduction$Order(parseInt(CurrentPath[3]));
                    ChangeLocationTo(ItemsName);
                } else {
                    // LOAD INDEX
                    Controller.isViewLoaded.Index[ItemsName] = true;
                    ChangeViewTo(ItemsName + 'Index');
                }
            };
           // this.onPathChanged();

            

            $scope.$watch(function () { return $location.path(); },
                function () { Controller.onPathChanged(); }, true);


            // EVENTS

            // mechanism to save ViewStates before app closes
            $window.onDestroyCallbacks = [];
            $window.registerOnDestroyCallback = function (callback) {
                if (InfrService.isFunction(callback)) {
                    $window.onDestroyCallbacks.push(callback);
                }
            };

            $window.onDestroy = function () {
                for (var callback of $window.onDestroyCallbacks) {
                    callback();
                }
            };

            // save ViewStates before window/tab close to let scopes save data etc.
            $window.onbeforeunload = function (e) {
                $window.onDestroy();
            };

            $scope.$on('$destroy', function () {
                $window.onDestroy();
            });

            $scope.$on('$locationChangeStart', function () {
                $window.onDestroy();
            });

}]);

