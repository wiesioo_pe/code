"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.operators', {
            url: '/Operators',
            data: {
                title: 'Operators'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Operators/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['OperatorsService', function (OperatorsService) {
                            return OperatorsService.get$Items();
                        }],
                        MainItemsService: ['OperatorsService', function (OperatorsService) {
                            return OperatorsService;
                        }],
                    }
                }
            }
        })

        .state('app.operatoredit', {
            url: '/Operators/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Operators/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['OperatorsService', function (OperatorsService) {
                            return OperatorsService.get$Items();
                        }],
                        MainItemsService: ['OperatorsService', function (OperatorsService) {
                            return OperatorsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();