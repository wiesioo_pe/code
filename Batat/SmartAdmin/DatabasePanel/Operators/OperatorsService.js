﻿"use strict";
console.log("Załadowałem OperatorsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("OperatorsService", OperatorsService);

    OperatorsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function OperatorsService ($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var OperatorsService = this;
        

        // SERVICE DATA

        var data = {
            ItemsNames: ['Operators', 'Groups']
        };

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        };

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        };

        this.getMainItemsName = function () { return "Operators"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.NameColumnWidth = "7em";
            ViewData.PositionColumnWidth = "7em";
            ViewData.FunctionColumnWidth = "6em";
            ViewData.NotesColumnWidth = "6em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        };

        this.filterItems = function () {
            //console.log(cache.Items.Operators);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Operators) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("OperatorsService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                var _PositionFilter = cache.ViewState.ViewSettings.PositionFilter;
                var _FunctionFilter = cache.ViewState.ViewSettings.FunctionFilter;

                // for each __item in Operators
                for (var i = 0; i < cache.Items.Operators.length; i++) {

                    var _Item = cache.Items.Operators[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    // Check if in group
                    var _isInGroup = false;
                    if (_GroupIdFilter == "All") {
                        _isInGroup = true;
                    } else {
                        var _matchedOperators = _Item.Groups.filter(function (__item) {
                            return __item.GroupId == _GroupIdFilter;
                        });
                        if (_matchedOperators[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check operator name filters
                    var _doesOperatorNameContain = false;

                    if (_NameFilter == "") {
                        _doesOperatorNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesOperatorNameContain = true;
                        }
                    }

                    // Check operator position filters
                    var _doesOperatorPositionContain = false;

                    if (_PositionFilter == "") {
                        _doesOperatorPositionContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Position.toLowerCase();
                        _LowerCaseFilter = _PositionFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesOperatorPositionContain = true;
                        }
                    }

                    // Check operator function filters
                    var _doesOperatorFunctionContain = false;

                    if (_FunctionFilter == "") {
                        _doesOperatorFunctionContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Function.toLowerCase();
                        _LowerCaseFilter = _FunctionFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesOperatorFunctionContain = true;
                        }
                    }

                    // Set _Item visibility
                    _Item.Visible = _isInGroup && _doesOperatorNameContain && _doesOperatorPositionContain && _doesOperatorFunctionContain;

                }
            }
        };

        this.saveViewState = function () {
            console.log('OperatorsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("OperetorsIndex", cache.ViewState);
        };

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        };

        this.clearViewState = function () {
            ViewStateService.clearViewState("OperetorsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        };

        this.onIndexDataDownloadCompleted = function (scope) {

        };

        this.getIndexHeaderDictionary = function () {

            var Dictionary = {};

            Dictionary.ItemsNameString = "Operatorzy";
            Dictionary.ActiveItemString = "Operator nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowego operatora";
            Button.Href = function () {
                return "#/Operators/Edit?Id=0";
            };
            Button.Click = function () {
                OperatorsService.set$ActiveItemId(0);
            };
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];

            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];

            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Operators/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);

            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Operators/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Imię i nazwisko";
            Dictionary.ColumnsNames[2] = "Stanowisko";
            Dictionary.ColumnsNames[3] = "Funkcja";
            Dictionary.ColumnsNames[4] = "Uwagi";
            return Dictionary;
        };

            
        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    OperatorsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return OperatorsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };

            
        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return OperatorsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    OperatorsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        };



    // EDIT METHODS

            
        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Operators, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        };

            
        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        };

            
        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        };

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        };

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Operators", cache.ActiveItem)
                .then(
                    function (response) {
                        cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                        OperatorsService.set$ActiveItemId(response.Id);
                        return true;
                    },
                    function (error) {
                        throw error.data;
                    });
        };
        
        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                    function (response) {
                        var SelectOptions = {};
                        return SelectOptions;
                    }, function (error) { throw false; });
        };

        this.isEditDisabled = function () {
            return false;
        };



    // PRIVATE FUNCTIONS

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.PositionFilter = "";
            cache.ViewState.ViewSettings.FunctionFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Position
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Function
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Notes

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("OperatorsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }
})();