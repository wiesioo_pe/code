﻿"use strict";
console.log("Załadowałem FixedCostsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("FixedCostsService", FixedCostsService);


    FixedCostsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function FixedCostsService($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var FixedCostsService = this;


        // SERVICE DATA

        var data = {
            ItemsNames: ['FixedCosts', 'FixedCostCategories', 'FixedCostTypes', 'Groups']
        }

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {},
            Temporary: { LastPeriod: "" }
        }

        var promises = {
            Items: {}
        }

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        }

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        }

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        }

        this.getMainItemsName = function () { return "FixedCosts"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "12em";
            ViewData.CategoryColumnWidth = "10em";
            ViewData.TypeColumnWidth = "10em";
            ViewData.StartDateColumnWidth = "8em";
            ViewData.StopDateColumnWidth = "8em";
            ViewData.AmountColumnWidth = "6em";
            ViewData.AmountPerPeriodColumnWidth = "8em";
            ViewData.DescColumnWidth = "15em";
            ViewData.PeriodColumnWidth = "6em";
            ViewData.isRecurringColumnWidth = "7em";
            ViewData.IconColumnWidth = "3em";

            return ViewData;
        }

        this.filterItems = function () {
            //console.log(cache.Items.FixedCosts);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.FixedCosts) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("FixedCostsService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _CategoryIdFilter = cache.ViewState.ViewSettings.FixedCostCategoryIdFilter;
                var _TypeIdFilter = cache.ViewState.ViewSettings.FixedCostTypeIdFilter;
                var _AmountFromFilter = cache.ViewState.ViewSettings.AmountFromFilter;
                var _AmountToFilter = cache.ViewState.ViewSettings.AmountToFilter;
                var _AmountPerPeriodFromFilter = cache.ViewState.ViewSettings.AmountPerPeriodFromFilter;
                var _AmountPerPeriodToFilter = cache.ViewState.ViewSettings.AmountPerPeriodToFilter;
                var _DescFilter = cache.ViewState.ViewSettings.DescFilter;
                var _PeriodFromFilter = cache.ViewState.ViewSettings.PeriodFromFilter;
                var _PeriodToFilter = cache.ViewState.ViewSettings.PeriodToFilter;
                var _isRecuuringFilter = cache.ViewState.ViewSettings.isRecurringFilter;
                var _isRecuuringFilterBool = _isRecuuringFilter == 'true';
                var _PeriodFilter = cache.ViewState.ViewSettings.PeriodFilter;

                if (cache.Temporary.LastPeriod != _PeriodFilter) {
                    FixedCostsService.periodChanged();
                    cache.Temporary.LastPeriod = _PeriodFilter;
                }

                // Start DATE
                var _StartDateFrom, _StartDateTo;
                try {
                    var _StartDatePartsFrom = cache.ViewState.ViewSettings.StartDateFromFilter.split('-');
                    _StartDateFrom = new Date(_StartDatePartsFrom[2], _StartDatePartsFrom[1] - 1, _StartDatePartsFrom[0]);
                }
                catch (ex) { }
                try {
                    var _StartDatePartsTo = cache.ViewState.ViewSettings.StartDateToFilter.split('-');
                    _StartDateTo = new Date(_StartDatePartsTo[2], _StartDatePartsTo[1] - 1, _StartDatePartsTo[0]);
                }
                catch (ex) { }


                // Stop DATE
                var _StopDateFrom, _StopDateTo;

                try {
                    var _StopDatePartsFrom = cache.ViewState.ViewSettings.StopDateFromFilter.split('-');
                    _StopDateFrom = new Date(_StopDatePartsFrom[2], _StopDatePartsFrom[1] - 1, _StopDatePartsFrom[0]);
                }
                catch (ex) { }
                try {
                    var _StopDatePartsTo = cache.ViewState.ViewSettings.StopDateToFilter.split('-');
                    _StopDateTo = new Date(_StopDatePartsTo[2], _StopDatePartsTo[1] - 1, _StopDatePartsTo[0]);
                }
                catch (ex) { }


                // for each item in FixedCosts
                for (var i = 0; i < cache.Items.FixedCosts.length; i++) {


                    var _Item = cache.Items.FixedCosts[i];
                    var _isInGroup = false;
                    var _isInCategory = false;
                    var _isOfType = false;
                    var _isBetweenStartDates = false;
                    var _isBetweenStopDates = false;
                    var _isAmountBetween = false, _isAmountFrom = false, _isAmountTo = false;
                    var _isAmountPerPeriodBetween = false, _isAmountPerPeriodFrom = false, _isAmountPerPeriodTo = false;
                    var _doesDescContain = false;
                    var _isPeriodBetween = false, _isPeriodFrom = false, _isPeriodTo = false;
                    var _isRecurrencyMet = false;

                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";


                    // Check if in group
                    if (_GroupIdFilter == "All") {
                        _isInGroup = true;
                    } else {
                        var MatchedFixedCosts = _Item.Groups.filter(function (item) {
                            return item.GroupId == _GroupIdFilter;
                        });
                        if (MatchedFixedCosts[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check if is in CategoryId
                    if (_CategoryIdFilter == "All") {
                        _isInCategory = true;
                    } else {
                        if (_Item.CategoryId == _CategoryIdFilter) {
                            _isInCategory = true;
                        }
                    }

                    // Check if is of TypeId
                    if (_TypeIdFilter == "All") {
                        _isOfType = true;
                    } else {
                        if (_Item.TypeId == _TypeIdFilter) {
                            _isOfType = true;
                        }
                    }

                    // Check if between Start Dates
                    var _isAboveFrom = false;
                    var _isBelowTo = false;

                    // Get _StartDate for _Item
                    var _StartDate;
                    try {
                        var StartDateParts = _Item.StartDate.split('-');
                        _StartDate = new Date(StartDateParts[2], StartDateParts[1] - 1, StartDateParts[0]);
                    }
                    catch (ex) { }


                    if (cache.ViewState.ViewSettings.StartDateFromFilter == "") {
                        _isAboveFrom = true;
                    }
                    else {
                        if (_StartDate >= _StartDateFrom) {
                            _isAboveFrom = true;
                        }
                    }

                    if (cache.ViewState.ViewSettings.StartDateToFilter == "") {
                        _isBelowTo = true;
                    }
                    else {
                        if (_StartDate <= _StartDateTo) {
                            _isBelowTo = true;
                        }
                    }

                    _isBetweenStartDates = _isAboveFrom && _isBelowTo;


                    // Check if between Stop Dates
                    _isAboveFrom = false;
                    _isBelowTo = false;

                    // Get _StopDate for _Item
                    var _StopDate;
                    try {
                        var StopDateParts = _Item.StopDate.split('-');
                        _StopDate = new Date(StopDateParts[2], StopDateParts[1] - 1, StopDateParts[0]);
                    }
                    catch (ex) { }


                    if (cache.ViewState.ViewSettings.StopDateFromFilter == "") {
                        _isAboveFrom = true;
                    }
                    else {
                        if (_StopDate >= _StopDateFrom) {
                            _isAboveFrom = true;
                        }
                    }

                    if (cache.ViewState.ViewSettings.StopDateToFilter == "") {
                        _isBelowTo = true;
                    }
                    else {
                        if (_StopDate <= _StopDateTo) {
                            _isBelowTo = true;
                        }
                    }

                    _isBetweenStopDates = _isAboveFrom && _isBelowTo;



                    // Check if Amount is between
                    if (_AmountFromFilter == "") {
                        _isAmountFrom = true;
                    }
                    else {
                        // If Amount and _AmountFromFilter Input are numbers
                        if (!(isNaN(_Item.Amount)) && !(isNaN(_AmountFromFilter))) {
                            if (Number(_Item.Amount) >= Number(_AmountFromFilter)) {
                                _isAmountFrom = true;
                            }
                        }
                    }

                    if (_AmountToFilter == "") {
                        _isAmountTo = true;
                    }
                    else {

                        // If Amount and _AmountToFilter Input are numbers
                        if (!(isNaN(_Item.Amount)) && !(isNaN(_AmountToFilter))) {
                            if (Number(_Item.Amount) <= Number(_AmountToFilter)) {
                                _isAmountTo = true;
                            }
                        }
                    }

                    _isAmountBetween = _isAmountFrom && _isAmountTo;


                    // Check if Amount Per Period is between

                    if (_AmountPerPeriodFromFilter == "") {
                        _isAmountPerPeriodFrom = true;
                    }
                    else {
                        // If AmountPerPeriod and _AmountPerPeriodFromFilter Input are numbers
                        if (!(isNaN(_Item.AmountPerPeriod)) && !(isNaN(_AmountPerPeriodFromFilter))) {
                            if (Number(_Item.AmountPerPeriod) >= Number(_AmountPerPeriodFromFilter)) {
                                _isAmountPerPeriodFrom = true;
                            }
                        }
                    }

                    if (_AmountPerPeriodToFilter == "") {
                        _isAmountPerPeriodTo = true;
                    }
                    else {

                        // If AmountPerPeriod and _AmountPerPeriodToFilter Input are numbers
                        if (!(isNaN(_Item.AmountPerPeriod)) && !(isNaN(_AmountPerPeriodToFilter))) {
                            if (Number(_Item.AmountPerPeriod) <= Number(_AmountPerPeriodToFilter)) {
                                _isAmountPerPeriodTo = true;
                            }
                        }
                    }

                    _isAmountPerPeriodBetween = _isAmountPerPeriodFrom && _isAmountPerPeriodTo;


                    // Check FixedCost desc filters
                    if (_DescFilter == "") {
                        _doesDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesDescContain = true;
                        }
                    }

                    // Check if Period is between
                    if (_PeriodFromFilter == "") {
                        _isPeriodFrom = true;
                    }
                    else {
                        // If Period and _PeriodFromFilter Input are numbers
                        if (!(isNaN(_Item.Period)) && !(isNaN(_PeriodFromFilter))) {
                            if (Number(_Item.Period) >= Number(_PeriodFromFilter)) {
                                _isPeriodFrom = true;
                            }
                        }
                    }

                    if (_PeriodToFilter == "") {
                        _isPeriodTo = true;
                    }
                    else {

                        // If Period and _PeriodToFilter Input are numbers
                        if (!(isNaN(_Item.Period)) && !(isNaN(_PeriodToFilter))) {
                            if (Number(_Item.Period) <= Number(_PeriodToFilter)) {
                                _isPeriodTo = true;
                            }
                        }
                    }

                    _isPeriodBetween = _isPeriodFrom && _isPeriodTo;


                    // Check if is Recuurency Met
                    if (_isRecuuringFilter == "All") {
                        _isRecurrencyMet = true;
                    } else {
                        if (_Item.isRecurring === _isRecuuringFilterBool) {
                            _isRecurrencyMet = true;
                        }
                    }


                    // Set _Item visibility
                    _Item.Visible = (_isInGroup && _isInCategory && _isOfType && _isBetweenStartDates && _isBetweenStopDates &&
                        _isAmountBetween && _isAmountPerPeriodBetween && _doesDescContain && _isPeriodBetween && _isRecurrencyMet);

                }
                FixedCostsService.calculateAmountPerPeriodSum();
            }
        }

        this.calculateAmountPerPeriod = function () {
            var _Period = cache.ViewState.ViewSettings.PeriodFilter;
            var _multiplyBy = 1;
            if (_Period == "dziennie") {
                _multiplyBy = 12 / 365;
            } else if (_Period == "tygodniowo") {
                _multiplyBy = 84 / 365;
            } else if (_Period == "miesięcznie") {
                _multiplyBy = 1;
            } else if (_Period == "kwartalnie") {
                _multiplyBy = 3;
            } else if (_Period == "rocznie") {
                _multiplyBy = 12;
            }
            $(cache.Items.FixedCosts).each(function () {
                if (!isNaN(this.Amount) && !isNaN(this.Period)) {
                    this.AmountPerPeriod = (this.Amount / this.Period) * _multiplyBy;
                }

            });
        }

        this.calculateAmountPerPeriodSum = function () {
            if ((cache.ViewState != null) && (cache.ViewState.TemporaryViewSettings != null)) {
                cache.ViewState.TemporaryViewSettings.AmountPerPeriodSum = 0;
                $(cache.Items.FixedCosts).each(function () {
                    if (this.Visible == true) {
                        cache.ViewState.TemporaryViewSettings.AmountPerPeriodSum += this.AmountPerPeriod;
                    }
                });
            }
        }

        this.periodChanged = function () {
            FixedCostsService.calculateAmountPerPeriod();
            FixedCostsService.calculateAmountPerPeriodSum();
        }

        this.saveViewState = function () {
            console.log('FixedCostsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("FixedCostsIndex", cache.ViewState);
        }

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        }

        this.clearViewState = function () {
            ViewStateService.clearViewState("FixedCostsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        }

        this.onIndexDataDownloadCompleted = function (scope) {
            FixedCostsService.calculateAmountPerPeriod();
            FixedCostsService.calculateAmountPerPeriodSum();
        }

        this.getIndexHeaderDictionary = function () {
            
            var Dictionary = {};

            Dictionary.ItemsNameString = "Koszty";
            Dictionary.ActiveItemString = "Koszt nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowy koszt";
            Button.Href = function () {
                return "#/FixedCosts/Edit?Id=0";
            };
            Button.Click = function () {
                FixedCostsService.set$ActiveItemId(0);
            }
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/FixedCosts/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/FixedCosts/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        }

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Data powstania";
            Dictionary.ColumnsNames[2] = "Data wygaśnięcia";
            Dictionary.ColumnsNames[3] = "Kategoria";
            Dictionary.ColumnsNames[4] = "Rodzaj";
            Dictionary.ColumnsNames[5] = "Kwota";
            Dictionary.ColumnsNames[6] = "Okresowy/jednorazowy";
            Dictionary.ColumnsNames[7] = "Okres";
            Dictionary.ColumnsNames[8] = "Opis";
            Dictionary.ColumnsNames[9] = "Kwota na okres";
            return Dictionary;
        }

        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    FixedCostsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return FixedCostsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        }

        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return FixedCostsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    FixedCostsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        }



        // EDIT METHODS

        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.FixedCosts, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        }

        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        }

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        }

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        }

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("FixedCosts", cache.ActiveItem)
                .then(
                function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    FixedCostsService.set$ActiveItemId(response.Id);
                    return true;
                },
                function (error) {
                    throw error.data;
                });
        }

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                function (response) {
                    var SelectOptions = {};
                    //FixedCostType options
                    SelectOptions.FixedCostType = [];
                    for (var FixedCostType of cache.Items.FixedCostTypes) {
                        var Option = {};
                        Option.Name = FixedCostType.Code + ' - ' + FixedCostType.Name;
                        Option.Desc = FixedCostType.Desc;
                        Option.Value = FixedCostType.FixedCostTypeId;
                        SelectOptions.FixedCostType.push(Option);
                    }
                    return SelectOptions;
                }, function (error) { throw false; });
        }

        this.isEditDisabled = function () {
            return false;
        }



        // Private functions

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.FixedCostCategoryIdFilter = "All";
            cache.ViewState.ViewSettings.FixedCostTypeIdFilter = "All";
            cache.ViewState.ViewSettings.StartDateFromFilter = "";
            cache.ViewState.ViewSettings.StartDateToFilter = "";
            cache.ViewState.ViewSettings.StopDateFromFilter = "";
            cache.ViewState.ViewSettings.StopDateToFilter = "";
            cache.ViewState.ViewSettings.AmountFromFilter = "";
            cache.ViewState.ViewSettings.AmountToFilter = "";
            cache.ViewState.ViewSettings.AmountPerPeriodFromFilter = "";
            cache.ViewState.ViewSettings.AmountPerPeriodToFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";
            cache.ViewState.ViewSettings.PeriodFromFilter = "";
            cache.ViewState.ViewSettings.PeriodToFilter = "";
            cache.ViewState.ViewSettings.isRecurringFilter = "All";
            cache.ViewState.ViewSettings.PeriodFilter = "miesięcznie";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Amount";
            cache.ViewState.ViewSettings.isOrderedAscending = true;
            cache.ViewState.ViewSettings.SecondOrderingPropertyName = "Id";

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Start Date
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Stop Date
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Category
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Type
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // Amount
            cache.ViewState.ViewSettings.ColumnsVisibility[6] = true; // isRecurring
            cache.ViewState.ViewSettings.ColumnsVisibility[7] = true; // Period
            cache.ViewState.ViewSettings.ColumnsVisibility[8] = true; // Desc
            cache.ViewState.ViewSettings.ColumnsVisibility[9] = true; // Amount Per Period

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;

            // Data variables in ViewSettings
            cache.ViewState.TemporaryViewSettings.AmountPerPeriodSum = 0;

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("FixedCostsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }

    }

    })();