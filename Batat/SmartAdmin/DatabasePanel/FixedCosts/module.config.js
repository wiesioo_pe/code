"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.fixedcosts', {
            url: '/FixedCosts',
            data: {
                title: 'FixedCosts'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/FixedCosts/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['FixedCostsService', function (FixedCostsService) {
                            return FixedCostsService.get$Items();
                        }],
                        MainItemsService: ['FixedCostsService', function (FixedCostsService) {
                            return FixedCostsService;
                        }],
                    }
                }
            }
        })

        .state('app.fixedcostedit', {
            url: '/FixedCosts/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/FixedCosts/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['FixedCostsService', function (FixedCostsService) {
                            return FixedCostsService.get$Items();
                        }],
                        MainItemsService: ['FixedCostsService', function (FixedCostsService) {
                            return FixedCostsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();