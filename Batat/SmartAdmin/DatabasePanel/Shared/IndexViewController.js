﻿"use strict";
console.log("Załadowałem IndexViewController.js");
/// <reference path="~/app/scripts/angular.js" />



(function () {
    
    angular.module("app").controller('IndexViewController', IndexViewController);

    // CONTROLLER
    IndexViewController.$inject = ['$scope', '$timeout'
        , 'ItemsService', 'ViewStateService', 'Items', 'MainItemsService'];
    
    function IndexViewController($scope, $timeout
        , ItemsService, ViewStateService
        // Resolved parameters (from $stateProvider)
        , Items, MainItemsService) {
            
        var $ctrl = this;
        $scope.$ctrl = this;

        var PrivateFunctions = {
            LoadControllerData: LoadControllerData
        };

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.ViewState = {};
            $ctrl.ViewState.ViewSettings = {};
            $ctrl.ViewState.SessionViewSettings = {};
            $ctrl.ViewState.TemporaryViewSettings = {};
            $ctrl.ViewData = {};
            $ctrl.IndexHeaderDictionary = {};
            $ctrl.ColumnsAndPropertiesNames = {};
            $ctrl.MainItemsService = MainItemsService;
            $ctrl.MainItemsName = $ctrl.MainItemsService.getMainItemsName();
            // initialize $ctrl
            PrivateFunctions.LoadControllerData();
            // and get Items
            $ctrl.Items = Items;
            // set $ctrl.MainItems
            $ctrl.MainItems = $ctrl.Items[$ctrl.MainItemsName];
            // call service onIndexDataDownloadCompleted($ctrl) to make Items-specific operations
            $ctrl.MainItemsService.onIndexDataDownloadCompleted($scope);
            // do the filtering and sorting of items 
            $ctrl.FilterItems();
            $ctrl.SortItems($ctrl.ViewState.ViewSettings.OrderedBy);
            //$ctrl.SortItems($ctrl.ViewState.ViewSettings.OrderedBy);
            // rewind elements table to the last position after filtered and sorted
            RewindTable();
        };

        this._$onChanges = function () {

        };
        // END HOOKS


        // VIEW METHODS

        

        // called when table header gets clicked
        this.SortItems = function (propertyName) {
            $ctrl.ViewState.TemporaryViewSettings.OrderBy = propertyName;
            ItemsService.sortItems($ctrl.MainItemsName, $ctrl.ViewState);
        };

        // called when filter has changed
        this.FilterItems = function () {
            //console.log("IndexController for " + $ctrl.MainItemsName + ": $ctrl.FilterItems() called");
            if ($ctrl.MainItemsService != null) {
                $ctrl.MainItemsService.filterItems();
                ViewStateService.updateViewData($ctrl.MainItems, $ctrl.ViewData);
            }
        };

        // called to change active Item
        this.SetActiveItem = function (ItemId) {
            if ($ctrl.MainItemsService != null) {
                $ctrl.MainItemsService.set$ActiveItemId(ItemId, $ctrl.ViewState);
            }
        };

        // called to check if date exceeded Today's (for class-red for example)
        this.CheckIfDateExceeded = function (Date) {
            return ViewStateService.checkIfDateExceeded(Date);
        };

        // called when Item is expandable and double click or clicked on id
        this.ExpandCollapseItem = function (ItemId) {
            ViewStateService.expandCollapseItem(ItemId, $ctrl.ViewState);
        };

        // called to check if item expanded for class-giving for example
        this.IsItemExpanded = function (ItemId) {
            return ViewStateService.isItemExpanded(ItemId, $ctrl.ViewState);
        };

        // called to collapse all expanded Items
        this.ClearExpanded = function () {
            $timeout(ViewStateService.clearExpanded($ctrl.ViewState), 0);
        };

        this.OnColumnVisibilityChange = function () {
            $timeout(function () { $ctrl.ViewData = $ctrl.MainItemsService.getViewData(); }, 0);
        };

        // WATCHERS AND EVENTS

        
        

        // PRIVATE FUNCTIONS

        // called when service gets available
        function LoadControllerData() {
            $ctrl.ViewState = $ctrl.MainItemsService.getViewState();
            //console.warn($ctrl.ViewState);
            $ctrl.ViewName = $ctrl.MainItemsName + "Index";
            //console.warn($ctrl.ViewName);
            $ctrl.ViewData = $ctrl.MainItemsService.getViewData();
            //console.warn($ctrl.ViewData);
            $ctrl.IndexHeaderDictionary = $ctrl.MainItemsService.getIndexHeaderDictionary($ctrl.ViewState);
            //console.warn($ctrl.IndexHeaderDictionary);
            $ctrl.ColumnsAndPropertiesNames = $ctrl.MainItemsService.getAllColumnsNames();
            //console.warn($ctrl.ColumnsAndPropertiesNames);
        }

        // rewind table with Items
        function RewindTable() {
            var div = document.getElementsByClassName('index-table')[0];
            $(div).scrollTop($ctrl.ViewState.SessionViewSettings.TableOffset);
        }



        // DEVELOPMENT CODE By Wiesioo
        this.show$scope = function () {
            console.warn($scope);
        };


        $scope.$on("$destroy", function () {
            $(document.getElementsByClassName('pinned-nav-buttons')[0]).remove();
        });

        // INIT
        this._$onInit();
    }

})();