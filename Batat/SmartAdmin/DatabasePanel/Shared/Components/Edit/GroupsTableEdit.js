﻿"use strict";
//console.log("Załadowałem GroupsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive('groupsTableEdit', function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/GroupsTableEdit.html?" + Math.random(),
            scope: {
                ItemGroups: "=?ngDataItemGroups",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: GroupsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    GroupsTableEditController.$inject = ['$scope', 'InfrService', 'ItemsService', 'FeaturesService'];
    function GroupsTableEditController($scope, InfrService, ItemsService, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            ItemsService.get.Items('Groups').then(function (data) { $ctrl.Groups = data; });
            if (!InfrService.isArray($ctrl.ItemGroups)) {
                $ctrl.ItemGroups = [];
            }
        };

        this._$onChanges = function () {

        };

        // END OF HOOKS

        this.RemoveGroup = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemGroups.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddGroup = function () {
            if (($ctrl.SelectedGroupId != null) && ($ctrl.SelectedGroupId > 0)) {
                var GroupsMatched = $.grep($ctrl.ItemGroups,
                    function (item) { return item.GroupId === $ctrl.SelectedGroupId; });
                if (GroupsMatched[0] == null) {
                    $ctrl.ItemGroups.push({ GroupId: $ctrl.SelectedGroupId });
                    $ctrl.EditedItemChanged();
                    $ctrl.SelectedGroupId = null;
                } else {
                    FeaturesService.smartModalOK("Grupa istnieje już na liście", "", function () { });
                }
            } else {
                FeaturesService.smartModalOK("Nie wybrano grupy", "", function () { });
            }

        };



        // INIT
        this._$onInit();
    }

})();

