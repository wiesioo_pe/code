﻿"use strict";
//console.log("Załadowałem SubjectEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("subjectEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/SubjectEdit.html?" + Math.random(),
            scope: {
                ActiveItem: '=?ngDataActiveItem',
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange",
                SelectOptions: "=?ngDataSelectOptions"
            },
            controller: SubjectEditController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });

    // CONTROLLER
    SubjectEditController.$inject = ['$scope', '$timeout', 'InfrService'];
    function SubjectEditController($scope, $timeout, InfrService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onChanges = function () {
        };

        // END HOOKS
        var dereg$watchActiveItem = $scope.$watch('$ctrl.ActiveItem', function () {
            if (InfrService.isObject($ctrl.ActiveItem) && $ctrl.ActiveItem.SubjectId != null) {
                $timeout(function () {
                    $ctrl.SelectOptions = $.extend(true, {}, $ctrl.SelectOptions);
                }, 0);
                dereg$watchActiveItem();
            }
        }, true);

        this.onSubjectSelectChange = function () {
            $ctrl.EditedItemChanged();
        };


        // PRIVATE FUNCTIONS
    }

})();
