﻿"use strict";
//console.log("Załadowałem MaterialsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("materialsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/MaterialsTableEdit.html?" + Math.random(),
            scope: {
                ItemMaterials: "=?ngDataItemMaterials",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: MaterialsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    MaterialsTableEditController.$inject = ['$scope', '$filter', 'ItemsService', 'FeaturesService'];
    function MaterialsTableEditController($scope, $filter, ItemsService, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.MaterialIdToAdd = null;
            $ctrl.AmountToAdd = 0;
            if ($ctrl.ItemMaterials == null) { $ctrl.ItemMaterials = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            (ItemsService.get.Items('Materials')).then(function (data) { $ctrl.Materials = data; });
        }
        // END OF HOOKS


        this.RemoveMaterial = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemMaterials.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddMaterial = function () {
            var parameterExists = null;
            if (($ctrl.SelectedMaterialId != null) && ($ctrl.SelectedMaterialId > 0)) {
                parameterExists = $.grep($ctrl.ItemMaterials, function (e)
                { return e.MaterialId == $ctrl.SelectedMaterialId })[0];
            }
            if (($ctrl.SelectedMaterialId == null) || ($ctrl.SelectedMaterialId <= 0)) {
                FeaturesService.smartModalOK("Nie wybrano materiału", "", function () { });
            } else if (parameterExists != null) {
                FeaturesService.smartModalOK("Materiał istnieje na liście", "Zmień ilość przy materiale z listy", function () { });
            } else if ($ctrl.AmountToAdd == null) {
                FeaturesService.smartModalOK("Podaj ilość materiału", "", function () { });
            } else {
                $ctrl.ItemMaterials.push({
                    MaterialId: $ctrl.SelectedMaterialId, Amount: $ctrl.AmountToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.SelectedMaterialId = null;
                $ctrl.AmountToAdd = 0;
            }
        }

        






        // INIT
        this._$onInit();
    }
})();
