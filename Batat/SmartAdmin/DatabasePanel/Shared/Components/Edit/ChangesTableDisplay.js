﻿"use strict";
//console.log("Załadowałem ChangesTableDisplay.js");
/// <reference path="~/app/scripts/angular.js" />
(function () {
    // COMPONENT
    angular.module("app").directive("changesTableDisplay", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/ChangesTableDisplay.html?" + Math.random(),
            scope: {
                ItemChanges: "=?ngDataItemChanges"
            },
            controller: ChangesTableDisplayController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    ChangesTableDisplayController.$inject = ['$scope', '$timeout', 'ItemsService'];
    function ChangesTableDisplayController($scope, $timeout, ItemsService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            (ItemsService.get.Items('Employees')).then(function (data) { $timeout(function () { $ctrl.Employees = data; }, 0); });
        }
        // END OF HOOKS






        // INIT
        this._$onInit();
    }

})();
