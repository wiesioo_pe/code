﻿"use strict";
//console.log("Załadowałem NotesTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("notesTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/NotesTableEdit.html?" + Math.random(),
            scope: {
                ItemNotes: "=?ngDataItemNotes",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: NotesTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    NotesTableEditController.$inject = ['$scope'];
    function NotesTableEditController($scope) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.NoteToAdd = null;
            if ($ctrl.ItemNotes == null) { $ctrl.ItemNotes = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
        }
        // END OF HOOKS


        this.RemoveNote = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemNotes.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddNote = function () {
            if (($ctrl.NoteToAdd == null) || ($ctrl.NoteToAdd == "")) {
                alert("Podaj notatkę.");
            } else {

                $ctrl.ItemNotes.push({
                    Note: $ctrl.NoteToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.NoteToAdd = null;
            }
        }








        // INIT
        this._$onInit();
    }
})();


