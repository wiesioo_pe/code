﻿"use strict";
//console.log("Załadowałem PartsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("partsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/PartsTableEdit.html?" + Math.random(),
            scope: {
                ItemParts: "=?ngDataItemParts",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: PartsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });

    // CONTROLLER
    PartsTableEditController.$inject = ['$scope', '$filter', 'InfrService', 'ItemsService', 'FeaturesService'];
    function PartsTableEditController($scope, $filter, InfrService, ItemsService, FeaturesService) {
            
        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.PartIdToAdd = null;
            $ctrl.AmountToAdd = 1;
            if ($ctrl.ItemParts == null) { $ctrl.ItemParts = []; }
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; }
            ItemsService.get.Items('Parts').then(function (data) { $ctrl.Parts = data; });
        };
        // END OF HOOKS



        this.RemovePart = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemParts.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddPart = function () {
            var partExists = null;
            if (($ctrl.SelectedPartId != null) && ($ctrl.SelectedPartId > 0)) {
                partExists = $.grep($ctrl.ItemParts, function (e) { return e.PartId == $ctrl.SelectedPartId; })[0];
            }
            if (($ctrl.SelectedPartId == null) || ($ctrl.SelectedPartId <= 0)) {
                FeaturesService.smartModalOK("Nie wybrano detalu", "", function () { });
            } else if (partExists != null) {
                FeaturesService.smartModalOK("Detal istnieje na liście", "Zmień ilość przy detalu z listy", function () { });
            } else if (($ctrl.AmountToAdd == null) || ($ctrl.AmountToAdd < 1)) {
                FeaturesService.smartModalOK("Podaj ilość detalu", "", function () { });
            } else {

                $ctrl.ItemParts.push({
                    PartId: $ctrl.SelectedPartId, Amount: $ctrl.AmountToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.SelectedPartId = null;
                $ctrl.AmountToAdd = 1;
            }
        };


        // INIT
        this._$onInit();
    }

})();
