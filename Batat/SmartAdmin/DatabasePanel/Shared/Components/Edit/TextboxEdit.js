﻿"use strict";
//console.log("Załadowałem TextboxEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("textboxEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/TextboxEdit.html?" + Math.random(),
            scope: {
                Model: '=?ngDataModel',
                Label: "@ngDataLabel",
                InputName: "@ngDataInputName",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: TextboxEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    TextboxEditController.$inject = ['$scope'];
    function TextboxEditController($scope) {

        var $ctrl = this;
        $scope.$ctrl = this;

    }

})();