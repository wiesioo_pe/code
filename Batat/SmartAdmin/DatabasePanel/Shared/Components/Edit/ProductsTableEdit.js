﻿"use strict";
//console.log("Załadowałem ProductsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("productsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/ProductsTableEdit.html?" + Math.random(),
            scope: {
                ItemProducts: "=?ngDataItemProducts",
                isEditDisabled: "=?ngDataIsEditDisabled",
                DefaultDiscount: "=?ngDataDefaultDiscount",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: ProductsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });
        
    // CONTROLLER
    ProductsTableEditController.$inject = ['$scope', '$filter', 'InfrService', 'ItemsService', 'FeaturesService'];
    function ProductsTableEditController($scope, $filter, InfrService, ItemsService, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            if ($ctrl.ItemProducts == null) { $ctrl.ItemProducts = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            if ($ctrl.DefaultDiscount == null) { $ctrl.DefaultDiscount = 0; };
            $ctrl.ProductIdToAdd = null;
            $ctrl.AmountToAdd = 1;
            $ctrl.DiscountToAdd = $ctrl.DefaultDiscount;
            (ItemsService.get.Items('Products')).then(function (data) { $ctrl.Products = data; });
        }

        this._$onChanges = function () {
            $ctrl.$onDefaultDiscountChange();
        }
        this.$onDefaultDiscountChange = function () { $ctrl.DiscountToAdd = $ctrl.DefaultDiscount; };
        var deregWatch$onDefaultDiscountChange = $scope.$watch('$ctrl.DefaultDiscount', function () {
            if (InfrService.isNumber($ctrl.DefaultDiscount)) {
                $ctrl.$onDefaultDiscountChange();
                deregWatch$onDefaultDiscountChange();
            }
        });
        // END OF HOOKS

        this.RemoveProduct = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemProducts.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddProduct = function () {
            var productExists = null;
            if ($ctrl.SelectedProductId != null) {
                productExists = $.grep($ctrl.ItemProducts, function (e)
                { return e.ProductId == $ctrl.SelectedProductId })[0];
            }
            if ($ctrl.SelectedProductId == null) {
                FeaturesService.smartModalOK("Nie wybrano produktu", "", function () { });
            } else if (productExists != null) {
                FeaturesService.smartModalOK("Produkt istnieje na liście", "Zmień ilość przy produkcie z listy", function () { });
            } else if (($ctrl.AmountToAdd == null) || ($ctrl.AmountToAdd < 1)) {
                FeaturesService.smartModalOK("Podaj ilość produktu", "", function () { });
            } else if (($ctrl.DiscountToAdd == null) || ($ctrl.DiscountToAdd < 0) || ($ctrl.DiscountToAdd > 100)) {
                FeaturesService.smartModalOK("Podaj rabat dla wybranego produktu", "", function () { });
            } else {                        
                var newProduct = {
                    ProductId: $ctrl.SelectedProductId, Amount: $ctrl.AmountToAdd,
                    Discount: $ctrl.DiscountToAdd
                };
                ItemsService.setProductPrices(newProduct);
                $ctrl.ItemProducts.push(newProduct);
                $ctrl.EditedItemChanged();
                $ctrl.SelectedProductId = null;
                $ctrl.AmountToAdd = 1;
                $ctrl.DiscountToAdd = $ctrl.DefaultDiscount;
            }
        }

        this.AmountChanged = function (Product) {
            $ctrl.EditedItemChanged();
            ItemsService.setProductPrices(Product);
        }

        this.DiscountChanged = function (Product) {
            $ctrl.EditedItemChanged();
            ItemsService.setProductPrices(Product);
        }

        






        // INIT
        this._$onInit();
    }

})();


