﻿"use strict";
//console.log("Załadowałem EditServerValidationError.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("editServerValidationError", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/EditServerValidationError.html?" + Math.random(),
            scope: {
                ServerValidationError: "=?ngDataServerValidationError"
            },
            controller: EditServerValidationErrorController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    EditServerValidationErrorController.$inject = ['$scope'];
    function EditServerValidationErrorController($scope) {

        var $ctrl = this;
        $scope.$ctrl = this;

    }
})();