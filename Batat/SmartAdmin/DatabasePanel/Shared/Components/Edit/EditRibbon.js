﻿"use strict";
//console.log("Załadowałem EditRibbon.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("editRibbon", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/EditRibbon.html?" + Math.random(),
            scope: {
                isEdited: "=?ngDataIsEdited",
                ResetActiveItem: "&ngFuncResetActiveItem",
                SaveActiveItem: "&ngFuncSaveActiveItem",
                isEditedItemChanged: "=?ngDataIsEditedItemChanged",
                isModelValid: "=?ngDataIsModelValid"
            },
            controller: EditButtonsController,
            controllerAs: '$ctrl',
            bindToController: true,
            link: function (scope, element, attrs) {
                var newContainer = document.getElementById('left-panel');
                if (newContainer != null) {
                    $(newContainer).append(element);
                } else {
                    setTimeout(function () {
                        newContainer = document.getElementById('left-panel');
                        $(newContainer).append(element);
                    }, 100);
                }
            }
        }
    });

    // CONTROLLER
    EditButtonsController.$inject = ['$scope', '$window', 'FeaturesService'];
    function EditButtonsController($scope, $window, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        this.Cancel = function () {
            if ($ctrl.isEditedItemChanged === true) {
                FeaturesService.smartModalYesNo("Czy chcesz porzucić wprowadzone zmiany i wyjść?", "", function () { $window.history.back(); }, function () { });
            } else {
                $window.history.back();
            }
        }
    }

})();