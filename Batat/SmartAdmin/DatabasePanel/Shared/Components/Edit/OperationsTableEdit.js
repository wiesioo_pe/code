﻿"use strict";
//console.log("Załadowałem OperationsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("operationsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/OperationsTableEdit.html?" + Math.random(),
            scope: {
                ItemOperations: "=?ngDataItemOperations",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: OperationsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });

    // CONTROLLER
    OperationsTableEditController.$inject = ['$scope', '$filter', '$timeout', 'InfrService', 'ItemsService', 'FeaturesService'];
    function OperationsTableEditController($scope, $filter, $timeout, InfrService, ItemsService, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.newOperationId = 1000000;
            $ctrl.ActiveItemId = null;
            $ctrl.OperationDescToAdd = null;
            $ctrl.Items = {};

            if ($ctrl.ItemOperations == null) { $ctrl.ItemOperations = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            ItemsService.get.Items('Operations').then(function (data) {
                $ctrl.Items.Operations = data;
                initializeItemOperations();
            });
            ItemsService.get.Items('OperationTypes').then(function (data) { $ctrl.OperationTypes = data; });
        }

        this._$onChanges = function () {
            $ctrl.$onItemOperationsChange();
        }
        this.$onItemOperationsChange = function () { initializeItemOperations(); };
        var deregWatch$onItemOperationsChange = $scope.$watch('$ctrl.ItemOperations', function () {
            if (InfrService.isArray($ctrl.ItemOperations)) {
                $ctrl.$onItemOperationsChange();
                deregWatch$onItemOperationsChange();
            }
        })
        // END OF HOOKS


        function initializeItemOperations() {
            if (InfrService.isArray($ctrl.ItemOperations) && InfrService.isArray($ctrl.Items.Operations)) {
                // setItemOperationsOperationTypesAndOBIdxs
                $timeout(function () {
                    for (var ItemOperation of $ctrl.ItemOperations) {

                        ItemOperation.OperationTypeId = $filter('getOperationTypeId')(ItemOperation.OperationId, $ctrl.Items.Operations);

                        ItemOperation.OperationsBeforeIdxs = [];
                        for (var OperationBefore of ItemOperation.OperationsBefore) {
                            var index = InfrService.getArrayElementIndexByProp($ctrl.ItemOperations, OperationBefore.OperationBeforeId, 'OperationId');
                            if ((index != null) && (index >= 0)) {
                                ItemOperation.OperationsBeforeIdxs.push(index);
                            }
                        }

                        // refresh OperationsBefore Checkboxes
                        ItemOperation.OperationsBeforeIdxs = ItemOperation.OperationsBeforeIdxs.slice();
                    }
                }, 0);
            }
        }

        

        // USER EVENTS REACTION
        this.OperationsBeforeCheckboxChanged = function (Operation) {

            ChangeOperationsBeforeIdxsForIds(Operation);

            for (var ItemOperation of $ctrl.ItemOperations) {
                for (var i = ItemOperation.OperationsBeforeIdxs.length - 1; i >= 0; i--) {
                    var idx = ItemOperation.OperationsBeforeIdxs[i];
                    if ($ctrl.isCheckboxDisabled(ItemOperation, idx) === true) {
                        removeOperationIndexFormOperationsBeforeIdxs(ItemOperation, idx);
                        ChangeOperationsBeforeIdxsForIds(ItemOperation);
                    }
                }
            }

            ChangeOperationsBeforeIdxsForIds(Operation);
            /*
            // check all operations for excessive OperationBefore   eg.:      OP3: OPB - 1,2; OP4: OPB: 2,3    -   OPB no. 2 is excessive 
            $timeout(function () {
                for (var i = $ctrl.ItemOperations.indexOf(Operation) + 1; i < $ctrl.ItemOperations.length; i++) {
                    var ItemOperation = $ctrl.ItemOperations[i];
                    for (var j = 0; j < i; j++) {
                        if ($ctrl.isCheckboxDisabled(ItemOperation, j)) {
                            var id = $ctrl.ItemOperations[j].OperationId;
                            var indexOfExcessiveOperationBefore = ItemOperation.OperationsBefore.indexOf(id);
                            if (indexOfExcessiveOperationBefore >= 0) {
                                ItemOperation.OperationsBefore.splice(indexOfExcessiveOperationBefore, 1)
                            }
                        }
                    }
                }
                Operation.OperationsBeforeIdxs = Operation.OperationsBeforeIdxs.slice();
            }, 0);
            */
            $ctrl.EditedItemChanged();
            console.warn($ctrl.ItemOperations);
        };

        this.RemoveOperation = function ($index) {
            if (!$ctrl.isEditDisabled) {
                if (confirm('Czy na pewno chcesz usunąć operację?')) {
                    removeOperationIdFromAllItemsOperationsBefore($ctrl.ItemOperations[$index].OperationId);
                    $ctrl.ItemOperations.splice($index, 1);
                    $ctrl.EditedItemChanged();
                } else {
                    // Do nothing!
                }
            }
        };

        this.AddOperation = function () {
            if ($ctrl.SelectedOperationType.Id == null || $ctrl.SelectedOperationType.Id <= 0) {
                FeaturesService.smartModalOK("Nie wybrano operacji", "", function () { });
            } else {
                var newOperation = {
                    OperationId: $ctrl.newOperationId, OperationTypeId: $ctrl.SelectedOperationType.Id,
                    Desc: $ctrl.OperationDescToAdd, OperationsBefore: []
                };
                $ctrl.newOperationId++;
                $ctrl.ItemOperations.push(newOperation);
                $ctrl.EditedItemChanged();

                $ctrl.SelectedOperationType.Id = null;
                $ctrl.SelectedOperation = null;
                $ctrl.OperationDescToAdd = null;
            }
        };

        this.CloneOperation = function () {
            console.log('TechnologyEdit: Adding existing operation.');
            var newOperation = {
                OperationId: $ctrl.newOperationId, OperationTypeId: $ctrl.SelectedOperation.OperationTypeId,
                Desc: $ctrl.SelectedOperation.Desc, OperationsBefore: $ctrl.SelectedOperation.OperationsBefore
            };
            $ctrl.newOperationId++;
            $ctrl.ItemOperations.push(newOperation);
            $ctrl.EditedItemChanged();
            $ctrl.SelectedOperation = null;
        }

        this.MoveOperationUp = function ($index) {

            var Operation = $ctrl.ItemOperations[$index];

            var UpperOperationId = $ctrl.ItemOperations[$index - 1].OperationId;

            if ((Operation != null) && (isOperationBeforeContained(Operation, UpperOperationId))) {
                FeaturesService.smartModalYesNo("Konflikt operacji", "Operacja, powyżej jest oznaczona jako 'operacja przed'. Przesunięcie spowoduje usunięcie tej operacji z listy operacji przed. Czy na pewno chcesz przesunąć?"
                , function () {
                    removeOperationIndexFormOperationsBeforeIdxs(Operation, $index - 1);
                    ChangeOperationsBeforeIdxsForIds(Operation);
                    moveUp();
                }, function () { });
            } else {
                moveUp();
            }

            function moveUp() {
                var PreviousOperation = $ctrl.ItemOperations[$index - 1];
                
                var cache = $.extend(true, {}, Operation);
                $ctrl.ItemOperations[$index] = PreviousOperation;
                $ctrl.ItemOperations[$index - 1] = cache;
                $ctrl.EditedItemChanged();
            }
        }

        this.MoveOperationDown = function ($index) {
            var cache = $.extend(true, {}, $ctrl.ItemOperations[$index]);
            $ctrl.ItemOperations[$index] = $ctrl.ItemOperations[$index + 1];
            $ctrl.ItemOperations[$index + 1] = cache;
            $ctrl.EditedItemChanged();
        }



        // INFRASTRUCTURE FOR VIEWS
        this.isCheckboxDisabled = function (Operation, indexOfCheckbox) {
            if (InfrService.isObject(Operation) && !isNaN(indexOfCheckbox)) {
                var isDisabled = false;

                var Id = $ctrl.ItemOperations[indexOfCheckbox].OperationId || 0;
                if ((Operation != null) && (isOperationBeforeContained(Operation, Id) === true)) {
                    isDisabled = true;
                }

                return isDisabled;
            } else {
                throw new Error('batat Error: Wrong parameters passed to isCheckboxDisabled');
            }
            
        }

        this.SetActiveItem = function (OperationId) {
            $ctrl.ActiveItemId = OperationId;
        }



        // PRIVATE FUNCTIONS

        this.getArray = function (num) {
            return new Array(num);
        }
        
        // converts OperationsBeforeIdxs (used by the select) to  OperationsBefore
        function ChangeOperationsBeforeIdxsForIds(Operation) {
            Operation.OperationsBefore = [];
            for (var index of Operation.OperationsBeforeIdxs) {
                AddOperationBeforeFromIndex(Operation, index);
            }
        }

        function AddOperationBeforeFromIndex(Operation, index) {
            var id = InfrService.getArrayElementPropByIndex($ctrl.ItemOperations, index, 'OperationId');
            //var isOnList = InfrService.getArrayElementIndexByProp();
            if ((id != null) && (id > 0)) {
                Operation.OperationsBefore.push({ OperationBeforeId: id });
            }
        };

        // added to remove indexes from OperationsBeforeIdxs to clear selections in the select's checkboxes
        // called when checking for enable/disable checkbox ($ctrl.$digest do that)
        function RemoveOperationIndexFromOperationBeforeIdxs(Operation, checkboxIndex) {
            var idx = Operation.OperationsBeforeIdxs.indexOf(checkboxIndex);
            if (idx >= 0) {
                Operation.OperationsBeforeIdxs.splice(idx, 1);
                ChangeOperationsBeforeIdxsForIds(Operation);
            }
        }

        function isOperationBeforeContained(Operation, SearchedId) {
            for (var OperationBeforeId of Operation.OperationsBefore) {
                var OperationBefore = InfrService.findFirst($ctrl.ItemOperations, function (item) { return item.OperationId == OperationBeforeId.OperationBeforeId; });
                if (OperationBefore != null) {
                    if (InfrService.getArrayElementIndexByProp(OperationBefore.OperationsBefore, SearchedId, 'OperationBeforeId') >= 0) {
                        return true;
                    } else {
                        if (isOperationBeforeContained(OperationBefore, SearchedId) === true) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        function removeOperationIdFromAllItemsOperationsBefore(OperationId) {
            for(var Operation of $ctrl.ItemOperations) {
                var index = InfrService.getArrayElementIndexByProp(Operation.OperationsBefore, OperationId, 'OperationBeforeId');
                if (index > -1) { Operation.OperationsBefore.splice(index, 1); }
            }
        }

        function removeOperationIndexFormOperationsBeforeIdxs(Operation, indexToRemove) {
            var idx = Operation.OperationsBeforeIdxs.indexOf(indexToRemove);
            if (idx >= 0) {
                Operation.OperationsBeforeIdxs.splice(idx, 1);
            }
        }


        







        // INIT
        this._$onInit();
    }

})();

