﻿"use strict";
//console.log("Załadowałem ComplaintsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("complaintsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/ComplaintsTableEdit.html?" + Math.random(),
            scope: {
                ItemComplaints: "=?ngDataItemComplaints",
                isEditDisabled: "=?ngDataIsEditDisabled",
                OrderId: "=?ngDataOrderId",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: ComplaintsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    ComplaintsTableEditController.$inject = ['$scope', '$timeout', '$filter', 'InfrService', 'ItemsService'];
    function ComplaintsTableEditController($scope, $timeout, $filter, InfrService, ItemsService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            if ($ctrl.ItemComplaints == null) { $ctrl.ItemComplaints = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            $ctrl.OrderProducts = [];
            $ctrl.ProductIdToAdd = null;
            $ctrl.AmountToAdd = 1;
            $ctrl.DescToAdd = "";
            $ctrl.DateToAdd = moment(new Date()).format('DD-MM-YYYY');
            $ctrl.CostToAdd = 0;
            $ctrl.isTotalToAdd = false;
            if ($ctrl.OrderId != null) { getOrderProducts(); };
            (ItemsService.get.Items('Products')).then(function (data) { $ctrl.Products = data; });
        }

        this._$onChanges = function () {
            $ctrl.$onOrderIdChange();
        }
        this.$onOrderIdChange = function () { if ($ctrl.OrderId != null) { getOrderProducts(); }; };
        $scope.$watch('$ctrl.OrderId', function () { $ctrl.$onOrderIdChange(); });
        // END OF HOOKS


        this.RemoveComplaint = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemComplaints.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddComplaint = function () {
            var productExists = false;
            if (($ctrl.SelectedProductId != null) && ($ctrl.SelectedProductId > 0)) {
                var complaintsMatched = $.grep($ctrl.ItemComplaints, function (e)
                { return e.ProductId == $ctrl.SelectedProductId });
                if (InfrService.isArrayAndNotEmpty(complaintsMatched)) {
                    for(var complaint of complaintsMatched) {
                        if (complaint.Date === $ctrl.DateToAdd) {
                            productExists = true;
                        }
                    }
                }
            }
            if (($ctrl.SelectedProductId == null) || ($ctrl.SelectedProductId <= 0)) {
                alert("Wybierz produkt.");
            } else if (productExists === true) {
                alert("Reklamacja na produkt z tego dnia istnieje na liście.");
            } else if (($ctrl.AmountToAdd == null) || ($ctrl.AmountToAdd < 1)) {
                alert("Podaj ilość produktu.");
            } else if ($ctrl.DateToAdd == null) {
                alert("Wybierz datę.");
            } else {                        

                $ctrl.ItemComplaints.push({
                    ComplaintId: 'Nowa reklamacja',
                    ProductId: $ctrl.SelectedProductId, Amount: $ctrl.AmountToAdd,
                    Desc: $ctrl.DescToAdd, Date: $ctrl.DateToAdd,
                    Cost: $ctrl.CostToAdd, isTotal: $ctrl.isTotalToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.SelectedProductId = null;
                $ctrl.AmountToAdd = 1;
                $ctrl.DescToAdd = "";
                $ctrl.CostToAdd = 0;
                $ctrl.isTotalToAdd = false;
            }
        }


        // PRIVATE FUNCTIONS

        function getOrderProducts() {
            (ItemsService.get.OrderProducts($ctrl.OrderId)).then(function (data) { $timeout(function () { $ctrl.OrderProducts = data; }, 0); });
        }






        // INIT
        this._$onInit();
    } 
})();