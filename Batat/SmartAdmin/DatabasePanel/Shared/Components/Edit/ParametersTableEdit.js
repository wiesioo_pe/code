﻿"use strict";
//console.log("Załadowałem ParametersTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("parametersTableEdit", {
        templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/ParametersTableEdit.html?" + Math.random(),
            bindings: {
                ItemParameters: "=?ngDataItemParameters",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: ParametersTableEditController
    });

    ParametersTableEditController.$inject = ['$scope', '$filter', 'InfrService', 'ItemsService'];
    function ParametersTableEditController($scope, $filter, InfrService, ItemsService) {

        var $ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.ParameterIdToAdd = null;
            $ctrl.ValueToAdd = 0;
            if ($ctrl.ItemParameters == null) { $ctrl.ItemParameters = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            (ItemsService.get.Items('Parameters')).then(function (data) { $ctrl.Parameters = data; });
        }
        // END OF HOOKS


        this.RemoveParameter = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemParameters.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddParameter = function () {
            var parameterExists = null;
            if (($ctrl.SelectedParameterId != null) && ($ctrl.SelectedParameterId > 0)) {
                parameterExists = $.grep($ctrl.ItemParameters, function (e)
                { return e.ParameterId == $ctrl.SelectedParameterId })[0];
            }
            if (($ctrl.SelectedParameterId == null) || ($ctrl.SelectedParameterId <= 0)) {
                FeaturesService.smartModalOK("Nie wybrano parametru", "", function () { });
            } else if (parameterExists != null) {
                FeaturesService.smartModalOK("Parametr istnieje na liście", "Zmień ilość przy parametrze z listy", function () { });
            } else if ($ctrl.ValueToAdd == null) {
                FeaturesService.smartModalOK("Podaj wartość parametru", "", function () { });
            } else {

                $ctrl.ItemParameters.push({
                    ParameterId: $ctrl.SelectedParameterId, Value: $ctrl.ValueToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.SelectedParameterId = null;
                $ctrl.ValueToAdd = 0;
            }
        }

        







        // INIT
        this._$onInit();
    }

})();


