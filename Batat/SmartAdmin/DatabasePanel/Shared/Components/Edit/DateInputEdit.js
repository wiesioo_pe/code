﻿"use strict";
//console.log("Załadowałem DateInputEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("dateInputEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/DateInputEdit.html?" + Math.random(),
            scope: {
                Model: '=?ngDataModel',
                Label: "@ngDataLabel",
                InputName: "@ngDataInputName",
                isEditDisabled: "=?ngDataIsEditDisabled",
                hideLabel: "@?ngDataHideLabel",
                EditedItemChanged: "&ngFuncOnChange",
                isRequired: "=?ngDataIsRequired",
                ValidationProperty: "=?ngDataValidationProperty"
            },
            controller: DateInputEditController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });


    // CONTROLLER
    DateInputEditController.$inject = ['$scope', '$timeout', 'InfrService'];
    function DateInputEditController($scope, $timeout, InfrService) {

        var $ctrl = this;
        $scope.$ctrl = this;


        // LIFECYCLE HOOKS

        this._$onInit = function () {
            $ctrl.DatepickerModel = null;
            if ($ctrl.Model === "") {
                $ctrl.Model = null;
            }
        };

        this._$onChanges = function () {
            $ctrl.$onModelChange();
        };

        this.$onModelChange = function () {
            if (InfrService.isString($ctrl.Model)) {
                if ($ctrl.Model === "") {
                    $ctrl.Model = null;
                }
            }
        };

        var deregWatch$onModelChange = $scope.$watch('$ctrl.Model', function () {
            if (InfrService.isString($ctrl.Model)) {
                $ctrl.$onModelChange();
                deregWatch$onModelChange();
            }
        });

        // END OF HOOKS


        this.onDateChange = function () {
            $ctrl.EditedItemChanged();
        };

        $ctrl.clearDate = function () {
            $timeout(function () {
                $ctrl.Model = null;
                $ctrl.EditedItemChanged();
            }, 0);
        };


        // INIT
        this._$onInit();
    }

})();

