﻿"use strict";
//console.log("Załadowałem DateTimeInputEdit.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("dateTimeInputEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/DateTimeInputEdit.html?" + Math.random(),
            scope: {
                Model: '=?ngDataModel',
                Label: "@ngDataLabel",
                InputName: "@ngDataInputName",
                isEditDisabled: "=?ngDataIsEditDisabled",
                hideLabel: "@?ngDataHideLabel",
                EditedItemChanged: "&ngFuncOnChange",
                isRequired: "=?ngDataIsRequired",
                ValidationProperty: "=?ngDataValidationProperty"
            },
            controller: DateTimeInputEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    DateTimeInputEditController.$inject = ['$scope', '$timeout', 'InfrService'];
    function DateTimeInputEditController($scope, $timeout, InfrService) {

        var $ctrl = this;
        $scope.$ctrl = this;


        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.DatepickerModel = null;
            if ($ctrl.Model === "") {
                $ctrl.Model = null;
            }
        }

        this._$onChanges = function () {
            $ctrl.$onModelChange();
        }
        this.$onModelChange = function () {
            if (InfrService.isString($ctrl.Model)) {
                if ($ctrl.Model === "") {
                    $ctrl.Model = null;
                }
                $timeout(function () {
                    if ($ctrl.Model != null) {
                        var DateFromModel = moment($ctrl.Model, 'DD-MM-YYYY HH:mm').toDate();
                        if (DateFromModel > new Date(Date.YEAR_2000_CONSTRUCT_VALUE)) {
                            $ctrl.DatepickerModel = DateFromModel;
                        }
                    }
                }, 0);
            }
        }
        var deregWatch$onModelChange = $scope.$watch('$ctrl.Model', function () {
            if (InfrService.isString($ctrl.Model)) {
                $ctrl.$onModelChange();
                deregWatch$onModelChange();
            }
        })
        // END OF HOOKS


        this.onDateChange = function () {
            $ctrl.Model = moment($ctrl.DatepickerModel).format('DD-MM-YYYY mm:HH');
            $ctrl.EditedItemChanged();
        }







        // INIT
        this._$onInit();
    }


})();


