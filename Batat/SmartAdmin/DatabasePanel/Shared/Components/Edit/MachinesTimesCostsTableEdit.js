﻿"use strict";
//console.log("Załadowałem MachinesTimesCostsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("app").directive("machinesTimesCostsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/MachinesTimesCostsTableEdit.html?" + Math.random(),
            scope: {
                ItemMachinesTimesCosts: "=?ngDataItemMachinesTimesCosts",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: MachinesTimesCostsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    MachinesTimesCostsTableEditController.$inject = ['$scope', '$filter', 'ItemsService', 'FeaturesService'];
    function MachinesTimesCostsTableEditController($scope, $filter, ItemsService, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;
        
        // LIFECYCLE HOOKS
        this._$onInit = function () {
            if ($ctrl.ItemMachinesTimesCosts == null) { $ctrl.ItemMachinesTimesCosts = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            $ctrl.MachineIdToAdd = null;
            $ctrl.UnitTimeToAdd = 1;
            $ctrl.PCTimeToAdd = 1;
            $ctrl.ToolCostToAdd = 1;
            $ctrl.OtherCostToAdd = 1;
            (ItemsService.get.Items('Machines')).then(function (data) { $ctrl.Machines = data; });
        }
        // END OF HOOKS


        this.RemoveMachineTimeCost = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemMachinesTimesCosts.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddMachineTimeCost = function () {
            var parameterExists = null;
            if (($ctrl.SelectedMachineId != null) && ($ctrl.SelectedMachineId > 0)) {
                parameterExists = $.grep($ctrl.ItemMachinesTimesCosts, function (e)
                { return e.MachineId == $ctrl.SelectedMachineId })[0];
            }
            if (($ctrl.SelectedMachineId == null) || ($ctrl.SelectedMachineId <= 0)) {
                FeaturesService.smartModalOK("Nie wybrano maszyny", "", function () { });
            } else if (parameterExists != null) {
                FeaturesService.smartModalOK("Maszyna istnieje na liście", "Zmień wartości przy maszynie z listy", function () { });
            } else if ($ctrl.UnitTimeToAdd == null) {
                FeaturesService.smartModalOK("Podaj czas jednostkowy", "", function () { });
            } else if ($ctrl.PCTimeToAdd == null) {
                FeaturesService.smartModalOK("Podaj czas przygotowawczo-zakończeniowy", "", function () { });
            } else if ($ctrl.ToolCostToAdd == null) {
                FeaturesService.smartModalOK("Podaj koszt narzędzi", "", function () { });
            } else if ($ctrl.OtherCostToAdd == null) {
                FeaturesService.smartModalOK("Podaj pozostałe koszty", "", function () { });
            } else {                        

                $ctrl.ItemMachinesTimesCosts.push({
                    MachineId: $ctrl.SelectedMachineId, UnitTime: $ctrl.UnitTimeToAdd,
                    PCTime: $ctrl.PCTimeToAdd, ToolCost: $ctrl.ToolCostToAdd, OtherCost: $ctrl.OtherCostToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.SelectedMachineId = null;
                $ctrl.UnitTimeToAdd = 1;
                $ctrl.PCTimeToAdd = 1;
                $ctrl.ToolCostToAdd = 1;
                $ctrl.OtherCostToAdd = 1;
            }
        }

        






        // INIT
        this._$onInit();
    }

})();



