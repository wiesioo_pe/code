﻿"use strict";
//console.log("Załadowałem TextInputEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("textInputEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/TextInputEdit.html?" + Math.random(),
            scope: {
                Model: '=?ngDataModel',
                Label: "@ngDataLabel",
                InputName: "@ngDataInputName",
                InputType: "@ngDataInputType",
                Min: "@?ngDataInputMin",
                Max: "@?ngDataInputMax",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: TextInputEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });


    // CONTROLLER
    TextInputEditController.$inject = ['$scope'];
    function TextInputEditController($scope) {

        var $ctrl = this;
        $scope.$ctrl = this;

    }

})();