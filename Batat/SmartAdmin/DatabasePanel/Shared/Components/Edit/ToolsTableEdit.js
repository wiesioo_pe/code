﻿"use strict";
//console.log("Załadowałem ToolsTableEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("toolsTableEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/ToolsTableEdit.html?" + Math.random(),
            scope: {
                ItemTools: "=?ngDataItemTools",
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange"
            },
            controller: ToolsTableEditController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });
                
    // CONTROLLER
    ToolsTableEditController.$inject = ['$scope', '$filter', 'ItemsService'];
    function ToolsTableEditController($scope, $filter, ItemsService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            if ($ctrl.ItemTools == null) { $ctrl.ItemTools = []; };
            if ($ctrl.isEditDisabled == null) { $ctrl.isEditDisabled = true; };
            $ctrl.ToolIdToAdd = null;
            $ctrl.UtilizationToAdd = 0;
            (ItemsService.get.Items('Tools')).then(function (data) { $ctrl.Tools = data; });
        }
        // END HOOKS


        this.RemoveTool = function ($index) {
            if (!$ctrl.isEditDisabled) {
                $ctrl.ItemTools.splice($index, 1);
                $ctrl.EditedItemChanged();
            }
        };

        this.AddTool = function () {
            var parameterExists = null;
            if (($ctrl.SelectedToolId != null) && ($ctrl.SelectedToolId > 0)) {
                parameterExists = $.grep($ctrl.ItemTools, function (e)
                { return e.ToolId == $ctrl.SelectedToolId })[0];
            }
            if (($ctrl.SelectedToolId == null) || ($ctrl.SelectedToolId <= 0)) {
                FeaturesService.smartModalOK("Nie wybrano narzędzia", "", function () { });
            } else if (parameterExists != null) {
                FeaturesService.smartModalOK("Narzędzie istnieje na liście", "Zmień zużycie przy narzędziu z listy", function () { });
            } else if ($ctrl.UtilizationToAdd == null) {
                FeaturesService.smartModalOK("Podaj wartość zużycia", "", function () { });
            } else {

                $ctrl.ItemTools.push({
                    ToolId: $ctrl.SelectedToolId, Utilization: $ctrl.UtilizationToAdd
                });
                $ctrl.EditedItemChanged();
                $ctrl.SelectedToolId = null;
                $ctrl.UtilizationToAdd = 0;
            }
        }

        






        // INIT
        this._$onInit();
    }

})();

