﻿"use strict";
//console.log("Załadowałem SelectListEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("selectListEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/SelectListEdit.html?" + Math.random(),
            scope: {
                Model: '=?ngDataModel',
                Label: "@ngDataLabel",
                InputName: "@ngDataInputName",
                isEditDisabled: "=?ngDataIsEditDisabled",
                SelectOptions: "=?ngDataSelectOptions",
                Placeholder: "@ngDataPlaceholder",
                onSelectChange: "&?ngDataOnSelectChange",
                selectName: "@ngDataSelectName",
                EditedItemChanged: "&ngFuncOnChange",
                isRequired: "=?ngDataIsRequired",
                ValidationProperty: "=?ngDataValidationProperty",
                canClear: "=?ngDataCanClear"
            },
            controller: SelectListEditController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });

    // CONTROLLER
    SelectListEditController.$inject = ['$scope', '$timeout', 'InfrService'];
    function SelectListEditController($scope, $timeout, InfrService) {

        var $ctrl = this;
        $scope.$ctrl = this;
        $ctrl.refreshCheckValue = 0;

        var init = true;
        $scope.$watch('$ctrl.SelectOptions', function () {
            if (init === false) {
                $timeout(function () {
                    var match = InfrService.findFirst($ctrl.SelectOptions, function (elem) { return elem.Value == $ctrl.Model; });
                    if (match == null) {
                        $ctrl.Model = null;
                    }
                    $ctrl.refreshCheckValue++;
                }, 0);
            } else { init = false; }
        }, false);

        // LIFECYCLE HOOKS
        this._$onInit = function () {

        };

        this._$onChanges = function () {
            $ctrl.$onModelChange();
        };

        this.$onModelChange = function (oldValue, newValue) {
            if (($ctrl.isRequired === true) && ($ctrl.Model != null)) {
                $ctrl.ValidationProperty = true;
            }
            else if ($ctrl.isRequired === false) {
                $ctrl.ValidationProperty = true;
            }
            else {
                $ctrl.ValidationProperty = false;
            }
        };

        $scope.$watch('$ctrl.Model', $ctrl.$onModelChange);
        // END HOOKS

        this.onSelected = function () {
            if (InfrService.isFunction($ctrl.EditedItemChanged)) { $ctrl.EditedItemChanged(); };
            if (InfrService.isFunction($ctrl.onSelectChange)) { $ctrl.onSelectChange(); };
        };

        


        // PRIVATE FUNCTIONS

      




        // INIT
        this._$onInit();
    }

})();