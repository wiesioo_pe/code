﻿"use strict";
//console.log("Załadowałem EditHeader.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("editHeader", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/EditHeader.html?" + Math.random(),
            scope: {
                isEdited: "=?ngDataIsEdited",
                isNew: "=?ngDataIsNew",
                ItemName: "@ngDataItemName",
                ItemId: "=?ngDataItemId",
                goBackUrl: "@ngDataGoBackUrl",
                isEditedItemChanged: "=?ngDataIsEditedItemChanged"
            },
            controller: EditHeaderController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    EditHeaderController.$inject = ['$scope', '$window', 'FeaturesService'];
    function EditHeaderController($scope, $window, FeaturesService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        this.GoBack = function () {
            if ($ctrl.isEditedItemChanged === true) {
                FeaturesService.smartModalYesNo("Czy chcesz porzucić wprowadzone zmiany i wyjść?", "", function () { $window.location.href = $ctrl.goBackUrl; }, function () { });
            } else {
                $window.location.href = $ctrl.goBackUrl;
            }
        }
    }
})();