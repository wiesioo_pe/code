﻿"use strict";
//console.log("Załadowałem SubjectAndContactEdit.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("subjectAndContactEdit", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Edit/SubjectAndContactEdit.html?" + Math.random(),
            scope: {
                ActiveItem: '=?ngDataActiveItem',
                isEditDisabled: "=?ngDataIsEditDisabled",
                EditedItemChanged: "&ngFuncOnChange",
                SelectOptions: "=?ngDataSelectOptionsAll",
                MainItemsService: "=?ngDataMainItemsService",
                ValidationObject: "=?ngDataValidationObject"
            },
            controller: SubjectAndContactEditController,
            controllerAs: '$ctrl',
            bindToController: true
        };
    });

    // CONTROLLER
    SubjectAndContactEditController.$inject = ['$scope', '$timeout', 'InfrService'];
    function SubjectAndContactEditController($scope, $timeout, InfrService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onChanges = function () {
            refreshContactSelectToNewData();
        };

        // END HOOKS
        var dereg$watchActiveItem = $scope.$watch('$ctrl.ActiveItem', function () {
            if (InfrService.isObject($ctrl.ActiveItem) && $ctrl.ActiveItem.SubjectId != null) {
                $timeout(function () {
                    $ctrl.SelectOptions = $.extend(true, {}, $ctrl.SelectOptions);
                    refreshContactSelectToNewData();
                }, 0);
                dereg$watchActiveItem();
            }
        }, true);

        this.onSubjectSelectChange = function () {
            $ctrl.EditedItemChanged();
            refreshContactSelectToNewData();
        };


        // PRIVATE FUNCTIONS
        function refreshContactSelectToNewData() {
            $timeout(function () {
                $ctrl.MainItemsService.getContactSelectOptionsForSubject($ctrl.ActiveItem.SubjectId)
                .then(function (response) {
                    $ctrl.SelectOptions.Contact = response;
                });
            }, 0);
        }
    }

})();
