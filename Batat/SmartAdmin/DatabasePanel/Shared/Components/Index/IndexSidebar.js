﻿"use strict";
console.log("Załadowałem IndexSidebar.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("indexSidebar", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Index/IndexSidebar.html?" + Math.random(),
            scope: {
                ViewState: "=?ngDataViewState",
                FilterItems: "&ngFuncFilterItems",
                ColumnsAndPropertiesNames: "=?ngDataColumnsVisibilityDictionary",
                ViewName: "=?ngDataViewName",
                OnColumnVisibilityChange: "&?ngFuncOnColumnVisibilityChange"
            },
            bindToController: true,
            controllerAs: '$ctrl',
            controller: IndexSidebarController
        }
    });

    // CONTROLLER
    IndexSidebarController.$inject = ['$scope', 'ItemsService'];
    function IndexSidebarController($scope, ItemsService) {
        
        var $ctrl = this;

        this.$onChanges = function () {
            if (($ctrl.ViewState != null) && ($ctrl.ViewState.ViewSettings != null)) {
                if ($ctrl.isFilterPresent('GroupIdFilter')) {
                    (ItemsService.get.Items('Groups')).then(function (data) { $ctrl.Groups = data; });
                }
                if ($ctrl.isFilterPresent('SubjectIdFilter')) {
                    (ItemsService.get.Items('Subjects')).then(function (data) { $ctrl.Subjects = data; });
                }
                if ($ctrl.isFilterPresent('EmployeeIdFilter')) {
                    (ItemsService.get.Items('Employees')).then(function (data) { $ctrl.Employees = data; });
                }
                if ($ctrl.isFilterPresent('OrderStateIdFilter')) {
                    (ItemsService.get.Items('OrderStates')).then(function (data) { $ctrl.OrderStates = data; });
                }
                if ($ctrl.isFilterPresent('FixedCostCategoryIdFilter')) {
                    (ItemsService.get.Items('FixedCostCategories')).then(function (data) { $ctrl.FixedCostCategories = data; });
                }
                if ($ctrl.isFilterPresent('FixedCostTypeIdFilter')) {
                    (ItemsService.get.Items('FixedCostTypes')).then(function (data) { $ctrl.FixedCostTypes = data; });
                }
                if ($ctrl.isFilterPresent('MachineTypeIdFilter')) {
                    (ItemsService.get.Items('MachineTypes')).then(function (data) { $ctrl.MachineTypes = data; });
                }
                if ($ctrl.isFilterPresent('MaterialTypeIdFilter')) {
                    (ItemsService.get.Items('MaterialTypes')).then(function (data) { $ctrl.MaterialTypes = data; });
                }
                if ($ctrl.isFilterPresent('ParameterIdFilter')) {
                    (ItemsService.get.Items('Parameters')).then(function (data) { $ctrl.Parameters = data; });
                }
                if ($ctrl.isFilterPresent('TechnologyIdFilter')) {
                    (ItemsService.get.Items('Technologies')).then(function (data) { $ctrl.Technologies = data; });
                }
                if ($ctrl.isFilterPresent('PartIdFilter')) {
                    (ItemsService.get.Items('Parts')).then(function (data) { $ctrl.Parts = data; });
                }
                if ($ctrl.isFilterPresent('OperationTypeIdFilter')) {
                    (ItemsService.get.Items('OperationTypes')).then(function (data) { $ctrl.OperationTypes = data; });
                }
                if ($ctrl.isFilterPresent('ToolTypeIdFilter')) {
                    (ItemsService.get.Items('ToolTypes')).then(function (data) { $ctrl.ToolTypes = data; });
                }
            }
        }
        
        var dereg = $scope.$watch('$ctrl.ViewState', function () {
            if ($ctrl.ViewState != null) {
                $ctrl.$onChanges();
                dereg();
            }
        });

        this.isFilterPresent = function (filterPropertyName) {
            if (($ctrl.ViewState != null) && ($ctrl.ViewState.ViewSettings != null) && $ctrl.ViewState.ViewSettings.hasOwnProperty(filterPropertyName)) {
                return true;
            } else {
                return false;
            }
        }
        
        this.UpdateUniversalFilter = function (UniversalFilter) {
            $ctrl.ViewState.ViewSettings.UniversalFilter = UniversalFilter;
            $ctrl.FilterItems();
        }
    }

})();