﻿"use strict";
console.log("Załadowałem IndexSidebarButtons.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("indexSidebarButtons", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Index/IndexSidebarButtons.html?" + Math.random(),
            scope: {
                ViewName: "=?ngDataViewName",
                ViewState: "=?ngDataViewState",
                MainItemsService: "=?ngDataMainItemsService"
            },
            bindToController: true,
            controllerAs: '$ctrl',
            controller: IndexSidebarButtonsController
        }
    });

    // CONTROLLER
    IndexSidebarButtonsController.$inject = ['ItemsService'];
    function IndexSidebarButtonsController(ItemsService) {
        
        var $ctrl = this;

        // View Settings
        this.QueryViewSettings = function () {
            ItemsService.queryViewSettings($ctrl.ViewName, $ctrl.ViewState.ViewSettings);
        }

        this.SaveViewSettings = function () {
            ItemsService.saveViewSettings($ctrl.ViewName, $ctrl.ViewState.ViewSettings);
        }

        this.ClearViewState = function () {
            if ($ctrl.MainItemsService != null) {
                $ctrl.MainItemsService.clearViewState();
                RewindTable();
            }
        }


        // PRIVATE FUNCTIONS

        function RewindTable() {
            var div = document.getElementsByClassName('index-table')[0];
            $(div).scrollTop(Controller.ViewState.SessionViewSettings.TableOffset);
        }
    }

})();