﻿"use strict";
console.log("Załadowałem IndexRibbon.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("indexRibbon", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Index/IndexRibbon.html?" + Math.random(),
            scope: {
                ItemsCount: "=?ngDataItemsCount",
                ActiveItemId: "=?ngDataActiveItemId",
                Dictionary: "=?ngDataDictionary",
                ClearExpanded: "&ngFuncClearExpanded",
                areItemsExpandable: "=?ngDataAreItemsExpandable"
            },
            bindToController: true,
            controllerAs: '$ctrl',
            controller: function () { },
            link: function (scope, element, attrs) {
                var newContainer = document.getElementById('left-panel');
                if (newContainer != null) {
                    $(newContainer).append(element);
                } else {
                    setTimeout(function () {
                        newContainer = document.getElementById('left-panel');
                        $(newContainer).append(element);
                    }, 100);
                }
            }
        }
    });

})();