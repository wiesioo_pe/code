﻿"use strict";
console.log("Załadowałem indexUniversalFilter.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("indexUniversalFilter", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Index/IndexUniversalFilter.html?" + Math.random(),
            scope: {
                updateUniversalFilter: "&ngFuncUpdateUniversalFilter",
                filterChanged: "&ngFuncUniversalFilterChanged"
            },
            bindToController: true,
            controllerAs: '$ctrl',
            controller: function () {

                var $ctrl = this;

                this.Model = "";

                this.universalFilterChanged = function () {
                    $ctrl.updateUniversalFilter({ 'UniversalFilter': $ctrl.Model });
                }

                this.universalFilterReset = function () {
                    $ctrl.Model = "";
                    $ctrl.universalFilterChanged();
                }
            },
            link: function (scope, element, attrs) {
                var newContainer = document.getElementById('page-footer');
                if(newContainer != null) {
                    $(newContainer).append(element);
                } else {
                    setTimeout(function () {
                        newContainer = document.getElementById('page-footer');
                        $(newContainer).append(element);
                    }, 100);
                }
            }
        }
    });

})();