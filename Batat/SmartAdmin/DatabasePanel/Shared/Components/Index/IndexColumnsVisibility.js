﻿"use strict";
console.log("Załadowałem IndexColumnsVisibility.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").directive("indexColumnsVisibility", function () {
        return {
            templateUrl: "/SmartAdmin/DatabasePanel/Shared/Components/Index/IndexColumnsVisibility.html?" + Math.random(),
            scope: {
                Dictionary: "=?ngDataDictionary",
                ColumnsVisibility: "=?ngDataColumnsVisibility",
                OnColumnVisibilityChange: "&?ngFuncOnColumnVisibilityChange"
            },
            bindToController: true,
            controllerAs: '$ctrl',
            controller: function () { }
        }
    });
})();