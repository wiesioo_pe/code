﻿"use strict";
console.log("Załadowałem ViewStateService.js");
/// <reference path="~/app/scripts/angular.js" />

angular.module("app").service("ViewStateService",
    ['InfrService', function (InfrService) {


        this.checkIfDateExceeded = function (DateToCheck) {
            var dateParts = null;
            if ((DateToCheck) && (typeof DateToCheck === "string")) {
                dateParts = DateToCheck.split('-');
            }
            if ((dateParts) && (dateParts.length >= 3)) {
                var _DateToCheck = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                var Today = new Date();
                if (_DateToCheck < Today) { return true; }
                else { return false; }
            } else {
                return false
            }
        }

        this.setExpandedColumnSpan = function (ViewState) {
            if ((ViewState != null) && (ViewState.ViewSettings != null)) {
                var ExpandedColumnSpan = 0;
                if (ViewState.ViewSettings.ColumnsVisibility
                    && (ViewState.ViewSettings.ColumnsVisibility.length > 0)) {
                    for (var columnVisibility of ViewState.ViewSettings.ColumnsVisibility) {
                        if (columnVisibility === true) { ExpandedColumnSpan += 1; };
                    }
                }
                ViewState.ViewSettings.ExpandedColumnSpan = ExpandedColumnSpan;
            }
        }

        this.expandCollapseItem = function (ItemId, ViewState) {
            if (ViewState.SessionViewSettings.ItemsExpanded.indexOf(ItemId) == -1) {
                ViewState.SessionViewSettings.ItemsExpanded.push(ItemId);
            } else {
                var index = ViewState.SessionViewSettings.ItemsExpanded.indexOf(ItemId);
                ViewState.SessionViewSettings.ItemsExpanded.splice(index, 1);
            }
        }

        this.clearExpanded = function (ViewState) {
            ViewState.SessionViewSettings.ItemsExpanded = [];
        }

        this.isItemExpanded = function (ItemId, ViewState) {
            if (ViewState.SessionViewSettings.ItemsExpanded) {
                if (ViewState.SessionViewSettings.ItemsExpanded.indexOf(ItemId) >= 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        this.saveViewState = function (ViewName, ViewState) {
            //console.log('ViewStateService: saving ViewState for ' + ViewName);
            //console.log(ViewState);
            localStorage.setItem(ViewName + 'ViewState.SessionViewSettings', JSON.stringify(ViewState.SessionViewSettings));
            localStorage.setItem(ViewName + 'ViewState.ViewSettings', JSON.stringify(ViewState.ViewSettings));
            localStorage.setItem(ViewName + 'ViewState.TemporaryViewSettings', JSON.stringify(ViewState.TemporaryViewSettings));
        }

        this.getViewState = function (ViewName) {
            var lsSessionViewSettings = localStorage.getItem(ViewName + 'ViewState.SessionViewSettings');
            var lsViewSettings = localStorage.getItem(ViewName + 'ViewState.ViewSettings');
            var ViewState = null;

            if ((lsSessionViewSettings != null) && (lsViewSettings != null)) {
                ViewState = {};
                ViewState.SessionViewSettings = JSON.parse(lsSessionViewSettings);
                ViewState.ViewSettings = JSON.parse(lsViewSettings);
                ViewState.TemporaryViewSettings = {};
                ViewState.isInitialized = true;
            }
            return ViewState;
        }

        this.clearViewState = function (ViewName, ViewState) {
            localStorage.removeItem(ViewName + 'ViewState.SessionViewSettings');
            localStorage.removeItem(ViewName + 'ViewState.ViewSettings');
            alert("LocalStorage: usunięto ustawienia " + ViewName);
        }

        this.updateViewData = function (MainItems, ViewData) {
            if (InfrService.isArrayAndNotEmpty(MainItems) && InfrService.isObject(ViewData)) {
                var ItemsMatched = MainItems.filter(function (obj) {
                    return obj.Visible == true;
                });
                ViewData.ItemsCount = ItemsMatched.length;
            }
        }
}]);