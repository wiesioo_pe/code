﻿"use strict";
console.log("Załadowałem EditViewComponent.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // COMPONENT
    angular.module("app").controller("EditViewController", EditViewController);


    // CONTROLLER
    EditViewController.$inject = ['$scope', '$stateParams', '$timeout', '$document', '$window', 'InfrService', 'ItemsService'
        // Resolved parameters (from $stateProvider)
        , 'Items', 'MainItemsService' ];

    function EditViewController($scope, $stateParams, $timeout, $document, $window, InfrService, ItemsService
        , Items, MainItemsService) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.ActiveItem = {};
            $ctrl.SelectOptions = {};
            $ctrl.isEditDisabled = true;
            $ctrl.isModelValid = false;
            $ctrl.isEditedItemChanged = false;
            $ctrl.MainItemsService = MainItemsService;
            $ctrl.MainItemsName = MainItemsService.getMainItemsName();
            $ctrl.ViewName = $ctrl.MainItemsName + "Edit";
            $ctrl.ValidationObject = {};
            $ctrl.Items = Items;
            // get ActiveItem
            MainItemsService.get$ActiveItem().then(function (response) {
                $ctrl.ActiveItem = response;
                $ctrl.ValidationObject = ItemsService.getValidationObjectForActiveItem($ctrl.ActiveItem);
            });
            // get promise of SelectOptions
            MainItemsService.get$EditSelectOptions().then(function (response) {
                $ctrl.SelectOptions = response;
            });
            // enable/disable edit
            $ctrl.isEditDisabled = MainItemsService.isEditDisabled();
        };

        // END HOOKS


        // VIEW METHODS

        // called when Undo changes button clicked
        this.ResetActiveItem = function () {
            MainItemsService.refresh$ActiveItem()
                .then(function (response) {
                    $timeout(function () { $ctrl.isEditedItemChanged = false; }, 0);
                    RewindBody();
                }, function (error) { });
        };

        // called when Save button changed
        this.SaveActiveItem = function () {
            if ($ctrl.isModelValid) {
                MainItemsService.save$ActiveItem()
                    .then(function (response) {
                        $timeout(function () {
                            $ctrl.isEditedItemChanged = false;
                            RewindBody();
                            $ctrl.ServerValidationError = null;
                        }, 0);
                    }, function (error) {
                        $timeout(function () {
                            $ctrl.isEditedItemChanged = false;
                            RewindBody();
                            $ctrl.ServerValidationError = error;
                        }, 0);
                    });
            }
        };

        // called when item changed - passed to all edit directives as callback
        this.EditedItemChanged = function () {
            $timeout(function () {
                $ctrl.isEditedItemChanged = true;
                $ctrl.isModelValid = ItemsService.isValidationObjectValid($ctrl.ValidationObject);
            }, 0);
        };


        // PRIVATE FUNCTIONS
        function RewindBody() {
            let speed = 600;
            $(document.getElementsByClassName('edit-body')[0]).animate({
                scrollTop: 0
            }, speed);
        }

        $scope.$on("$destroy", function () {
            $(document.getElementsByClassName('pinned-nav-buttons')[0]).remove();
        });

        // INIT
        this._$onInit();
    }
})();
