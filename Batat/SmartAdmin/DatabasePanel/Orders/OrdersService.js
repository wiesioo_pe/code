﻿"use strict";
console.log("Załadowałem OrdersService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("OrdersService", OrdersService);


    OrdersService.$inject = ['$timeout', '$interval', '$q', '$filter', '$http'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function OrdersService($timeout, $interval, $q, $filter, $http
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        let OrdersService = this;


    // SERVICE DATA

        let data = {
            ItemsNames: ['Orders', 'Products', 'Parts', 'OrderStates', 'Groups', 'Subjects', 'Contacts', 'Employees']
        };

        let cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        let promises = {
            Items: {}
        };

        let PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getOrderStateId: getOrderStateId,
            getViewState: getViewState
        };


    // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();



    // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        };

        this.getMainItemsName = function () { return "Orders"; };

        this.getViewData = function () {

            let table = document.getElementsByClassName('index-table-container')[0].children[0];
            let fontSize = parseFloat(getStyle(table, 'font-size').replace("px", ""));
            let tableWidth = table.offsetWidth;

            let ViewWidths = [];
            ViewWidths[0] = 6; // Id
            ViewWidths[1] = 10; // Groups
            ViewWidths[2] = 10; // Subject
            ViewWidths[3] = 10; // Contact
            ViewWidths[4] = 8; // ReceiveDate
            ViewWidths[5] = 8; // AccomplishDate
            ViewWidths[6] = 8; // DeliveryDate
            ViewWidths[7] = 20; // Desc
            ViewWidths[8] = 8; // Employee
            ViewWidths[9] = 6; // OrderState

            let sum = 0;
            for (let i = 0; i < cache.ViewState.ViewSettings.ColumnsVisibility.length; i++) {
                if (cache.ViewState.ViewSettings.ColumnsVisibility[i] === true) {
                    sum += ViewWidths[i];
                }
            }

            let ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "6em";
            ViewData.GroupsColumnWidth = Math.trunc(ViewWidths[1] / sum * 100);
            ViewData.SubjectColumnWidth = Math.trunc(ViewWidths[2] / sum * 100);
            ViewData.ContactColumnWidth = Math.trunc(ViewWidths[3] / sum * 100);
            ViewData.ReceiveDateColumnWidth = Math.trunc(ViewWidths[4] / sum * 100);
            ViewData.AccomplishDateColumnWidth = Math.trunc(ViewWidths[5] / sum * 100);
            ViewData.DeliveryDateColumnWidth = Math.trunc(ViewWidths[6] / sum * 100);
            ViewData.DescColumnWidth = Math.trunc(ViewWidths[7] / sum * 100);
            ViewData.EmployeeColumnWidth = Math.trunc(ViewWidths[8] / sum * 100);
            ViewData.OrderStateColumnWidth = "6em";

            return ViewData;
        };

        this.filterItems = function () {

            if (InfrService.isArrayAndNotEmpty(cache.Items.Orders) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                let _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                let _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                let _SubjectIdFilter = cache.ViewState.ViewSettings.SubjectIdFilter;
                let _EmployeeIdFilter = cache.ViewState.ViewSettings.EmployeeIdFilter;
                let _OrderStateIdFilter = cache.ViewState.ViewSettings.OrderStateIdFilter;
                let _PriorityIdFilter = cache.ViewState.ViewSettings.PriorityIdFilter;
                let _DescFilter = cache.ViewState.ViewSettings.DescFilter;
                let _idFilter = cache.ViewState.ViewSettings.IdFilter;


                // RECEIVE DATE
                let _ReceiveDateFrom, _ReceiveDateTo;
                try {
                    let _ReceiveDatePartsFrom = cache.ViewState.ViewSettings.ReceiveDateFromFilter.split('-');
                    _ReceiveDateFrom = new Date(_ReceiveDatePartsFrom[2], _ReceiveDatePartsFrom[1] - 1, _ReceiveDatePartsFrom[0]);
                }
                catch (ex) { }
                try {
                    let _ReceiveDatePartsTo = cache.ViewState.ViewSettings.ReceiveDateToFilter.split('-');
                    _ReceiveDateTo = new Date(_ReceiveDatePartsTo[2], _ReceiveDatePartsTo[1] - 1, _ReceiveDatePartsTo[0]);
                }
                catch (ex) { }

                // ACCOMPLISH DATE
                let _AccomplishDateFrom, _AccomplishDateTo;

                try {
                    let _AccomplishDatePartsFrom = cache.ViewState.ViewSettings.AccomplishDateFromFilter.split('-');
                    _AccomplishDateFrom = new Date(_AccomplishDatePartsFrom[2], _AccomplishDatePartsFrom[1] - 1, _AccomplishDatePartsFrom[0]);
                }
                catch (ex) { }
                try {
                    let _AccomplishDatePartsTo = cache.ViewState.ViewSettings.AccomplishDateToFilter.split('-');
                    _AccomplishDateTo = new Date(_AccomplishDatePartsTo[2], _AccomplishDatePartsTo[1] - 1, _AccomplishDatePartsTo[0]);
                }
                catch (ex) { }


                // DELIVERY DATE
                let _DeliveryDateFrom, _DeliveryDateTo;

                try {
                    let _DeliveryDatePartsFrom = cache.ViewState.ViewSettings.DeliveryDateFromFilter.split('-');
                    _DeliveryDateFrom = new Date(_DeliveryDatePartsFrom[2], _DeliveryDatePartsFrom[1] - 1, _DeliveryDatePartsFrom[0]);
                }
                catch (ex) { }
                try {
                    let _DeliveryDatePartsTo = cache.ViewState.ViewSettings.DeliveryDateToFilter.split('-');
                    _DeliveryDateTo = new Date(_DeliveryDatePartsTo[2], _DeliveryDatePartsTo[1] - 1, _DeliveryDatePartsTo[0]);
                }
                catch (ex) { }


                // ID MATCH TABLE
                let idMatchTable = [];
                let _idFilterLowercase = _idFilter.replace(/\s/g, '');
                let _idFilterParts = _idFilterLowercase.split(",");
                if (InfrService.isArrayAndNotEmpty(_idFilterParts)) {
                    for (let part of _idFilterParts) {
                        if (InfrService.isInt(part)) {
                            idMatchTable.push(parseInt(part));
                        } else if (part.indexOf("-") >= 0) {
                            let rangeParts = part.split("-");
                            if (rangeParts.length == 2) {
                                if (InfrService.isInt(rangeParts[0]) && InfrService.isInt(rangeParts[1])) {
                                    let rangeStart = parseInt(rangeParts[0]);
                                    let rangeEnd = parseInt(rangeParts[1]);
                                    for (let i = rangeStart; i <= rangeEnd; i++) {
                                        idMatchTable.push(i);
                                    }
                                }
                            }
                        }
                    }
                }


                // for each item in cache.Items.Orders
                for (let i = 0; i < cache.Items.Orders.length; i++) {


                    let _Item = cache.Items.Orders[i];
                    let _isMatchedUniversal = false;
                    let _isInGroup = false;
                    let _isForSubject = false;
                    let _isByEmployee = false;
                    let _isInOrderState = false;
                    let _isOfPriority = false;
                    let _isBetweenReceiveDates = false;
                    let _isBetweenAccomplishDates = false;
                    let _isBetweenDeliveryDates = false;
                    let _isOfId = false;

                    let _LowerCaseProperty = "";
                    let _LowerCaseFilter = "";


                    // Check Universal filter
                    if (_UniversalFilter === "") {
                        _isMatchedUniversal = true;
                    } else {
                        let lowerCaseUniversalFilter = _UniversalFilter.toLowerCase();
                        // Check in Groups names
                        if (InfrService.isArrayAndNotEmpty(_Item.Groups)) {
                            let GroupsMatchedIds = [];
                            for (let group of cache.Items.Groups) {
                                let lowerCaseGroupName = group.Name.toLowerCase();
                                if (lowerCaseGroupName.indexOf(lowerCaseUniversalFilter) >= 0) {
                                    GroupsMatchedIds.push(group.GroupId);
                                }
                            }
                            for (let groupId of GroupsMatchedIds) {
                                if (InfrService.doesArrayContainElementByProp(_Item.Groups, groupId, 'GroupId')) {
                                    _isMatchedUniversal = true;
                                }
                            }
                        }

                        // Check in Subjects names
                        if (_isMatchedUniversal === false) {
                            if (_Item.SubjectId != null) {
                                let SubjectName = $filter('getSubjectProperty')(_Item.SubjectId, 'Name', cache.Items.Subjects);
                                if (InfrService.isString(SubjectName)) {
                                    let lowerCaseSubjectName = SubjectName.toLowerCase();
                                    if (lowerCaseSubjectName.indexOf(lowerCaseUniversalFilter) >= 0) {
                                        _isMatchedUniversal = true;
                                    }
                                }
                            }
                        }

                        // Check in Contacts names
                        if (_isMatchedUniversal === false) {
                            if (_Item.ContactId != null) {
                                let ContactName = $filter('getContactProperty')(_Item.ContactId, 'Name', cache.Items.Contacts);
                                if (InfrService.isString(ContactName)) {
                                    let lowerCaseContactName = ContactName.toLowerCase();
                                    if (lowerCaseContactName.indexOf(lowerCaseUniversalFilter) >= 0) {
                                        _isMatchedUniversal = true;
                                    }
                                }
                            }
                        }

                        // Check in Dates
                        if (_isMatchedUniversal === false) {
                            if (InfrService.isString(_Item.ReceiveDate)) {
                                if (_Item.ReceiveDate.indexOf(lowerCaseUniversalFilter) >= 0) {
                                    _isMatchedUniversal = true;
                                }
                            }
                        }
                        if (_isMatchedUniversal === false) {
                            if (InfrService.isString(_Item.AccomplishDate)) {
                                if (_Item.AccomplishDate.indexOf(lowerCaseUniversalFilter) >= 0) {
                                    _isMatchedUniversal = true;
                                }
                            }
                        }
                        if (_isMatchedUniversal === false) {
                            if (InfrService.isString(_Item.DeliveryDate)) {
                                if (_Item.DeliveryDate.indexOf(lowerCaseUniversalFilter) >= 0) {
                                    _isMatchedUniversal = true;
                                }
                            }
                        }

                        // Check in Desc
                        if (_isMatchedUniversal === false) {
                            if (InfrService.isString(_Item.Desc)) {
                                let lowerCaseDesc = _Item.Desc.toLowerCase();
                                if (lowerCaseDesc.indexOf(lowerCaseUniversalFilter) >= 0) {
                                    _isMatchedUniversal = true;
                                }
                            }
                        }

                        // Check in Employees names
                        if (_isMatchedUniversal === false) {
                            if (_Item.EmployeeId != null) {
                                let EmployeeName = $filter('getEmployeeProperty')(_Item.EmployeeId, 'Name', cache.Items.Employees);
                                if (InfrService.isString(EmployeeName)) {
                                    let lowerCaseEmployeeName = EmployeeName.toLowerCase();
                                    if (lowerCaseEmployeeName.indexOf(lowerCaseUniversalFilter) >= 0) {
                                        _isMatchedUniversal = true;
                                    }
                                }
                            }
                        }

                    }

                    // Check if of ID
                    if (_idFilter === "") {
                        _isOfId = true;
                    } else {
                        if (idMatchTable.indexOf(_Item.Id) >= 0) {
                            _isOfId = true;
                        }
                    }


                    // Check if in group
                    if (_GroupIdFilter === "All") {
                        _isInGroup = true;
                    } else {
                        let MatchedOrders = _Item.Groups.filter(function (item) {
                            return item.GroupId === _GroupIdFilter;
                        });
                        if (MatchedOrders[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check if is for Subject
                    if (_SubjectIdFilter === "All") {
                        _isForSubject = true;
                    } else {
                        if (_Item.SubjectId === _SubjectIdFilter) {
                            _isForSubject = true;
                        }
                    }

                    // Check if is by Employee
                    if (_EmployeeIdFilter === "All") {
                        _isByEmployee = true;
                    } else {
                        if (_Item.EmployeeId === _EmployeeIdFilter) {
                            _isByEmployee = true;
                        }
                    }

                    // Check if is in OrderState
                    if (_OrderStateIdFilter === "All") {
                        _isInOrderState = true;
                    } else {
                        if (_Item.StateId === _OrderStateIdFilter) {
                            _isInOrderState = true;
                        }
                    }

                    // Check if is of Priority
                    if (_PriorityIdFilter === "All") {
                        _isOfPriority = true;
                    } else {
                        if (_Item.PriorityId === _PriorityIdFilter) {
                            _isOfPriority = true;
                        }
                    }


                    // Check if between Receive Dates
                    let _isAboveFrom = false;
                    let _isBelowTo = false;

                    // Get _ReceiveDate for _Item
                    let _ReceiveDate;
                    try {
                        let ReceiveDateParts = _Item.ReceiveDate.split('-');
                        _ReceiveDate = new Date(ReceiveDateParts[2], ReceiveDateParts[1] - 1, ReceiveDateParts[0]);
                    }
                    catch (ex) { }


                    if (cache.ViewState.ViewSettings.ReceiveDateFromFilter === "") {
                        _isAboveFrom = true;
                    }
                    else {
                        if (_ReceiveDate >= _ReceiveDateFrom) {
                            _isAboveFrom = true;
                        }
                    }

                    if (cache.ViewState.ViewSettings.ReceiveDateToFilter === "") {
                        _isBelowTo = true;
                    }
                    else {
                        if (_ReceiveDate <= _ReceiveDateTo) {
                            _isBelowTo = true;
                        }
                    }

                    _isBetweenReceiveDates = _isAboveFrom && _isBelowTo;


                    // Check if between Accomplish Dates
                    _isAboveFrom = false;
                    _isBelowTo = false;

                    // Get _AccomplishDate for _Item
                    let _AccomplishDate;
                    try {
                        let AccomplishDateParts = _Item.AccomplishDate.split('-');
                        _AccomplishDate = new Date(AccomplishDateParts[2], AccomplishDateParts[1] - 1, AccomplishDateParts[0]);
                    }
                    catch (ex) { }


                    if (cache.ViewState.ViewSettings.AccomplishDateFromFilter === "") {
                        _isAboveFrom = true;
                    }
                    else {
                        if (_AccomplishDate >= _AccomplishDateFrom) {
                            _isAboveFrom = true;
                        }
                    }

                    if (cache.ViewState.ViewSettings.AccomplishDateToFilter === "") {
                        _isBelowTo = true;
                    }
                    else {
                        if (_AccomplishDate <= _AccomplishDateTo) {
                            _isBelowTo = true;
                        }
                    }

                    _isBetweenAccomplishDates = _isAboveFrom && _isBelowTo;

                    // Check if between Delivery Dates
                    _isAboveFrom = false;
                    _isBelowTo = false;

                    // Get _DeliveryDate for _Item
                    let _DeliveryDate;
                    try {
                        let DeliveryDateParts = _Item.DeliveryDate.split('-');
                        _DeliveryDate = new Date(DeliveryDateParts[2], DeliveryDateParts[1] - 1, DeliveryDateParts[0]);
                    }
                    catch (ex) { }


                    if (cache.ViewState.ViewSettings.DeliveryDateFromFilter === "") {
                        _isAboveFrom = true;
                    }
                    else {
                        if (_DeliveryDate >= _DeliveryDateFrom) {
                            _isAboveFrom = true;
                        }
                    }

                    if (cache.ViewState.ViewSettings.DeliveryDateToFilter === "") {
                        _isBelowTo = true;
                    }
                    else {
                        if (_DeliveryDate <= _DeliveryDateTo) {
                            _isBelowTo = true;
                        }
                    }

                    _isBetweenDeliveryDates = _isAboveFrom && _isBelowTo;

                    // Check desc filters
                    let _doesItemDescContain = false;

                    if (_DescFilter === "") {
                        _doesItemDescContain = true;
                    }
                    else {
                        if (InfrService.isString(_Item.Desc)) {
                            _LowerCaseProperty = _Item.Desc.toLowerCase();
                        }
                        if (InfrService.isString(_DescFilter)) {
                            _LowerCaseFilter = _DescFilter.toLowerCase();
                        }
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesItemDescContain = true;
                        }
                    }

                    // Set _Item visibility
                    _Item.Visible = (_isMatchedUniversal && _isOfId && _isInGroup && _isForSubject && _isByEmployee && _isInOrderState && _isOfPriority
                        && _isBetweenReceiveDates && _isBetweenAccomplishDates && _isBetweenDeliveryDates
                        && _doesItemDescContain);
                }
            }
        };

        this.saveViewState = function () {
            console.log('OrdersService: saving ViewState');
            let div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("OrdersIndex", cache.ViewState);
        };

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        };

        this.clearViewState = function () {
            ViewStateService.clearViewState("OrdersIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        };

        this.onIndexDataDownloadCompleted = function ($scope) {
            // add watch for ColumnsVisibility changes to recalculate colspan for expanded item
            $scope.$watch(function () { return cache.ViewState.ViewSettings.ColumnsVisibility },
                function () {
                    ViewStateService.setExpandedColumnSpan(cache.ViewState);
                    cache.ViewState.ViewSettings.ExpandedColumnSpan += 2;
                }, true);
        };

        this.getIndexHeaderDictionary = function () {

            let Dictionary = {};

            Dictionary.ItemsNameString = "Zamówienia";
            Dictionary.ActiveItemString = "Zamówienie nr: ";

            Dictionary.LeftButtonsGroups = [];
            let ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            let Button = {};
            Button.Title = "Dodaj nowe zamówienie";
            Button.Href = function () {
                return "#/Orders/Edit?Id=0";
            };
            Button.Click = function () {
                OrdersService.set$ActiveItemId(0);
            };
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.FontAwsomeClass = "fa-plus-square";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Orders/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.FontAwsomeClass = "fa-pencil";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0
                    && PrivateFunctions.getOrderStateId(cache.ViewState.SessionViewSettings.ActiveItemId) === 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Szczegóły
            Button = {};
            Button.Title = "Szczegóły";
            Button.Href = function () {
                return "#/Orders/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-details-item";
            Button.FontAwsomeClass = "fa-list-alt";
            Button.IsVisible = function () {
                return PrivateFunctions.getOrderStateId(cache.ViewState.SessionViewSettings.ActiveItemId) > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Orders/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.FontAwsomeClass = "fa-copy";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0
                    && PrivateFunctions.getOrderStateId(cache.ViewState.SessionViewSettings.ActiveItemId) > -1;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            // Dolna grupa
            ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Autoryzuj
            Button = {};
            Button.Title = "Autoryzuj";
            Button.Href = function () {
                return "#/Orders/Authorize/" + cache.ViewState.SessionViewSettings.ActiveItemId + "/";
            };
            Button.Click = function () {
                OrdersService.authorize$Order(cache.ActiveItem.Id);
            };
            Button.ImageClass = "batat-icon-authorize-item-gray";
            Button.IsVisible = function () {
                if (PrivateFunctions.getOrderStateId(cache.ViewState.SessionViewSettings.ActiveItemId) === 0) {
                    return true;
                } else {
                    return false;
                }
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Cofnij autoryzację
            Button = {};
            Button.Title = "Cofnij autoryzację";
            Button.Href = function () {
                return "#/Orders/Unauthorize/" + cache.ViewState.SessionViewSettings.ActiveItemId + "/";
            };
            Button.Click = function () {
                OrdersService.unauthorize$Order(cache.ActiveItem.Id);
            };
            Button.ImageClass = "batat-icon-unauthorize-item-gray";
            Button.IsVisible = function () {
                if (PrivateFunctions.getOrderStateId(cache.ViewState.SessionViewSettings.ActiveItemId) === 1) {
                    return true;
                } else {
                    return false;
                }
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Do produkcji
            Button = {};
            Button.Title = "Do produkcji";
            Button.Href = function () {
                return "#/Orders/IntoProduction/" + cache.ViewState.SessionViewSettings.ActiveItemId + "/";
            };
            Button.Click = function () {
                OrdersService.sendIntoProduction$Order(cache.ActiveItem.Id);
            };
            Button.ImageClass = "batat-icon-into-production-item-gray";
            Button.IsVisible = function () {
                if (PrivateFunctions.getOrderStateId(cache.ViewState.SessionViewSettings.ActiveItemId) === 1) {
                    return true;
                } else {
                    return false;
                }
            };
            ButtonsGroup.Buttons.push(Button);

            Dictionary.RightButtonsGroups.push(ButtonsGroup);
            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            let Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = { Name: "Id", canHide: false };
            Dictionary.ColumnsNames[1] = { Name: "Grupy", canHide: true };
            Dictionary.ColumnsNames[2] = { Name: "Odbiorca", canHide: true };
            Dictionary.ColumnsNames[3] = { Name: "Osoba do kontaktu", canHide: true };
            Dictionary.ColumnsNames[4] = { Name: "Data wpłynięcia", canHide: true };
            Dictionary.ColumnsNames[5] = { Name: "Termin wykonania", canHide: true };
            Dictionary.ColumnsNames[6] = { Name: "Termin dostawy", canHide: true };
            Dictionary.ColumnsNames[7] = { Name: "Opis", canHide: true };
            Dictionary.ColumnsNames[8] = { Name: "Osoba odpowiedzialna", canHide: true };
            Dictionary.ColumnsNames[9] = { Name: "Osoba odpowiedzialna", canHide: false };
            return Dictionary;
        };

        
        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId === 0) {
                    OrdersService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return OrdersService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };
        
        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return OrdersService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = 0;
                    OrdersService.set$ActiveItemId(0);
                    cache.ActiveItem.StateId = 0;
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        };



    // EDIT METHODS

        
        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                let ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Orders, "Id");

                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        };

        
        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        };

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        };

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        };

        
        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                    function (response) {
                        let SelectOptions = {};
                        //Priority options
                        SelectOptions.Priority = [];
                        let Option = {};
                        Option.Name = 'Normalny';
                        Option.Value = 0;
                        SelectOptions.Priority.push(Option);

                        Option = {};
                        Option.Name = 'Wysoki';
                        Option.Value = 1;
                        SelectOptions.Priority.push(Option);

                        //Subject options
                        SelectOptions.Subject = [];
                        for (let Subject of cache.Items.Subjects) {
                            if (Subject.isCustomer === true) {
                                let Option = {};
                                Option.Name = Subject.Id + ' - ' + Subject.ShortName;
                                Option.Desc = Subject.Name;
                                Option.Value = Subject.Id;
                                SelectOptions.Subject.push(Option);
                            }
                        }
                        OrdersService.getContactSelectOptionsForSubject(cache.ActiveItem.SubjectId)
                            .then(function (response) { SelectOptions.Contact = response; });

                        // Employee options
                        SelectOptions.Employee = [];
                        for (let Employee of cache.Items.Employees) {
                            let Option = {};
                            Option.Name = Employee.Name;
                            Option.Value = Employee.UUID;
                            SelectOptions.Employee.push(Option);
                        }
                        return SelectOptions;
                    }, function (error) { throw false; });
        };

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Orders", cache.ActiveItem)
                .then(function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    OrdersService.set$ActiveItemId(response.Id);
                    return true;
                },
                    function (error) {
                        throw error.data;
                    });
        };

        this.isEditDisabled = function () {
            let ActiveItemId = cache.ViewState.SessionViewSettings.ActiveItemId;
            let StateId = PrivateFunctions.getOrderStateId(ActiveItemId);

            let isEditDisabled = {
                Never: false,
                OrderState012: (StateId < 3),
                OrderState123: (StateId > 0),
                OrderState3: (StateId === 3)
            };


            return isEditDisabled;
        };

        this.getContactSelectOptionsForSubject = function (SubjectId) {
            return $q.all(promises.Items).then(function (response) {
                let ContactSelectOptions = [];
                if (InfrService.isArrayAndNotEmpty(cache.Items.Contacts)) {
                    let MatchedContacts = $.grep(cache.Items.Contacts, function (e) { return e.SubjectId == SubjectId });
                    if (InfrService.isArrayAndNotEmpty(MatchedContacts)) {
                        for (let Contact of MatchedContacts) {
                            let Option = {};
                            Option.Name = Contact.Id + ' - ' + Contact.Name;
                            Option.Desc = Contact.Position + ' - ' + Contact.BusinessPhone;
                            Option.Value = Contact.Id;
                            ContactSelectOptions.push(Option);
                        }
                    }
                }
                return ContactSelectOptions;
            }, function (error) { throw false; });
        };

        this.authorize$Order = function (orderId) {
            $http.get('/Orders/Authorize/' + orderId)
                .then(function (response) {
                    ItemsService.setOrderSpecificProperties(response);
                    ItemsService.replaceItem(response.data, "Orders");
                }, function (error) { });
        };

        this.unauthorize$Order = function (orderId) {
            $http.get('/Orders/Unauthorize/' + orderId)
                .then(function (response) {
                    ItemsService.setOrderSpecificProperties(response);
                    ItemsService.replaceItem(response.data, "Orders");
                }, function (error) { });
        };

        this.sendIntoProduction$Order = function (orderId) {
            $http.get('/Orders/IntoProduction/' + orderId)
                .then(function (response) {
                    ItemsService.setOrderSpecificProperties(response);
                    ItemsService.replaceItem(response.data, "Orders");
                    ItemsService.get.Items('OrderInstances').then(function (response) { console.warn('OrdersInstances'); console.warn(response); });
                }, function (error) { });
        };

    // ITEMS SPECIFIC METHODS

        this.getDefaultDiscount = function () {
            if (InfrService.isArrayAndNotEmpty(cache.Items.Subjects) && cache.ActiveItem.SubjectId) {
                let MatchedSubjects = $.grep(cache.Items.Subjects, function (e) { return e.SubjectId == cache.ActiveItem.SubjectId });
                if (InfrService.isArrayAndNotEmpty(MatchedSubjects)) {
                    return MatchedSubjects[0].Discount;
                } else {
                    return 0;
                }
            }
        };
        



    // PRIVATE FUNCTIONS

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering letiables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.SubjectIdFilter = "All";
            cache.ViewState.ViewSettings.EmployeeIdFilter = "All"; 
            cache.ViewState.ViewSettings.OrderStateIdFilter = "All";
            cache.ViewState.ViewSettings.PriorityIdFilter = "All";
            cache.ViewState.ViewSettings.IdFilter = "";
            cache.ViewState.ViewSettings.ReceiveDateFromFilter = "";
            cache.ViewState.ViewSettings.ReceiveDateToFilter = ""; 
            cache.ViewState.ViewSettings.AccomplishDateFromFilter = "";
            cache.ViewState.ViewSettings.AccomplishDateToFilter = "";
            cache.ViewState.ViewSettings.DeliveryDateFromFilter = "";
            cache.ViewState.ViewSettings.DeliveryDateToFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";

            // Ordering letiables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;
            cache.ViewState.ViewSettings.SecondOrderingPropertyName = "DeliveryDate";

            // Visibility letiables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Subject
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Contact
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // ReceiveDate
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // AccomplishDate
            cache.ViewState.ViewSettings.ColumnsVisibility[6] = true; // DeliveryDate
            cache.ViewState.ViewSettings.ColumnsVisibility[7] = true; // Desc
            cache.ViewState.ViewSettings.ColumnsVisibility[8] = true; // Employee
            cache.ViewState.ViewSettings.ColumnsVisibility[9] = true; // OrderState

            // set ViewState.ViewSettings.ExpandedColumnSpan
            ViewStateService.setExpandedColumnSpan(cache.ViewState);

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function getOrderStateId(orderId) {
            //console.log(orderId);
            //console.log(InfrService.isArrayAndNotEmpty(cache.Items.Orders));
            if (InfrService.isArrayAndNotEmpty(cache.Items.Orders) && (orderId != null)) {
                let matched = cache.Items.Orders.filter(function (item) { return item.Id === orderId; });
                if (InfrService.isArrayAndNotEmpty(matched)) {
                    return matched[0].StateId;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }

        function getViewState() {
            let LSViewState = ViewStateService.getViewState("OrdersIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

})();