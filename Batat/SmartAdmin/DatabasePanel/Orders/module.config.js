"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.orders', {
            url: '/Orders',
            data: {
                title: 'Orders'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Orders/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['OrdersService', function (OrdersService) {
                            return OrdersService.get$Items();
                        }],
                        MainItemsService: ['OrdersService', function (OrdersService) {
                            return OrdersService;
                        }],
                    }
                }
            }
        })

        .state('app.editorder', {
            url: '/Orders/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Orders/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['OrdersService', function (OrdersService) {
                            return OrdersService.get$Items();
                        }],
                        MainItemsService: ['OrdersService', function (OrdersService) {
                            return OrdersService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();