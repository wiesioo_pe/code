"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.parameters', {
            url: '/Parameters',
            data: {
                title: 'Parameters'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Parameters/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['ParametersService', function (ParametersService) {
                            return ParametersService.get$Items();
                        }],
                        MainItemsService: ['ParametersService', function (ParametersService) {
                            return ParametersService;
                        }],
                    }
                }
            }
        })

        .state('app.parameteredit', {
            url: '/Parameters/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Parameters/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['ParametersService', function (ParametersService) {
                            return ParametersService.get$Items();
                        }],
                        MainItemsService: ['ParametersService', function (ParametersService) {
                            return ParametersService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();