﻿"use strict";
console.log("Załadowałem ParametersService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("ParametersService", ParametersService);


    ParametersService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];
    
    function ParametersService($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var ParametersService = this;
        

        // SERVICE DATA

        var data = {
            ItemsNames: ['Parameters']
            }

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        }

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        }

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        }

        this.getMainItemsName = function () { return "Parameters"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.NameColumnWidth = "7em";
            ViewData.UnitColumnWidth = "7em";
            ViewData.DescColumnWidth = "6em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        }

        this.filterItems = function () {
            //console.log(cache.Items.Parameters);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Parameters) && (cache.ViewState.ViewSettings != null)) {

                //console.log("ParametersService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                var _UnitFilter = cache.ViewState.ViewSettings.UnitFilter;
                var _DescFilter = cache.ViewState.ViewSettings.DescFilter;

                // for each __item in Parameters
                for (var i = 0; i < cache.Items.Parameters.length; i++) {

                    var _Item = cache.Items.Parameters[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";


                    // Check Parameter name filters
                    var _doesParameterNameContain = false;

                    if (_NameFilter == "") {
                        _doesParameterNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesParameterNameContain = true;
                        }
                    }

                    // Check Parameter unit filters
                    var _doesParameterUnitContain = false;

                    if (_UnitFilter == "") {
                        _doesParameterUnitContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Unit.toLowerCase();
                        _LowerCaseFilter = _UnitFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesParameterUnitContain = true;
                        }
                    }

                    // Check Parameter unit filters
                    var _doesParameterDescContain = false;

                    if (_DescFilter == "") {
                        _doesParameterDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesParameterDescContain = true;
                        }
                    }



                    // Set _Item visibility
                    _Item.Visible = (_doesParameterNameContain && _doesParameterUnitContain && _doesParameterDescContain);

                }
            }
        }

        this.saveViewState = function () {
            console.log('ParametersService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("ParametersIndex", cache.ViewState);
        }

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        }

        this.clearViewState = function () {
            ViewStateService.clearViewState("ParametersIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        }

        this.onIndexDataDownloadCompleted = function (scope) {
            
        }

        this.getIndexHeaderDictionary = function () {
            
            var Dictionary = {};

            Dictionary.ItemsNameString = "Parametry";
            Dictionary.ActiveItemString = "Parametr nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowy parametr";
            Button.Href = function () {
                return "#/Parameters/Edit?Id=0";
            };
            Button.Click = function () {
                ParametersService.set$ActiveItemId(0);
            }
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Parameters/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Parameters/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        }

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Nazwa";
            Dictionary.ColumnsNames[1] = "Jednostka";
            Dictionary.ColumnsNames[2] = "Opis";
            return Dictionary;
        }

        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    ParametersService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return ParametersService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        }


        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return ParametersService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    ParametersService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        }



        // EDIT METHODS

        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Parameters, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        }


        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        }

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        }

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        }

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Parameters", cache.ActiveItem)
                .then(
                function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    ParametersService.set$ActiveItemId(response.Id);
                    return true;
                },
                function (error) {
                    throw error.data;
                });
        }

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                function (response) {
                    var SelectOptions = {};
                    return SelectOptions;
                }, function (error) { throw false; });
        }

        this.isEditDisabled = function () {
            return false;
        }



        // Private functions

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.UnitFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Unit
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Desc

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("ParametersIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

})();