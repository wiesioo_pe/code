﻿"use strict";
console.log("Załadowałem SubjectsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("SubjectsService", SubjectsService);


    SubjectsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];
    
    function SubjectsService($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var SubjectsService = this;
        

        // SERVICE DATA

        var data = {
            ItemsNames: ['Subjects', 'Contacts', 'Groups']
        };

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        };

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded."; });
        };

        this.getMainItemsName = function () { return "Subjects"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.NameColumnWidth = "7em";
            ViewData.ShortNameColumnWidth = "6em";
            ViewData.VATRegNumColumnWidth = "6em";
            ViewData.WWWColumnWidth = "15em";
            ViewData.AddressColumnWidth = "6em";
            ViewData.PhoneColumnWidth = "6em";
            ViewData.DescColumnWidth = "10em";
            ViewData.LogoColumnWidth = "3em";
            ViewData.ContactsColumnWidth = "3em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        };

        this.filterItems = function () {
            //console.log(cache.Items.Subjects);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Subjects) && cache.ViewState.ViewSettings.GroupIdFilter != null) {

                //console.log("SubjectsService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _SubjectTypeFilter = cache.ViewState.ViewSettings.SubjectTypeFilter;
                var _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                var _ShortNameFilter = cache.ViewState.ViewSettings.ShortNameFilter;
                var _VATRegNumFilter = cache.ViewState.ViewSettings.VATRegNumFilter;
                var _WWWFilter = cache.ViewState.ViewSettings.WWWFilter;
                var _AddressFilter = cache.ViewState.ViewSettings.AddressFilter;
                var _PhoneFilter = cache.ViewState.ViewSettings.PhoneFilter;
                var _DescFilter = cache.ViewState.ViewSettings.DescFilter;
                var _ContactsFilter = cache.ViewState.ViewSettings.ContactsFilter;

                // for each __item in Subjects
                for (var i = 0; i < cache.Items.Subjects.length; i++) {

                    var _Item = cache.Items.Subjects[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    // Check if in group
                    var _isInGroup = false;
                    if (_GroupIdFilter === "All") {
                        _isInGroup = true;
                    } else {
                        var _matchedSubjects = _Item.Groups.filter(function (__item) {
                            return __item.GroupId === _GroupIdFilter;
                        });
                        if (_matchedSubjects[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check subject type
                    var _isOfType = false;
                    if (_SubjectTypeFilter === "All") {
                        _isOfType = true;
                    } else {
                        if ((_SubjectTypeFilter === "Customers") && (_Item.isCustomer === true)) {
                            _isOfType = true;
                        } else if ((_SubjectTypeFilter === "Suppliers") && (_Item.isSupplier === true)) {
                            _isOfType = true;
                        } else if ((_SubjectTypeFilter === "Other") && (_Item.isCustomer === false) && (_Item.isSupplier === false)) {
                            _isOfType = true;
                        }
                    }

                    // Check Subject name filters
                    var _doesSubjectNameContain = false;

                    if (_NameFilter === "") {
                        _doesSubjectNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectNameContain = true;
                        }
                    }

                    // Check Subject short name filters
                    var _doesSubjectShortNameContain = false;

                    if (_ShortNameFilter === "") {
                        _doesSubjectShortNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.ShortName.toLowerCase();
                        _LowerCaseFilter = _ShortNameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectShortNameContain = true;
                        }
                    }

                    // Check Subject VATRegNum filters
                    var _doesSubjectVATRegNumContain = false;

                    if (_VATRegNumFilter === "") {
                        _doesSubjectVATRegNumContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.VATRegNum.toLowerCase();
                        _LowerCaseFilter = _VATRegNumFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectVATRegNumContain = true;
                        }
                    }

                    // Check Subject www filters
                    var _doesSubjectWWWContain = false;

                    if (_WWWFilter === "") {
                        _doesSubjectWWWContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.WWW.toLowerCase();
                        _LowerCaseFilter = _WWWFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectWWWContain = true;
                        }
                    }

                    // Check Subject address filters
                    var _doesSubjectAddressContain = false;

                    if (_AddressFilter === "") {
                        _doesSubjectAddressContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Address.toLowerCase();
                        _LowerCaseFilter = _AddressFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectAddressContain = true;
                        }
                    }

                    // Check Subject phone filters
                    var _doesSubjectPhoneContain = false;

                    if (_PhoneFilter == "") {
                        _doesSubjectPhoneContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Phone.toLowerCase();
                        _LowerCaseFilter = _PhoneFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectPhoneContain = true;
                        }
                    }

                    // Check Subject desc filters
                    var _doesSubjectDescContain = false;

                    if (_DescFilter == "") {
                        _doesSubjectDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesSubjectDescContain = true;
                        }
                    }

                    // Set _Item visibility
                    _Item.Visible = (_isInGroup && _isOfType && _doesSubjectNameContain && _doesSubjectShortNameContain && _doesSubjectVATRegNumContain && _doesSubjectWWWContain && _doesSubjectAddressContain && _doesSubjectPhoneContain && _doesSubjectDescContain);

                }
            }
        };

        this.saveViewState = function () {
            console.log('SubjectsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("SubjectsIndex", cache.ViewState);
        };

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        };

        this.clearViewState = function () {
            ViewStateService.clearViewState("SubjectsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        };

        this.onIndexDataDownloadCompleted = function (scope) {

        };

        this.getIndexHeaderDictionary = function () {

            var Dictionary = {};

            Dictionary.ItemsNameString = "Kontrahenci";
            Dictionary.ActiveItemString = "Kontrahent nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowego kontrahenta";
            Button.Href = function () {
                return "#/Subjects/Edit?Id=0";
            };
            Button.Click = function () {
                SubjectsService.set$ActiveItemId(0);
            };

            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Subjects/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Subjects/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Nazwa pełna";
            Dictionary.ColumnsNames[2] = "Nazwa skrócona";
            Dictionary.ColumnsNames[3] = "NIP";
            Dictionary.ColumnsNames[4] = "WWW";
            Dictionary.ColumnsNames[5] = "Adres";
            Dictionary.ColumnsNames[6] = "Telefony";
            Dictionary.ColumnsNames[7] = "Opis";
            Dictionary.ColumnsNames[8] = "Logo";
            Dictionary.ColumnsNames[9] = "Osoby do kontaktu";
            return Dictionary;
        };

        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId === 0) {
                    SubjectsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return SubjectsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };


        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return SubjectsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    SubjectsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        };



        // EDIT METHODS

        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Subjects, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        };


        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        };

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        };

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        };

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Subjects", cache.ActiveItem)
                .then(
                    function (response) {
                        cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                        SubjectsService.set$ActiveItemId(response.Id);
                        return true;
                    },
                    function (error) {
                        throw error.data;
                    });
        };

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                    function (response) {
                        var SelectOptions = {};
                        return SelectOptions;
                    }, function (error) { throw false; });
        };

        this.isEditDisabled = function () {
            return false;
        }



        // Private functions

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.SubjectTypeFilter = "All";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.ShortNameFilter = "";
            cache.ViewState.ViewSettings.VATRegNumFilter = "";
            cache.ViewState.ViewSettings.WWWFilter = "";
            cache.ViewState.ViewSettings.AddressFilter = "";
            cache.ViewState.ViewSettings.PhoneFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";
            cache.ViewState.ViewSettings.ContactsFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // ShortName
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // VATRegNum
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // WWW
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // Address
            cache.ViewState.ViewSettings.ColumnsVisibility[6] = true; // Phones
            cache.ViewState.ViewSettings.ColumnsVisibility[7] = true; // Desc
            cache.ViewState.ViewSettings.ColumnsVisibility[8] = true; // Logo
            cache.ViewState.ViewSettings.ColumnsVisibility[9] = true; // Contacts

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("SubjectsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

})();