"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.subjects', {
            url: '/Subjects',
            data: {
                title: 'Subjects'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Subjects/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['SubjectsService', function (SubjectsService) {
                            return SubjectsService.get$Items();
                        }],
                        MainItemsService: ['SubjectsService', function (SubjectsService) {
                            return SubjectsService;
                        }],
                    }
                }
            }
        })

        .state('app.subjectedit', {
            url: '/Subjects/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Subjects/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['SubjectsService', function (SubjectsService) {
                            return SubjectsService.get$Items();
                        }],
                        MainItemsService: ['SubjectsService', function (SubjectsService) {
                            return SubjectsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();