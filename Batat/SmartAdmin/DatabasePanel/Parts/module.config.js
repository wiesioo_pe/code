"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.parts', {
            url: '/Parts',
            data: {
                title: 'Parts'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Parts/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['PartsService', function (PartsService) {
                            return PartsService.get$Items();
                        }],
                        MainItemsService: ['PartsService', function (PartsService) {
                            return PartsService;
                        }],
                    }
                }
            }
        })

        .state('app.partedit', {
            url: '/Parts/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Parts/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['PartsService', function (PartsService) {
                            return PartsService.get$Items();
                        }],
                        MainItemsService: ['PartsService', function (PartsService) {
                            return PartsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();