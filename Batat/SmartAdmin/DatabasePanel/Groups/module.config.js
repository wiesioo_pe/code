"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.groups', {
            url: '/Groups',
            data: {
                title: 'Groups'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Groups/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['GroupsService', function (GroupsService) {
                            return GroupsService.get$Items();
                        }],
                        MainItemsService: ['GroupsService', function (GroupsService) {
                            return GroupsService;
                        }],
                    }
                }
            }
        })

        .state('app.groupedit', {
            url: '/Groups/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Groups/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['GroupsService', function (GroupsService) {
                            return GroupsService.get$Items();
                        }],
                        MainItemsService: ['GroupsService', function (GroupsService) {
                            return GroupsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();