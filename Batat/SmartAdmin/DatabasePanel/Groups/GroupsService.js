﻿"use strict";
console.log("Załadowałem GroupsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("GroupsService", GroupsService);


    GroupsService.$inject = ['$q', 'InfrService', 'ItemsService', 'ViewStateService'];
    
    function GroupsService($q, InfrService, ItemsService, ViewStateService) {

        var GroupsService = this;


        // SERVICE DATA

        var data = {
            ItemsNames: ['Groups']
        };

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        };

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

        // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();


        // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded."; });
        };

        this.getMainItemsName = function () { return "Groups"; };

        this.getViewData = function () {
            let ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.NameColumnWidth = "8em";
            ViewData.CodeColumnWidth = "5em";
            ViewData.DescColumnWidth = "7em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        };

        this.filterItems = function () {
            //console.log(cache.Items.Groups);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Groups) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("GroupsService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                let _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                let _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                let _CodeFilter = cache.ViewState.ViewSettings.CodeFilter;
                let _DescFilter = cache.ViewState.ViewSettings.DescFilter;


                // for each __item in Groups
                for (let i = 0; i < cache.Items.Groups.length; i++) {

                    let _Item = cache.Items.Groups[i];
                    let _LowerCaseProperty = "";
                    let _LowerCaseFilter = "";

                    // Check Group name filters
                    let _doesGroupNameContain = false;

                    if (_NameFilter === "") {
                        _doesGroupNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesGroupNameContain = true;
                        }
                    }

                    // Check Group code filters
                    let _doesGroupCodeContain = false;

                    if (_CodeFilter === "") {
                        _doesGroupCodeContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Code.toLowerCase();
                        _LowerCaseFilter = _CodeFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesGroupCodeContain = true;
                        }
                    }

                    // Check Group desc filters
                    let _doesGroupDescContain = false;

                    if (_DescFilter === "") {
                        _doesGroupDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesGroupDescContain = true;
                        }
                    }



                    // Set _Item visibility
                    _Item.Visible = (_doesGroupNameContain && _doesGroupCodeContain && _doesGroupDescContain);

                }
            }
        };

        this.saveViewState = function () {
            console.log('GroupsService: saving ViewState');
            let div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("GroupsIndex", cache.ViewState);
        };

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        };

        this.clearViewState = function () {
            ViewStateService.clearViewState("FixedCostsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        };

        this.onIndexDataDownloadCompleted = function (scope) {

        };

        this.getIndexHeaderDictionary = function () {

            let Dictionary = {};

            Dictionary.ItemsNameString = "Grupy";
            Dictionary.ActiveItemString = "Grupa nr: ";

            Dictionary.LeftButtonsGroups = [];
            let ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            let Button = {};
            Button.Title = "Dodaj nową grupę";
            Button.Href = function () {
                return "#/Groups/Edit?Id=0";
            };
            Button.Click = function () {
                GroupsService.set$ActiveItemId(0);
            };

            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];

            // Górna grupa
            ButtonsGroup = {};
            ButtonsGroup.Buttons = [];

            // Button Edit
            Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Groups/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);

            // Button Klonuj
            Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Groups/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            let Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Nazwa";
            Dictionary.ColumnsNames[1] = "Kod";
            Dictionary.ColumnsNames[2] = "Opis";
            return Dictionary;
        };

        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId === 0) {
                    GroupsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return GroupsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };


        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return GroupsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    GroupsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        };



        // EDIT METHODS

        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return $q.all(promises.Items).then(function () {
                let ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Groups, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        };


        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        };

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        };

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        };

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Groups", cache.ActiveItem)
                .then(
                    function (response) {
                        cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                        GroupsService.set$ActiveItemId(response.Id);
                        return true;
                    },
                    function (error) {
                        throw error.data;
                    });
        };

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                    function (response) {
                        return {};
                    }, function (error) { throw false; });
        };

        this.isEditDisabled = function () {
            return false;
        };


        // Private functions

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.CodeFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Code
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Desc

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            let LSViewState = ViewStateService.getViewState("GroupsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

    })();