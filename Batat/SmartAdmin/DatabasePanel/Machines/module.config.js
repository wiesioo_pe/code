"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.machines', {
            url: '/Machines',
            data: {
                title: 'Machines'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Machines/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['MachinesService', function (MachinesService) {
                            return MachinesService.get$Items();
                        }],
                        MainItemsService: ['MachinesService', function (MachinesService) {
                            return MachinesService;
                        }],
                    }
                }
            }
        })

        .state('app.machineedit', {
            url: '/Machines/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Machines/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['MachinesService', function (MachinesService) {
                            return MachinesService.get$Items();
                        }],
                        MainItemsService: ['MachinesService', function (MachinesService) {
                            return MachinesService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();