﻿"use strict";
console.log("Załadowałem MachinesService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("MachinesService", MachinesService);

    MachinesService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function MachinesService ($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var MachinesService = this;


    // SERVICE DATA

        var data = {
            ItemsNames: ['Machines', 'Groups', 'MachineTypes']
        }

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        }

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

    // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




    // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        }

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        }

        this.getMainItemsName = function () { return "Machines"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.CodeColumnWidth = "5em";
            ViewData.TypeColumnWidth = "7em";
            ViewData.ModelColumnWidth = "6em";
            ViewData.ManufacturerColumnWidth = "6em";
            ViewData.NotesColumnWidth = "15em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        }

        this.filterItems = function () {
            //console.log(cache.Items.Machines);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Machines) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("MachinesService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _TypeIdFilter = cache.ViewState.ViewSettings.MachineTypeIdFilter;
                var _CodeFilter = cache.ViewState.ViewSettings.CodeFilter;
                var _ModelFilter = cache.ViewState.ViewSettings.ModelFilter;
                var _ManufacturerFilter = cache.ViewState.ViewSettings.ManufacturerFilter;

                // for each __item in Machines
                for (var i = 0; i < cache.Items.Machines.length; i++) {

                    var _Item = cache.Items.Machines[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    // Check if in group
                    var _isInGroup = false;
                    if (_GroupIdFilter == "All") {
                        _isInGroup = true;
                    } else {
                        var _matchedMachines = _Item.Groups.filter(function (__item) {
                            return __item.GroupId == _GroupIdFilter;
                        });
                        if (_matchedMachines[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check if of Type
                    var _isOfType = false;
                    if (_TypeIdFilter == "All") {
                        _isOfType = true;
                    } else {
                        if (_Item.MachineTypeId == _TypeIdFilter) {
                            _isOfType = true;
                        }
                    }

                    // Check Machine code filters
                    var _doesMachineCodeContain = false;

                    if (_CodeFilter == "") {
                        _doesMachineCodeContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Code.toLowerCase();
                        _LowerCaseFilter = _CodeFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesMachineCodeContain = true;
                        }
                    }

                    // Check Machine model filters
                    var _doesMachineModelContain = false;

                    if (_ModelFilter == "") {
                        _doesMachineModelContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Model.toLowerCase();
                        _LowerCaseFilter = _ModelFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesMachineModelContain = true;
                        }
                    }

                    // Check Machine manufacturer filters
                    var _doesMachineManufacturerContain = false;

                    if (_ManufacturerFilter == "") {
                        _doesMachineManufacturerContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Manufacturer.toLowerCase();
                        _LowerCaseFilter = _ManufacturerFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesMachineManufacturerContain = true;
                        }
                    }


                    // Set _Item visibility
                    _Item.Visible = (_isInGroup && _isOfType && _doesMachineCodeContain && _doesMachineModelContain && _doesMachineManufacturerContain);

                }
            }
        }

        this.saveViewState = function () {
            console.log('MachinesService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("MachinesIndex", cache.ViewState);
        }

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        }

        this.clearViewState = function () {
            ViewStateService.clearViewState("MachinesIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        }

        this.onIndexDataDownloadCompleted = function (scope) {
            
        }

        this.getIndexHeaderDictionary = function () {

            var Dictionary = {};

            Dictionary.ItemsNameString = "Maszyny";
            Dictionary.ActiveItemString = "Maszyna nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nową maszynę";
            Button.Href = function () {
                return "#/Machines/Edit?Id=0";
            };
            Button.Click = function () {
                MachinesService.set$ActiveItemId(0);
            }
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Machines/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Machines/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Kod";
            Dictionary.ColumnsNames[2] = "Typ";
            Dictionary.ColumnsNames[3] = "Model";
            Dictionary.ColumnsNames[4] = "Producent";
            Dictionary.ColumnsNames[5] = "Uwagi";
            return Dictionary;
        };

            
        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    MachinesService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return MachinesService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };

            
        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return MachinesService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    MachinesService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        }


    // EDIT METHODS

            
        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Machines, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        }

            
        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        }

            
        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        }

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        }

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Machines", cache.ActiveItem)
                .then(
                function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    MachinesService.set$ActiveItemId(response.Id);
                    return true;
                },
                function (error) {
                    throw error.data;
                });
        }

        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
               .then(
               function (response) {
                   var SelectOptions = {};

                    //MachineType options
                    SelectOptions.MachineType = [];
                    for (var MachineType of cache.Items.MachineTypes) {
                        var Option = {};
                        Option.Name = MachineType.Code + ' - ' + MachineType.Name;
                        Option.Desc = MachineType.Desc;
                        Option.Value = MachineType.Id;
                        SelectOptions.MachineType.push(Option);
                    }

                    return SelectOptions;
               }, function (error) { throw false; });
        }

        this.isEditDisabled = function () {
            return false;
        }



    // PRIVATE FUNCTIONS

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.MachineTypeIdFilter = "All";
            cache.ViewState.ViewSettings.CodeFilter = "";
            cache.ViewState.ViewSettings.ModelFilter = "";
            cache.ViewState.ViewSettings.ManufacturerFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Code
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Type
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Model
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Manufacturer
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // Notes

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("MachinesIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }

})();