﻿"use strict";
console.log("Załadowałem OperationsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("OperationsService", OperationsService);


    OperationsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function OperationsService ($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var OperationsService = this;
        

    // SERVICE DATA

        var data = {
            ItemsNames: ['Operations', 'Parts', 'Groups', 'OperationTypes', 'Technologies', 'Tools', 'Materials']
            }

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        }

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

    // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




    // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        }

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        }

        this.getMainItemsName = function () { return "Operations"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.CodeColumnWidth = "5em";
            ViewData.PartColumnWidth = "7em";
            ViewData.TechnologyColumnWidth = "7em";
            ViewData.TypeColumnWidth = "6em";
            ViewData.NotesColumnWidth = "10em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        }

        this.filterItems = function () {
            //console.log('OperationsService');
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Operations) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {
                //console.log('Filtering');

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _TechnologyIdFilter = cache.ViewState.ViewSettings.TechnologyIdFilter;
                var _PartIdFilter = cache.ViewState.ViewSettings.PartIdFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _CodeFilter = cache.ViewState.ViewSettings.CodeFilter;
                var _TypeIdFilter = cache.ViewState.ViewSettings.OperationTypeIdFilter;

                // for each __item in Operations
                for (var i = 0; i < cache.Items.Operations.length; i++) {


                    var _Item = cache.Items.Operations[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    // Check if in group
                    var _isInGroup = false;
                    if (_GroupIdFilter == "All") {
                        _isInGroup = true;
                    } else {
                        var _matchedOperations = _Item.Groups.filter(function (__item) {
                            return __item.GroupId == _GroupIdFilter;
                        });
                        if (_matchedOperations[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    // Check if in technology
                    var _isInTechnology = false;
                    if (_TechnologyIdFilter == "All") {
                        _isInTechnology = true;
                    } else {
                        if (_Item.TechnologyId == _TechnologyIdFilter) {
                            _isInTechnology = true;
                        }
                    }

                    // Check if operation type
                    var _isOfOperationType = false;
                    if (_TypeIdFilter == "All") {
                        _isOfOperationType = true;
                    } else {
                        if (_Item.OperationTypeId == _TypeIdFilter) {
                            _isOfOperationType = true;
                        }
                    }

                    var _isForPart = false;
                    // Check if is for Part
                    if (_PartIdFilter == "All") {
                        _isForPart = true;
                    } else {
                        if (_Item.PartId == _PartIdFilter) {
                            _isForPart = true;
                        }
                    }

                    // Check operation code filters
                    var _doesOperationCodeContain = false;

                    if (_CodeFilter == "") {
                        _doesOperationCodeContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Code.toLowerCase();
                        _LowerCaseFilter = _CodeFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesOperationCodeContain = true;
                        }
                    }


                    // Set _Item visibility
                    _Item.Visible = (_isInTechnology && _isInGroup && _isOfOperationType && _isForPart && _doesOperationCodeContain);

                }
            }
        }

        this.saveViewState = function () {
            console.log('OperationsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("OperationsIndex", cache.ViewState);
        }

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        }

        this.clearViewState = function () {
            ViewStateService.clearViewState("OperationsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        }

        this.onIndexDataDownloadCompleted = function (scope) {
            
        }

        this.getIndexHeaderDictionary = function () {
            
            var Dictionary = {};

            Dictionary.ItemsNameString = "Operacje";
            Dictionary.ActiveItemString = "Operacja nr: ";

            Dictionary.LeftButtonsGroups = [];
            

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Operations/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        }

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Kod";
            Dictionary.ColumnsNames[2] = "Detal";
            Dictionary.ColumnsNames[3] = "Technologia";
            Dictionary.ColumnsNames[4] = "Typ";
            Dictionary.ColumnsNames[5] = "Uwagi";
            return Dictionary;
        }

            
        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                if ((ItemId != null) && (ItemId > 0)) {
                    cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                    return OperationsService.refresh$ActiveItem();
                } else {
                    return $q.reject(false);
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        }



    // EDIT METHODS

            
        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Operations, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        }

            
        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        }

            
        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        }

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        }

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Operations", cache.ActiveItem)
                .then(
                function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    OperationsService.set$ActiveItemId(response.Id);
                    return true;
                },
                function (error) {
                    throw error.data;
                });
        }
        
        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
               .then(
               function (response) {
                   var SelectOptions = {};

                   //OperationType options
                   SelectOptions.OperationType = [];
                   for (var OperationType of cache.Items.OperationTypes) {
                       var Option = {};
                       Option.Name = OperationType.Code + ' - ' + OperationType.Name;
                       Option.Desc = OperationType.Desc;
                       Option.Value = OperationType.OperationTypeId;
                       SelectOptions.OperationType.push(Option);
                   }

                   return SelectOptions;
               }, function (error) { throw false; });
        }

        this.isEditDisabled = function () {
            return false;
        }



    // PRIVATE FUNCTIONS

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.TechnologyIdFilter = "All";
            cache.ViewState.ViewSettings.PartIdFilter = "All";
            cache.ViewState.ViewSettings.OperationTypeIdFilter = "All";
            cache.ViewState.ViewSettings.CodeFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Code 
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Part
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Technology
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Type
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // Notes

            ViewStateService.setExpandedColumnSpan(cache.ViewState);

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("OperationsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }
    }
})();