"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.operations', {
            url: '/Operations',
            data: {
                title: 'Operations'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Operations/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['OperationsService', function (OperationsService) {
                            return OperationsService.get$Items();
                        }],
                        MainItemsService: ['OperationsService', function (OperationsService) {
                            return OperationsService;
                        }],
                    }
                }
            }
        })

        .state('app.operationedit', {
            url: '/Operations/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Operations/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['OperationsService', function (OperationsService) {
                            return OperationsService.get$Items();
                        }],
                        MainItemsService: ['OperationsService', function (OperationsService) {
                            return OperationsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();