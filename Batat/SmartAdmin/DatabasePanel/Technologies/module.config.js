"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.technologies', {
            url: '/Technologies',
            data: {
                title: 'Technologies'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Technologies/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['TechnologiesService', function (TechnologiesService) {
                            return TechnologiesService.get$Items();
                        }],
                        MainItemsService: ['TechnologiesService', function (TechnologiesService) {
                            return TechnologiesService;
                        }],
                    }
                }
            }
        })

        .state('app.technologyedit', {
            url: '/Technologies/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Technologies/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['TechnologiesService', function (TechnologiesService) {
                            return TechnologiesService.get$Items();
                        }],
                        MainItemsService: ['TechnologiesService', function (TechnologiesService) {
                            return TechnologiesService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();