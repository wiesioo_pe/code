﻿"use strict";
console.log("Załadowałem TechnologiesService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
    // SERVCICE
    angular.module("app").service("TechnologiesService", TechnologiesService);

    TechnologiesService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function TechnologiesService ($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var TechnologiesService = this;
        

    // SERVICE DATA

        var data = {
            ItemsNames: ['Technologies', 'Operations', 'Groups', 'OperationTypes', 'Parts']
        };

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        };

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

    // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




    // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        };

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded."; });
        };

        this.getMainItemsName = function () { return "Orders"; };

        this.getViewData = function () {
            let ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.CodeColumnWidth = "5em";
            ViewData.NameColumnWidth = "7em";
            ViewData.PartColumnWidth = "6em";
            ViewData.NotesColumnWidth = "6em";
            ViewData.IconColumnWidth = "6em";

            return ViewData;
        };

        this.filterItems = function () {
            //console.log(cache.Items.Technologies);
            //console.log(cache.ViewState);
            if (InfrService.isArrayAndNotEmpty(cache.Items.Technologies) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                //console.log("OrdersService: Filtering items.");
                //console.log(cache.ViewState.SessionViewSettings);

                let _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                let _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                let _PartIdFilter = cache.ViewState.ViewSettings.PartIdFilter;
                let _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                let _CodeFilter = cache.ViewState.ViewSettings.CodeFilter;

                // for each item in Technologies
                for (let i = 0; i < cache.Items.Technologies.length; i++) {

                    let _Item = cache.Items.Technologies[i];
                    let _LowerCaseProperty = "";
                    let _LowerCaseFilter = "";

                    let _isInGroup = false;
                    // Check if in group
                    if (_GroupIdFilter === "All") {
                        _isInGroup = true;
                    } else {
                        let _matchedTechnologies = _Item.Groups.filter(function (__item) {
                            return __item.GroupId === _GroupIdFilter;
                        });
                        if (_matchedTechnologies[0] != null) {
                            _isInGroup = true;
                        }
                    }

                    let _isForPart = false;
                    // Check if is for Part
                    if (_PartIdFilter === "All") {
                        _isForPart = true;
                    } else {
                        if (_Item.PartId === _PartIdFilter) {
                            _isForPart = true;
                        }
                    }


                    // Check product name filters
                    let _doesTechnologyNameContain = false;

                    if (_NameFilter === "") {
                        _doesTechnologyNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesTechnologyNameContain = true;
                        }
                    }

                    // Check product code filters
                    let _doesTechnologyCodeContain = false;

                    if (_CodeFilter === "") {
                        _doesTechnologyCodeContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Code.toLowerCase();
                        _LowerCaseFilter = _CodeFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesTechnologyCodeContain = true;
                        }
                    }



                    // Set Technology visibility
                    _Item.Visible = _isInGroup && _isForPart && _doesTechnologyNameContain && _doesTechnologyCodeContain;

                }
            }
        };

        this.saveViewState = function () {
            console.log('TechnologiesService: saving ViewState');
            let div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("TechnologiesIndex", cache.ViewState);
        };

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        };

        this.clearViewState = function () {
            ViewStateService.clearViewState("TechnologiesIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        };

        this.onIndexDataDownloadCompleted = function (scope) {
            // add watch for ColumnsVisibility changes to recalculate colspan for expanded item
            scope.$watch(function () { return cache.ViewState.ViewSettings.ColumnsVisibility; },
                function () { ViewStateService.setExpandedColumnSpan(cache.ViewState); }, true);
        };

        this.getIndexHeaderDictionary = function () {

            let Dictionary = {};

            Dictionary.ItemsNameString = "Technologie";
            Dictionary.ActiveItemString = "Technologia nr: ";

            Dictionary.LeftButtonsGroups = [];
            let ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            let Button = {};
            Button.Title = "Dodaj nową technologię";
            Button.Href = function () {
                return "#/Technologies/Edit?Id=0";
            };
            Button.Click = function () {
                TechnologiesService.set$ActiveItemId(0);
            };

            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];

            // Górna grupa
            ButtonsGroup = {};
            ButtonsGroup.Buttons = [];

            // Button Edit
            Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Technologies/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);

            // Button Klonuj
            Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Technologies/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        };

        this.getAllColumnsNames = function () {
            let Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Kod";
            Dictionary.ColumnsNames[2] = "Nazwa";
            Dictionary.ColumnsNames[3] = "Detal";
            Dictionary.ColumnsNames[4] = "Uwagi";
            return Dictionary;
        };

            
        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    TechnologiesService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return TechnologiesService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        };

            
        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return TechnologiesService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    cache.ActiveItem.Code = null;
                    TechnologiesService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        };



    // EDIT METHODS

            
        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return $q.all(promises.Items).then(function () {
                let ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Technologies, "Id");
                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        };

            
        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        };

        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        };

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        };

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Technologies", cache.ActiveItem)
                .then(
                    function (response) {
                        cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                        TechnologiesService.set$ActiveItemId(response.Id);
                        ItemsService.update.Items("Operations").then(function (response) { $timeout(function () { }, 0); }, function (error) { });
                        return true;
                    },
                    function (error) {
                        throw error.data;
                    });
        };
        
        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
                .then(
                    function (response) {
                        let SelectOptions = {};

                        //Part options
                        SelectOptions.Part = [];
                        for (let Part of cache.Items.Parts) {
                            let Option = {};
                            Option.Name = Part.Code + ' - ' + Part.Name;
                            Option.Desc = Part.Desc;
                            Option.Value = Part.Id;
                            SelectOptions.Part.push(Option);
                        }

                        return SelectOptions;
                    }, function (error) { throw false; });
        };

        this.isEditDisabled = function () {
            return false;
        };



    // PRIVATE FUNCTIONS

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {

            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering Variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.PartIdFilter = "All";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.CodeFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Code
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Part
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Notes

            ViewStateService.setExpandedColumnSpan(cache.ViewState);

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            let LSViewState = ViewStateService.getViewState("TechnologiesIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }

    }
})();