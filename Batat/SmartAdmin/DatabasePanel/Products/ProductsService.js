﻿"use strict";
console.log("Załadowałem ProductsService.js");
/// <reference path="~/app/scripts/angular.js" />

(function () {
        // SERVCICE
    angular.module("app").service("ProductsService", ProductsService);


    ProductsService.$inject = ['$timeout', '$interval', '$q', '$filter'
        , 'InfrService', 'ItemsService', 'ViewStateService', 'MessageProvider'];

    function ProductsService($timeout, $interval, $q, $filter
        , InfrService, ItemsService, ViewStateService, MessageProvider) {

        var ProductsService = this;
            

    // SERVICE DATA

        var data = {
            ItemsNames: ['Products', 'Parts', 'Parameters', 'Groups']
            }

        var cache = {
            Items: {},
            ViewState: {},
            ActiveItem: {}
        };

        var promises = {
            Items: {}
        }

        var PrivateFunctions = {
            downloadItems: downloadItems,
            initializeViewState: initializeViewState,
            getViewState: getViewState
        };

    // INIT

        PrivateFunctions.downloadItems();
        PrivateFunctions.getViewState();




    // INDEX METHODS

        this.getItemsNames = function () {
            return data.ItemsNames;
        }

        this.get$Items = function () {
            return $q.all(promises.Items).then(function (response) { return cache.Items; }, function (error) { throw "Items couldn't be downloaded." });
        }

        this.getMainItemsName = function () { return "Products"; };

        this.getViewData = function () {
            var ViewData = {};
            // ViewData
            ViewData.IdColumnWidth = "3em";
            ViewData.GroupsColumnWidth = "8em";
            ViewData.CodeColumnWidth = "5em";
            ViewData.NameColumnWidth = "7em";
            ViewData.ParametersColumnWidth = "15em";
            ViewData.DescColumnWidth = "15em";
            ViewData.NetPriceColumnWidth = "6em";

            return ViewData;
        }

        this.filterItems = function () {
            if (InfrService.isArrayAndNotEmpty(cache.Items.Products) && (cache.ViewState.ViewSettings.GroupIdFilter != null)) {

                var _UniversalFilter = cache.ViewState.ViewSettings.UniversalFilter;
                var _GroupIdFilter = cache.ViewState.ViewSettings.GroupIdFilter;
                var _ParameterIdFilter = cache.ViewState.ViewSettings.ParameterIdFilter;
                var _ParameterFromFilter = cache.ViewState.ViewSettings.ParameterFromFilter;
                var _ParameterToFilter = cache.ViewState.ViewSettings.ParameterToFilter;
                var _NameFilter = cache.ViewState.ViewSettings.NameFilter;
                var _CodeFilter = cache.ViewState.ViewSettings.CodeFilter;
                var _DescFilter = cache.ViewState.ViewSettings.DescFilter;


                // for each item in Products
                for (var i = 0; i < cache.Items.Products.length; i++) {


                    var _Item = cache.Items.Products[i];
                    var _LowerCaseProperty = "";
                    var _LowerCaseFilter = "";

                    var _isInGroup = false;

                    // Check group filters
                    if (_GroupIdFilter == "All") {
                        _isInGroup = true;
                    } else {
                        var _MatchedGroups = _Item.Groups.filter(function (__item) {
                            return __item.GroupId == _GroupIdFilter;
                        });
                        if (_MatchedGroups[0] != null) {
                            _isInGroup = true;
                        }
                    }


                    // Check Parameter filters
                    var _isParameterBetween = false, _isParameterFrom = false, _isParameterTo = false;

                    if (_ParameterIdFilter == "All") {
                        _isParameterFrom = true;
                        _isParameterTo = true;
                    } else {
                        var _MatchedParameters = _Item.Parameters.filter(function (__item) {
                            return __item.ParameterId == _ParameterIdFilter;
                        });
                        if (_MatchedParameters[0] != null) {
                            if (_ParameterFromFilter == "") {
                                _isParameterFrom = true;
                            }
                            else {
                                // If Parameter Value and _ParameterFromFilter Input are numbers
                                if (!(isNaN(_MatchedParameters[0].Value)) && !(isNaN(_ParameterFromFilter))) {
                                    if (Number(_MatchedParameters[0].Value) >= Number(_ParameterFromFilter)) {
                                        _isParameterFrom = true;
                                    }
                                }
                                    // but if they're strings
                                else {
                                    if (_MatchedParameters[0].Value >= _ParameterFromFilter) {
                                        _isParameterFrom = true;
                                    }
                                }
                            }
                            if (_ParameterToFilter == "") {
                                _isParameterTo = true;
                            }
                            else {

                                // If Parameter Value and _ParameterToFilter Input are numbers
                                if (!(isNaN(_MatchedParameters[0].Value)) && !(isNaN(_ParameterToFilter))) {
                                    if (Number(_MatchedParameters[0].Value) <= Number(_ParameterToFilter)) {
                                        _isParameterTo = true;
                                    }
                                }
                                    // but if they're strings
                                else {
                                    if (_MatchedParameters[0].Value <= _ParameterToFilter) {
                                        _isParameterTo = true;
                                    }
                                }
                            }
                        }
                    }

                    _isParameterBetween = _isParameterFrom && _isParameterTo;


                    // Check product name filters
                    var _doesItemNameContain = false;

                    if (_NameFilter == "") {
                        _doesItemNameContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Name.toLowerCase();
                        _LowerCaseFilter = _NameFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesItemNameContain = true;
                        }
                    }

                    // Check product code filters
                    var _doesItemCodeContain = false;

                    if (_CodeFilter == "") {
                        _doesItemCodeContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Code.toLowerCase();
                        _LowerCaseFilter = _CodeFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesItemCodeContain = true;
                        }
                    }

                    // Check desc filters
                    var _doesItemDescContain = false;

                    if (_DescFilter == "") {
                        _doesItemDescContain = true;
                    }
                    else {
                        _LowerCaseProperty = _Item.Desc.toLowerCase();
                        _LowerCaseFilter = _DescFilter.toLowerCase();
                        if (_LowerCaseProperty.indexOf(_LowerCaseFilter) !== -1) {
                            _doesItemDescContain = true;
                        }
                    }


                    // Set _Item visibility
                    _Item.Visible = (_isInGroup && _isParameterBetween && _doesItemNameContain && _doesItemCodeContain && _doesItemDescContain);

                }
            }
        }

        this.saveViewState = function () {
            console.log('ProductsService: saving ViewState');
            var div = document.getElementsByClassName('index-table')[0];
            cache.ViewState.SessionViewSettings.TableOffset = div.scrollTop;
            ViewStateService.saveViewState("ProductsIndex", cache.ViewState);
        }

        this.getViewState = function () {
            return PrivateFunctions.getViewState();
        }

        this.clearViewState = function () {
            ViewStateService.clearViewState("ProductsIndex", cache.ViewState);
            PrivateFunctions.initializeViewState();
        }

        this.onIndexDataDownloadCompleted = function (scope) {
            // add watch for ColumnsVisibility changes to recalculate colspan for expanded item
            scope.$watch(function () { return cache.ViewState.ViewSettings.ColumnsVisibility },
                function () { ViewStateService.setExpandedColumnSpan(cache.ViewState); }, true);

        }

        this.getIndexHeaderDictionary = function () {

            var Dictionary = {};

            Dictionary.ItemsNameString = "Produkty";
            Dictionary.ActiveItemString = "Produkt nr: ";

            Dictionary.LeftButtonsGroups = [];
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            var Button = {};
            Button.Title = "Dodaj nowy produkt";
            Button.Href = function () {
                return "#/Products/Edit?Id=0";
            };
            Button.Click = function () {
                ProductsService.set$ActiveItemId(0);
            }
            Button.ImageClass = "batat-icon-add-new-item-gray";
            Button.IsVisible = function () {
                return true;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.LeftButtonsGroups.push(ButtonsGroup);

            Dictionary.RightButtonsGroups = [];
            // Górna grupa
            var ButtonsGroup = {};
            ButtonsGroup.Buttons = [];
            // Button Edit
            var Button = {};
            Button.Title = "Edytuj";
            Button.Href = function () {
                return "#/Products/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId;
            };
            Button.ImageClass = "batat-icon-edit-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            // Button Klonuj
            var Button = {};
            Button.Title = "Klonuj";
            Button.Href = function () {
                return "#/Products/Edit?Id=" + cache.ViewState.SessionViewSettings.ActiveItemId + "&Clone=true";
            };
            Button.ImageClass = "batat-icon-clone-item";
            Button.IsVisible = function () {
                return cache.ViewState.SessionViewSettings.ActiveItemId > 0;
            };
            ButtonsGroup.Buttons.push(Button);
            Dictionary.RightButtonsGroups.push(ButtonsGroup);

            return Dictionary;
        }

        this.getAllColumnsNames = function () {
            var Dictionary = {};
            Dictionary.ColumnsNames = [];
            Dictionary.ColumnsNames[0] = "Grupy";
            Dictionary.ColumnsNames[1] = "Kod";
            Dictionary.ColumnsNames[2] = "Nazwa";
            Dictionary.ColumnsNames[3] = "Parametry";
            Dictionary.ColumnsNames[4] = "Opis";
            Dictionary.ColumnsNames[5] = "Cena netto";
            return Dictionary;
        }

            
        this.set$ActiveItemId = function (ItemId) {
            if (cache.ViewState.SessionViewSettings.doChangeActiveItem === true) {
                cache.ViewState.SessionViewSettings.ActiveItemId = ItemId;
                if (ItemId == null) {
                    return $q.resolve(false);
                } else if (ItemId == 0) {
                    ProductsService.resetActiveItem();
                    return $q.resolve(true);
                } else {
                    return ProductsService.refresh$ActiveItem();
                }
            } else {
                cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                return $q.reject(false);
            }
        }

            
        this.set$ActiveItemIdAsClone = function (ItemId) {
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            return ProductsService.set$ActiveItemId(ItemId)
                .then(function () {
                    cache.ActiveItem.Id = null;
                    ProductsService.set$ActiveItemId(null);
                    cache.ActiveItem.Changes = [];
                    return true;
                }, function (error) { throw false; });
        }


    // EDIT METHODS

            
        this.refresh$ActiveItem = function () {
            // ($q.all(promises.Items)) --- when all Items are downloaded
            // .then --- update cahce.ActiveItem with new properties
            // return --- return promise of refreshing
            return ($q.all(promises.Items)).then(function () {
                var ItemCopy = ItemsService.getItemCopy(cache.ViewState.SessionViewSettings.ActiveItemId, cache.Items.Products, "Id");

                InfrService.copyObjectProperties(cache.ActiveItem, ItemCopy);
                return true;
            }, function (error) { throw false; });
        }

            
        this.get$ActiveItem = function () {
            return $q.resolve(cache.ActiveItem);
        }

            
        this.resetActiveItem = function () {
            ItemsService.resetItem(cache.ActiveItem);
        }

        this.getActiveItemId = function () {
            return cache.ViewState.SessionViewSettings.ActiveItemId;
        }

        
        this.get$EditSelectOptions = function () {
            return $q.all(promises.Items)
               .then(
               function (response) {
                    var SelectOptions = {};
                    return SelectOptions;
               }, function (error) { throw false; });
        }

        this.save$ActiveItem = function () {
            return ItemsService.save.Item("Products", cache.ActiveItem)
                .then(
                function (response) {
                    cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
                    ProductsService.set$ActiveItemId(response.Id);
                    return true;
                },
                function (error) {
                    throw error.data;
                });
        }

        this.isEditDisabled = function () {
            return false;
        }


    // PRIVATE FUNCTIONS

        function downloadItems() {
            return ItemsService.downloadItems(data.ItemsNames, promises, cache);
        }

        function initializeViewState() {
            
            cache.ViewState.ViewSettings = {};
            cache.ViewState.SessionViewSettings = {};
            cache.ViewState.TemporaryViewSettings = {};

            // Filtering variables
            cache.ViewState.ViewSettings.UniversalFilter = "";
            cache.ViewState.ViewSettings.GroupIdFilter = "All";
            cache.ViewState.ViewSettings.ParameterIdFilter = "All";
            cache.ViewState.ViewSettings.ParameterFromFilter = "";
            cache.ViewState.ViewSettings.ParameterToFilter = "";
            cache.ViewState.ViewSettings.NameFilter = "";
            cache.ViewState.ViewSettings.CodeFilter = "";
            cache.ViewState.ViewSettings.DescFilter = "";

            // Ordering variables
            cache.ViewState.ViewSettings.OrderedBy = "Id";
            cache.ViewState.ViewSettings.isOrderedAscending = true;

            // Visibility Variables
            cache.ViewState.ViewSettings.ColumnsVisibility = [];
            cache.ViewState.ViewSettings.ColumnsVisibility[0] = true; // Groups
            cache.ViewState.ViewSettings.ColumnsVisibility[1] = true; // Code
            cache.ViewState.ViewSettings.ColumnsVisibility[2] = true; // Name
            cache.ViewState.ViewSettings.ColumnsVisibility[3] = true; // Parameters
            cache.ViewState.ViewSettings.ColumnsVisibility[4] = true; // Desc
            cache.ViewState.ViewSettings.ColumnsVisibility[5] = true; // NetPrice

            ViewStateService.setExpandedColumnSpan(cache.ViewState);

            // Active Item
            cache.ViewState.SessionViewSettings.ActiveItemId = 0;
            cache.ViewState.SessionViewSettings.TableOffset = 0;
            cache.ViewState.SessionViewSettings.doChangeActiveItem = true;
            cache.ViewState.SessionViewSettings.ItemsExpanded = [];

            cache.ViewState.isInitialized = true;
        }

        function getViewState() {
            var LSViewState = ViewStateService.getViewState("ProductsIndex");
            if (LSViewState != null) {
                InfrService.copyObjectProperties(cache.ViewState, LSViewState);
            } else {
                PrivateFunctions.initializeViewState();
            }
            return cache.ViewState;
        }

        function ChangeDefaultDiscount() {
            if (InfrService.isArrayAndNotEmpty(cache.Items.Subjects) && (cache.ActiveItem != null) && InfrService.isNumberAndNonNegative(cache.ActiveItem.SubjectId)) {
                var MatchedSubjects = $.grep(cache.Items.Subjects, function (e) { return e.SubjectId == cache.ActiveItem.SubjectId });
                if (InfrService.isArrayAndNotEmpty(MatchedSubjects)) {
                    cache.ActiveItem.DefaultDiscount = MatchedSubjects[0].Discount;
                } else {
                    cache.ActiveItem.DefaultDiscount = 0;
                }
            }
        }
    }
})();