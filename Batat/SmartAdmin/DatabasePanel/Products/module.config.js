"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.products', {
            url: '/Products',
            data: {
                title: 'Products'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Products/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['ProductsService', function (ProductsService) {
                            return ProductsService.get$Items();
                        }],
                        MainItemsService: ['ProductsService', function (ProductsService) {
                            return ProductsService;
                        }],
                    }
                }
            }
        })

        .state('app.editproduct', {
            url: '/Products/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Products/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['ProductsService', function (ProductsService) {
                            return ProductsService.get$Items();
                        }],
                        MainItemsService: ['ProductsService', function (ProductsService) {
                            return ProductsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();