"use strict";

(function () {
    angular.module('app').config(['$stateProvider', function ($stateProvider) {

        $stateProvider

        .state('app.tools', {
            url: '/Tools',
            data: {
                title: 'Tools'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Tools/Index.html?' + Math.random(),
                    controller: 'IndexViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['ToolsService', function (ToolsService) {
                            return ToolsService.get$Items();
                        }],
                        MainItemsService: ['ToolsService', function (ToolsService) {
                            return ToolsService;
                        }],
                    }
                }
            }
        })

        .state('app.tooledit', {
            url: '/Tools/Edit',
            data: {
                title: 'Edit'
            },
            views: {
                "content@app": {
                    templateUrl: 'DatabasePanel/Tools/Edit.html?' + Math.random(),
                    controller: 'EditViewController',
                    controllerAs: '$ctrl',
                    resolve: {
                        Items: ['ToolsService', function (ToolsService) {
                            return ToolsService.get$Items();
                        }],
                        MainItemsService: ['ToolsService', function (ToolsService) {
                            return ToolsService;
                        }],
                    }
                }
            }
        })

        



    }]);


})();