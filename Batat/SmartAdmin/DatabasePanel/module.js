"use strict";


angular.module('app')

.config(function ($urlRouterProvider, $stateProvider) {

    $stateProvider

        .state('app', {
            abstract: true,
            views: {
                root: {
                    templateUrl: 'app/layout/DatabasePanel.layout.tpl.html',
                    controller: 'DatabasePanelRootController'
                }
            }
        });

    $urlRouterProvider.otherwise('/Orders');

})

