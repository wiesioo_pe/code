﻿"use strict";
console.log("Załadowałem batat httpFactories.js");
/// <reference path="~/app/scripts/angular.js" />

var baseUrl = 'http://batat.webserwer.pl';
baseUrl = "";

angular.module("batat").factory('OrdersEntry', function ($resource) {
    return $resource(baseUrl+'/api/OrdersApi?:query', { query: "@query"}, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('OrderInstancesEntry', function ($resource) {
    return $resource(baseUrl+'/api/OrderInstancesApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('OrderStatesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaOrderStates?:query', { query: "@query" }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});


angular.module("batat").factory('ProductsEntry', function ($resource) {
    return $resource(baseUrl+'/api/ProductsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('PartsEntry', function ($resource) {
    return $resource(baseUrl+'/api/PartsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('ParametersEntry', function ($resource) {
    return $resource(baseUrl+'/api/ParametersApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});


angular.module("batat").factory('GroupsEntry', function ($resource) {
    return $resource(baseUrl + '/api/GroupsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('TechnologiesEntry', function ($resource) {
    return $resource(baseUrl+'/api/TechnologiesApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('OperationsEntry', function ($resource) {
    return $resource(baseUrl+'/api/OperationsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});



angular.module("batat").factory('OperationTypesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaOperationTypes?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('SubjectsEntry', function ($resource) {
    return $resource(baseUrl+'/api/SubjectsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('ContactsEntry', function ($resource) {
    return $resource(baseUrl+'/api/ContactsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('EmployeesEntry', function ($resource) {
    return $resource(baseUrl+'/Account/Employees?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('ViewSettingsEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaViewSettings?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" },
            isArray: false,
            async: false
        },
        postSettings: {
            method: 'POST',
            params: { query: "@query" },
            isArray: false
        }
    });
});

angular.module("batat").factory('OperatorsEntry', function ($resource) {
    return $resource(baseUrl+'/api/OperatorsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('MachinesEntry', function ($resource) {
    return $resource(baseUrl+'/api/MachinesApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('ToolsEntry', function ($resource) {
    return $resource(baseUrl+'/api/ToolsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('MaterialsEntry', function ($resource) {
    return $resource(baseUrl+'/api/MaterialsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('MachineTypesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaMachineTypes?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('ToolTypesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaToolTypes?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('MaterialTypesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaMaterialTypes?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('FixedCostsEntry', function ($resource) {
    return $resource(baseUrl+'/api/FixedCostsApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('FixedCostCategoriesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaFixedCostCategories?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('FixedCostTypesEntry', function ($resource) {
    return $resource(baseUrl+'/Infrastructure/spaFixedCostTypes?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        }
    });
}); 

angular.module("batat").factory('TechnologyPickEntry', function ($resource) {
    return $resource(baseUrl+'/api/TechnologyPickApi?:query', { query: "@query" }, {
        search: {
            method: 'GET',
            params: { query: "@query" }
        },
        saveAndGetChanged: {
            method: 'POST',
            isArray: true,
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('BoxesEntry', function ($resource) {
    return $resource(baseUrl +'/api/BoxesApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            isArray: true,
            params: { query: "@query" }
        }
    });
});

angular.module("batat").factory('TrolleysEntry', function ($resource) {
    return $resource(baseUrl + '/api/TrolleysApi?:query', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            isArray: true,
            params: { query: "@query" }
        }
    });
});


angular.module("batat").factory('CurrentUserEntry', function ($resource) {
    return $resource(baseUrl+'/Account/CurrentUser?:query', { query: "@query" }, {
        get$$One: {
            method: 'GET',
            params: { query: "@query" },
            isArray: false
        }
    });
});


angular.module("batat").factory('TrolleyEventsApiEntry', function ($resource) {
    return $resource(baseUrl + '/api/TrolleyEventsApi?:query', { query: "@query" }, {
        report$$event: {
            method: 'POST',
            params: { query: "@query" },
            isArray: false
        }
    });
});


angular.module("batat").factory('NewestItemsEntry', function ($resource) {
    return $resource(baseUrl + '/Infrastructure/GetNewestItems?:query', { query: "@query" }, {
        get: {
            method: 'POST',
            params: { query: "@query" },
            isArray: false
        }
    });
});

angular.module("batat").factory('MachineMonitorLiteState', function ($resource) {
    return $resource('http://test.batat.webserwer.pl/api/MachineMonitorLite/State/Last', {
        query: "@query"
    }, {
        search: {
            method: 'GET',
            params: { query: "@query" },
            headers: { 'Cache-Control': 'no-cache' }
        }
    });
});