﻿"use strict";
console.log("Załadowałem InfrService.js");
/// <reference path="~/app/scripts/angular.js" />

angular.module("batat").service("InfrService",
    ['$filter', '$timeout',
    function ($filter, $timeout) {

        var InfrService = this;


        this.isObject = function (obj) {
            return ((obj != null) && (typeof obj === 'object'));
        }

        this.isString = function (str) {
            return ((str != null) && (typeof str === 'string'));
        }

        this.isStringAndNumber = function (num) {
            return (InfrService.isString(num) && (!isNaN(num)));
        }

        this.isNumber = function (num) {
            return ((num != null) && (typeof num === 'number'));
        }

        this.isStringAndInt = function (num) {
            return (InfrService.isStringAndNumber(num) && (parseInt(num).toString() != "NaN"));
        }

        this.isInt = function (num) {
            return (InfrService.isNumber(num) && (parseInt(num).toString() != "NaN"));
        }

        this.isNumberNullOrZero = function (num) {
            return ((num == null) || (num == 0));
        }

        this.isStringANumber = function (str) {
            return ((str != null) && !isNaN(str));
        }

        this.isNumberAndNonNegative = function (num) {
            if (InfrService.isNumber(num)) {
                if (num >= 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        this.isBoolean = function (obj) {
            return (typeof obj === 'boolean');
        }

        this.isArrayAndNotEmpty = function (Array) {
            return ((Array != null) && (Object.prototype.toString.call(Array) === '[object Array]') 
                && (Array.length > 0));
        }

        this.isArray = function (Array) {
            return ((Array != null) && (Object.prototype.toString.call(Array) === '[object Array]'));
        }

        this.isFunction = function (obj) {
            var getType = {};
            return ((obj != null) && (getType.toString.call(obj) === '[object Function]'))
        }

        this.isStringAndNotEmpty = function (str) {
            return ((str != null) && (str !== ""));
        }

        this.getNumberFromString = function (str) {
            if (InfrService.isStringAndNotEmpty(str)) {
                var num = Number(str);
                if (InfrService.isNumber(num)) {
                    return num;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        this.isNumber = function (num) {
            return ((num != null) && (!isNaN(num)));
        }

        this.isViewStateInitialized = function (ViewState) {
            return ((ViewState != null) && (ViewState.isInitialized === true));
        }

        this.doesArrayContainElementByProp = function (array, propertyValue, propertyName) {
            //console.log('InfrService: checking if array contain element by prop');
            //console.log(array);
            //console.log(propertyValue);
            //console.log(propertyName);
            //console.log('Checking...');
            if (InfrService.isArrayAndNotEmpty(array)) {
                var matched = $.grep(array, function (e) {  
                    if (e.hasOwnProperty(propertyName)) {
                        return e[propertyName] == propertyValue;
                    } else {
                        return false;
                    }
                });
                return InfrService.isArrayAndNotEmpty(matched);
            } else {
                return false;
            }
        }

        this.getArrayElementPropByIndex = function (array, index, propertyName) {
            if (InfrService.isArrayAndNotEmpty(array) && (array.length >= index + 1)) {
                if (array[index].hasOwnProperty(propertyName)) {
                    return (array[index])[propertyName];
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        this.getArrayElementIndexByProp = function (array, propertyValue, propertyName) {
            if (InfrService.isArray(array)) {
                var matched = $.grep(array, function (e) {
                    if (e.hasOwnProperty(propertyName)) {
                        if (e[propertyName] == propertyValue) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                });
                if (InfrService.isArrayAndNotEmpty(matched)) {
                    return array.indexOf(matched[0]);
                } else {
                    return -1;
                }
            } else {
                throw new Error('batat Error: Wrong parameters passed to InfrService.getArrayElementIndexByProp');
            }
        }

        this.getObjectCopy = function (Obj) {
            return $.extend(true, {}, Obj);
        }

        this.findFirst = function (elems, validateCb) {
            if(InfrService.isArray(elems) && InfrService.isFunction(validateCb)) {
                for (var i = 0 ; i < elems.length ; ++i) {
                    if (validateCb(elems[i], i)) {
                        return elems[i];
                    }
                }
                return null;
            } else {
                throw new Error('batat Error: Wrong parameters passed to InfrService.findFirst');
            }
        }

        this.findIndexOfFirst = function (elems, validateCb) {
            var i;
            for (i = 0 ; i < elems.length ; ++i) {
                if (validateCb(elems[i], i)) {
                    return i;
                }
            }
            return -1;
        }

        this.copyObjectProperties = function (objA, objB) {
            for (var prop in objB) {
                if (prop != null) {
                    if (objB.hasOwnProperty(prop)) {
                        if ((prop[0] != '$') && (prop[0] != '_')) {
                            objA[prop] = objB[prop];
                        }
                    }
                }
            }
        }

        this.copyObjectPropertiesWithoutArrays = function (objA, objB) {
            for (var prop in objB) {
                if (prop != null) {
                    if (objB.hasOwnProperty(prop)) {
                        if ((prop[0] != '$') && (prop[0] != '_') && (!InfrService.isArray(objA[prop]))) {
                            objA[prop] = objB[prop];
                        }
                    }
                }
            }
        }

        this.copyArrayToObject = function (objA, objB) {

        }

        this.reset$select = function ($select) {
            if (InfrService.isObject($select) && InfrService.isFunction($select.refresh)) {
               // $select.selected = InfrService.getObjectCopy($select.selected);
                $select.refresh();
            }
        }

        this.refresh$select = function ($select) {
            if (InfrService.isObject($select) && InfrService.isFunction($select.refresh)) {
                $select.refresh();
            }
        }
        
        this.digestChanges = function (promise) {
            return promise.then(function (response) { $timeout(function () { }, 0); return response; }, function (error) { throw error; });
        }

        this.copyArrayByProp = function (ArrayTo, ArrayFrom, PropName) {
            //console.warn('InfrService: copyArrayByProp(ArrayTo, ArrayFrom, PropName)');
            //console.warn(ArrayTo);
            //console.warn(ArrayFrom);
            //console.warn(PropName);
            if (InfrService.isArray(ArrayTo) && InfrService.isArray(ArrayFrom) && InfrService.isString(PropName)) {
                for (var item of ArrayFrom) {
                    var element = InfrService.findFirst(ArrayTo, function (elem) { return elem[PropName] == item[PropName]; });
                    if (element != null) {
                        InfrService.copyObjectProperties(element, item);
                    } else {
                        ArrayTo.push(item);
                    }
                }
                //console.warn('After copying');
                //console.warn(ArrayTo);
            } else {
                throw new Error('batat Error: Wrong attributes passed to InfrService.copyArrayByProp', [ArrayTo, ArrayFrom, PropName]);
            }
            
        }
}]);