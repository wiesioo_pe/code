﻿"use strict";
console.log("Załadowałem ItemsService.js");
/// <reference path="~/app/scripts/angular.js" />

angular.module("batat").service("ItemsService",
    ["$rootScope", "$filter", "$timeout", "$q", "$injector",
    "InfrService", "OrdersEntry", "ProductsEntry", "PartsEntry", "OperationsEntry",
    "TechnologiesEntry", "OperatorsEntry", "MachinesEntry", "ParametersEntry", "GroupsEntry", "FixedCostsEntry",
    "SubjectsEntry", "ContactsEntry", "EmployeesEntry", "OrderStatesEntry", "OperationTypesEntry", "MachineTypesEntry",
    "FixedCostCategoriesEntry", "FixedCostTypesEntry", "ViewSettingsEntry", "ToolsEntry", "ToolTypesEntry", 
        "MaterialsEntry", "MaterialTypesEntry", "TrolleysEntry", "TrolleyEventsApiEntry", "NewestItemsEntry",
        function ($rootScope, $filter, $timeout, $q, $injector,
        InfrService, OrdersEntry, ProductsEntry, PartsEntry, OperationsEntry,
        TechnologiesEntry, OperatorsEntry, MachinesEntry, ParametersEntry, GroupsEntry, FixedCostsEntry, SubjectsEntry,
        ContactsEntry, EmployeesEntry, OrderStatesEntry, OperationTypesEntry, MachineTypesEntry, FixedCostCategoriesEntry,
        FixedCostTypesEntry, ViewSettingsEntry, ToolsEntry, ToolTypesEntry,
            MaterialsEntry, MaterialTypesEntry, TrolleysEntry, TrolleyEventsApiEntry, NewestItemsEntry) {

            var ItemsService = this;

            // Zmienić wszystkie metody save
            // Dodać do apiControllerów return ItemVM z put oraz post



            // public properties
            this.get = {};
            this.save = {};
            this.update = {};

            // private properties
            var query = {};
            var promise = {};
            var queryInProgress = {};

            var data = {};
            
            var itemsNames = ["Orders", "Products", "Parts", "Operations", "Technologies", "Operators",
                "Machines", "Parameters", "Groups", "FixedCosts", "Subjects", "Contacts", "Employees",
                "OrderStates", "OperationTypes", "MachineTypes", "FixedCostCategories", "FixedCostTypes",
                "Tools", "ToolTypes", "Materials", "MaterialTypes", "OrderInstances",
                "Boxes", "Trolleys"];

            // Initialize items arrays as empty
            for (var itemsName of itemsNames) {
                data[itemsName] = [];
                queryInProgress[itemsName] = false;
            }

            this.getItemsNames = function () {
                return itemsNames;
            };

            this.isValidItemsName = function (itemsName) {
                if (itemsNames.indexOf(itemsName) >= 0) { return true; }
                else { return false; }
            };

            this.downloadItems = function (itemsNames, promises, cache) {
                if (InfrService.isArray(itemsNames) && (InfrService.isObject(promises.Items)) && (InfrService.isObject(cache.Items))) {
                    for (var itemsName of itemsNames) {
                        if (ItemsService.isValidItemsName(itemsName)) {
                            // !!! ASYNC
                            promises.Items[itemsName] = (ItemsService.get.Items(itemsName))
                                .then(function (response) {
                                    cache.Items[response.itemsName] = response;
                                    return response;
                                }, function (error) { throw error; });
                        } else {
                            throw new Error("batat Error: Wrong item name passed to ItemsService.downloadItems"
                                , [itemsName]);
                        }
                    }
                } else {
                    throw new Error("batat Error: Wrong parameters passed to ItemsService.downloadItems"
                        , [itemsNames, promises, cache]);
                }
            };

            // returns promise
            this.$$downloadSingleItems = function(itemsName) {
                return ItemsService.get.Items(itemsName);
            };

            this.isDataDownloadCompleted = function (isDataLoaded, ItemsNames) {
                var isAllLoaded = false;
                if (ItemsNames && (ItemsNames.length > 0)) {
                    isAllLoaded = true;
                    for (var itemName of ItemsNames) {
                        isAllLoaded = isAllLoaded && isDataLoaded[itemName];
                    }
                }

                return isAllLoaded;
            };

            this.setInnerProductsPrices = function (Items) {
                if (InfrService.isArrayAndNotEmpty(data.Products) && InfrService.isArrayAndNotEmpty(Items)) {
                    for (var Item of Items) {
                        if (InfrService.isArrayAndNotEmpty(Item.Products)) {
                            for (var Product of Item.Products) {
                                ItemsService.setProductPrices(Product);
                            }
                        }
                    }
                }
            };

            this.setProductPrices = function (Product) {
                if (Product != null) {
                    var NetPrice = $filter("getProductNetPrice")(Product.ProductId, data.Products);
                    Product.NetPriceAfterDiscount = NetPrice * (100 - Product.Discount) / 100;
                    Product.NetWorthAfterDiscount = Product.NetPriceAfterDiscount * Product.Amount;
                }
            };

            this.queryViewSettings = function (ViewName, ViewState) {
                ViewSettingsEntry.search({
                    query: "Name=" + ViewName
                },
                    // Po udanym załadowaniu danych z serwera
                    function (data) {
                        if (data != null) {
                            for (var p in data) {
                                if (p != null) {
                                    if (data.hasOwnProperty(p) && ViewState.ViewSettings.hasOwnProperty(p)) {
                                        ViewState.ViewSettings[p] = data[p];
                                    }
                                }
                            }
                        }
                        alert("Pobrano ustawienia użytkownika dla: " + ViewName);
                    }
                );
            };

            this.saveViewSettings = function (ViewName, ViewState) {
                ViewSettingsEntry.postSettings({
                    query: "Name=" + ViewName
                }, JSON.stringify(ViewState.ViewSettings));
                alert("Zapisano bieżące ustawienia dla:" + ViewName);
            };

            this.reorderItems = function (Items, propertyName, ViewState, secondPropertyName) {

                if (Items && Items.length >= 2) {
                    Items = Items.sort(function (a, b) {

                        var first = "";
                        var second = "";
                        var secondProperty1 = "", secondProperty2 = "";
                        var result = null;

                        // Get data for second feature sorting
                        if (secondPropertyName != null) {
                            if (secondPropertyName.indexOf("Date") >= 0) {
                                var dateParts = null;
                                if (typeof a[secondPropertyName] === "string") {
                                    dateParts = a[secondPropertyName].split("-");
                                }
                                try {
                                    if (dateParts && dateParts[2]) {
                                        secondProperty1 = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // Note: months are 0-based
                                    }
                                    dateParts = null;
                                    if (typeof b[secondPropertyName] === "string") {
                                        dateParts = b[secondPropertyName].split("-");
                                    }
                                    if (dateParts && dateParts[2]) {
                                        secondProperty2 = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                    }
                                }
                                catch (ex) { }
                            } else {
                                secondProperty1 = a[secondPropertyName];
                                secondProperty2 = b[secondPropertyName];
                            }
                        }

                        // Check first-feature sorting
                        if (propertyName.indexOf("Date") >= 0) {
                            dateParts = null;
                            if (typeof a[propertyName] === "string") {
                                dateParts = a[propertyName].split("-");
                            }
                            if (dateParts && dateParts[2]) {
                                first = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                            }
                            dateParts = null;
                            if (typeof b[propertyName] === "string") {
                                dateParts = b[propertyName].split("-");
                            }
                            if (dateParts && dateParts[2]) {
                                second = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                            }
                        } else if (propertyName === "SubjectId") {
                            if (a.SubjectId) { first = $filter("getSubjectName")(a.SubjectId, $scope.Subjects); }
                            if (b.SubjectId) { second = $filter("getSubjectName")(b.SubjectId, $scope.Subjects); }
                        } else if (propertyName === "ContactId") {
                            if (a.ContactId) { first = $filter("getContactName")(a.ContactId, $scope.Contacts); }
                            if (b.ContactId) { second = $filter("getContactName")(b.ContactId, $scope.Contacts); }
                        } else if (propertyName === "PartId") {
                            if (a.PartId) { first = $filter("getPartName")(a.ContactId, $scope.Parts); }
                            if (b.PartId) { second = $filter("getPartName")(b.ContactId, $scope.Parts); }
                        } else {
                            if (a[propertyName]) { first = a[propertyName]; }
                            if (b[propertyName]) { second = b[propertyName]; }
                        }

                        if (typeof first === "string") { first = first.toLowerCase(); }
                        if (typeof second === "string") { second = second.toLowerCase(); }
                        if (typeof secondProperty1 === "string") { secondProperty1 = secondProperty1.toLowerCase(); }
                        if (typeof secondProperty2 === "string") { secondProperty2 = secondProperty2.toLowerCase(); }

                        if (ViewState.ViewSettings.isOrderedAscending === true) {
                            if (first === second) { result = secondProperty1 > secondProperty2 ? 1 : -1; }
                            else if (first > second) { result = -1; }
                            else { result = 1; }
                        }
                        else {
                            if (first === second) { result = secondProperty1 > secondProperty2 ? 1 : -1; }
                            else if (first < second) { result = -1; }
                            else { result = 1; }
                        }


                        return result;
                    });
                    ViewState.ViewSettings.isOrderedAscending = !ViewState.ViewSettings.isOrderedAscending;
                    ViewState.ViewSettings.OrderedBy = propertyName;
                }
            };

            this.sortItems = function (ItemsName, ViewState) {

                var Items = data[ItemsName];
                if (InfrService.isArrayAndNotEmpty(Items) && (Items.length >= 2)
                    && InfrService.isViewStateInitialized(ViewState)
                    && (ViewState.TemporaryViewSettings.OrderBy != null)) {

                    var propertyName = ViewState.TemporaryViewSettings.OrderBy;
                    var secondPropertyName = ViewState.ViewSettings.SecondOrderingPropertyName;

                    Items.sort(function (Item1, Item2) {

                        var first = "";
                        var second = "";
                        var secondProperty1 = "", secondProperty2 = "";
                        var result = null;
                        var dateParts = null;

                        // Get data for second feature sorting
                        if ((secondPropertyName != null) && (Item1[secondPropertyName] != null)) {
                            if (secondPropertyName.indexOf("Date") >= 0) {
                                if (typeof Item1[secondPropertyName] === "string") {
                                    dateParts = Item1[secondPropertyName].split("-");
                                }
                                try {
                                    if (dateParts && dateParts[2]) {
                                        secondProperty1 = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // Note: months are 0-based
                                    }
                                    dateParts = null;
                                    if (typeof Item2[secondPropertyName] === "string") {
                                        dateParts = Item2[secondPropertyName].split("-");
                                    }
                                    if (dateParts && dateParts[2]) {
                                        secondProperty2 = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                                    }
                                }
                                catch (ex) { }
                            } else {
                                secondProperty1 = Item1[secondPropertyName];
                                secondProperty2 = Item2[secondPropertyName];
                            }
                        }

                        // Check first-feature sorting
                        if (propertyName == "Id") {
                            first = Item1.Id;
                            second = Item2.Id;
                        } else if (propertyName.indexOf("Date") >= 0) {
                            dateParts = null;
                            if (typeof Item1[propertyName] === "string") {
                                dateParts = Item1[propertyName].split("-");
                            }
                            if (dateParts && dateParts[2]) {
                                first = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                            } else {
                                first = null;
                            }
                            dateParts = null;
                            if (typeof Item2[propertyName] === "string") {
                                dateParts = Item2[propertyName].split("-");
                            }
                            if (dateParts && dateParts[2]) {
                                second = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                            } else {
                                second = null;
                            }
                        } else if (propertyName === "SubjectId") {
                            first = $filter("getSubjectName")(Item1.SubjectId, data.Subjects);
                            second = $filter("getSubjectName")(Item2.SubjectId, data.Subjects);
                        } else if (propertyName === "ContactId") {
                            first = $filter("getContactName")(Item1.ContactId, data.Contacts);
                            second = $filter("getContactName")(Item2.ContactId, data.Contacts);
                        } else if (propertyName === "PartId") {
                            first = $filter("getPartName")(Item1.PartId, data.Parts);
                            second = $filter("getPartName")(Item2.PartId, data.Parts);
                        } else {
                            first = Item1[propertyName];
                            second = Item2[propertyName];
                        }

                        if (typeof first === "string") { first = first.toLowerCase(); }
                        if (typeof second === "string") { second = second.toLowerCase(); }
                        if (typeof secondProperty1 === "string") { secondProperty1 = secondProperty1.toLowerCase(); }
                        if (typeof secondProperty2 === "string") { secondProperty2 = secondProperty2.toLowerCase(); }

                        if (ViewState.ViewSettings.isOrderedAscending === true) {
                            if (first === second) { result = secondProperty1 > secondProperty2 ? 1 : -1; }
                            else if (first > second) { result = -1; }
                            else { result = 1; }
                        }
                        else {
                            if (first === second) { result = secondProperty1 > secondProperty2 ? 1 : -1; }
                            else if (first < second) { result = -1; }
                            else { result = 1; }
                        }

                        return result;
                    });
                    ViewState.ViewSettings.isOrderedAscending = !ViewState.ViewSettings.isOrderedAscending;
                    ViewState.ViewSettings.OrderedBy = ViewState.TemporaryViewSettings.OrderBy;
                    ViewState.TemporaryViewSettings.OrderBy = null;
                }
            };

            this.getItemCopy = function(ItemId, Items, IdPropName) {
                var ItemCopy = null;
                //console.log('ItemsService: getting item with ' + IdPropName + ' = ' + ItemId);
                if (InfrService.isArrayAndNotEmpty(Items) && (ItemId != null) && (IdPropName != null)) {
                    var ItemMatched = InfrService.findFirst(Items, function(obj) { return obj[IdPropName] == ItemId; });
                    if (ItemMatched != null) {
                        ItemCopy = InfrService.getObjectCopy(ItemMatched);
                    } else {
                        ItemCopy = InfrService.getObjectCopy(Items[0]);
                        ItemsService.resetItem(ItemCopy);
                    }
                }
                //console.log(Items);
                //console.log(ItemCopy);
                return ItemCopy;
            };

            this.resetItem = function(Item) {
                for (var prop in Item) {
                    if (prop != null) {
                        if (Item.hasOwnProperty(prop)) {
                            if (InfrService.isArray(Item[prop])) {
                                Item[prop] = [];
                            }
                            else if (InfrService.isObject(Item[prop])) {
                                Item[prop] = null;
                            } else if (InfrService.isString(Item[prop])) {
                                Item[prop] = null;
                            } else {
                                Item[prop] = null;
                            }
                        }
                    }
                }
            };

            this.isItemChanged = function(Item, ItemsName) {
                var Items = data[ItemsName];
                if (InfrService.isArrayAndNotEmpty(Items)) {
                    var ItemCopy = ItemsService.getItemCopy(Item.Id, Items, "Id");
                    for (var prop in ItemCopy) {
                        if (prop != null) {
                            if (ItemCopy.hasOwnProperty(prop)) {
                                if (prop.charAt(0) !== "$") {
                                    var isChanged = _.isEqual(ItemCopy[prop], Item[prop]);
                                    if (isChanged === true) {
                                        console.log("ItemsService: Checking '" + ItemsName + "' items for differences: " + isChanged);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            };

            this.getItemsFactory = function(itemsName) {
                if (ItemsService.isValidItemsName(itemsName)) {
                    let itemsEntry = itemsName + "Entry";
                    return $injector.get(itemsEntry);
                } else {
                    throw new Error("batat Error: Wrong parameters passes to ItemsService.getItemsFactory(itemsName)"
                        , [itemsName]);
                }
            };

            this.replaceItem = function(Item, ItemsName) {

                if (ItemsService.isValidItemsName(ItemsName) && (Item != null)) {
                    var OldItem = InfrService.findFirst(data[ItemsName], function(elem) { return elem.Id === Item.Id; });
                    if (OldItem != null) {
                        if (InfrService.isObject(OldItem)) {
                            InfrService.copyObjectProperties(OldItem, Item);
                        }
                    } else {
                        throw new Error("batat Error: Can't replaceitem inside ItemsService.replaceItem. OldItem doesn't exist", [Item, ItemsName]);
                    }
                } else {
                    throw new Error("batat Error: Wrong parameters passed to ItemsService.replaceItem", [Item, ItemsName]);
                }

            };

            this.getValidationObjectForActiveItem = function(ActiveItem) {
                var ValidationObject = {};
                for (var prop in ActiveItem) {
                    if (prop != null) {
                        if (ActiveItem.hasOwnProperty(prop)) {
                            if ((prop[0] != "$") && (prop[0] != "_")) {
                                ValidationObject[prop] = true;
                            }
                        }
                    }
                }
                return ValidationObject;
            };

            this.isValidationObjectValid = function(ValidationObject) {
                var isValid = true;

                for (var prop in ValidationObject) {
                    if (prop != null) {
                        if (ValidationObject.hasOwnProperty(prop)) {
                            if ((prop[0] != "$") && (prop[0] != "_")) {
                                isValid = ValidationObject[prop] && isValid;
                            }
                        }
                    }
                }

                return isValid;
            };

            this.setOrderInstanceSpecificProperties = function (OrderInstance) {
                if (InfrService.isObject(OrderInstance)) {
                    OrderInstance.ProductionParts.Expanded = false;
                    OrderInstance.PlannedRatio = OrderInstance.PlannedAmount / OrderInstance.ToDoAmount;
                    OrderInstance.PlannedRatio = Math.ceil(OrderInstance.PlannedRatio * 100) / 100;
                    OrderInstance.MadeRatio = OrderInstance.MadeAmount / OrderInstance.ToDoAmount;
                    OrderInstance.MadeRatio = Math.random() * OrderInstance.PlannedRatio;
                    OrderInstance.MadeRatio = Math.ceil(OrderInstance.MadeRatio * 100) / 100;
                    for (var ProductionPart of OrderInstance.ProductionParts) {
                        ProductionPart.ProductionProducts.Expanded = false;
                        ProductionPart.Visible = true;
                        ProductionPart.PlannedRatio = ProductionPart.PlannedAmount / ProductionPart.ToDoAmount;
                        ProductionPart.PlannedRatio = Math.ceil(ProductionPart.PlannedRatio * 100) / 100;
                        ProductionPart.MadeRatio = ProductionPart.MadeAmount / ProductionPart.ToDoAmount;
                        ProductionPart.MadeRatio = Math.random() * ProductionPart.PlannedRatio;
                        ProductionPart.MadeRatio = Math.ceil(ProductionPart.MadeRatio * 100) / 100;
                        for (var ProductionProduct of ProductionPart.ProductionProducts) {
                            ProductionProduct.ProductionTechnologies.Expanded = false;
                            ProductionProduct.Visible = true;
                            ProductionProduct.PlannedRatio = ProductionProduct.PlannedAmount / ProductionProduct.ToDoAmount;
                            ProductionProduct.PlannedRatio = Math.ceil(ProductionProduct.PlannedRatio * 100) / 100;
                            ProductionProduct.MadeRatio = ProductionProduct.MadeAmount / ProductionProduct.ToDoAmount;
                            ProductionProduct.MadeRatio = Math.random() * ProductionProduct.PlannedRatio;
                            ProductionProduct.MadeRatio = Math.ceil(ProductionProduct.MadeRatio * 100) / 100;
                            for (var ProductionTechnology of ProductionProduct.ProductionTechnologies) {
                                ProductionTechnology.ProductionOperations.Expanded = false;
                                ProductionTechnology.Visible = true;
                                for (var ProductionOperation of ProductionTechnology.ProductionOperations) {
                                    ProductionOperation.Boxes.Expanded = false;
                                    ProductionOperation.Visible = true;
                                    for (var Box of ProductionOperation.Boxes) {
                                        Box.Visible = true;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    throw new Error("batat Error: Wrong parameters passed to ItemsService.setOrderInstanceSpecificProperties(OrderInstance)", [OrderInstance]);
                }
            };

            this.setOrderSpecificProperties = function (Order) {
                var newDate = null;
                if (InfrService.isStringAndNotEmpty(Order.ReceiveDate)) {
                    let m = moment(Order.ReceiveDate, "YYYY-MM-DDTHH:mm:ss");
                    Order.ReceiveDate = m.format("YYYY-MM-DD");
                }
                if (InfrService.isStringAndNotEmpty(Order.AccomplishDate)) {
                    let m = moment(Order.AccomplishDate, "YYYY-MM-DDTHH:mm:ss");
                    Order.AccomplishDate = m.format("YYYY-MM-DD");
                }
                if (InfrService.isStringAndNotEmpty(Order.DeliveryDate)) {
                    let m = moment(Order.DeliveryDate, "YYYY-MM-DDTHH:mm:ss");
                    Order.DeliveryDate = m.format("YYYY-MM-DD");
                }
            };

            this.setItemSpecificProperties = function (Item, ItemsName) {
                if (ItemsName === "OrderInstances") {
                    ItemsService.setOrderInstanceSpecificProperties(Item);
                } else if (ItemsName === "Orders") {
                    ItemsService.setOrderSpecificProperties(Item);
                    promise.Products.then(function () { ItemsService.setInnerProductsPrices([Item]); }, function () { });
                } else if (ItemsName === "Products") {
                    ItemsService.setInnerProductsPrices([Item]);
                } else if (ItemsName === "Trolleys") {
                    // MOVED TO TROLLEY SERVICE
                }
            };

            this.setItemSpecificPropertiesInArray = function (ItemsName, array) {
                if (ItemsName === "OrderInstances") {
                    for (var OrderInstance of array) {
                        ItemsService.setOrderInstanceSpecificProperties(OrderInstance);
                    }
                } else if (ItemsName === "Orders") {
                    for (var Order of array) {
                        ItemsService.setOrderSpecificProperties(Order);
                    }
                    promise.Products.then(function () { ItemsService.setInnerProductsPrices(array); }, function () { });
                } else if (ItemsName === "Products") {
                    ItemsService.setInnerProductsPrices(array);
                } else if (ItemsName === "Trolleys") {
                    // MOVED TO TROLLEY SERVICE
                }
            };

            this.updateItemBeforeSave = function (itemsName, Item) {

                var ItemCopy = $.extend(true, {}, Item);

                if (itemsName === "Orders") {
                    if (ItemCopy.ReceiveDate != null) {
                        ItemCopy.ReceiveDate += " 00:00";
                    }
                    if (ItemCopy.DeliveryDate != null) {
                        ItemCopy.DeliveryDate += " 00:00";
                    }
                    if (ItemCopy.AccomplishDate != null) {
                        ItemCopy.AccomplishDate += " 00:00";
                    }
                }

                return ItemCopy;
            };


            // ITEMS QUERY AND GET
            this.get.Items = function (itemsName) {
                if (data[itemsName] == null || data[itemsName].length === 0) {
                    return query.Items(itemsName, "Ids=All&LastUpdate=");
                } else {
                    let q = $q.defer();
                    q.resolve(data[itemsName]);
                    return q.promise;
                }
            };

            query.Items = function (itemsName, queryString) {
                if (ItemsService.isValidItemsName(itemsName)) {
                    if (queryInProgress[itemsName] === false) {
                        queryInProgress[itemsName] = true;
                        promise[itemsName] = (ItemsService.getItemsFactory(itemsName)).query({ query: queryString })
                            .$promise.then(function (response) {
                                ItemsService.setItemSpecificPropertiesInArray(itemsName, response);
                                InfrService.copyArrayByProp(data[itemsName], response, "Id");
                                $(data[itemsName]).each(function () {
                                    this.Visible = true;
                                });
                                data[itemsName].itemsName = itemsName;
                                $rootScope.$emit("DataReceived", [itemsName]);
                                queryInProgress[itemsName] = false;
                                data[itemsName].LastUpdate = moment().format("YYYY-MM-DDTHH:mm:ss");
                                data.LastUpdate = moment().format("YYYY-MM-DDTHH:mm:ss");
                                return data[itemsName];
                            }, function (error) { throw error; });
                        return promise[itemsName];
                    }
                    else {
                        return promise[itemsName];
                    }
                } else {
                    throw new Error("batat Error: Wrong attributes passed to ItemsService.query.Items(itemsName, queryString)"
                        , [itemsName, queryString]);
                }
            };

            this.update.Items = function (itemsName) {
                console.warn("ItemsService.update.Items: " + itemsName);
                if (ItemsService.isValidItemsName(itemsName)) {
                    var LastUpdateDateTime = new Date(Date.YEAR_2000_CONSTRUCT_VALUE);
                    if (InfrService.isArrayAndNotEmpty(data[itemsName])) {
                        for (var Item of data[itemsName]) {
                            if (InfrService.isString(Item.LastUpdate)) {
                                var ItemLastUpdate = moment(Item.LastUpdate, "YYYY-MM-DDTHH:mm:ss").toDate();
                                if (ItemLastUpdate > LastUpdateDateTime) {
                                    LastUpdateDateTime = ItemLastUpdate;
                                }
                            }
                        }
                    }
                    return query.Items(itemsName, "Ids=All&LastUpdate=" + moment(LastUpdateDateTime).format("YYYY-MM-DDTHH:mm:ss"));
                } else {
                    throw new Error("batat Error: Wrong attributes passed to ItemsService.update.Items(itemsName)"
                        , [itemsName, queryString])
                }
            };

            this.updateAllItems = function (itemsNamesArray) {
                return NewestItemsEntry.get({ ItemsNames: itemsNamesArray, query: "LastUpdate=" + data.LastUpdate })
                    .$promise.then(function (response) {
                        var isNull = true;
                        for (var items of itemsNamesArray) {
                            if ((response[items] != null) && (response[items].length > 0)) {
                                isNull = false;
                                for (var item of response[items]) {
                                    ItemsService.replaceItem(item, items);
                                }
                            }
                        }
                        if (isNull === true) {
                            throw false;
                        } else {
                            data.LastUpdate = moment().format("YYYY-MM-DDTHH:mm:ss");
                            return true;
                        }
                    }, function () { throw false; });
            };

            this.save.Item = function (itemsName, Item) {
                if (ItemsService.isValidItemsName(itemsName)) {
                    Item = ItemsService.updateItemBeforeSave(itemsName, Item);

                    return (ItemsService.getItemsFactory(itemsName)).save(Item)
                        .$promise.then(function (response) {
                            // if saved insert or replace inside table
                            var item = InfrService.findFirst(data[itemsName], function (e) { return e.Id === response.Id });
                            if (item != null) {
                                ItemsService.setItemSpecificProperties(response, itemsName);
                                InfrService.copyObjectProperties(item, response);
                            } else {
                                response.Visible = true;
                                ItemsService.setItemSpecificProperties(response, itemsName);
                                data[itemsName].unshift(response);
                            }
                            return response;
                        }, function (error) {
                            throw error;
                        });

                }
                else {
                    throw new Error("batat Error: Wrong attributes passed to ItemsService.save.Item(itemsName, Item)"
                        , [itemsName, queryString])
                }
            };

            // Order // for complaints
            this.get.Order = function (OrderId) {
                var OrdersMatched = $.grep(data.Orders, function (e) { return e.Id === OrderId; });
                if (InfrService.isArrayAndNotEmpty(OrdersMatched)) {
                    return OrdersMatched[0];
                } else {
                    return null;
                }
            };

            // OrderProducts // for complaints
            this.get.OrderProducts = function (OrderId) {
                var gettingOrderProducts = $q.defer();
                gettingOrderProducts.isResolved = false;
                promise.OrderProducts = gettingOrderProducts.promise;

                if (data.Products.length === 0) {
                    query.Products();
                    promise.Products.then(function (result) {
                        if (data.Orders.length !== 0) {
                            if (gettingOrderProducts.isResolved === false) {
                                resolveOrderProducts(gettingOrderProducts, OrderId);
                            }
                        }
                    }, function () { })
                } else {
                    if (data.Orders.length !== 0) {
                        if (gettingOrderProducts.isResolved === false) {
                            resolveOrderProducts(gettingOrderProducts, OrderId);
                        }
                    }
                }

                if (data.Orders.length === 0) {
                    query.Items("Orders", "All");
                    promise.Orders.then(function (result) {
                        if (data.Products.length !== 0) {
                            if (gettingOrderProducts.isResolved === false) {
                                resolveOrderProducts(gettingOrderProducts, OrderId);
                            }
                        }
                    }, function () { })
                } else {
                    if (data.Products.length !== 0) {
                        if (gettingOrderProducts.isResolved === false) {
                            resolveOrderProducts(gettingOrderProducts, OrderId);
                        }
                    }
                }

                return promise.OrderProducts;

                function resolveOrderProducts(gettingOrderProducts, OrderId) {
                    gettingOrderProducts.isResolved = true;
                    var OrderProducts = [];
                    var Order = ItemsService.get.Order(OrderId);
                    if (InfrService.isArrayAndNotEmpty(Order.Products)) {
                        for (var Product of Order.Products) {
                            OrderProducts.push(ItemsService.get.Product(Product.ProductId))
                        }
                    } else {
                        console.log("Order.Products array empty");
                    }

                    gettingOrderProducts.resolve(OrderProducts);
                }
            };

            // Product
            this.get.Product = function (ProductId) {
                var ProductsMatched = $.grep(data.Products, function (e) { return e.Id == ProductId; });
                if (InfrService.isArrayAndNotEmpty(ProductsMatched)) {
                    return ProductsMatched[0];
                } else {
                    return null;
                }
            };

}]);