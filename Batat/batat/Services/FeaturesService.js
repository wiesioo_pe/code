﻿

"use strict";
console.log("Załadowałem FeaturesService.js");
/// <reference path="~/app/scripts/angular.js" />

angular.module("batat").service("FeaturesService",
    ['$filter', '$timeout', 'InfrService',
    function ($filter, $timeout, InfrService) {

        var FeaturesService = this;

        this.smartModalYesNo = function (title, content, callbackYes, callbackNo) {
            $.SmartMessageBox({
                title: title,
                content: content,
                buttons: '[Nie][Tak]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Tak") {
                    $.smallBox({
                        title: "Callback function",
                        content: "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    if (InfrService.isFunction(callbackYes)) {
                        callbackYes();
                    } else {
                        throw "Wrong parameter: FeaturesService.smartModalYesNo(title, content, callbackYes, callbackNo). callbackYes is not a function.";
                    }
                }
                if (ButtonPressed === "Nie") {
                    $.smallBox({
                        title: "Callback function",
                        content: "<i class='fa fa-clock-o'></i> <i>You pressed No...</i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    if (InfrService.isFunction(callbackNo)) {
                        callbackNo();
                    } else {
                        throw "Wrong parameter: FeaturesService.smartModalYesNo(title, content, callbackYes, callbackNo). callbackNo is not a function.";
                    }
                }
            });
        };

        this.smartModalYesNoWithTextarea = function (title, content, placeholder, callbackYes, callbackNo) {
            $.SmartMessageBox({
                title: title,
                content: content,
                buttons: "[Tak][Nie]",
                input: "textarea",
                cols: 30,
                rows: 3,
                placeholder: placeholder
            }, function (ButtonPress, Value) {
                if (ButtonPress == "Tak") {
                    callbackYes(Value);
                } else {
                    callbackNo();
                }
            });
        }

        this.smartModalOKCancelWithTextarea = function (title, content, placeholder, callbackOK, callbackCancel) {
            $.SmartMessageBox({
                title: title,
                content: content,
                buttons: "[OK][Anuluj]",
                input: "textarea",
                cols: 30,
                rows: 3,
                placeholder: placeholder
            }, function (ButtonPress, Value) {
                if (ButtonPress == "Tak") {
                    callbackOK(Value);
                } else {
                    callbackCancel();
                }
            });
        }

        this.smartTabletModalOKCancelWithTextarea = function (title, content, placeholder, callbackOK, callbackCancel) {
            $.SmartMessageBoxForTablet({
                title: title,
                content: content,
                buttons: "[OK][Anuluj]",
                input: "textarea",
                cols: 30,
                rows: 6,
                placeholder: placeholder
            }, function (ButtonPress, Value) {
                if (ButtonPress == "OK") {
                    callbackOK(Value);
                } else {
                    callbackCancel(Value);
                }
            });
        }


        this.smartModalOK = function (title, content, callbackOK) {
            $.SmartMessageBox({
                title: title,
                content: content,
                buttons: '[OK]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "OK") {
                    $.smallBox({
                        title: "Callback function",
                        content: "<i class='fa fa-clock-o'></i> <i>You pressed Yes...</i>",
                        color: "#659265",
                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                        timeout: 4000
                    });
                    if (InfrService.isFunction(callbackOK)) {
                        callbackOK();
                    } else {
                        throw "Wrong parameter: FeaturesService.smartModalOK(title, content, callbackOK). callbackOK is not a function.";
                    }
                }
            });
        };
}]);