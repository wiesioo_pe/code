﻿"use strict";
console.log("Załadowałem batat filters.js");
/// <reference path="~/app/scripts/angular.js" />

batat.filters = (function () {

    

    // ORDERS FILTERS
    angular.module('batat').filter('getOrderStateColor', getOrderStateColor);
    getOrderStateColor.$inject = ['InfrService'];
    function getOrderStateColor(InfrService) {
        return function (item) {
            if (item == 0) {
                return "#FF8A57";
            } else if (item == 1) {
                return "#FDE288";
            } else if (item == 2) {
                return "#77A0D2";
            } else if (item == 3) {
                return "#66B682";
            }
        }
    }

    angular.module('batat').filter('getOrderPriorityName', getOrderPriorityName);
    getOrderPriorityName.$inject = ['InfrService'];
    function getOrderPriorityName(InfrService) {
        return function (item) {
            if (item == 0) {
                return "Niski";
            } else if (item == 1) {
                return "Wysoki";
            } else {
                return "Błąd priorytetu";
            }
        }
    }

    angular.module('batat').filter('getOrderProperty', getOrderProperty);
    getOrderProperty.$inject = ['InfrService'];
    function getOrderProperty(InfrService) {
        return function (id, propName, Orders) {
            if (Orders) {
                var Order = $.grep(Orders, function (e) { return e.Id == id })[0];
                if (Order != null) {
                    return Order[propName] || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }




    // PRODUCTS FILTERS
    angular.module('batat').filter('getProductName', getProductName);
    getProductName.$inject = ['InfrService'];
    function getProductName(InfrService) {
        return function (item, products) {
            if(products) {
                var product = $.grep(products, function (e) { return e.Id == item })[0];
                if (product) {
                    return product.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getProductCode', getProductCode);
    getProductCode.$inject = ['InfrService'];
    function getProductCode(InfrService) {
        return function (item, products) {
            if(products) {
                var product = $.grep(products, function (e) { return e.Id == item })[0];
                if (product) {
                    return product.Code || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getProductNetPrice', getProductNetPrice);
    getProductNetPrice.$inject = ['InfrService'];
    function getProductNetPrice(InfrService) {
        return function (item, products) {
            if (products) {
                var product = $.grep(products, function (e) { return e.Id == item })[0];
                if (product) {
                    return product.NetPrice || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getProductProperty', getProductProperty);
    getProductProperty.$inject = ['InfrService'];
    function getProductProperty(InfrService) {
        return function (id, propName, Products) {
            if (Products) {
                var Product = $.grep(Products, function (e) { return e.Id == id })[0];
                if (Product != null) {
                    return Product[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    


    // PARTS FILTERS
    angular.module('batat').filter('getPartName', getPartName);
    getPartName.$inject = ['InfrService'];
    function getPartName(InfrService) {
        return function (item, Parts) {
            //Is array?
            if (Parts) {  
                var part = $.grep(Parts, function (e) { return e.Id == item })[0];
                if (part) {
                    return part.Name || null;
                }
                else {
                    return null;
                }
            }
            // Not an array
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getPartCode', getPartCode);
    getPartCode.$inject = ['InfrService'];
    function getPartCode(InfrService) {
        return function (item, Parts) {
            if (Object.prototype.toString.call(Parts) === '[object Array]') {
                var part = $.grep(Parts, function (e) { return e.Id == item })[0];
                if (part) {
                    return part.Code || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getPartProperty', getPartProperty);
    getPartProperty.$inject = ['InfrService'];
    function getPartProperty(InfrService) {
        return function (id, propName, Parts) {
            if (Parts) {
                var Part = $.grep(Parts, function (e) { return e.Id == id })[0];
                if (Part != null) {
                    return Part[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }




    // OPERATIONS FILTERS
    angular.module('batat').filter('getOperationName', getOperationName);
    getOperationName.$inject = ['InfrService'];
    function getOperationName(InfrService) {
        return function (item, operations) {
            if (operations) {
                var Operation = $.grep(operations, function (e) { return e.Id == item })[0];
                if (Operation) {
                    return Operation.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getOperationCode', getOperationCode);
    getOperationCode.$inject = ['InfrService'];
    function getOperationCode(InfrService) {
        return function (item, operations) {
            if (operations) {
                var Operation = $.grep(operations, function (e) { return e.Id == item })[0];
                if (Operation) {
                    return Operation.Code || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getOperationTypeId', getOperationTypeId);
    getOperationTypeId.$inject = ['InfrService'];
    function getOperationTypeId(InfrService) {
        return function (item, operations) {
            if (operations) {
                var Operation = $.grep(operations, function (e) { return e.Id == item })[0];
                if (Operation) {
                    return Operation.OperationTypeId || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getOperationTypeNameByOperationId', getOperationTypeNameByOperationId);
    getOperationTypeNameByOperationId.$inject = ['InfrService'];
    function getOperationTypeNameByOperationId(InfrService) {
        return function (OperationId, Operations, OperationTypes) {
            if (InfrService.isArrayAndNotEmpty(Operations) && InfrService.isArrayAndNotEmpty(OperationTypes)) {
                var Operation = InfrService.findFirst(Operations, function (e) { return e.Id == OperationId });
                if (Operation != null) {
                    var OperationType = InfrService.findFirst(OperationTypes, function (e) { return e.Id == Operation.OperationTypeId });
                    if (OperationType != null) {
                        return OperationType.Name || null;
                    }
                    else {
                        return null;
                    }
                } else {
                    return null;
                }
                
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getOperationTypeName', getOperationTypeName);
    getOperationTypeName.$inject = ['InfrService'];
    function getOperationTypeName(InfrService) {
        return function (OperationTypeId, OperationTypes) {
            if (InfrService.isArrayAndNotEmpty(OperationTypes)) {
                var OperationType = InfrService.findFirst(OperationTypes, function (e) { return e.Id == OperationTypeId });
                if (OperationType != null) {
                    return OperationType.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getOperationProperty', getOperationProperty);
    getOperationProperty.$inject = ['InfrService'];
    function getOperationProperty(InfrService) {
        return function (id, propName, Operations) {
            if (Operations) {
                var Operation = $.grep(Operations, function (e) { return e.Id == id })[0];
                if (Operation != null) {
                    return Operation[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }


    // OPERATION TYPES
    angular.module('batat').filter('getOperationTypeProperty', getOperationTypeProperty);
    getOperationTypeProperty.$inject = ['InfrService'];
    function getOperationTypeProperty(InfrService) {
        return function (id, propName, OperationTypes) {
            if (OperationTypes) {
                var OperationType = $.grep(OperationTypes, function (e) { return e.Id == id })[0];
                if (OperationType != null) {
                    return OperationType[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }


    // TECHNOLOGIES FILTERS
    angular.module('batat').filter('getTechnologyName', getTechnologyName);
    getTechnologyName.$inject = ['InfrService'];
    function getTechnologyName(InfrService) {
        return function (id, technologies) {
            if (technologies) {
                var Technology = $.grep(technologies, function (e) { return e.Id == id })[0];
                if (Technology) {
                    return Technology.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }
    
    angular.module('batat').filter('getTechnologyProperty', getTechnologyProperty);
    getTechnologyProperty.$inject = ['InfrService'];
    function getTechnologyProperty(InfrService) {
        return function (id, propName, Technologies) {
            if (Technologies) {
                var Technology = $.grep(Technologies, function (e) { return e.Id == id })[0];
                if (Technology != null) {
                    return Technology[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }




    // INFRASTRUCTURE FILTERS
    angular.module('batat').filter('getGroupName', getGroupName);
    getGroupName.$inject = ['InfrService'];
    function getGroupName(InfrService) {
        return function (item, Groups) {
            if (Groups) {
                var Group = $.grep(Groups, function (e) { return e.Id == item })[0];
                if (Group) {
                    return Group.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }
   
    angular.module('batat').filter('getGroupCode', getGroupCode);
    getGroupCode.$inject = ['InfrService'];
    function getGroupCode(InfrService) {
        return function (item, Groups) {
            if (Groups) {
                var Group = $.grep(Groups, function (e) { return e.Id == item })[0];
                if (Group) {
                    return Group.Code || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }
    
    angular.module('batat').filter('getGroupDesc', getGroupDesc);
    getGroupDesc.$inject = ['InfrService'];
    function getGroupDesc(InfrService) {
        return function (item, Groups) {
            if (Groups) {
                var Group = $.grep(Groups, function (e) { return e.Id == item })[0];
                if (Group) {
                    return Group.Desc || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }
     
    angular.module('batat').filter('getEmployeeName', getEmployeeName);
    getEmployeeName.$inject = ['InfrService'];
    function getEmployeeName(InfrService) {
        return function (item, Employees) {
            if (Employees) {
                var Employee = $.grep(Employees, function (e) { return e.Id === item })[0];
                if (Employee) {
                    return Employee.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getEmployeeProperty', getEmployeeProperty);
    getEmployeeProperty.$inject = ['InfrService'];
    function getEmployeeProperty(InfrService) {
        return function (id, propName, Employees) {
            if (Employees) {
                var Employee = $.grep(Employees, function (e) { return e.Id == id })[0];
                if (Employee != null) {
                    return Employee[propName] || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }


    // CUSTOMERS & CONTACTS FILTERS
    angular.module('batat').filter('getSubjectName', getSubjectName);
    getSubjectName.$inject = ['InfrService'];
    function getSubjectName(InfrService) {
        return function (item, Subjects) {
            if (Subjects) {
                var Subject = $.grep(Subjects, function (e) { return e.Id == item })[0];
                if (Subject) {
                    return Subject.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getSubjectProperty', getSubjectProperty);
    getSubjectProperty.$inject = ['InfrService'];
    function getSubjectProperty(InfrService) {
        return function (id, propName, Subjects) {
            if (Subjects) {
                var Subject = $.grep(Subjects, function (e) { return e.Id == id })[0];
                if (Subject != null) {
                    return Subject[propName] || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getContactName', getContactName);
    getContactName.$inject = ['InfrService'];
    function getContactName(InfrService) {
        return function (item, Contacts) {
            if (Contacts) {
                var Contact = $.grep(Contacts, function (e) { return e.Id == item })[0];
                if (Contact) {
                    return Contact.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getContactProperty', getContactProperty);
    getContactProperty.$inject = ['InfrService'];
    function getContactProperty(InfrService) {
        return function (id, propName, Contacts) {
            if (Contacts) {
                var Contact = $.grep(Contacts, function (e) { return e.Id == id })[0];
                if (Contact != null) {
                    return Contact[propName] || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }



    // PARAMETERS FILTERS
    angular.module('batat').filter('getParameterName', getParameterName);
    getParameterName.$inject = ['InfrService'];
    function getParameterName(InfrService) {
        return function (item, Parameters) {
            if (Parameters) {
                var Parameter = $.grep(Parameters, function (e) { return e.Id == item })[0];
                if (Parameter) {
                    return Parameter.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getParameterUnit', getParameterUnit);
    getParameterUnit.$inject = ['InfrService'];
    function getParameterUnit(InfrService) {
        return function (item, Parameters) {
            if (Parameters) {
                var Parameter = $.grep(Parameters, function (e) { return e.Id == item })[0];
                if (Parameter) {
                    return Parameter.Unit || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    
   


    // MACHINES FILTERS
    angular.module('batat').filter('getMachineTypeName', getMachineTypeName);
    getMachineTypeName.$inject = ['InfrService'];
    function getMachineTypeName(InfrService) {
        return function (item, MachineTypes) {
            if (MachineTypes) {
                var MachineType = $.grep(MachineTypes, function (e) { return e.Id == item })[0];
                if (MachineType) {
                    return MachineType.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMachineCode', getMachineCode);
    getMachineCode.$inject = ['InfrService'];
    function getMachineCode(InfrService) {
        return function (itemId, Machines) {
            if (Machines) {
                var Machine = $.grep(Machines, function (e) { return e.Id == itemId })[0];
                if (Machine) {
                    return Machine.Code || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMachineModel', getMachineModel);
    getMachineModel.$inject = ['InfrService'];
    function getMachineModel(InfrService) {
        return function (itemId, Machines) {
            if (Machines) {
                var Machine = $.grep(Machines, function (e) { return e.Id == itemId })[0];
                if (Machine) {
                    return Machine.Model || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMachineManufacturer', getMachineManufacturer);
    getMachineManufacturer.$inject = ['InfrService'];
    function getMachineManufacturer(InfrService) {
        return function (itemId, Machines) {
            if (Machines) {
                var Machine = $.grep(Machines, function (e) { return e.Id == itemId })[0];
                if (Machine) {
                    return Machine.Manufacturer || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMachineProperty', getMachineProperty);
    getMachineProperty.$inject = ['InfrService'];
    function getMachineProperty(InfrService) {
        return function (id, propName, Machines) {
            if (Machines) {
                var Machine = $.grep(Machines, function (e) { return e.Id == id })[0];
                if (Machine != null) {
                    return Machine[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMachineTypeProperty', getMachineTypeProperty);
    getMachineTypeProperty.$inject = ['InfrService'];
    function getMachineTypeProperty(InfrService) {
        return function (id, propName, MachineTypes) {
            if (MachineTypes) {
                var MachineType = $.grep(MachineTypes, function (e) { return e.Id == id })[0];
                if (MachineType != null) {
                    return MachineType[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }


    // OPERATORS FILTERS
    angular.module('batat').filter('getOperatorProperty', getOperatorProperty);
    getOperatorProperty.$inject = ['InfrService'];
    function getOperatorProperty(InfrService) {
        return function (id, propName, Operators) {
            if (Operators) {
                var Operator = $.grep(Operators, function (e) { return e.Id == id })[0];
                if (Operator != null) {
                    return Operator[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }


    // TOOLS FILTERS
    angular.module('batat').filter('getToolTypeName', getToolTypeName);
    getToolTypeName.$inject = ['InfrService'];
    function getToolTypeName(InfrService) {
        return function (item, ToolTypes) {
            if (ToolTypes) {
                var ToolType = $.grep(ToolTypes, function (e) { return e.Id == item })[0];
                if (ToolType) {
                    return ToolType.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getToolName', getToolName);
    getToolName.$inject = ['InfrService'];
    function getToolName(InfrService) {
        return function (item, Tools) {
            if (Tools) {
                var Tool = $.grep(Tools, function (e) { return e.Id == item })[0];
                if (Tool) {
                    return Tool.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }



  
    // MATERIALS FILTERS
    angular.module('batat').filter('getMaterialTypeName', getMaterialTypeName);
    getMaterialTypeName.$inject = ['InfrService'];
    function getMaterialTypeName(InfrService) {
        return function (item, MaterialTypes) {
            if (MaterialTypes) {
                var MaterialType = $.grep(MaterialTypes, function (e) { return e.Id == item })[0];
                if (MaterialType) {
                    return MaterialType.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMaterialName', getMaterialName);
    getMaterialName.$inject = ['InfrService'];
    function getMaterialName(InfrService) {
        return function (item, Materials) {
            if (Materials) {
                var Material = $.grep(Materials, function (e) { return e.Id == item })[0];
                if (Material) {
                    return Material.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getMaterialUnit', getMaterialUnit);
    getMaterialUnit.$inject = ['InfrService'];
    function getMaterialUnit(InfrService) {
        return function (item, Materials) {
            if (Materials) {
                var Material = $.grep(Materials, function (e) { return e.Id == item })[0];
                if (Material) {
                    return Material.Unit || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }




    // FIXED COSTS FILTERS
    angular.module('batat').filter('getFixedCostCategoryName', getFixedCostCategoryName);
    getFixedCostCategoryName.$inject = ['InfrService'];
    function getFixedCostCategoryName(InfrService) {
        return function (item, FixedCostCategories) {
            if (FixedCostCategories) {
                var FixedCostCategory = $.grep(FixedCostCategories, function (e) { return e.Id == item })[0];
                if (FixedCostCategory) {
                    return FixedCostCategory.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getFixedCostTypeName', getFixedCostTypeName);
    getFixedCostTypeName.$inject = ['InfrService'];
    function getFixedCostTypeName(InfrService) {
        return function (item, FixedCostTypes) {
            if (FixedCostTypes) {
                var FixedCostType = $.grep(FixedCostTypes, function (e) { return e.Id == item })[0];
                if (FixedCostType) {
                    return FixedCostType.Name || null;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getRecurringYesNo', getRecurringYesNo);
    getRecurringYesNo.$inject = ['InfrService'];
    function getRecurringYesNo(InfrService) {
        return function (item) {
            if (item === true) {
                return "okresowy";
            }
            else if (item === false) {
                return "jednorazowy";
            } else {
                return null;
            }
        };
    }

    angular.module('batat').filter('getFixedCostPeriod', getFixedCostPeriod);
    getFixedCostPeriod.$inject = ['InfrService'];
    function getFixedCostPeriod(InfrService) {
        return function (item, isRecurring) {
            if (isRecurring === true) {
                return item;
            }
            else {
                return "-";
            }
        };
    }
    



    // OTHER
    angular.module('batat').filter('getIdFormattedString', getIdFormattedString);
    getIdFormattedString.$inject = ['InfrService'];
    function getIdFormattedString(InfrService) {
        return function (id) {
            if (id < 1000) {
                return ("000" + id).slice(-3);
            } else if (id < 10000) {
                return ("0000" + id).slice(-4);
            } else {
                return ("00000" + id).slice(-5);
            }
        }
    }

    angular.module('batat').filter('getCurrency', getCurrency);
    getCurrency.$inject = ['InfrService'];
    function getCurrency(InfrService) {
        return function (item) {
            if ((item)&&(!isNaN(item))) {
                item = item.toFixed(2);
                item = addSpaceEvery3Digits(item);
                return item + "zł";
            } else {
                return null;
            }
        };
    }

    angular.module('batat').filter('addSpaceEvery3Digits', addSpaceEvery3Digits);
    addSpaceEvery3Digits.$inject = ['InfrService'];
    function addSpaceEvery3Digits(Number) {
        var str = Number.toString().split('.');
        if (str[0].length >= 4) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
        }
        if (str[1] && str[1].length >= 4) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }

    angular.module('batat').filter('getRandomId', getRandomId);
    getRandomId.$inject = ['InfrService'];
    function getRandomId(InfrService) {
        return function(input) {
            return Math.random() + (new Date()).getTime();
        }
    }

    // getMonthName
    angular.module('batat').filter('getMonthName', getMonthName);
    getMonthName.$inject = ['InfrService'];
    function getMonthName(InfrService) {
        return function (monthNumber) {
            
            var number = null;
            if (InfrService.isStringAndNumber(monthNumber)) {
                number = parseInt(monthNumber);
            } else {
                number = monthNumber;
            }
            
            if ((number >= 0) && (number <= 11)) {
                return batatLocales.monthNames[number];
            }
            else {
                return null;
            }
        }
    }

    // getMonthNameShort
    angular.module('batat').filter('getMonthNameShort', getMonthNameShort);
    getMonthNameShort.$inject = ['InfrService'];
    function getMonthNameShort(InfrService) {
        return function (monthNumber) {
            var number = null;
            if (InfrService.isStringAndNumber(monthNumber)) {
                number = parseInt(monthNumber);
            } else {
                number = monthNumber;
            }

            if ((number >= 0) && (number <= 11)) {
                return batatLocales.monthNamesShort[number];
            }
            else {
                return null;
            }
        }
    }

    // getDayOfWeekName
    angular.module('batat').filter('getDayOfWeekName', getDayOfWeekName);
    getDayOfWeekName.$inject = ['InfrService'];
    function getDayOfWeekName(InfrService) {
        return function (dayNumber) {
            var number = null;
            if (InfrService.isStringAndNumber(dayNumber)) {
                number = parseInt(dayNumber);
            } else {
                number = dayNumber;
            }

            if ((number >= 0) && (number <= 6)) {
                return batatLocales.dayNames[number];
            }
            else {
                return null;
            }
        }
    }

    // getDayOfWeekNameShort
    angular.module('batat').filter('getDayOfWeekNameShort', getDayOfWeekNameShort);
    getDayOfWeekNameShort.$inject = ['InfrService'];
    function getDayOfWeekNameShort(InfrService) {
        return function (dayNumber) {
            var number = null;
            if (InfrService.isStringAndNumber(dayNumber)) {
                number = parseInt(dayNumber);
            } else {
                number = dayNumber;
            }

            if ((number >= 0) && (number <= 6)) {
                return batatLocales.dayNamesShort[number];
            }
            else {
                return null;
            }
        }
    }



    // PRODUCTION
    angular.module('batat').filter('getProductionProductProperty', getProductionProductProperty);
    getProductionProductProperty.$inject = ['InfrService'];
    function getProductionProductProperty(InfrService) {
        return function (OrderProduct, PropertyName, PartId, OrderInstances) {
            var OrderInstance = InfrService.findFirst(OrderInstances,
                function (elem) { return (elem.OrderId === OrderProduct.OrderId); });
            if (OrderInstance != null) {
                var ProductionPart = InfrService.findFirst(OrderInstance.ProductionParts,
                    function (elem) { return (elem.PartId === PartId); });
                if (ProductionPart != null) {
                    var ProductionProduct = InfrService.findFirst(ProductionPart.ProductionProducts,
                        function (elem) { return (elem.ProductId === OrderProduct.ProductId); });
                    if (ProductionProduct != null) {
                        return ProductionProduct[PropertyName];
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    angular.module('batat').filter('getOrderInstancesByFilters', getOrderInstancesByFilters);
    getOrderInstancesByFilters.$inject = ['InfrService'];
    function getOrderInstancesByFilters(InfrService) {
        return function (OrderInstances, Filters, Items) {
            console.warn("Filters: getOrderInstancesByFilters. Run time: ");
            var TimeElapsed = performance.now();
            var FilteredOrderInstances = $.extend(true, [], OrderInstances);

            if (InfrService.isObject(Filters) && InfrService.isArrayAndNotEmpty(OrderInstances)) {
                if (Filters.PartId != null) {
                    FilteredOrderInstances = $.grep(FilteredOrderInstances,
                        function (elem) { return (InfrService.findIndexOfFirst(elem.ProductionParts, function (elem2) { return elem2.PartId == Filters.PartId; }) >= 0); });
                }
                if (Filters.SubjectId != null) {
                    FilteredOrderInstances = $.grep(FilteredOrderInstances,
                        function (elem) {
                            var Order = InfrService.findFirst(Items.Orders, function (elem2) { return (elem2.Id == elem.OrderId); });
                            if (Order != null) {
                                if (Order.SubjectId == Filters.SubjectId) { return true; } else { return false; }
                            } else {
                                return false;
                            }
                        });
                }
            }
            console.warn(performance.now() - TimeElapsed);
            return FilteredOrderInstances;
        }
    }

    angular.module('batat').filter('getProductionPartsByFilters', getProductionPartsByFilters);
    getProductionPartsByFilters.$inject = ['InfrService'];
    function getProductionPartsByFilters(InfrService) {
        return function (ProductionParts, Filters, Items) {
            console.warn("Filters: getProductionPartsByFilters. Run time: ");
            var TimeElapsed = performance.now();
            var FilteredProductionParts = $.extend(true, [], ProductionParts);

            if (InfrService.isObject(Filters) && InfrService.isArrayAndNotEmpty(ProductionParts)) {
                if (Filters.PartId != null) {
                    FilteredProductionParts = $.grep(FilteredProductionParts,
                        function (elem) { return (elem.PartId == Filters.PartId); });
                }
                
            }
            console.warn(performance.now() - TimeElapsed);
            return FilteredProductionParts;
        }
    }

    angular.module('batat').filter('getProductionPartsByPart', getProductionPartsByPart);
    getProductionPartsByPart.$inject = ['InfrService'];
    function getProductionPartsByPart(InfrService) {
        return function (ProductionParts, PartId) {
            if (PartId != null) {
                if (InfrService.isArrayAndNotEmpty(ProductionParts)) {
                    return $.grep(ProductionParts, function (elem) { return elem.PartId == PartId; });
                } else {
                    return null;
                }
            } else {
                return ProductionParts;
            }
            
        }
    }

    angular.module('batat').filter('getTechnologiesByPart', getTechnologiesByPart);
    getTechnologiesByPart.$inject = ['InfrService'];
    function getTechnologiesByPart(InfrService) {
        return function (Technologies, PartId) {
            if (InfrService.isArrayAndNotEmpty(Technologies)) {
                return $.grep(Technologies, function (elem) { return elem.PartId == PartId; });
            }
        }
    }

    angular.module('batat').filter('getBoxProperty', getBoxProperty);
    getBoxProperty.$inject = ['InfrService'];
    function getBoxProperty(InfrService) {
        return function (id, propName, Boxes) {
            if (Boxes) {
                var Box = $.grep(Boxes, function (e) { return e.Id == id })[0];
                if (Box != null) {
                    return Box[propName];
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        };
    }

    

})();

angular.module('batat').filter('reverse', function () {
    return function (items) {
        if ((items) && (Object.prototype.toString.call(items)==='[object Array]') && (items.length > 1)) {
            return items.slice().reverse();
        } else {
            return items;
        }
    };
});

angular.module('batat').filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop] && item[prop].toString && (item[prop].toString().toLowerCase().indexOf(text) !== -1)) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module('batat').filter('getOperationsBeforeArray', function () {
    return function (item) {
        if ((item) && (item > 0)) {
            var array = [];
            for (var i = 0; i < item; i++) {
                array.push(i);
            }
            return array;
        } else {
            return null;
        }
    };
});





        

