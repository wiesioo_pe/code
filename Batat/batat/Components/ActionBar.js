﻿"use strict";
//console.log("Załadowałem ActionBar.js");
/// <reference path="~/app/scripts/angular.js" />


(function () {
    // COMPONENT
    angular.module("batat").directive("actionBar", function () {
        return {
            templateUrl: "/batat/Components/ActionBar.html?" + Math.random(),
            scope: {
            },
            controller: ActionBarController,
            controllerAs: '$ctrl',
            bindToController: true
        }
    });

    // CONTROLLER
    ActionBarController.$inject = ['$scope', '$window'];
    function ActionBarController($scope, $window) {

        var $ctrl = this;
        $scope.$ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {

        }
        // END OF HOOKS




        // INIT
        this._$onInit();
    }

})();