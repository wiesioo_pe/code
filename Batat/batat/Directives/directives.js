﻿"use strict";
console.log("Załadowałem batat directives.js");
/// <reference path="~/app/scripts/angular.js" />


// routeLoadingIndicator Directive
(function () {
    angular.module("batat").directive("routeLoadingIndicator", ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            template: "",
            scope: {},
            link: routeLoadingIndicatorLink
        }

        function routeLoadingIndicatorLink($scope, $element, $attrs) {

            $rootScope.$on('$routeChangeStart', function () {
                window.loading_view_screen = (window.myPleaseWait("Lading route"));
            });

            $rootScope.$on('$routeChangeSuccess', function () {
                window.loading_view_screen = (window.myPleaseWait("Creating view"));
                console.log("");
                console.log("*****************ROUTE CHANGE******************     " + $location.path());
                console.log("");
            });

            window.onbeforeunload = function (e) {
                window.loading_view_screen = (window.myPleaseWait("Redirecting"));
            };

            $rootScope.$on('$viewContentLoaded', function () {
                if (window.loading_view_screen) {
                    window.loading_view_screen.finish();
                    window.loading_view_screen = null;
                }
            });
        }
    }]);
})();

// ngDatePicker Directive
(function () {
    angular.module("batat").directive("ngDatePicker", function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                'ngModel': '='
            },
            link: ngDatePickerLink
        };

        function ngDatePickerLink($scope, $element, $attrs) {
            $element.datepicker({
                dateFormat: 'dd-mm-yy',
                language: 'pl',
                orientation: 'bottom left',
                changeDate: function (dp, $input) {
                    $scope.date = $input.val();
                    $scope.$apply();
                },
                clearBtn: true,
                autoclose: true
            });
        }
    });
})();

// ngSelect2 Directive
// IMPORTANT - inside select2.js added null check at: 
//    this.$element.on('change.select2',.... if(self.dataAdapter != null) {......
(function () {
    angular.module("batat").directive("ngSelect2", ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: { 'ngModel': '=' },
            link: ngSelect2Link
        };

        function ngSelect2Link($scope, $element, $attrs) {
            $element.select2({
                widgetPositioning: {
                    vertical: 'bottom',
                    horizontal: 'left'
                }
            });
            var initial = true;
            $scope.$watch('ngModel', function (newValue, oldValue) {
                $timeout(function () {
                    $element.select2({ data: { id: "", text: "" } }).val(newValue); }, 0);
            }, true);
        }
    }]);
})();

// lockScrollwheel Directive
(function () {
    angular.module("batat").directive("lockScrollwheel", function () {
        return {
            restrict: 'A',
            link: lockScrollwheelLink
        }

        function lockScrollwheelLink($scope, $element, $attrs) {
            $element.on('mousewheel', function (e) {
                var event = e.originalEvent,
                    d = event.wheelDelta || -event.detail;
                this.scrollTop += (d < 0 ? 1 : -1) * 30;
                e.preventDefault();
            });
        }
    });
})();


(function () {
    angular.module("batat").directive("elementRefreshOnTriggerChange", ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: { 'ngDataTrigger': '=' },
            link: elementRefreshOnTriggerChangeLink
        };

        function elementRefreshOnTriggerChangeLink($scope, $element, $attrs) {
            $scope.$watch('ngDataTrigger', function (newValue, oldValue) {
                $timeout(function () { }, 0);
            }, true);

        }
    }]);
})();