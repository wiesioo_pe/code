﻿"use strict";
console.log("Załadowałem batat messages.js");

(function () {

    angular.module("batat").provider('MessageProvider', MessageProvider);

    MessageProvider.$inject = [];
    function MessageProvider() {

        var MessageProvider = this;
        
        MessageProvider.Messages = {
            Items: {
                onItemSave: {
                    Success: "Zapisano w bazie danych",
                    Error: "Wystąpił problem z zapisem w bazie danych",

                },
                onItemReset: {
                    Success: "Cofnięto wprowadzone zmiany"
                },
                onItemsQuery: {
                    Error: "Nie można załadować danych z serwera",
                    Success: "Dane pomyślnie załadowane z serwera"
                }
            },

        };

        MessageProvider.Dictionary = {
            Items: {
                Contact: "Kontakt",
                Contacts: "Kontakty",
                FixedCost: "Koszt",
                FixedCosts: "Koszty",
                Group: "Grupa",
                Groups: "Grupy",
                Machine: "Maszyna",
                Machines: "Maszyny",
                Material: "Materiał",
                Materials: "Materiały",
                Operation: "Operacja",
                Operations: "Operacje",
                Operator: "Operator",
                Operators: "Operators",
                Order: "Zamówienie",
                Orders: "Zamówienia",
                Parameter: "Parametr",
                Parameters: "Parametry",
                Part: "Detal",
                Parts: "Detale",
                Product: "Produkt",
                Products: "Produkty",
                Subject: "Kontrahent",
                Subjects: "Kontrahenci",
                Technology: "Technologia",
                Technologies: "Technologie",
                Tool: "Narzędzie",
                Tools: "Narzędzia"
            }
        }

        this.$get = function () {
            return {
                Messages: MessageProvider.Messages,
                Dictionary: MessageProvider.Dictionary
            };
        };
    }
})();
