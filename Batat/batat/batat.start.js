﻿"use strict";
console.log("Załadowałem batat.start.js");

// Old
// angular.module("batat", ["ngRoute", "ngResource", 'ui.select', 'ngSanitize', 'ngMaterial', 'ngAnimate', 'g1b.datetime-range', 'g1b.time-range']);

angular.module("batat", []);

angular.module("batat").config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
}]);

var batat = {
    controllers: {},
    directives: {},
    filters: {},
    helpers: {}
};

var debugMode = true;
batat.debug = function () {
    if (debugMode === true) {
        debugger;
    }
}

Object.defineProperties(Date, {
    MIN_CONSTRUCT_VALUE: {
        value: -8640000000000000 // A number, not a date
    },
    MAX_CONSTRUCT_VALUE: {
        value: 8640000000000000
    },
    YEAR_2000_CONSTRUCT_VALUE: {
        value: 946688000000
    }
});

Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}

var batatLocales = {
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
    dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
    dayNamesShort: ["Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "So"]
};

function getStyle(el, styleProp) {
    var camelize = function (str) {
        return str.replace(/\-(\w)/g, function (str, letter) {
            return letter.toUpperCase();
        });
    };

    if (el.currentStyle) {
        return el.currentStyle[camelize(styleProp)];
    } else if (document.defaultView && document.defaultView.getComputedStyle) {
        return document.defaultView.getComputedStyle(el, null)
                                   .getPropertyValue(styleProp);
    } else {
        return el.style[camelize(styleProp)];
    }
}

// TEMPORARY DISABLE SCROLL **************************************

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

// END TEMPORARY DISABLE SCROLL **********************************************************************

var doesArrayContain = function (needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if (!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (needle) {
            var i = -1, index = -1;

            for (i = 0; i < this.length; i++) {
                var item = this[i];

                if ((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};