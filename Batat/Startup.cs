﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Batat.Startup))]
namespace Batat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
