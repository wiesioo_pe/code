﻿"use strict";
console.log("Załadowałem loginPage.start.js");



angular.module("loginPage", ["ngRoute", "ngResource", "batat"]);

angular.module("loginPage").config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
    console.warn('loginPage module instantiated');
}]);

    

(function () {

    angular.module("loginPage").controller("LoginPageController", LoginPageController);

    LoginPageController.$inject = ["InfrService", "CurrentUserEntry"];

    function LoginPageController(InfrService, CurrentUserEntry) {

        var $ctrl = this;

        // LIFECYCLE HOOKS
        this._$onInit = function () {
            $ctrl.CurrentUser = null;
            CurrentUserEntry.get$$One().$promise.then(function (response) { $ctrl.CurrentUser = response; }, function (error) { });
        }
        // END OF HOOKS






        // INIT
        this._$onInit();
    }

})();