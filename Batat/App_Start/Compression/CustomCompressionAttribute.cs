﻿using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Batat.App_Start.Compression
{
    public class GzipCompressAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionContext)
        {
            bool isCompressionSupported = CompressionHelper.IsCompressionSupported();
            string acceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];

            if (isCompressionSupported)
            {
                var content = actionContext.Response.Content;
                var byteArray = content == null ? null : content.ReadAsByteArrayAsync().Result;
                MemoryStream memoryStream = new MemoryStream(byteArray);

                if (acceptEncoding.Contains("gzip"))
                {
                    actionContext.Response.Content = new ByteArrayContent(CompressionHelper.Compress(memoryStream.ToArray(), false));
                    actionContext.Response.Content.Headers.Remove("Content-Type");
                    actionContext.Response.Content.Headers.Add("Content-encoding", "gzip");
                    actionContext.Response.Content.Headers.Add("Content-Type", "application/json");
                }
                else
                {
                    actionContext.Response.Content = new ByteArrayContent(CompressionHelper.Compress(memoryStream.ToArray()));
                    actionContext.Response.Content.Headers.Remove("Content-Type");
                    actionContext.Response.Content.Headers.Add("Content-encoding", "deflate");
                    actionContext.Response.Content.Headers.Add("Content-Type", "application/json");
                }
            }

            base.OnActionExecuted(actionContext);
        }
    }
}