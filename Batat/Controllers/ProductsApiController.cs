﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class ProductsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<ProductVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Products
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new ProductVM(b));
        }

        // GET api/<controller>/5
        public ProductVM Get(int id)
        {
            var product = Context.Products.Find(id);
            var productVm = new ProductVM(product);
            return productVm;
        }

        // POST api/<controller>
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]ProductAndFilesVM ProductAndFilesVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji produktu"};
            Product productDb = Context.Products.Find(ProductAndFilesVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            ProductEditHelper.CheckIfProductExists(ref productDb, user, errorVm);
            int changesCount = productDb.Changes.Count;

            ProductEditHelper.CheckGroupsChange(ProductAndFilesVM, productDb, user, Context, errorVm);
            ProductEditHelper.CheckNameChange(ProductAndFilesVM, productDb, user, errorVm);
            ProductEditHelper.CheckCodeChange(ProductAndFilesVM, productDb, user, errorVm);
            ProductEditHelper.CheckDescChange(ProductAndFilesVM, productDb, user, errorVm);
            ProductEditHelper.CheckNetPriceChange(ProductAndFilesVM, productDb, user, errorVm);
            ProductEditHelper.CheckProductsChange(ProductAndFilesVM, productDb, user, Context, errorVm);
            ProductEditHelper.CheckPartsChange(ProductAndFilesVM, productDb, user, Context, errorVm);
            ProductEditHelper.CheckParametersChange(ProductAndFilesVM, productDb, user, Context, errorVm);
            ProductEditHelper.CheckFilesChange(ProductAndFilesVM, productDb, user, Context, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= productDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            productDb.LastUpdate = DateTime.Now;

            // ZAPISANIE PRODUKTU
            if (productDb.Id == 0)
            {
                Context.Products.Add(productDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(productDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new ProductVM(productDb));

        }

        // PUT api/<controller>/5
        [ResponseType(typeof(OrderVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]ProductVM ProductVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}