﻿using System.Linq;
using System.Web.Mvc;
using Batat.Helpers.Extensions.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Batat.Models.Helpers.ProductionPanel;
using Batat.SeedDatabase.Seeds;

namespace Batat.Controllers
{
    [AllowAnonymous]
    public class SeedDatabaseController : Controller
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        public ActionResult RecreateDatabase()
        {
            TempData["Message"] = SeedDatabaseHelper.DropAllTablesWithSqlQuery();

            
            var dbMigrationsConfiguration =
                new System.Data.Entity.Migrations.DbMigrationsConfiguration<ApplicationDataDbContext>
                {
                    AutomaticMigrationsEnabled = true,
                    AutomaticMigrationDataLossAllowed = true,
                    MigrationsDirectory = @"Migrations\ApplicationDataMigrations"
                };

            var dbMigrator = new System.Data.Entity.Migrations.DbMigrator(dbMigrationsConfiguration);
            dbMigrator.Update();
            

            SeedDatabase.Seeds.SeedDatabase.AddUsersAndRoles(Context);
            
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedDatabasePanel()
        {
            // Seed Groups
            if (!Context.Groups.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedGroups(Context);
            }
            if (!Context.OperationTypes.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedOperationTypes(Context);
            }
            if (!Context.MachineTypes.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedMachineTypes(Context);
            }

            // Seed Subjects i Contacts

            if (!Context.Subjects.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedSubjects(Context);
            }

            // Seed Orders, Produkty, Detale i Parametry
            if (!Context.Parameters.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedParameters(Context);
            }

            if (!Context.Parts.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedParts(Context);
            }

            if (!Context.Products.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedProducts(Context);
            }

            // Seed Technologies, Operations, Machines i Operatorzy
            if (!Context.Technologies.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedToolTypes(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedTools(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedMaterialTypes(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedMaterials(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedOperators(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedMachines(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedTechnologies(Context);
            }

            // Seed Fixed costs
            if (!Context.FixedCosts.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedFixedCostCategories(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedFixedCostTypes(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedFixedCosts(Context);
            }

            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedOrderInstances()
        {
            if (!Context.OrderInstances.Any())
                SeedDatabase.Seeds.SeedDatabase.SeedOrderInstances(Context);

            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedTechnologyPicks()
        {
            if (!Context.ProductionTechnologyBundles.Any())
                new TechnologyPicksSeeder(Context).Seed();

            return RedirectToAction("Index", "Home", null);
        }
        public ActionResult SeedTroleys()
        {
            if (Context.Trolleys.All(a => a.MachineId == 0))
                new TroleySeeder(Context).Seed();
                
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult RemoveOrderInstances()
        {
            Context.ProductionTechnologies.RemoveRange(Context.ProductionTechnologies);
            Context.ProductionOperations.RemoveRange(Context.ProductionOperations);
            Context.ProductionTechnologyBundles.RemoveRange(Context.ProductionTechnologyBundles);
            Context.Boxes.RemoveRange(Context.Boxes);
            Context.Trolleys.RemoveRange(Context.Trolleys);
            Context.SaveChanges();

            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedGroups()
        {
            if (!Context.Groups.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedGroups(Context);
            }

            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedParameters()
        {
            if (!Context.Parameters.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedParameters(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedParts()
        {
            if (!Context.Parts.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedParts(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedProducts()
        {
            if (!Context.Products.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedProducts(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedSubjects()
        {
            if (!Context.Subjects.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedSubjects(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedOrders()
        {
            if (!Context.Orders.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedOrders(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedOperators()
        {
            if (!Context.Operators.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedOperators(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedMachines()
        {
            if (!Context.Machines.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedMachines(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedTools()
        {
            if (!Context.Tools.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedToolTypes(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedTools(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedMaterials()
        {
            if (!Context.Materials.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedMaterialTypes(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedMaterials(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedTechnologies()
        {
            if (!Context.Technologies.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedTechnologies(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedFixedCosts()
        {
            if (!Context.FixedCosts.Any())
            {
                SeedDatabase.Seeds.SeedDatabase.SeedFixedCostCategories(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedFixedCostTypes(Context);
                SeedDatabase.Seeds.SeedDatabase.SeedFixedCosts(Context);
            }
            return RedirectToAction("Index", "Home", null);
        }

        public ActionResult SeedTest()
        {
            RecreateDatabase();
            SeedDatabase.Seeds.SeedDatabase.AddUsersAndRoles(Context);
            SeedDatabase.Seeds.SeedDatabase.SeedTestInstance(Context);

            var user = Context.Users.First();

            foreach(var order in Context.Orders.ToList())
            {
                OrderEditHelper.AutorizeOrder(order, user, Context);
                OrderDatabaseHelper.IntoProduction(order, user, Context);
            }

            return RedirectToAction("Index", "Home", null);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}