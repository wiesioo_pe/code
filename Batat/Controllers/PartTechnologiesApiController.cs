﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Controllers
{
    [AllowAnonymous]
    public class PartTechnologiesApiController : ApiController
    {
        private ApplicationDataDbContext ApplicationDataDbContext { get; } = ApplicationDataDbContext.Create();


        // GET api/<controller>
        public IEnumerable<PiotrPP_PartTechnologyVM> Get(string PartUUID = null)
        {
            return ApplicationDataDbContext.Technologies
                .Where(a => a.PartUUID == PartUUID)
                .ToList()
                .Select(b => new PiotrPP_PartTechnologyVM(b, ApplicationDataDbContext));
        }
    }
}