﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class GroupsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<GroupVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Groups
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new GroupVM(b));
        }

        // GET api/<controller>/5
        public GroupVM Get(int id)
        {
            var group = Context.Groups.Find(id);
            var groupVm = new GroupVM(group);
            return groupVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(GroupVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]GroupVM GroupVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji grupy"};
            var groupDb = Context.Groups.Find(GroupVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            GroupEditHelper.CheckIfGroupExists(ref groupDb, user, errorVm);
            int changesCount = groupDb.Changes.Count;

            GroupEditHelper.CheckNameChange(GroupVM, groupDb, user, errorVm);
            GroupEditHelper.CheckCodeChange(GroupVM, groupDb, user, errorVm);
            GroupEditHelper.CheckDescChange(GroupVM, groupDb, user, errorVm);

            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= groupDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            groupDb.LastUpdate = DateTime.Now;

            // ZAPISANIE GRUPY
            if (groupDb.Id == 0)
            {
                Context.Groups.Add(groupDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(groupDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new GroupVM(groupDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(GroupVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]GroupVM GroupVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}