﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using Batat.App_Start.Compression;
using Batat.Models.API.ProductionPanel;
using Batat.Models.Database.DbContexts;

namespace Batat.Controllers
{
    public class OrderInstancesApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        [GzipCompress]
        public IEnumerable<OrderInstanceVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            string[] stringIds = Ids?.Split(',');
            var ids = stringIds
                .Select(a => { _ = int.TryParse(a, out int result); return result; })
                .Where(b => b > 0);
            bool areIdsProvided = ids.Any();

            if (!areIdsProvided)
            {
                ids = Context.OrderInstances
                    .Where(a => a.LastUpdate > lastUpdate)
                    .Select(b => b.Id);
            }

            var query = Context.OrderInstances
                    .Include("ProductionProducts.ProductionParts.ProductionTechnologies")
                    .Include("ProductionProducts.ProductionProducts.ProductionParts.ProductionTechnologies")
                    .Include("ProductionProducts.ProductionProducts.ProductionProducts.ProductionParts.ProductionTechnologies");

            foreach (var id in ids)
            {
                var orderInstance = query.SingleOrDefault(a => a.Id == id);
                if (orderInstance != null)
                {
                    yield return new OrderInstanceVM(orderInstance, Context, areIdsProvided);
                }
            }
        }

        // GET api/<controller>/5
        public OrderInstanceVM Get(int id)
        {
            var orderInstance = Context.OrderInstances.Find(id);
            var orderInstanceVm = new OrderInstanceVM(orderInstance, Context, true);
            return orderInstanceVm;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}