﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class OperationsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<OperationVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if(!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            IEnumerable<Operation> operations;
            if (String.IsNullOrWhiteSpace(Ids) || Ids == "All")
            {
                operations = Context.Operations
                    .Where(a => a.LastUpdate > lastUpdate);
            }
            else
            {
                operations = Ids.Split(',')
                    .Distinct()
                    .Select(a => Context.Operations.Find(a))
                    .Where(b => b != null);
            }

            return operations
                .Select(b => new OperationVM(b));
        }

        // GET api/<controller>/5
        public OperationVM Get(int id)
        {
            var operation = Context.Operations.Find(id);
            var operationVm = new OperationVM(operation);
            return operationVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(OperationVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]OperationAndFilesVM OperationAndFilesVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji operacji"};
            Operation operationDb = Context.Operations.Find(OperationAndFilesVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            OperationEditHelper.CheckIfOperationExists(ref operationDb, user, errorVm);
            int changesCount = operationDb.Changes.Count;

            OperationEditHelper.CheckGroupsChange(OperationAndFilesVM, operationDb, user, Context, errorVm);
            OperationEditHelper.CheckTypeChange(OperationAndFilesVM, operationDb, user, Context, errorVm);
            OperationEditHelper.CheckParametersChange(OperationAndFilesVM, operationDb, user, Context, errorVm);
            OperationEditHelper.CheckToolsChange(OperationAndFilesVM, operationDb, user, Context, errorVm);
            OperationEditHelper.CheckMaterialsChange(OperationAndFilesVM, operationDb, user, Context, errorVm);
            OperationEditHelper.CheckMachinesChange(OperationAndFilesVM, operationDb, user, Context, errorVm);
            OperationEditHelper.CheckNotesChange(OperationAndFilesVM, operationDb, user, errorVm);
            OperationEditHelper.CheckDescChange(OperationAndFilesVM, operationDb, user, errorVm);
            OperationEditHelper.CheckFilesChange(OperationAndFilesVM, operationDb, user, errorVm);
            OperationEditHelper.CheckProcessSheetFileChange(OperationAndFilesVM, operationDb, user, Context, errorVm);

            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= operationDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            operationDb.LastUpdate = DateTime.Now;

            // ZAPISANIE OPERACJI
            if (operationDb.Id == 0)
            {
                Context.Operations.Add(operationDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(operationDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new OperationVM(operationDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(OperationVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]OperationVM OperationVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}