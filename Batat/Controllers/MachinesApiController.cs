﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class MachinesApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<MachineVM> Get(string Ids = null, string LastUpdate = null)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Machines
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new MachineVM(b));
        }

        // GET api/<controller>/5
        public MachineVM Get(int id)
        {
            var machine = Context.Machines.Find(id);
            var machineVm = new MachineVM(machine);
            return machineVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(MachineVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]MachineVM MachineVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji maszyny"};
            Machine machineDb = Context.Machines.Find(MachineVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            MachineEditHelper.CheckIfMachineExists(ref machineDb, user, errorVm);
            int changesCount = machineDb.Changes.Count;

            MachineEditHelper.CheckGroupsChange(MachineVM, machineDb, user, Context, errorVm);
            MachineEditHelper.CheckCodeChange(MachineVM, machineDb, user, errorVm);
            MachineEditHelper.CheckMachineTypeIdChange(MachineVM, machineDb, user, Context, errorVm);
            MachineEditHelper.CheckModelChange(MachineVM, machineDb, user, errorVm);
            MachineEditHelper.CheckManufacturerChange(MachineVM, machineDb, user, errorVm);
            MachineEditHelper.CheckNotesChange(MachineVM, machineDb, user, errorVm);
            MachineEditHelper.CheckParametersChange(MachineVM, machineDb, user, Context, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= machineDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            machineDb.LastUpdate = DateTime.Now;

            // ZAPISANIE MASZYNY
            if (machineDb.Id == 0)
            {
                Context.Machines.Add(machineDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(machineDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new MachineVM(machineDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(MachineVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]MachineVM MachineVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}