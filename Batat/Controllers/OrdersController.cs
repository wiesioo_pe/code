﻿using System.Web.Mvc;
using Batat.Helpers.Extensions.DatabasePanel;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Batat.Controllers
{
    [AllowAnonymous]
    // [SSLHelper.RequreSecureConnectionFilter]
    public class OrdersController : Controller
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        public ActionResult Authorize(int? id)
        {
            Order order = Context.Orders.Find(id);
            if (order != null)
            {
                var userId = User.Identity.GetUserId();
                var user = Context.Users.Find(userId);
                order = OrderEditHelper.AutorizeOrder(order, user, Context);
            }

            var json = JsonConvert.SerializeObject(order, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss" });

            return Content(json, "application/json");
        }

        public ActionResult Unauthorize(int? id)
        {
            Order order = Context.Orders.Find(id);
            if (order != null)
            {
                var userId = User.Identity.GetUserId();
                var user = Context.Users.Find(userId);

                order = OrderEditHelper.UnautorizeOrder(order, user, Context);
            }
            var json = JsonConvert.SerializeObject(order, new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss" });

            return Content(json, "application/json");
        }

        public ActionResult IntoProduction(int? id)
        {
            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            Order order = Context.Orders.Find(id);
            if (order != null)
            {
                order = OrderDatabaseHelper.IntoProduction(order, user, Context);
            }

            var json = JsonConvert.SerializeObject(order, new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss" });
            return Content(json, "application/json");
        }
    }

}
