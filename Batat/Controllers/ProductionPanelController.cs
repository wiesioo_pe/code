using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using Batat.Models.Helpers;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PdfSharp.Pdf;

namespace Batat.Controllers
{
    // [SSLHelper.RequreSecureConnectionFilter]
    [System.Web.Mvc.Authorize(Roles = "Administrator, Manager")]
    public class ProductionPanelController : Controller
    {
        private ApplicationDataDbContext context { get; } = ApplicationDataDbContext.Create();


        public ActionResult Index()
        {
            return File(Server.MapPath("/ProductionPanel/") + "index.htm", "text/html");
        }

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult getPartSheet([FromUri] int OperationBundleId = 0)
        {
            var document = new Document
            {
                Info =
                {
                    Title = "Generated Part Sheet",
                    Subject = "Part Sheet for use in production",
                    Author = "Ipomea"
                }
            };


            MigraDocHelper.DefineStyles(document);
            var documentSection = MigraDocHelper.CreatePageLayout(document, context);
            MigraDocHelper.CreatePartSheetPageContent(documentSection, OperationBundleId, context);
            MigraDocHelper.FillPartSheetContent(document);


            var path = System.Web.HttpContext.Current.Server.MapPath("~/UserFiles/Generated"); //Path

            if (!System.IO.Directory.Exists(path))          //Check if directory exist
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }
            string fileName = "PartSheet_" + "BundleId" + OperationBundleId + ".pdf";
            string wholePath = Path.Combine(path, fileName); //Path with filename
            var pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always) {Document = document};
            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save(wholePath);

            var urlObj = new FileUrlClass()
            {
                FileName = fileName,
                FileType = "application/pdf",
                FileUrl = "/UserFiles/Generated/" + fileName
            };

            var json = JsonConvert.SerializeObject(urlObj, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss" });
            return Content(json, "application/json");
        }

        [System.Web.Mvc.AllowAnonymous]
        public ActionResult GetTrolleysForProductionTechnologyBundle([FromUri] int ProductionTechnologyBundleId)
        {
            var trolleys = this.context.Trolleys.Where(a => a.ProductionTechnologyBundleId == ProductionTechnologyBundleId).ToList().Select(a => new TrolleyVM(a)).ToList();

            var json = JsonConvert.SerializeObject(trolleys, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss" });
            return Content(json, "application/json");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }

            base.Dispose(disposing);
        }

    }

}
