﻿using Batat.Helpers.Extensions.ProductionPanel;
using Batat.Models.API.ProductionPanel;
using Batat.Models.Database.DbContexts;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Batat.Controllers
{
    public class TrolleyController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        [HttpGet]
        [Route("api/Trolley/Remove")]
        public void Remove(int? Id, string UUID)
        {
            TrolleyDatabaseHelper.RemoveTrolley(Id, UUID, Context);
        }

        [HttpPost]
        [Route("api/Trolley/Update")]
        public void Update([FromUri] int? Id, [FromUri] string UUID, [FromBody] TrolleyUpdateDTO trolleyUpdateDTO)
        {
            TrolleyDatabaseHelper.UpdateTrolley(Id, UUID, trolleyUpdateDTO, Context);
        }

        [HttpPost]
        [Route("api/Trolley/Save")]
        public void Save([FromBody] TrolleySaveDTO trolleySaveDTO)
        {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("Endpoint unsupported any more (talk to WIPI for details)") });
        }

    }
}