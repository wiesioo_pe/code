﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Controllers
{
    [AllowAnonymous]
    [AllowCrossSiteJson]
    public class OrderPartApiController : ApiController
    {
        private ApplicationDataDbContext context { get; } = ApplicationDataDbContext.Create();

        // POST api/<controller>
        public IEnumerable<PiotrPP_OrderInstanceVM> Post([FromBody] PiotrPP_OrderPartApiModel OrderPartApiModel, [FromUri] string filterParam = null)
        {
            var orders = new List<PiotrPP_OrderInstanceVM>();

            if (OrderPartApiModel.OrderProductPairs != null && OrderPartApiModel.OrderProductPairs.Count > 0 && !String.IsNullOrEmpty(OrderPartApiModel.PartUUID))
            {
                foreach(var orderProductPair in OrderPartApiModel.OrderProductPairs)
                    if (context.OrderInstances.Any(a => a.OrderUUID == orderProductPair.OrderUUID))
                    {
                    var orderInstance = context.OrderInstances.Single(a => a.OrderUUID == orderProductPair.OrderUUID);
                    orders.Add(new PiotrPP_OrderInstanceVM(orderInstance, OrderPartApiModel.PartUUID, orderProductPair.ProductUUID, context));
                    }
            }
            return orders;
        }

    }
}