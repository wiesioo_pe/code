﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class TechnologiesApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<TechnologyVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Technologies
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new TechnologyVM(b, Context));
        }

        // GET api/<controller>/5
        public TechnologyVM Get(int id)
        {
            var technology = Context.Technologies.Find(id);
            var technologyVm = new TechnologyVM(technology, Context);
            return technologyVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(TechnologyVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]TechnologyVM TechnologyVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji technologii"};
            var technologyDb = Context.Technologies.Find(TechnologyVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            TechnologyEditHelper.CheckIfTechnologyExists(ref technologyDb, user, Context, errorVm);
            int changesCount = technologyDb.Changes.Count;

            TechnologyEditHelper.CheckNameChange(TechnologyVM, technologyDb, user, errorVm);
            TechnologyEditHelper.CheckGroupsChange(TechnologyVM, technologyDb, user, Context, errorVm);
            TechnologyEditHelper.CheckOperationsErrors(TechnologyVM, technologyDb, user, Context, errorVm);
            var didOperationsChange = TechnologyEditHelper.CheckOperationsChangeWithoutSaving(TechnologyVM, technologyDb, Context);
            var didPartChange = TechnologyEditHelper.CheckPartChange(TechnologyVM, technologyDb, user, Context, errorVm);
            TechnologyEditHelper.CheckNotesChange(TechnologyVM, technologyDb, user, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= technologyDb.Changes.Count && didOperationsChange != true)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            technologyDb.LastUpdate = DateTime.Now;

            // ZAPISANIE TECHNOLOGII
            if (technologyDb.Id == 0)
            {
                Context.Technologies.Add(technologyDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(technologyDb).State = EntityState.Modified;
                Context.SaveChanges();
            }


            if (didPartChange)
            {
                TechnologyEditHelper.UpdateTechnologyCode(technologyDb, Context);
                Context.Entry(technologyDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            var areNewOperationsAdded = false;
            if (didOperationsChange)
            {
                areNewOperationsAdded = TechnologyEditHelper.CheckOperationsChange(TechnologyVM, technologyDb,
                    user, Context);
                Context.Entry(technologyDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            if (didPartChange || areNewOperationsAdded)
            {
                TechnologyEditHelper.UpdateOperationsCodes(technologyDb, Context);
            }

            return request.CreateResponse(HttpStatusCode.OK, new TechnologyVM(technologyDb, Context));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(TechnologyVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]TechnologyVM TechnologyVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}