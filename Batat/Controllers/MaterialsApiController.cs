﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class MaterialsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<MaterialVM> Get(string ids, string lastUpdate)
        {
            DateTime defaultLastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(lastUpdate))
            {
                DateTime.TryParseExact(lastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out defaultLastUpdate);
            }

            return Context.Materials
                .Where(a => a.LastUpdate > defaultLastUpdate)
                .ToList()
                .Select(b => new MaterialVM(b));
        }

        // GET api/<controller>/5
        public MaterialVM Get(int id)
        {
            var material = Context.Materials.Find(id);
            var materialVm = new MaterialVM(material);
            return materialVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(MaterialVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]MaterialVM MaterialVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji materiału"};
            Material materialDb = Context.Materials.Find(MaterialVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            MaterialEditHelper.CheckIfMaterialExists(ref materialDb, user, errorVm);
            int changesCount = materialDb.Changes.Count;

            MaterialEditHelper.CheckGroupsChange(MaterialVM, materialDb, user, Context, errorVm);
            MaterialEditHelper.CheckNameChange(MaterialVM, materialDb, user, errorVm);
            MaterialEditHelper.CheckCodeChange(MaterialVM, materialDb, user, errorVm);
            MaterialEditHelper.CheckMaterialTypeIdChange(MaterialVM, materialDb, user, Context, errorVm);
            MaterialEditHelper.CheckDescChange(MaterialVM, materialDb, user, errorVm);
            MaterialEditHelper.CheckParametersChange(MaterialVM, materialDb, user, Context, errorVm);
            MaterialEditHelper.CheckUnitChange(MaterialVM, materialDb, user, errorVm);
            MaterialEditHelper.CheckUnitCostChange(MaterialVM, materialDb, user, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= materialDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            materialDb.LastUpdate = DateTime.Now;

            // ZAPISANIE ZAMÓWIENIA
            if (materialDb.Id == 0)
            {
                Context.Materials.Add(materialDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(materialDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new MaterialVM(materialDb));

        }

        // PUT api/<controller>/5
        [ResponseType(typeof(MaterialVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]MaterialVM MaterialVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}