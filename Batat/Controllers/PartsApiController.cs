﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class PartsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<PartVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Parts
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new PartVM(b));
        }

        // GET api/<controller>/5
        public PartVM Get(int id)
        {
            var part = Context.Parts.Find(id);
            var partVm = new PartVM(part);
            return partVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(PartVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]PartAndFilesVM PartAndFilesVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji detalu"};
            var partDb = Context.Parts.Find(PartAndFilesVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            PartEditHelper.CheckIfPartExists(ref partDb, user, errorVm);
            int changesCount = partDb.Changes.Count;

            PartEditHelper.CheckGroupsChange(PartAndFilesVM, partDb, user, Context, errorVm);
            PartEditHelper.CheckNameChange(PartAndFilesVM, partDb, user, errorVm);
            PartEditHelper.CheckCodeChange(PartAndFilesVM, partDb, user, errorVm);
            PartEditHelper.CheckDescChange(PartAndFilesVM, partDb, user, errorVm);
            PartEditHelper.CheckParametersChange(PartAndFilesVM, partDb, user, Context, errorVm);
            PartEditHelper.CheckFilesChange(PartAndFilesVM, partDb, user, Context, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= partDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            partDb.LastUpdate = DateTime.Now;

            // ZAPISANIE DETALU
            if (partDb.Id == 0)
            {
                Context.Parts.Add(partDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(partDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new PartVM(partDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(PartVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]PartVM PartVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}