﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class FixedCostsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<FixedCostVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.FixedCosts
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new FixedCostVM(b));
        }

        // GET api/<controller>/5
        public FixedCostVM Get(int id)
        {
            var fixedCost = Context.FixedCosts.Find(id);
            var fixedCostVm = new FixedCostVM(fixedCost);
            return fixedCostVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(FixedCostVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]FixedCostVM FixedCostVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji kosztu"};
            var fixedCostDb = Context.FixedCosts.Find(FixedCostVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            FixedCostEditHelper.CheckIfFixedCostExists(ref fixedCostDb, user, errorVm);
            int changesCount = fixedCostDb.Changes.Count;

            FixedCostEditHelper.CheckGroupsChange(FixedCostVM, fixedCostDb, user, Context, errorVm);
            FixedCostEditHelper.CheckCategoryChange(FixedCostVM, fixedCostDb, user, Context, errorVm);
            FixedCostEditHelper.CheckTypeChange(FixedCostVM, fixedCostDb, user, Context, errorVm);
            FixedCostEditHelper.CheckStartDateChange(FixedCostVM, fixedCostDb, user, errorVm);
            FixedCostEditHelper.CheckStopDateChange(FixedCostVM, fixedCostDb, user, errorVm);
            FixedCostEditHelper.CheckAmountChange(FixedCostVM, fixedCostDb, user, errorVm);
            FixedCostEditHelper.CheckPeriodChange(FixedCostVM, fixedCostDb, user, errorVm);
            FixedCostEditHelper.CheckIsRecurringChange(FixedCostVM, fixedCostDb, user, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= fixedCostDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            fixedCostDb.LastUpdate = DateTime.Now;

            // ZAPISANIE KOSZTU
            if (fixedCostDb.Id == 0)
            {
                Context.FixedCosts.Add(fixedCostDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(fixedCostDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new FixedCostVM(fixedCostDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(FixedCostVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]FixedCostVM FixedCostVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}