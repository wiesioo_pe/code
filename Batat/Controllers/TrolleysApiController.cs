﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Controllers
{
    public class TrolleysApiController : ApiController
    {
        private ApplicationDataDbContext ApplicationDataDbContext { get; } = ApplicationDataDbContext.Create();

        [HttpGet]
        public IEnumerable<TrolleyVM> Get(DateTime? StartTime = null, DateTime? EndTime = null, string LastUpdate = null)
        {
            DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var lastUpdate);

            IEnumerable<Trolley> trolleys;
            StartTime = StartTime ?? DateTime.Now;
            EndTime = EndTime ?? DateTime.MaxValue;
            
            trolleys = ApplicationDataDbContext.Trolleys
                .Where(a => (a.PlannedStartTime >= StartTime.Value
                                && a.PlannedStartTime <= EndTime.Value
                                || a.PlannedEndTime >= StartTime.Value
                                && a.PlannedEndTime <= EndTime.Value) && a.LastUpdate > lastUpdate);


            return trolleys.Select(a => new TrolleyVM(a));
        }

        [HttpGet]
        [Route("api/Trolleys/{id}")]
        public TrolleyVM GetSingle(int id)
        {
            var trolley = ApplicationDataDbContext.Trolleys.Find(id);
            return new TrolleyVM(trolley);
        }
    }
}