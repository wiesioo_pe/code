﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class SubjectsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<SubjectVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Subjects
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new SubjectVM(b));
        }

        // GET api/<controller>/5
        public SubjectVM Get(int id)
        {
            var subject = Context.Subjects.Find(id);
            var subjectVm = new SubjectVM(subject);
            return subjectVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(SubjectVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]SubjectAndLogoVM SubjectAndLogoVM)
        {
            var content = Request;
            var errorVm = new ErrorVM {Header = "Błędy walidacji kontrahenta"};
            var subjectDb = Context.Subjects.Find(SubjectAndLogoVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            SubjectEditHelper.CheckIfSubjectExists(ref subjectDb, user, errorVm);
            int changesCount = subjectDb.Changes.Count;

            SubjectEditHelper.CheckGroupsChange(SubjectAndLogoVM, subjectDb, user, Context, errorVm);
            SubjectEditHelper.CheckNameChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckShortNameChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckVatRegNumChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckWwwChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckStreetChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckCityChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckPostalCodeChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckStateChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckPhoneChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckDiscountChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckDescChange(SubjectAndLogoVM, subjectDb, user, errorVm);
            SubjectEditHelper.CheckContactsChange(SubjectAndLogoVM, subjectDb, user, Context, errorVm);
            SubjectEditHelper.CheckLogoChange(SubjectAndLogoVM, subjectDb, user, Context, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= subjectDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            subjectDb.LastUpdate = DateTime.Now;

            // ZAPISANIE KONTRAHENTA
            if (subjectDb.Id == 0)
            {
                Context.Subjects.Add(subjectDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(subjectDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new SubjectVM(subjectDb));

        }

        // PUT api/<controller>/5
        [ResponseType(typeof(SubjectVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]SubjectVM SubjectVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}