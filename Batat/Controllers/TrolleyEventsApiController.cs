﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Controllers
{
    public class TrolleyEventsApiController : ApiController
    {
        private ApplicationDataDbContext context { get; } = ApplicationDataDbContext.Create();

        [HttpPost]
        public TrolleyVM Post([System.Web.Http.FromBody] OperatorTaskEvent OperatorTaskEvent)
        {
            var trolley = context.Trolleys.Find(OperatorTaskEvent.TrolleyId);
            if (trolley == null)
            {
                trolley = context.Trolleys.SingleOrDefault(a => a.UUID == OperatorTaskEvent.TrolleyUUID);

                if (trolley == null)
                {
                    throw new System.Web.Http.HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Wrong Id or UUID") });
                }
            }

            trolley.Events.Add(new TrolleyEventClass { EventType = OperatorTaskEvent.EventType, Note = OperatorTaskEvent.Note, Time = DateTime.Now });
            trolley.LastUpdate = DateTime.Now;
            switch (OperatorTaskEvent.EventType)
            {
                case 300:
                case 1000:
                    if (int.TryParse(OperatorTaskEvent.Note, out var amount))
                    {
                        trolley.MadeAmount += amount;
                    }
                    else
                    {
                        throw new System.Web.Http.HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotAcceptable) { Content = new StringContent("Wrong parameters") });
                    }
                    break;

                // Start wózka
                case 200:
                // Przerwa
                case 210:
                // Zatrzymanie wózka
                case 220:
                // Awaria maszyny
                case 233:
                default:
                    trolley.StateId = OperatorTaskEvent.EventType;
                    break;
            }

            context.Entry(trolley).State = EntityState.Modified;
            context.SaveChanges();

            return new TrolleyVM(trolley);
        }
    }
}