﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class OperatorsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<OperatorVM> Get(string Ids = null, string LastUpdate = null)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Operators
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new OperatorVM(b));
        }

        // GET api/<controller>/5
        public OperatorVM Get(int id)
        {
            var @operator = Context.Operators.Find(id);
            var operatorVm = new OperatorVM(@operator);
            return operatorVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(OperatorVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]OperatorAndPhotoVM OperatorAndPhotoVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji operatora"};
            var operatorDb = Context.Operators.Find(OperatorAndPhotoVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            OperatorEditHelper.CheckIfOperatorExists(ref operatorDb, user, errorVm);
            int changesCount = operatorDb.Changes.Count;

            OperatorEditHelper.CheckGroupsChange(OperatorAndPhotoVM, operatorDb, user, Context, errorVm);
            OperatorEditHelper.CheckNameChange(OperatorAndPhotoVM, operatorDb, user, errorVm);
            OperatorEditHelper.CheckPositionChange(OperatorAndPhotoVM, operatorDb, user, errorVm);
            OperatorEditHelper.CheckFunctionChange(OperatorAndPhotoVM, operatorDb, user, errorVm);
            OperatorEditHelper.CheckHourlyRateChange(OperatorAndPhotoVM, operatorDb, user, errorVm);
            OperatorEditHelper.CheckOvertimeRateChange(OperatorAndPhotoVM, operatorDb, user, errorVm);
            OperatorEditHelper.CheckNotesChange(OperatorAndPhotoVM, operatorDb, user, errorVm);
            OperatorEditHelper.CheckPhotoChange(OperatorAndPhotoVM, operatorDb, user, Context, errorVm);

            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= operatorDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            operatorDb.LastUpdate = DateTime.Now;

            // ZAPISANIE OPERATORA
            if (operatorDb.Id == 0)
            {
                Context.Operators.Add(operatorDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(operatorDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new OperatorVM(operatorDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(OperatorVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]OperatorVM OperatorVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}