﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Batat.App_Start.Compression;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Controllers
{
    public class BoxesApiController : ApiController
    {
        private ApplicationDataDbContext ApplicationDataDbContext { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        [GzipCompress]
        public IEnumerable<BoxVM> Get(string Ids=null, bool? Refresh=null, DateTime? LastUpdate = null, DateTime? StartTime=null, DateTime? EndTime=null)
        {
            LastUpdate = LastUpdate ?? DateTime.MinValue;
            StartTime = StartTime ?? DateTime.Now;
            EndTime = EndTime ?? DateTime.MaxValue;

            var ids = Ids?.Split(',').Where(a => int.TryParse(a, out int parsed) && parsed > 0).Select(b => int.Parse(b));
            if (ids?.Any() == true)
            {
                var boxes = ApplicationDataDbContext.Boxes
                    .Where(a => ids.Any(id => id == a.Id) && a.LastUpdate > LastUpdate)
                    .ToList();

                return boxes.Select(a => new BoxVM(a));
            }
            else
            {
                var boxIds = ApplicationDataDbContext.Trolleys
                .Where(a => (a.PlannedStartTime >= StartTime
                                && a.PlannedStartTime <= EndTime
                                || a.PlannedEndTime >= StartTime
                                && a.PlannedEndTime <= EndTime))
                .SelectMany(b => b.Boxes);

                var boxes = ApplicationDataDbContext.Boxes
                    .Where(a => boxIds.Any(b => b.BoxId == a.Id) && a.LastUpdate > LastUpdate)
                    .ToList();

                return boxes.Select(a => new BoxVM(a));
            }
        }
            

        // GET api/<controller>/5
        public BoxVM Get(int id)
        {
            var box = ApplicationDataDbContext.Boxes.Find(id);
            return new BoxVM(box);
        }
    }
}