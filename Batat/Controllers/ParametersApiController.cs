﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class ParametersApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<ParameterVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Parameters
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new ParameterVM(b));
        }

        // GET api/<controller>/5
        public ParameterVM Get(int id)
        {
            var parameter = Context.Parameters.Find(id);
            var parameterVm = new ParameterVM(parameter);
            return parameterVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(ParameterVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]ParameterVM ParameterVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji parametru"};
            var parameterDb = Context.Parameters.Find(ParameterVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            ParameterEditHelper.CheckIfParameterExists(ref parameterDb, user, errorVm);
            int changesCount = parameterDb.Changes.Count;

            ParameterEditHelper.CheckNameChange(ParameterVM, parameterDb, user, errorVm);
            ParameterEditHelper.CheckUnitChange(ParameterVM, parameterDb, user, errorVm);
            ParameterEditHelper.CheckDescChange(ParameterVM, parameterDb, user, errorVm);

            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= parameterDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            parameterDb.LastUpdate = DateTime.Now;

            // ZAPISANIE PARAMETRU
            if (parameterDb.Id == 0)
            {
                Context.Parameters.Add(parameterDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(parameterDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new ParameterVM(parameterDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(ParameterVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]ParameterVM ParameterVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}