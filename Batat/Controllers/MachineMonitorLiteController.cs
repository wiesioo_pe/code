﻿using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.MachineMonitorLite;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Http;

namespace Batat.Controllers
{
    public class MachineMonitorLiteController : ApiController
    {
        private ApplicationDataDbContext ApplicationDataDbContext { get; } = ApplicationDataDbContext.Create();

        [HttpPost]
        [Route("api/MachineMonitorLite/Status")]
        public DeviceConfigDTO PostStatus([FromBody] MachineStatusDTO machineStatusDTO)
        {
            var requestTime = DateTime.Now;
            if (machineStatusDTO == null)
            {
                return null;
            }

            var machine = ApplicationDataDbContext.Machines.Find(1) ?? new Machine();
            var status = new MachineMonitorLiteStatus
            { 
                StatusJson = JsonConvert.SerializeObject(machineStatusDTO),
                MachineId = machine.Id,
                MachineUUID = machine.UUID
            };

            foreach (var stateDTO in machineStatusDTO.stateData)
            {
                ApplicationDataDbContext.MachineStates.Add(new MachineState(stateDTO, machine.Id, machine.UUID, requestTime, machineStatusDTO.conTimestamp));
            }

            ApplicationDataDbContext.MachineMonitoLiteStatuses.Add(status);
            ApplicationDataDbContext.SaveChangesAsync();
            return new DeviceConfigDTO
            {
                configNumber = 14,
                firmware = 1043,
                ssid1 = "FunBox3-C7E0",
                pass1 = "5KCQJ66U7G7Y",
                ssid2 = "SmartRot-9F164A",
                pass2 = "internet123",
                serwer = @"http://batat.webserwer.pl/api/MachineMonitorLite/Status",
                timeoutWifi = 15000,
                timeoutSerwer = 3000,
                connectionDelay = 2000,
                connectionLimit = 3,
                interval = 500,
                counter1_enable = 1,
                counter2_enable = 1
            };
        }

        [HttpGet]
        [Route("api/MachineMonitorLite/Status/Last")]
        public string GetLastStatus()
        {
            return ApplicationDataDbContext.MachineMonitoLiteStatuses.OrderByDescending(a => a.Id).FirstOrDefault()?.StatusJson;
        }

        [HttpGet]
        [Route("api/MachineMonitorLite/State/Last")]
        public MachineState GetLastState()
        {
            return ApplicationDataDbContext.MachineStates.OrderByDescending(a => a.Time).FirstOrDefault();
        }
    }
}