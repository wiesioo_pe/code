﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Batat.Helpers.Extensions.ProductionPanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;

namespace Batat.Controllers
{
    [AllowAnonymous]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TechnologyPickApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // POST api/<controller>
        public List<ProductionTechnology> Post([FromBody]TechnologyPickVM TechnologyPickVM)
        {
            return TechnologyPickDatabaseHelper.PickTechnology(TechnologyPickVM, Context);
        }

        [HttpDelete]
        [Route("api/TechnologyPickApi/{UUID}")]
        // DELETE api/<controller>
        public void Delete(string UUID)
        {
            TechnologyPickDatabaseHelper.DeletePickedTechnology(UUID, Context);
        }
    }
}