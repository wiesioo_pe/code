﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class ContactsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<ContactVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Contacts
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new ContactVM(b));
        }

        // GET api/<controller>/5
        public ContactVM Get(int id)
        {
            var contact = Context.Contacts.Find(id);
            return new ContactVM(contact);
        }

        // POST api/<controller>
        [ResponseType(typeof(ContactVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]ContactAndPhotoVM ContactAndPhotoVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji kontaktu"};

            var contactDb = Context.Contacts.Find(ContactAndPhotoVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            ContactEditHelper.CheckIfContactExists(ref contactDb, user, errorVm);
            int changesCount = contactDb.Changes.Count;

            ContactEditHelper.CheckGroupsChange(ContactAndPhotoVM, contactDb, user, Context, errorVm);
            ContactEditHelper.CheckNameChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckSubjectChange(ContactAndPhotoVM, contactDb, user, Context, errorVm);
            ContactEditHelper.CheckDivisionChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckPositionChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckOfficePhoneChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckBusinessPhoneChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckPrivatePhoneChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckFaxChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckeMailChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckDescChange(ContactAndPhotoVM, contactDb, user, errorVm);
            ContactEditHelper.CheckPhotoChange(ContactAndPhotoVM, contactDb, user, Context, errorVm);


            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= contactDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            contactDb.LastUpdate = DateTime.Now;

            // ZAPISANIE KONTAKTU
            if (contactDb.Id == 0)
            {
                Context.Contacts.Add(contactDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(contactDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new ContactVM(contactDb));

        }

        // PUT api/<controller>/5
        [ResponseType(typeof(ContactVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]ContactVM ContactVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}