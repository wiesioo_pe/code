﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrdersApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<OrderVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Orders
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new OrderVM(b));
        }

        // GET api/<controller>/5
        public OrderVM Get(int id)
        {
            var order = Context.Orders.Find(id);
            var orderVm = new OrderVM(order);
            return orderVm;
        }

        // POST api/<controller>
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]OrderVM OrderVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji zamówienia"};
            List<ComplaintClass> complaintsToAdd = new List<ComplaintClass>();

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            var orderDb = Context.Orders.Find(OrderVM.Id);
            OrderEditHelper.CheckIfOrderExists(OrderVM, ref orderDb, user, errorVm);
            int changesCount = orderDb.Changes.Count;

            switch (orderDb.StateId)
            {
                case 0:
                    OrderEditHelper.CheckGroupsChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckReceiveDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckAccomplishDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckDeliveryDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckPriorityChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckSubjectChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckContactChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckDescChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckEmployeeChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckProductsChange(OrderVM, orderDb, user, Context, errorVm);
                    break;
                case 1:
                    OrderEditHelper.CheckGroupsChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckAccomplishDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckDeliveryDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckPriorityChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckContactChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckDescChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckEmployeeChange(OrderVM, orderDb, user, Context, errorVm);
                    break;
                case 2:
                    OrderEditHelper.CheckGroupsChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckAccomplishDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckDeliveryDateChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckPriorityChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckContactChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckDescChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckEmployeeChange(OrderVM, orderDb, user, Context, errorVm);
                    break;
                case 3:
                    OrderEditHelper.CheckGroupsChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckEmployeeChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckContactChange(OrderVM, orderDb, user, Context, errorVm);
                    OrderEditHelper.CheckDescChange(OrderVM, orderDb, user, errorVm);
                    OrderEditHelper.CheckComplaintsChange(OrderVM, orderDb, complaintsToAdd, user, Context, errorVm);
                    break;
            }

            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= orderDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            orderDb.LastUpdate = DateTime.Now;

            // ZAPISANIE ZAMÓWIENIA
            if (orderDb.Id == 0)
            {
                Context.Orders.Add(orderDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(orderDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            OrderEditHelper.SaveNewComplaints(orderDb, complaintsToAdd, user,
                Context);

            return request.CreateResponse(HttpStatusCode.OK, new OrderVM(orderDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(OrderVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]OrderVM OrderVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}