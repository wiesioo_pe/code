﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Batat.Models;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Helpers.DatabasePanel;
using Microsoft.AspNet.Identity;

namespace Batat.Controllers
{
    public class ToolsApiController : ApiController
    {
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        // GET api/<controller>
        public IEnumerable<ToolVM> Get(string Ids, string LastUpdate)
        {
            DateTime lastUpdate = DateTime.MinValue;
            if (!String.IsNullOrWhiteSpace(LastUpdate))
            {
                DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastUpdate);
            }

            return Context.Tools
                .Where(a => a.LastUpdate > lastUpdate)
                .ToList()
                .Select(b => new ToolVM(b));
        }

        // GET api/<controller>/5
        public ToolVM Get(int id)
        {
            var tool = Context.Tools.Find(id);
            var toolVm = new ToolVM(tool);
            return toolVm;
        }

        // POST api/<controller>
        [ResponseType(typeof(ToolVM))]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]ToolVM ToolVM)
        {
            var errorVm = new ErrorVM {Header = "Błędy walidacji narzędzia"};
            Tool toolDb = Context.Tools.Find(ToolVM.Id);

            var userId = User.Identity.GetUserId();
            var user = Context.Users.Find(userId);

            ToolEditHelper.CheckIfToolExists(ref toolDb, user, errorVm);
            int changesCount = toolDb.Changes.Count;

            ToolEditHelper.CheckGroupsChange(ToolVM, toolDb, user, Context, errorVm);
            ToolEditHelper.CheckNameChange(ToolVM, toolDb, user, errorVm);
            ToolEditHelper.CheckCodeChange(ToolVM, toolDb, user, errorVm);
            ToolEditHelper.CheckToolTypeIdChange(ToolVM, toolDb, user, Context, errorVm);
            ToolEditHelper.CheckDescChange(ToolVM, toolDb, user, errorVm);
            ToolEditHelper.CheckParametersChange(ToolVM, toolDb, user, Context, errorVm);
            ToolEditHelper.CheckUnitCostChange(ToolVM, toolDb, user, errorVm);

            if (errorVm.Messages.Count != 0)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            if (changesCount >= toolDb.Changes.Count)
            {
                errorVm.Header = "Brak zmian";
                errorVm.Messages.Add("Przesłany element nie zawierał żadnych zmian");
                return request.CreateResponse(HttpStatusCode.BadRequest, errorVm);
            }

            toolDb.LastUpdate = DateTime.Now;

            // ZAPISANIE NARZĘDZIA
            if (toolDb.Id == 0)
            {
                Context.Tools.Add(toolDb);
                Context.SaveChanges();
            }
            else
            {
                Context.Entry(toolDb).State = EntityState.Modified;
                Context.SaveChanges();
            }

            return request.CreateResponse(HttpStatusCode.OK, new ToolVM(toolDb));
        }

        // PUT api/<controller>/5
        [ResponseType(typeof(ToolVM))]
        public HttpResponseMessage Put(HttpRequestMessage request, int id, [FromBody]ToolVM ToolVM)
        {
            var errorVm = new ErrorVM {Header = "Nie zaimplementowano"};
            return request.CreateResponse(HttpStatusCode.NotImplemented, errorVm);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}