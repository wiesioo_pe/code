﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Batat.Models;
using Batat.Models.API;
using Batat.Models.API.ProductionPanel;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace Batat.Controllers
{
    [AllowAnonymous]
    public class InfrastructureController : Controller
    {
        private ViewSettingsDbContext ViewSettingsDbContext { get; } = ViewSettingsDbContext.Create();
        private ApplicationDataDbContext Context { get; } = ApplicationDataDbContext.Create();

        [AllowAnonymous]
        public ActionResult spaOrderStates()
        {
            var orderStates = new List<OrderStateVM>();
            foreach (var state in OrdersLists.State)
            {
                orderStates.Add(new OrderStateVM(OrdersLists.State.IndexOf(state), state));
            }

            var json = Json(orderStates, JsonRequestBehavior.AllowGet);
            return json;
        }

        [AllowAnonymous]
        public ActionResult spaOperationTypes()
        {
            var operationTypes = Context.OperationTypes
                .ToList()
                .Select(operationType => new OperationTypeVM(operationType)).ToList();

            var json = Json(operationTypes, JsonRequestBehavior.AllowGet);
            return json;
        }

        [AllowAnonymous]
        public ActionResult spaMachineTypes()
        {
            var machineTypes = Context.MachineTypes
                .ToList()
                .Select(a => new MachineTypeVM(a));

            var json = Json(machineTypes, JsonRequestBehavior.AllowGet);
            return json;
        }

        [AllowAnonymous]
        public ActionResult spaToolTypes()
        {
            var toolTypes = Context.ToolTypes;
            var json = Json(toolTypes, JsonRequestBehavior.AllowGet);
            return json;
        }

        [AllowAnonymous]
        public ActionResult spaMaterialTypes()
        {
            var materialTypes = Context.MaterialTypes;
            var json = Json(materialTypes, JsonRequestBehavior.AllowGet);
            return json;
        }


        [HttpGet]
        public ActionResult spaViewSettings(string Name)
        {
            var settings = new ViewSettings();
            if (ViewSettingsDbContext.ViewSettings.Any(a => a.UserName == User.Identity.GetUserId() && a.Name == Name))
            {
                settings = ViewSettingsDbContext.ViewSettings.Single(a => a.UserName == User.Identity.GetUserId() && a.Name == Name);
            }

            return Content(settings.Json);
        }

        [HttpPost]
        public ActionResult spaViewSettings([System.Web.Http.FromUri] string Name, [System.Web.Http.FromBody] ViewSettings ViewSettings)
        {
            HttpContext.Request.InputStream.Position = 0;
            var postedSettingsJson = new System.IO.StreamReader(HttpContext.Request.InputStream).ReadToEnd();

            if (ViewSettingsDbContext.ViewSettings.Any(a => a.UserName == User.Identity.GetUserId() && a.Name == Name))
            {
                var settings = ViewSettingsDbContext.ViewSettings.Single(a => a.UserName == User.Identity.GetUserId() && a.Name == Name);
                settings.Json = postedSettingsJson;
                ViewSettingsDbContext.Entry(settings).State = EntityState.Modified;
                ViewSettingsDbContext.SaveChanges();
            }
            else
            {
                var settings = new ViewSettings
                {
                    Name = Name,
                    UserName = User.Identity.GetUserId(),
                    Json = postedSettingsJson
                };
                ViewSettingsDbContext.ViewSettings.Add(settings);
                ViewSettingsDbContext.SaveChanges();
            }

            /* Read as dynamic data
            dynamic data = System.Web.Helpers.Json.Decode(result);
            var OrdersIndexSettingsVM = new OrdersIndexSettingsVM(data);
            
            if (ViewSettingsDbContext.OrdersIndexSettings.Where(a => a.UserName == User.Identity.GetUserId()).Any())
            {
                var OrdersIndexSettingsDB = ViewSettingsDbContext.OrdersIndexSettings.Where(a => a.UserName == User.Identity.GetUserId()).Single();
                ViewSettingsHelper.SetOrderIndexSettingsDB(ref OrdersIndexSettingsDB, OrdersIndexSettingsVM);
                ViewSettingsDbContext.Entry(OrdersIndexSettingsDB).State = EntityState.Modified;
                ViewSettingsDbContext.SaveChanges();
            }
            else
            {
                var SettingsDB = new OrdersIndexSettingsDB(OrdersIndexSettingsVM, User.Identity.GetUserId());
                ViewSettingsDbContext.OrdersIndexSettings.Add(SettingsDB);
                ViewSettingsDbContext.SaveChanges();
            }
            */
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult spaFixedCostCategories()
        {
            var fixedCostCategories = Context.FixedCostCategories
                .ToList()
                .Select(fixedCostCategory => new FixedCostCategoryVM(fixedCostCategory));

            var json = Json(fixedCostCategories, JsonRequestBehavior.AllowGet);
            return json;
        }

        [AllowAnonymous]
        public ActionResult spaFixedCostTypes()
        {
            var fixedCostTypes = Context.FixedCostTypes
                    .ToList()
                    .Select(fixedCostType => new FixedCostTypeVM(fixedCostType));

            var json = Json(fixedCostTypes, JsonRequestBehavior.AllowGet);
            return json;
        }


        [HttpPost]
        public ActionResult GetNewestItems([System.Web.Http.FromUri] string LastUpdate, [System.Web.Http.FromBody] string[] ItemsNames)
        {
            DateTime.TryParseExact(LastUpdate, @"yyyy-MM-dd\THH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var LastUpdateDateTime);
            var json = "{";
            foreach (var itemsName in ItemsNames)
            {
                switch (itemsName)
                {
                    case "Orders":
                        if (json.Length > 1) { json += ","; }
                        var newOrdersDB = Context.Orders.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newOrdersVM = newOrdersDB.Select(newOrderDB => new OrderVM(newOrderDB)).ToList();

                        json += "\"Orders\":" + JsonConvert.SerializeObject(newOrdersVM);
                        break;

                    case "Products":
                        var newProductsDB = Context.Products.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newProductsVM =
                            newProductsDB.Select(newProductDB => new ProductVM(newProductDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Products\":" + JsonConvert.SerializeObject(newProductsVM);
                        break;

                    case "Parts":
                        var newPartsDB = Context.Parts.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newPartsVM = newPartsDB.Select(newPartDB => new PartVM(newPartDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Parts\":" + JsonConvert.SerializeObject(newPartsVM);
                        break;

                    case "Parameters":
                        var newParametersDB = Context.Parameters.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newParametersVM =
                            newParametersDB.Select(newParameterDB => new ParameterVM(newParameterDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Parameters\":" + JsonConvert.SerializeObject(newParametersVM);
                        break;

                    case "Groups":
                        var newGroupsDB = Context.Groups.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newGroupsVM =
                            newGroupsDB.Select(newGroupDB => new GroupVM(newGroupDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Groups\":" + JsonConvert.SerializeObject(newGroupsVM);
                        break;

                    case "Operations":
                        var newOperationsDB = Context.Operations.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newOperationsVM =
                            newOperationsDB.Select(newOperationDB => new OperationVM(newOperationDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Operations\":" + JsonConvert.SerializeObject(newOperationsVM);
                        break;

                    case "Technologies":
                        var newTechnologiesDB = Context.Technologies.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newTechnologiesVM = new List<TechnologyVM>();
                        foreach (var newTechnologyDB in newTechnologiesDB)
                        {
                            newTechnologiesVM.Add(new TechnologyVM(newTechnologyDB, Context));
                        }

                        if (json.Length > 1) { json += ","; }
                        json += "\"Technologies\":" + JsonConvert.SerializeObject(newTechnologiesVM);
                        break;

                    case "Operators":
                        var newOperatorsDB = Context.Operators.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newOperatorsVM =
                            newOperatorsDB.Select(newOperatorDB => new OperatorVM(newOperatorDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Operators\":" + JsonConvert.SerializeObject(newOperatorsVM);
                        break;

                    case "Machines":
                        var newMachinesDB = Context.Machines.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newMachinesVM =
                            newMachinesDB.Select(newMachineDB => new MachineVM(newMachineDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Machines\":" + JsonConvert.SerializeObject(newMachinesVM);
                        break;

                    case "Subjects":
                        var newSubjectsDB = Context.Subjects.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newSubjectsVM =
                            newSubjectsDB.Select(newSubjectDB => new SubjectVM(newSubjectDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Subjects\":" + JsonConvert.SerializeObject(newSubjectsVM);
                        break;

                    case "Contacts":
                        var newContactsDB = Context.Contacts.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newContactsVM =
                            newContactsDB.Select(newContactDB => new ContactVM(newContactDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Contacts\":" + JsonConvert.SerializeObject(newContactsVM);
                        break;

                    case "Tools":
                        var newToolsDB = Context.Tools.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newToolsVM = newToolsDB.Select(newToolDB => new ToolVM(newToolDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Tools\":" + JsonConvert.SerializeObject(newToolsVM);
                        break;

                    case "Materials":
                        var newMaterialsDB = Context.Materials.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newMaterialsVM =
                            newMaterialsDB.Select(newMaterialDB => new MaterialVM(newMaterialDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Materials\":" + JsonConvert.SerializeObject(newMaterialsVM);
                        break;

                    case "FixedCosts":
                        var newFixedCostsDB = Context.FixedCosts.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newFixedCostsVM =
                            newFixedCostsDB.Select(newFixedCostDB => new FixedCostVM(newFixedCostDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"FixedCosts\":" + JsonConvert.SerializeObject(newFixedCostsVM);
                        break;

                    case "OrderInstances":
                        var newOrderInstancesDB = Context.OrderInstances.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newOrderInstancesVM =
                            newOrderInstancesDB.Select(newOrderInstanceDB => new OrderInstanceVM(newOrderInstanceDB, Context, true)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"OrderInstances\":" + JsonConvert.SerializeObject(newOrderInstancesVM);
                        break;

                    case "Boxes":
                        var newBoxesDB = Context.Boxes.Where(a => a.LastUpdate > LastUpdateDateTime).ToArray();
                        var newBoxesVM =
                            newBoxesDB.Select(newBoxDB => new BoxVM(newBoxDB)).ToList();

                        if (json.Length > 1) { json += ","; }
                        json += "\"Boxes\":" + JsonConvert.SerializeObject(newBoxesVM);
                        break;
                }
            }

            json += "}";
            return Content(json, "application/json");
        }

        // Zwraca ProductionTechnologiesVM bez listy ProductionOperations
        public ActionResult ProductionTechnologiesShort()
        {
            var productionTechnologiesShort = new List<ProductionTechnologyVM>();
            foreach (var productionTechnology in Context.ProductionTechnologies)
            {
                productionTechnologiesShort.Add(new ProductionTechnologyVM(productionTechnology, Context));
            }
            var json = Json(productionTechnologiesShort, JsonRequestBehavior.AllowGet);
            return json;
        }

        public ActionResult OffHours([System.Web.Http.FromUri] string FromTime, [System.Web.Http.FromUri] string ToTime)
        {
            DateTime.TryParseExact(FromTime, @"yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var from);
            DateTime.TryParseExact(ToTime, @"yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var to);

            var offHours = new List<(string, string, string)>()
            {
                ("Monday", "16:00:00", "16:00:00"),
                ("Tuesday", "16:00:00", "16:00:00"),
                ("Wednesday", "16:00:00", "16:00:00"),
                ("Thursday", "16:00:00", "16:00:00"),
                ("Friday", "16:00:00", "64:00:00")
            };
            
            var json = Json(offHours, JsonRequestBehavior.AllowGet);
            return json;
        }

        public FilePathResult GetHtmlFile(string name)
        {
            return File(name, "text/html");
        }
    }
}