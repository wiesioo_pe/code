﻿using System.Web.Mvc;
using Batat.Models.Database.DbContexts;

namespace Batat.Controllers
{
    // [SSLHelper.RequreSecureConnectionFilter]
    [Authorize(Roles = "Administrator, Office, Manager")]
    public class DatabasePanelController : Controller
    {
        private ApplicationDataDbContext ApplicationDataDbContext { get; } = ApplicationDataDbContext.Create();


        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                ApplicationDataDbContext.Dispose();
            }

            base.Dispose(disposing);
        }

    }

}
