﻿using System;

namespace Batat.Helpers.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool Between(this DateTime input, DateTime date1, DateTime date2)
        {
            return input >= date1 && input <= date2;
        }
    }
}
