﻿using Batat.Models.API.ProductionPanel;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using Batat.Models.Database.ProductionPanel.Extensions;
using Batat.Models.ProductionPanel.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Batat.Helpers.Extensions.ProductionPanel
{
    public static class TrolleyDatabaseHelper
    {
        public static void UpdateTrolley(int? Id, string UUID, TrolleyUpdateDTO trolleyUpdateDTO, ApplicationDataDbContext Context)
        {
            if (trolleyUpdateDTO == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NoContent) { Content = new StringContent("No data in body") });
            }

            var trolley = GetTrolley(Id, UUID, Context);
            var machine = GetMachine(trolleyUpdateDTO.MachineId, trolleyUpdateDTO.MachineUUID, Context);
            var operation = Context.Operations.Find(trolley.OperationId);

            UpdateMachineAndPlannedTime(trolley, operation, machine, trolleyUpdateDTO.PlannedStartTime, Context);
            trolley.StateId = 100;

            foreach (var boxId in trolley.Boxes.ToList())
            {
                var box = Context.Boxes.Find(boxId.BoxId);
                var orderInstance = Context.OrderInstances.Find(box.OrderInstanceId);
                var productionProductInHeirarchy = orderInstance.GetProductionProductInHierarchy(box.ProductionProductUUID);
                var productionProduct = productionProductInHeirarchy.ProductionProduct;
                var productionProductFather = productionProductInHeirarchy.ProductionProductFather;
                var productionProductGrandfather = productionProductInHeirarchy.ProductionProductGrandfather;
                var productionPart = productionProduct.ProductionParts.Single(a => a.Id == box.ProductionPartId);
                var productionTechnology = productionPart.ProductionTechnologies.Single(a => a.Id == box.ProductionTechnologyId);
                var productionOperation = productionTechnology.ProductionOperations.Single(a => a.Id == box.ProductionOperationId);
                List<Box> boxes = productionOperation.Boxes.Select(a => Context.Boxes.Find(a.BoxId)).ToList();
                
                productionOperation.UpdateMadeAndPlannedAmounts(boxes);
                Context.Entry(productionOperation).State = EntityState.Modified;
                productionTechnology.UpdateMadeAndPlannedAmounts();
                Context.Entry(productionTechnology).State = EntityState.Modified;
                productionPart.UpdateMadeAndPlannedAmounts();
                Context.Entry(productionPart).State = EntityState.Modified;
                productionProduct.UpdateMadeAndPlannedAmounts();
                Context.Entry(productionProduct).State = EntityState.Modified;
                if (productionProductFather != null)
                {
                    productionProductFather.UpdateMadeAndPlannedAmounts();
                    Context.Entry(productionProductFather).State = EntityState.Modified;
                }

                if (productionProductGrandfather != null)
                {
                    productionProductGrandfather.UpdateMadeAndPlannedAmounts();
                    Context.Entry(productionProductGrandfather).State = EntityState.Modified;
                }

                orderInstance.UpdateMadeAndPlannedAmounts();
                Context.Entry(orderInstance).State = EntityState.Modified;
            }

            Context.Entry(trolley).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public static Trolley SaveTrolley(TrolleySaveDTO trolleySaveDTO, ApplicationDataDbContext Context)
        {
            if (trolleySaveDTO == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NoContent) { Content = new StringContent("No data in body") });
            }

            var machine = GetMachine(trolleySaveDTO.MachineId, trolleySaveDTO.MachineUUID, Context);
            var bundle = GetBundle(trolleySaveDTO.ProductionTechnologyBundleId, trolleySaveDTO.ProductionTechnologyBundleUUID, Context);
            var operation = GetOperation(trolleySaveDTO.OperationId, trolleySaveDTO.OperationUUID, Context);

            if (Context.Trolleys.Any(a => a.ProductionTechnologyBundleId == bundle.Id
                    && a.OperationId == operation.Id))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict) { Content = new StringContent("Trolley already exists") });
            }

            return CreateTrolley(bundle, machine, operation, trolleySaveDTO.PlannedStartTime, Context);
        }

        public static void RemoveTrolley(int? Id, string UUID, ApplicationDataDbContext Context)
        {
            var trolley = GetTrolley(Id, UUID, Context);
            List<int> BoxesIds = trolley.Boxes.Select(a => a.BoxId).ToList();

            foreach (int boxIds in BoxesIds)
            {
                Box box = Context.Boxes.Find(boxIds);

                OrderInstance orderInstance = Context.OrderInstances.Find(box.OrderInstanceId);
                if (orderInstance == null)
                {
                    continue;
                }

                ProductionProduct productionProduct = null;
                ProductionProduct productionProductFather = null;
                ProductionProduct productionProductGrandfather = null;
                if (orderInstance.ProductionProducts.All(a => a.Id != box.ProductionProductId))
                {
                    if (orderInstance.ProductionProducts.All(a => a.ProductionProducts.All(b => b.Id != box.ProductionProductId)))
                    {
                        productionProductGrandfather = orderInstance.ProductionProducts.Single(a => a.ProductionProducts.Any(b => b.ProductionProducts.Any(c => c.Id == box.ProductionProductId)));
                        productionProductFather = productionProductGrandfather.ProductionProducts.Single(a => a.ProductionProducts.Any(b => b.Id == box.ProductionProductId));
                    }
                    else
                    {
                        productionProductFather = orderInstance.ProductionProducts.Single(a => a.ProductionProducts.Any(b => b.Id == box.ProductionProductId));
                    }

                    productionProduct = productionProductFather.ProductionProducts.Single(a => a.Id == box.ProductionProductId);
                }
                else
                {
                    productionProduct = orderInstance.ProductionProducts.Single(a => a.Id == box.ProductionProductId);
                }

                ProductionPart productionPart = productionProduct.ProductionParts.Single(a => a.Id == box.ProductionPartId);
                ProductionTechnology productionTechnology = productionPart.ProductionTechnologies.Single(a => a.Id == box.ProductionTechnologyId);
                ProductionOperation productionOperation = productionTechnology.ProductionOperations.Single(a => a.Id == box.ProductionOperationId);

                // Update amounts
                productionOperation.PlannedPercent = (Math.Round(productionOperation.ToDoAmount * productionOperation.PlannedPercent / 100) - box.ToDoAmount) / productionOperation.ToDoAmount * 100;
                productionOperation.MadeAmount -= box.MadeAmount;

                productionTechnology.UpdateMadeAndPlannedAmounts();
                productionPart.UpdateMadeAndPlannedAmounts();
                productionProduct.UpdateMadeAndPlannedAmounts();
                productionProductFather?.UpdateMadeAndPlannedAmounts();
                productionProductGrandfather?.UpdateMadeAndPlannedAmounts();
                orderInstance.UpdateMadeAndPlannedAmounts();

                Context.Entry(orderInstance).State = EntityState.Modified;
            }

            trolley.StateId = 0;
            trolley.MachineId = 0;
            trolley.MachineUUID = null;
            trolley.PlannedStartTime = null;
            trolley.PlannedEndTime = null;
            trolley.isMachineTimeTaken = false;
            trolley.LastUpdate = DateTime.Now;

            Context.Entry(trolley).State = EntityState.Modified;

            Context.SaveChanges();
        }


        private static Trolley GetTrolley(int? Id, string UUID, ApplicationDataDbContext Context)
        {
            if (Id == null || Id == 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(Id)) });
            }

            if (string.IsNullOrWhiteSpace(UUID) || !Guid.TryParse(UUID, out Guid _))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(UUID)) });
            }

            var trolley = Context.Trolleys.Find(Id);
            if (trolley == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(nameof(Id)) });
            }
            else if (trolley.UUID != UUID)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict) { Content = new StringContent(nameof(UUID)) });
            }

            return trolley;
        }

        private static Machine GetMachine(int machineId, string machineUUID, ApplicationDataDbContext Context)
        {
            if (machineId == 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleyUpdateDTO.MachineId)) });
            }

            if (string.IsNullOrWhiteSpace(machineUUID) || !Guid.TryParse(machineUUID, out Guid _))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleyUpdateDTO.MachineUUID)) });
            }

            var machine = Context.Machines.Find(machineId);
            if (machine == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(nameof(TrolleyUpdateDTO.MachineId)) });
            }

            if (machine.UUID != machineUUID)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict) { Content = new StringContent(nameof(TrolleyUpdateDTO.MachineUUID)) });
            }

            return machine;
        }

        private static ProductionTechnologyBundle GetBundle(int productionTechnologyBundleId, string productionTechnologyBundleUUID, ApplicationDataDbContext Context)
        {
            if (productionTechnologyBundleId == 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleySaveDTO.ProductionTechnologyBundleId)) });
            }

            if (string.IsNullOrWhiteSpace(productionTechnologyBundleUUID) || !Guid.TryParse(productionTechnologyBundleUUID, out Guid _))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleySaveDTO.ProductionTechnologyBundleUUID)) });
            }

            var bundle = Context.ProductionTechnologyBundles.Find(productionTechnologyBundleId);
            if (bundle == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(nameof(TrolleySaveDTO.ProductionTechnologyBundleId)) });
            }

            if (bundle.UUID != productionTechnologyBundleUUID)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict) { Content = new StringContent(nameof(TrolleySaveDTO.ProductionTechnologyBundleUUID)) });
            }

            return bundle;
        }

        private static Operation GetOperation(int operationId, string operationUUID, ApplicationDataDbContext Context)
        {
            if (operationId == 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleySaveDTO.OperationId)) });
            }

            if (string.IsNullOrWhiteSpace(operationUUID) || !Guid.TryParse(operationUUID, out Guid _))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleySaveDTO.OperationUUID)) });
            }

            var operation = Context.Operations.Find(operationId);
            if (operation == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(nameof(TrolleySaveDTO.OperationId)) });
            }

            if (operation.UUID != operationUUID)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict) { Content = new StringContent(nameof(TrolleySaveDTO.OperationUUID)) });
            }

            return operation;
        }


        private static Trolley CreateTrolley(ProductionTechnologyBundle bundle, Machine machine, Operation operation, DateTime plannedStartTime, ApplicationDataDbContext Context)
        {
            var trolley = new Trolley
            {
                Id = Context.Trolleys.Count() + 1,
                OperationId = operation.Id,
                OperationUUID = operation.UUID,
                TechnologyId = operation.TechnologyId,
                TechnologyUUID = operation.TechnologyUUID,
                PartId = operation.PartId,
                PartUUID = operation.PartUUID
            };

            var productionTechnologyIds = bundle.ProductionTechnologies.Select(a => a.ProductionTechnologyId);
            var productionOperations = Context.ProductionOperations.Where(a => productionTechnologyIds.Any(b => b == a.ProductionTechnologyId) && a.OperationId == operation.Id).ToList();

            var createdBoxes = new List<Box>();
            for (var i = 0; i < productionOperations.Count; i++)
            {
                var productionOperation = productionOperations[i];
                var productionTechnology = Context.ProductionTechnologies.Find(productionOperation.ProductionTechnologyId);
                var orderInstance = Context.OrderInstances.Find(productionTechnology.OrderInstanceId);
                var productionPart = Context.ProductionParts.Find(productionTechnology.ProductionPartId);
                var productionProduct = Context.ProductionProducts.Find(productionPart.ProductionProductId);
                bool isOperationFirstInTrolley = productionOperations.First() == productionOperation;
                var box = new Box
                {
                    OrderId = orderInstance.OrderId,
                    OrderUUID = orderInstance.UUID,
                    PartId = productionPart.PartId,
                    PartUUID = productionPart.PartUUID,
                    ProductId = productionProduct.ProductId,
                    ProductUUID = productionProduct.ProductUUID,
                    TechnologyId = productionTechnology.TechnologyId,
                    TechnologyUUID = productionTechnology.TechnologyUUID,
                    OperationId = productionOperation.OperationId,
                    OperationUUID = productionOperation.OperationUUID,
                    OrderInstanceId = orderInstance.Id,
                    OrderInstanceUUID = orderInstance.UUID,
                    ProductionPartId = productionPart.Id,
                    ProductionPartUUID = productionPart.UUID,
                    ProductionProductId = productionProduct.Id,
                    ProductionProductUUID = productionProduct.UUID,
                    ProductionTechnologyId = productionTechnology.Id,
                    ProductionTechnologyUUID = productionTechnology.UUID,
                    ProductionOperationId = productionOperation.Id,
                    ProductionOperationUUID = productionOperation.UUID,
                    ToDoAmount = productionOperation.ToDoAmount,
                    MadeAmount = 0,
                    TrolleyId = trolley.Id,
                    TrolleyUUID = trolley.UUID,
                    OperationBundleOrder = i + 1
                };

                Context.Boxes.Add(box);
                Context.Entry(box).State = EntityState.Added;
                Context.SaveChanges();
                createdBoxes.Add(box);
            }

            trolley.ToDoAmount = createdBoxes.Sum(a => a.ToDoAmount);
            trolley.Boxes.AddRange(createdBoxes.Select(a => new BoxIdClass(a.Id, a.UUID)));
            UpdateMachineAndPlannedTime(trolley, operation, machine, plannedStartTime, Context);

            Context.Trolleys.Add(trolley);
            Context.Entry(trolley).State = EntityState.Added;
            Context.SaveChanges();

            return trolley;
        }


        private static void UpdateMachineAndPlannedTime(Trolley trolley, Operation operation, Machine machine, DateTime plannedStartTime, ApplicationDataDbContext Context)
        {
            if (operation.MachinesTimesCosts.All(a => a.MachineId != machine.Id))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleyUpdateDTO.MachineId)) });
            }

            if (plannedStartTime < DateTime.Now.AddYears(-1) || plannedStartTime > DateTime.Now.AddYears(1)
                || plannedStartTime.Hour < 6 || plannedStartTime.Hour >= 16)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TrolleyUpdateDTO.PlannedStartTime)) });
            }

            var machineTimeCost = operation.MachinesTimesCosts.Single(a => a.MachineId == machine.Id);
            var isMachinetimeTaken = machineTimeCost.UnitTime > 0;
            var unitTime = isMachinetimeTaken ? machineTimeCost.UnitTime : operation.MachinesTimesCosts[0].UnitTime;
            var pcTime = isMachinetimeTaken ? machineTimeCost.PCTime : operation.MachinesTimesCosts[0].PCTime;
            var duration = TimeSpan.FromMinutes(trolley.ToDoAmount * Decimal.ToDouble(unitTime) + Decimal.ToDouble(pcTime));
            var plannedEndTime = plannedStartTime;
            while (duration.Hours > 8)
            {
                duration -= TimeSpan.FromHours(8);
                plannedEndTime += TimeSpan.FromDays(1);
                if (plannedEndTime.DayOfWeek == DayOfWeek.Saturday)
                {
                    plannedEndTime.AddDays(2);
                }
                if (plannedEndTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    plannedEndTime.AddDays(1);
                }
            }

            plannedEndTime += duration;
            if (plannedEndTime.DayOfWeek == DayOfWeek.Saturday)
            {
                plannedEndTime.AddDays(2);
            }

            if (plannedEndTime.DayOfWeek == DayOfWeek.Sunday)
            {
                plannedEndTime.AddDays(1);
            }

            var machineTrolleys = Context.Trolleys
                .Where(a => a.Id != trolley.Id && a.MachineId == machine.Id && a.PlannedStartTime.Value.Year == plannedStartTime.Year && a.PlannedStartTime.Value.Month == plannedStartTime.Month && a.PlannedStartTime.Value.Day == plannedStartTime.Day)
                .ToList();

            if (machineTrolleys.Any(a =>
                a.PlannedStartTime.Value.Between(plannedStartTime, plannedEndTime)
                || a.PlannedEndTime.Value.Between(plannedStartTime, plannedEndTime)
                || plannedStartTime.Between(a.PlannedStartTime.Value, a.PlannedEndTime.Value)
                || plannedEndTime.Between(a.PlannedStartTime.Value, a.PlannedEndTime.Value)))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict) { Content = new StringContent(nameof(TrolleyUpdateDTO.PlannedStartTime)) });
            }

            trolley.MachineId = machine.Id;
            trolley.MachineUUID = machine.UUID;
            trolley.PlannedStartTime = plannedStartTime;
            trolley.PlannedEndTime = plannedEndTime;
            trolley.isMachineTimeTaken = isMachinetimeTaken;
            trolley.LastUpdate = DateTime.Now;
            trolley.Events.Add(new TrolleyEventClass()
            {
                EventType = 100,
                Note = null,
                Time = DateTime.Now
            });

            SetScheduleStateForAllBundledTrolleys(trolley, operation.TechnologyId, Context);
        }

        private static void SetScheduleStateForAllBundledTrolleys(Trolley trolley, int technologyId, ApplicationDataDbContext Context)
        {
            var bundledTrolleys = Context.Trolleys.Where(a => a.ProductionTechnologyBundleId == trolley.ProductionTechnologyBundleId).ToList();
            var operations = Context.Operations.Where(a => a.TechnologyId == technologyId).OrderBy(b => b.Id).ToList();

            foreach (var operation in operations)
            {
                foreach (var operationBefore in operation.OperationsBefore)
                {
                    var currentTrolley = bundledTrolleys.Single(a => a.OperationId == operation.Id);
                    var trolleyBefore = bundledTrolleys.Single(a => a.OperationId == operationBefore.OperationBeforeId);
                    var currentTrolleyBoxIds = currentTrolley.Boxes.Select(a => a.BoxId).ToList();
                    var orderIds = Context.Boxes.Where(a => currentTrolleyBoxIds.Contains(a.Id)).Select(b => b.OrderId).Distinct().ToList();
                    var ordersAccomplishDates = Context.Orders.Where(order => orderIds.Any(orderId => orderId == order.Id)).Select(order => order.AccomplishDate).ToList();

                    if(ordersAccomplishDates.Any(a => a < currentTrolley.PlannedEndTime))
                    {
                        currentTrolley.ScheduleErrors = SetState(currentTrolley.ScheduleErrors, ScheduleError.AccomplishDateExceeded, true);
                        currentTrolley.LastUpdate = DateTime.Now;
                        Context.Entry(trolleyBefore).State = EntityState.Modified;
                    }

                    bool isTrolleyBeforeNotScheduled = trolleyBefore.StateId < 100;
                    if (GetState(currentTrolley.ScheduleErrors, ScheduleError.TrolleyBeforeNotScheduled) != isTrolleyBeforeNotScheduled)
                    {
                        currentTrolley.ScheduleErrors = SetState(currentTrolley.ScheduleErrors, ScheduleError.TrolleyBeforeNotScheduled, isTrolleyBeforeNotScheduled);
                        currentTrolley.LastUpdate = DateTime.Now;
                        Context.Entry(trolleyBefore).State = EntityState.Modified;
                    }

                    if (!isTrolleyBeforeNotScheduled)
                    {
                        continue;
                    }

                    bool areTwoTrolleysIncorrectlyScheduled = currentTrolley.PlannedStartTime < trolleyBefore.PlannedEndTime;
                    if (GetState(currentTrolley.ScheduleErrors, ScheduleError.TrolleyBeforeIncorrectlyScheduled) != areTwoTrolleysIncorrectlyScheduled)
                    {
                        currentTrolley.ScheduleErrors = SetState(currentTrolley.ScheduleErrors, ScheduleError.TrolleyBeforeIncorrectlyScheduled, areTwoTrolleysIncorrectlyScheduled);
                        currentTrolley.LastUpdate = DateTime.Now;
                        Context.Entry(currentTrolley).State = EntityState.Modified;
                    }

                    if (GetState(trolleyBefore.ScheduleErrors, ScheduleError.NextTrolleyIncorrectlyScheduled) != areTwoTrolleysIncorrectlyScheduled)
                    {
                        trolleyBefore.ScheduleErrors = SetState(trolleyBefore.ScheduleErrors, ScheduleError.NextTrolleyIncorrectlyScheduled, areTwoTrolleysIncorrectlyScheduled);
                        trolleyBefore.LastUpdate = DateTime.Now;
                        Context.Entry(trolleyBefore).State = EntityState.Modified;
                    }
                }
            }
        }

        public enum ScheduleError
        {
            TrolleyBeforeNotScheduled = 1,
            TrolleyBeforeIncorrectlyScheduled = 2,
            NextTrolleyNotScheduled = 3,
            NextTrolleyIncorrectlyScheduled = 4,
            AccomplishDateExceeded = 5
        }

        public static bool GetState(int data, ScheduleError bitMeaning)
        {
            return (data & (1 << (int)bitMeaning - 1)) != 0;
        }

        public static int SetState(int data, ScheduleError bitMeaning, bool value)
        {
            if (value)
                data |= 1 << (int)bitMeaning;
            else
                data &= ~(1 << (int)bitMeaning);

            return data;
        }
    }
}
