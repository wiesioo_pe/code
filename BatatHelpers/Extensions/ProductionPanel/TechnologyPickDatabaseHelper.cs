﻿using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using Batat.Models.Database.ProductionPanel.Extensions;
using Batat.Models.ProductionPanel.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Batat.Helpers.Extensions.ProductionPanel
{
    public static class TechnologyPickDatabaseHelper
    {
        public static List<ProductionTechnology> PickTechnology(TechnologyPickVM technologyPickVM, ApplicationDataDbContext Context)
        {
            if (technologyPickVM == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NoContent) { Content = new StringContent("No data in body") });
            }

            if (technologyPickVM.Amounts.Count <= 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(technologyPickVM.Amounts)) });
            }

            var part = GetPart(technologyPickVM, Context);
            var technology = GetTechnology(technologyPickVM, Context);

            if (part.Id != technology.PartId)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(technologyPickVM.PartUUID) + " doesn't match" + nameof(technologyPickVM.TechnologyUUID)) });
            }

            var newProductionTechnologies = new List<ProductionTechnology>();
            var productionTechnologyBundle = CreateNewProductionTechnologyBundle(technologyPickVM, Context);
            var createdBoxes = new List<Box>();

            foreach (var productionProductAmount in technologyPickVM.Amounts)
            {
                string orderInstanceUUID = Context.ProductionProducts.Single(a => a.UUID == productionProductAmount.ProductionProductUUID).OrderInstanceUUID;
                OrderInstance orderInstance = Context.OrderInstances
                    .Include("ProductionProducts.ProductionParts.ProductionTechnologies")
                    .Include("ProductionProducts.ProductionProducts.ProductionParts.ProductionTechnologies")
                    .Include("ProductionProducts.ProductionProducts.ProductionProducts.ProductionParts.ProductionTechnologies")
                    .Single(a => a.UUID == orderInstanceUUID);

                var productionProductInHeirarchy = orderInstance.GetProductionProductInHierarchy(productionProductAmount.ProductionProductUUID);
                ProductionProduct productionProduct = productionProductInHeirarchy.ProductionProduct;
                ProductionProduct productionProductFather = productionProductInHeirarchy.ProductionProductFather;
                ProductionProduct productionProductGrandfather = productionProductInHeirarchy.ProductionProductGrandfather;

                ProductionPart productionPart = productionProduct.ProductionParts.Single(a => a.PartId == part.Id);

                ProductionTechnology productionTechnology = new ProductionTechnology(productionPart, technology, productionProductAmount.PlanAmount, productionTechnologyBundle.Id, productionTechnologyBundle.UUID);
                productionPart.ProductionTechnologies.Add(productionTechnology);
                productionPart.TechnologyPickedAmount += productionProductAmount.PlanAmount;
                productionPart.LastUpdate = DateTime.Now;
                Context.Entry(productionPart).State = EntityState.Modified;
                Context.Entry(productionTechnology).State = EntityState.Added;
                Context.SaveChanges();

                productionTechnology.ProductionOperations.Fill(productionTechnology, technology.Operations, productionProductAmount.PlanAmount, Context);
                Context.Entry(productionTechnology).State = EntityState.Modified;
                Context.SaveChanges();

                createdBoxes.AddRange(productionTechnology.ProductionOperations.CreateBoxes(orderInstance, productionPart, productionProduct, productionTechnology, Context));

                productionPart.UpdateMadeAndPlannedAmounts();
                productionProduct.UpdateMadeAndPlannedAmounts();
                productionProductFather?.UpdateMadeAndPlannedAmounts();
                productionProductGrandfather?.UpdateMadeAndPlannedAmounts();
                orderInstance.UpdateMadeAndPlannedAmounts();

                Context.Entry(orderInstance).State = EntityState.Modified;
                Context.Entry(productionProduct).State = EntityState.Modified;
                if (productionProductFather != null)
                {
                    Context.Entry(productionProductFather).State = EntityState.Modified;
                }
                if (productionProductGrandfather != null)
                {
                    Context.Entry(productionProductGrandfather).State = EntityState.Modified;
                }

                Context.Entry(productionPart).State = EntityState.Modified;
                Context.SaveChanges();

                productionTechnologyBundle.AddProductionTechnology(productionTechnology);
                productionTechnology.UpdateProductionTechnologyBundleId(productionTechnologyBundle);

                Context.Entry(productionTechnology).State = EntityState.Modified;
                Context.Entry(productionTechnologyBundle).State = EntityState.Modified;
                Context.SaveChanges();

                newProductionTechnologies.Add(productionTechnology);
            }

            foreach (var operationIdClass in technology.Operations)
            {
                var boxes = createdBoxes.Where(a => a.OperationId == operationIdClass.OperationId);
                var trolley = new Trolley()
                {
                    OperationId = operationIdClass.OperationId,
                    OperationUUID = operationIdClass.OperationUUID,
                    Boxes = boxes.Select(a => new BoxIdClass(a.Id, a.UUID)).ToList(),
                    TechnologyId = technology.Id,
                    TechnologyUUID = technology.UUID,
                    PartId = part.Id,
                    PartUUID = part.UUID,
                    ProductionTechnologyBundleId = productionTechnologyBundle.Id,
                    ProductionTechnologyBundleUUID = productionTechnologyBundle.UUID,
                    ToDoAmount = boxes.Sum(a => a.ToDoAmount)
                };

                Context.Trolleys.Add(trolley);
                Context.SaveChanges();

                foreach (var box in boxes)
                {
                    box.TrolleyId = trolley.Id;
                    box.TrolleyUUID = trolley.UUID;
                    Context.Entry(box).State = EntityState.Modified;
                }
            }

            Context.SaveChanges();

            return newProductionTechnologies;
        }

        public static void DeletePickedTechnology(string UUID, ApplicationDataDbContext Context)
        {
            if (UUID == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NoContent) { Content = new StringContent("No UUID provided") });
            }

            var productionTechnologyBundle = Context.ProductionTechnologyBundles.Single(a => a.UUID == UUID);
            if (productionTechnologyBundle == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NoContent) { Content = new StringContent("Wrong UUID provided") });
            }

            var productionTechnologyIds = productionTechnologyBundle.ProductionTechnologies.Select(a => a.ProductionTechnologyId).Distinct().ToList();
            var ptbBoxes = Context.Boxes.Where(a => productionTechnologyIds.Contains(a.ProductionTechnologyId)).ToList();
            var trolleyIds = ptbBoxes.Select(a => a.TrolleyId).Distinct().ToList();
            var ptbTrolleys = Context.Trolleys.Where(a => trolleyIds.Contains(a.Id)).ToList();
            if (ptbTrolleys.Any(a => a.StateId >= 200))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { Content = new StringContent("At leat one operation was already started") });
            }

            var ptbProductionTechnologies = Context.ProductionTechnologies.Where(a => productionTechnologyIds.Contains(a.Id)).ToList();
            var ptbProductionOperations = Context.ProductionOperations.Where(a => productionTechnologyIds.Contains(a.ProductionTechnologyId)).ToList();
            var ptbProductionPartIds = ptbBoxes.Select(a => a.ProductionPartId).Distinct().ToList();
            Context.Boxes.RemoveRange(ptbBoxes);
            Context.Trolleys.RemoveRange(ptbTrolleys);
            Context.ProductionTechnologies.RemoveRange(ptbProductionTechnologies);
            Context.ProductionOperations.RemoveRange(ptbProductionOperations);
            Context.ProductionTechnologyBundles.Remove(productionTechnologyBundle);

            Context.SaveChanges();

            var ptbProductionParts = Context.ProductionParts
                .Include("ProductionTechnologies.ProductionOperations")
                .Where(a => ptbProductionPartIds.Contains(a.Id)).ToList();

            foreach (var productionPart in ptbProductionParts)
            {
                productionPart.UpdateMadeAndPlannedAmounts();
                Context.Entry(productionPart).State = EntityState.Modified;
                Context.SaveChanges();

                var productionProduct = Context.ProductionProducts.Find(productionPart.ProductionProductId);
                productionProduct.UpdateMadeAndPlannedAmounts();
                Context.Entry(productionProduct).State = EntityState.Modified;
                Context.SaveChanges();

                if (productionProduct.ProductionProductId > 0)
                {
                    var productionProductFather = Context.ProductionProducts.Find(productionProduct.ProductionProductId);
                    productionProductFather.UpdateMadeAndPlannedAmounts();
                    Context.Entry(productionProductFather).State = EntityState.Modified;
                    Context.SaveChanges();

                    if (productionProductFather.ProductionProductId > 0)
                    {
                        var productionProductGrandather = Context.ProductionProducts.Find(productionProductFather.ProductionProductId);
                        productionProductGrandather.UpdateMadeAndPlannedAmounts();
                        Context.Entry(productionProductGrandather).State = EntityState.Modified;
                        Context.SaveChanges();
                    }
                }

                var orderInstance = Context.OrderInstances.Find(productionPart.OrderInstanceId);
                orderInstance.UpdateMadeAndPlannedAmounts();
                Context.Entry(orderInstance).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        #region Private helpers

        private static Part GetPart(TechnologyPickVM technologyPickVM, ApplicationDataDbContext Context)
        {
            if (string.IsNullOrWhiteSpace(technologyPickVM.PartUUID) || !Guid.TryParse(technologyPickVM.PartUUID, out Guid _))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TechnologyPickVM.PartUUID)) });
            }

            if (Context.Parts.All(a => a.UUID != technologyPickVM.PartUUID))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(nameof(TechnologyPickVM.PartUUID)) });
            }

            var part = Context.Parts.Single(a => a.UUID == technologyPickVM.PartUUID);

            return part;
        }

        private static Technology GetTechnology(TechnologyPickVM technologyPickVM, ApplicationDataDbContext Context)
        {
            if (string.IsNullOrWhiteSpace(technologyPickVM.TechnologyUUID) || !Guid.TryParse(technologyPickVM.TechnologyUUID, out Guid _))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(nameof(TechnologyPickVM.PartUUID)) });
            }

            if (Context.Technologies.All(a => a.UUID != technologyPickVM.TechnologyUUID))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(nameof(TechnologyPickVM.TechnologyUUID)) });
            }

            var technology = Context.Technologies.Single(a => a.UUID == technologyPickVM.TechnologyUUID);

            return technology;
        }

        private static ProductionTechnologyBundle CreateNewProductionTechnologyBundle(TechnologyPickVM technologyPickVM, ApplicationDataDbContext Context)
        {
            var productionTechnologyBundle = new ProductionTechnologyBundle(technologyPickVM, Context);
            Context.ProductionTechnologyBundles.Add(productionTechnologyBundle);
            Context.SaveChanges();

            return productionTechnologyBundle;
        }

        #endregion

        #region Public Extensions

        public static IEnumerable<Box> CreateBoxes(
            this List<ProductionOperation> productionOperations,
            OrderInstance orderInstance,
            ProductionPart productionPart,
            ProductionProduct productionProduct,
            ProductionTechnology productionTechnology,
            ApplicationDataDbContext context)
        {
            var createdBoxes = new List<Box>();
            foreach (var productionOperation in productionOperations)
            {

                var box = new Box()
                {
                    OperationId = productionOperation.OperationId,
                    OperationUUID = productionOperation.OperationUUID,
                    OrderInstanceId = productionOperation.OrderInstanceId,
                    OrderInstanceUUID = productionOperation.OrderInstanceUUID,
                    OrderId = orderInstance.OrderId,
                    OrderUUID = orderInstance.OrderUUID,
                    PartId = productionPart.PartId,
                    PartUUID = productionPart.PartUUID,
                    ProductId = productionProduct.ProductId,
                    ProductUUID = productionProduct.ProductUUID,
                    TechnologyId = productionTechnology.TechnologyId,
                    TechnologyUUID = productionTechnology.TechnologyUUID,
                    ProductionOperationId = productionOperation.Id,
                    ProductionOperationUUID = productionOperation.UUID,
                    ProductionPartId = productionPart.Id,
                    ProductionPartUUID = productionPart.UUID,
                    ProductionProductId = productionProduct.Id,
                    ProductionProductUUID = productionProduct.UUID,
                    ProductionTechnologyId = productionTechnology.Id,
                    ProductionTechnologyUUID = productionTechnology.UUID,
                    ToDoAmount = productionOperation.ToDoAmount
                };

                context.Boxes.Add(box);
                context.SaveChanges();

                productionOperation.Boxes.Add(new BoxIdClass(box.Id, box.UUID));
                context.SaveChanges();

                createdBoxes.Add(box);
            }

            return createdBoxes;
        }

        public static void Fill(
            this List<ProductionOperation> productionOperations,
            ProductionTechnology productionTechnology,
            List<OperationIdClass> operations,
            int toDoAmount,
            ApplicationDataDbContext context)
        {
            foreach (var idClass in operations)
            {
                var productionOperation = new ProductionOperation(productionTechnology, idClass, toDoAmount);
                productionOperations.Add(productionOperation);
            }

            context.SaveChanges();
        }

        #endregion
    }
}
