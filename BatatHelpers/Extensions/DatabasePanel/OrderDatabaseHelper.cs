﻿using Batat.Models.Authentication;
using Batat.Models.Database.DatabasePanel;
using Batat.Models.Database.DbContexts;
using Batat.Models.Database.ProductionPanel;
using Batat.Models.Helpers.DatabasePanel;

namespace Batat.Helpers.Extensions.DatabasePanel
{
    public static class OrderDatabaseHelper
    {
        public static Order IntoProduction(Order order, ApplicationUser user, ApplicationDataDbContext Context)
        {
            OrderEditHelper.IntoProduction(order, user, Context);
            OrderInstance.CreateInDatabase(order, Context);
            Context.SaveChanges();

            return order;
        }
    }
}
